           Frames, Time (ms), Min, Max, Avg
from start:
sxCharStr: 2664,   60000,     34,  48,  44.400
string:    2653,   60000,     30,  49,  44.217
Coins already touched:
sxCharStr: 2824,   60000,     40,  50,  47.067
           2811,   60000,     33,  51,  46.850
string:    2668,   60000,     39,  48,  44.467
           2803,   60000,     40,  50,  46.717
5mins, from start:
sxCharStr: 13474,  300000,    38,  49,  44.913
string:    13265,  300000,    29,  49,  44.217

8000 coin
5mins, from start:
sxCharStr: 6948,   300000,    16,  25,  23.160
string:    6865,   300000,    15,  25,  22.883


  /*
  int bazz1 = sizeof(Object); // old: 84 bytes; new 64
  int bazz2 = sizeof(BasicObject); // old: 92; new 72
  int bazz3 = sizeof(AnimObject); // old: 152; new 128
  sxCharStr test1;
  sxCharStr test2("test String");
  sxCharStr test3("");
  sxCharStr test4(test2);

  test1 = test3;
  test1 = test2;
  test3 = "Maszkabál";
  //sxLOG_E("teszt: %s / %s / %s / %s", test1.getString(), test2.getString(), test3.getString(), test4.getString());

  sxString str1 = "test String";
  sxString str2 = "test String";
  int te = 0;
  if(test1<test2){
    te = 1;
  }
  if(str1<str2){
    if(1 != te){
      sxLOG_E("NOPE");
    }
  }else{
    if(0 != te){
      sxLOG_E("NOPE");
    }
  }

  te = 0;
  if(test2<test1){
    te = 1;
  }
  if(str2<str1){
    if(1 != te){
      sxLOG_E("NOPE");
    }
  }else{
    if(0 != te){
      sxLOG_E("NOPE");
    }
  }

  sxString str3 = "Maszkabál";
  te = 0;
  if(test1<test3){
    te = 1;
  }
  if(str1<str3){
    if(1 != te){
      sxLOG_E("NOPE");
    }
  }else{
    if(0 != te){
      sxLOG_E("NOPE");
    }
  }

  te = 0;
  if(test3<test1){
    te = 1;
  }
  if(str3<str1){
    if(1 != te){
      sxLOG_E("NOPE");
    }
  }else{
    if(0 != te){
      sxLOG_E("NOPE");
    }
  }
  */