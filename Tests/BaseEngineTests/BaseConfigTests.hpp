#pragma once
#include "TestTemplate.hpp"
#include "BaseConfig.hpp"

class BaseConfigTests : public TestTemplate
{
public:
  ~BaseConfigTests()
  {
    delete baseConfig;
  }
  virtual void run();

private:
  INIReader* reader;
  BaseConfig* baseConfig;
  void createBaseConfigInstance();
  void checkReadValues();
};

inline
void BaseConfigTests::createBaseConfigInstance()
{
  baseConfig = new BaseConfig("main_config_test.ini");
}

inline
void BaseConfigTests::run()
{
  sxLOG(LL_TRACE, "Tests Started.");
  createBaseConfigInstance();
  checkReadValues();
  sxLOG(LL_TRACE, "Tests Finished.");
}

inline
void BaseConfigTests::checkReadValues()
{
  sxVec2Int   logicalSize = baseConfig->getWindowLogicalSize();
  sxVec2Int   screenSize = baseConfig->getWindowScreenSize();
  sxBool      fullscreen = baseConfig->isFullscreen();
  sxString windowName = baseConfig->getWindowName();
  sxString spriteScalingMethod = baseConfig->getSpriteScalingMethod();
  sxColor backgroundColor = baseConfig->getBackroundColor();

  if( logicalSize.x != 1920 ||
      logicalSize.y != 1080 )
  {
    sxLOG(LL_ERROR, "Logical Size is %d, %d", logicalSize.x, logicalSize.y);
  }
  if( screenSize.x != 2560 ||
      screenSize.y != 600 )
  {
    sxLOG(LL_ERROR, "Logical Size is %d, %d", screenSize.x, screenSize.y);
  }
  if(!fullscreen){
    sxLOG(LL_ERROR, "fullscreen is false.");
  }
  if(0 != windowName.compare("Very Test - Very Window")){
    sxLOG(LL_ERROR, "Window name is: %s", windowName.c_str());
  }
  if(0 != spriteScalingMethod.compare("best")){
    sxLOG(LL_ERROR, "Sprite Scaling is: %s", spriteScalingMethod.c_str());
  }
  if( 123 != backgroundColor.r ||
      241 != backgroundColor.g ||
      90  != backgroundColor.b ||
      255 != backgroundColor.a )
  {
    sxLOG(LL_ERROR, "background colors are not matching: r=%d, g=%d, b=%d, a=%d",
      backgroundColor.r, backgroundColor.g, backgroundColor.b, backgroundColor.a);
  }
}

