#pragma once
#include <SDL.h>
#include "sxTypes.h"
#include "LogManager.h"

SDL_Window* window;
SDL_Renderer* renderer;

#define ASSERT(cond) assert(cond,__FUNCTION__,__LINE__)
#define ASSERT_EQ(a,b) ASSERT(a==b)
#define ASSERT_NEQ(a,b) ASSERT(a!=b)
#define ASSERT_TRUE(a) ASSERT(a==SDL_TRUE)
#define ASSERT_FALSE(a) ASSERT(a==SDL_FALSE)

#define ASSERT_STOP(cond) \
{ \
  if(!ASSERT(cond)){ \
    return; \
  } \
}
// given string equals with error log
#define ASSERT_ELOG_EQ(msg){ \
  sxString errorStr = LogManager::getErrorMessages(); \
  if(!ASSERT_EQ(errorStr.compare(msg), 0)){ \
    sxLOG_E("Expected message:\n*%s*\nReceived message:\n*%s*\n", \
            msg, errorStr.c_str()); \
  } \
  LogManager::clearErrorAndWarningMessages(); \
}

#define DEFINE_TEST(a) a():TestTemplate(__FUNCTION__){}

static const sxInt32 TEST_SUCCESS = 0;
static const sxInt32 TEST_FAILURE = 1;

class TestTemplate
{
public:
  TestTemplate(sxString _name);
  virtual ~TestTemplate() {}

  void execute();

  virtual void setUp() {}
  virtual void run() = 0;
  virtual void tearDown() {}

  sxString getName() const {return name;}
  sxInt32 getSuccess() const {return success;}

private:
  sxString name;
  sxInt32 success;

protected:
  bool assert(bool condition, const char * function, int line);
  void writeAndCleanErrorLogs();
  bool isLogErrorFree();
};

TestTemplate::TestTemplate(sxString _name)
{
  name = _name;
  success = TEST_SUCCESS;
}

void TestTemplate::execute()
{
  success = TEST_SUCCESS;

  setUp();
  run();
  tearDown();
}

bool TestTemplate::assert(bool condition, const char * function, int line)
{
  if(!condition){
    success = TEST_FAILURE;
    writeAndCleanErrorLogs();
    sxLOG_E("\n    ASSERT FAILED! %s::%s(%d)", getName().c_str(), function, line);
    LogManager::clearErrorAndWarningMessages();
  }
  return condition;
}

inline
void TestTemplate::writeAndCleanErrorLogs()
{
  sxString msgs = LogManager::getErrorMessages();
  if(!msgs.empty()){
    sxLOG_E("Errors not handled properly:\nSTART\n%s\nEND\n\n", msgs.c_str());
  }
  msgs = LogManager::getWarningMessages();
  if(!msgs.empty()){
    sxLOG_E("Warnings not handled properly:\nSTART\n%s\nEND\n\n", msgs.c_str());
  }
  LogManager::clearErrorAndWarningMessages();
}


bool TestTemplate::isLogErrorFree()
{
  if(LogManager::getErrorMessages().empty()){
    return true;
  }
  return false;
}