// remove this define to use Visual Studio's memleak detection
#ifdef __WIN32__
#define _USE_VLD
#endif

#ifndef __sdlANDROID__
  #ifdef _USE_VLD
    #include <vld.h>
  #else
    #ifdef __WIN32__
      #define _CRTDBG_MAP_ALLOC
      #include <stdlib.h>
      #include <crtdbg.h>
    #endif
  #endif
#endif

#include "LogManager.h"
#include "TestRunner.hpp"

#include "IniReaderTests.hpp"
//#include "BaseConfigTests.hpp"
//#include "TextureManagerTests.hpp"

int main(int argc, char **argv)
{
#ifndef __sdlANDROID__
#ifndef _USE_VLD
#ifdef  __WIN32__
  _CrtSetDbgFlag ( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
#endif
#endif
#endif

  LogManager::init();
  LoadedFiles::init();
  TestRunner runner;

  // Add new tests here
  runner.addTest(new INIReaderTests());
  //runner.addTest(new BaseConfigTests());
  //runner.addTest(new TextureManagerTests());

  sxLOG(LL_TRACE, "Starting TestRunner");
  sxInt32 result = runner.run();
  sxLOG(LL_TRACE, "Exiting..");

  getLoadedFiles->destroy();
  return result;
}