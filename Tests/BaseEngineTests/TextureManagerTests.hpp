#pragma once
#include "TestTemplate.hpp"
#include "TextureManager.h"

static const sxString assetFile = "main_assets_test.ini";

class TextureManagerTests : public TestTemplate
{
public:
  virtual void run();

private:
  void testErrorCaseManagerNotInitialized();
  void testCreateAndDestroyTextureManager();
  void testInitializeTexturemanager();
  void testLoadAssets();
  void testGetInvalidtexture();
  void testGetValidTexture();
};

inline
void TextureManagerTests::run()
{
  sxLOG(LL_TRACE, "Tests Started.");
  sxLOG(LL_TRACE, "Error Case:");
  testErrorCaseManagerNotInitialized();
  testCreateAndDestroyTextureManager();
  testInitializeTexturemanager();
  testLoadAssets();
  testGetInvalidtexture();
  testGetValidTexture();
  sxLOG(LL_TRACE, "Tests Finished.");
}

inline
void TextureManagerTests::testErrorCaseManagerNotInitialized()
{
  try{
    getTextureManager->loadNewAssets(assetFile);
  }catch(...){
    sxLOG(LL_TRACE, "Ok, exception caught");
    return;
  }
  sxLOG(LL_ERROR, "Exception is not happened but it should have happen!");
}

inline
void TextureManagerTests::testCreateAndDestroyTextureManager()
{
  TextureManager::createTextureManager();
  getTextureManager->destroyTextureManager();
  TextureManager::createTextureManager();
  getTextureManager->destroyTextureManager();
}

inline
void TextureManagerTests::testInitializeTexturemanager()
{
  TextureManager::createTextureManager();
  getTextureManager->initialize(renderer);
  getTextureManager->destroyTextureManager();
}

inline
void TextureManagerTests::testLoadAssets()
{
  TextureManager::createTextureManager();
  getTextureManager->initialize(renderer);
  getTextureManager->loadNewAssets(assetFile);
  getTextureManager->destroyTextureManager();
}

inline
void TextureManagerTests::testGetInvalidtexture()
{
  SDL_Texture* texture;
  TextureManager::createTextureManager();
  getTextureManager->initialize(renderer);
  getTextureManager->loadNewAssets(assetFile);
  texture = getTextureManager->getTexture("nonExistantTexture");
  if(NULL != texture){
    sxLOG(LL_ERROR, "This texture should not exist!");
  }
  getTextureManager->destroyTextureManager();
}

inline
void TextureManagerTests::testGetValidTexture()
{
  SDL_Texture* texture;
  TextureManager::createTextureManager();
  getTextureManager->initialize(renderer);
  getTextureManager->loadNewAssets(assetFile);
  texture = getTextureManager->getTexture("COIN");
  if(NULL == texture){
    sxLOG(LL_ERROR, "This texture should have been valid!");
  }
  getTextureManager->destroyTextureManager();
}
