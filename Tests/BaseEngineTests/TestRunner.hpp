#pragma once
#include "TestTemplate.hpp"

static const char * SEPARATOR = "\n============================================================\n";

class TestRunner
{
public:
  TestRunner();
  ~TestRunner();
  void addTest(TestTemplate * test);
  sxInt32 run();
  void draw();
private:
  std::vector<TestTemplate*> tests;
  void createWindowAndRenderer();
  void destroyWindowAndRenderer();
};

inline
TestRunner::TestRunner()
{
  sxLOG(LL_TRACE, "\n======== TESTRUNNER STARTS ========\n");
  createWindowAndRenderer();
}

inline
TestRunner::~TestRunner()
{
  for(sxUInt32 i=0; i<tests.size(); ++i){
    delete tests[i];
  }
  destroyWindowAndRenderer();
}

inline
void TestRunner::createWindowAndRenderer()
{
  window = SDL_CreateWindow("Hello World!", 100, 100, 800, 600, SDL_WINDOW_SHOWN);
  if (window == NULL){
    sxLOG(LL_ERROR, "SDL_CreateWindow Error: %s", SDL_GetError());
    return;
  }

  renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
  if (renderer == NULL){
    sxLOG(LL_ERROR, "SDL_CreateRenderer Error: %s", SDL_GetError());
    return;
  }
}

inline
void TestRunner::destroyWindowAndRenderer()
{
  SDL_DestroyRenderer(renderer);
  SDL_DestroyWindow(window);
}

inline
void TestRunner::addTest(TestTemplate * test)
{
  tests.push_back(test);
}

inline
sxInt32 TestRunner::run()
{
  sxLOG_T(SEPARATOR);
  sxInt32 result = TEST_SUCCESS;
  for(sxUInt32 i=0; i<tests.size(); ++i){
    sxLOG_T("Starting test: %s", tests[i]->getName().c_str());
    tests[i]->execute();
    if (tests[i]->getSuccess() == TEST_SUCCESS) {
      sxLOG_T("TEST PASSED.%s", SEPARATOR);
    }else{
      result = TEST_FAILURE;
      sxLOG_E("TEST FAILED.%s", SEPARATOR);
    }
  }
  return result;
}

inline
void TestRunner::draw()
{
}
