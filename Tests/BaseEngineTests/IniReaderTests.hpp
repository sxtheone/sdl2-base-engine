#include "TestTemplate.hpp"
#include <INIReader.h>

class INIReaderTests : public TestTemplate
{
public:
  DEFINE_TEST(INIReaderTests);

  virtual void setUp();
  virtual void run();
  virtual void tearDown();

private:
  INIReader* reader;
  void testDuplicatedValueError();
  void testInteger();
  void testReal();
  void testString();
  void testBoolean();
  void testComment();
  void testTables();
  void testGetFirstSectionNextSection();
  void testVec2Int();
  void testVec2Real();
};

inline
void INIReaderTests::setUp()
{
  reader = new INIReader("inireader_test.ini");
}

inline
void INIReaderTests::run()
{
  testDuplicatedValueError(); writeAndCleanErrorLogs();
  testInteger(); writeAndCleanErrorLogs();
  testReal(); writeAndCleanErrorLogs();
  testString(); writeAndCleanErrorLogs();
  testBoolean(); writeAndCleanErrorLogs();
  testComment(); writeAndCleanErrorLogs();
  testTables(); writeAndCleanErrorLogs();
  testGetFirstSectionNextSection(); writeAndCleanErrorLogs();
  testVec2Int(); writeAndCleanErrorLogs();
  testVec2Real(); writeAndCleanErrorLogs();
}

inline
void INIReaderTests::tearDown()
{
  delete reader;
}

inline
void INIReaderTests::testDuplicatedValueError()
{
  ASSERT_ELOG_EQ(
  "!E! INIParser::putValueToMap: Duplicated value: more integers section / s t i l l m o r e\n"
  "!E! INIParser::processFile: Error in line: s t i l l m o r e = 8 ;duplicated value error\r\n\n");
}

inline
void INIReaderTests::testInteger()
{
  //ASSERT_EQ(reader->setSection("integers section"),ReturnCodes::RC_SUCCESS,
  ASSERT_TRUE(reader->setSection("integers section"));
  
  sxInt32 result = 0;  
  reader->getInteger("anInteger", result);
  ASSERT_EQ(result, 6);

  reader->getInteger("another integer", result);
  ASSERT_EQ(result, 1025);

  //ASSERT_EQ(reader->setSection("more integers section"), ReturnCodes::RC_SUCCESS,
  ASSERT_TRUE(reader->setSection("more integers section"));

  reader->getInteger("s t i l l m o r e", result);
  ASSERT_EQ(result, 987654321);
}

inline
void INIReaderTests::testReal()
{
  //ASSERT_EQ(reader->setSection("REAL section"), ReturnCodes::RC_SUCCESS,
  ASSERT_TRUE(reader->setSection("REAL section"));
  sxFloat result;
  reader->getReal("pi", result);
  ASSERT(3.141 <= result);
  result = 1234;
  reader->getReal("another pi", result);
  ASSERT_EQ(result, 1234);
  ASSERT_ELOG_EQ("!E! INIReader::strToReal: conversion to real failed: ,14159\n");
  
  sxVec2Int vecResult;
  reader->getVec2Int("another pi", vecResult);
  ASSERT_EQ(vecResult.x, 3);
  ASSERT_EQ(vecResult.y, 14159);
}

inline
void INIReaderTests::testString()
{
  //ASSERT_EQ(reader->setSection("String Section"), ReturnCodes::RC_SUCCESS,
  ASSERT_TRUE(reader->setSection("String Section"));

  sxString result;
  sxString tempStr;

  tempStr = "The big blue Fox fucked the little Yellow PIGEON .";
  reader->getStr("first string", result);
  ASSERT_EQ(tempStr.compare(result), 0);

  tempStr = "1111011111101111";
  reader->getStr("row00", result);
  ASSERT_EQ(tempStr.compare(result), 0);
}

inline
void INIReaderTests::testBoolean()
{
  //ASSERT_EQ(reader->setSection("Boolean"), ReturnCodes::RC_SUCCESS,
  ASSERT_TRUE(reader->setSection("Boolean"));

  sxBool result;
  reader->getBoolean("booleanCrazyness", result);
  ASSERT_TRUE(result);

  reader->getBoolean("NotTrueBool", result);
  ASSERT_FALSE(result);
}

inline
void INIReaderTests::testComment()
{
  //ASSERT_EQ(reader->setSection("CommentTest"), ReturnCodes::RC_SUCCESS,
  ASSERT_TRUE(reader->setSection("CommentTest"));

  sxString result;
  sxString tempStr;

  tempStr = "1";
  reader->getStr("test1", result);
  ASSERT_EQ(tempStr.compare(result), 0);

  tempStr = "2\ntest3"; // because the test3 was erroneous after this line, it became a multiline
  reader->getStr("test2", result);
  ASSERT_EQ(tempStr.compare(result), 0);

  tempStr = "";
  result = "";
  reader->getStr("test3", result);
  ASSERT_EQ(tempStr.compare(result), 0);
  ASSERT_ELOG_EQ("!E! INIReader::fieldValid: Requested field not exists: CommentTest / test3\n");

  tempStr = "4#5#6";
  result = "";
  reader->getStr("test4", result);
  ASSERT_EQ(tempStr.compare(result), 0);

  tempStr = "";
  result = "";
  reader->getStr("test5", result);
  ASSERT_EQ(tempStr.compare(result), 0);
  ASSERT_ELOG_EQ("!E! INIReader::fieldValid: Requested field not exists: CommentTest / test5\n");

  tempStr = "";
  result = "";
  reader->getStr("test6", result);
  ASSERT_EQ(tempStr.compare(result), 0);
  ASSERT_ELOG_EQ("!E! INIReader::fieldValid: Requested field not exists: CommentTest / test6\n");
}

inline
void INIReaderTests::testTables()
{
  //ASSERT_EQ(reader->setSection("Tables"), ReturnCodes::RC_SUCCESS,
  ASSERT_TRUE(reader->setSection("Tables"));

  sxString result;
  sxString tempStr = "\
1111011111101111\n\
1111012222101111\n\
1111011111101111\n\
1112000110002111\n\
1111220110221111\n\
1111120110211111\n\
1111120110211111\n\
1111120110211111\n\
1111322313223111\n\
1111333313333111\n\
1111112222211111\n\
1111117171711111\n\
1111111414111111";
  reader->getStr("tiles", result);
  ASSERT_EQ(tempStr.compare(result), 0);

  tempStr = "\
RRRRRRRRRRRRRRRR\n\
RRRRRRRRRRRRRRRR\n\
RRRRRRRRRRRRRRRR\n\
RRRRRRRRRRRRRRRR\n\
RRRRRRRRRRRRRRRR\n\
RRRRRRRRRRRRRRRR\n\
RRRRRRRRRRRRRRRR\n\
RRRRRRRRRRRRRRRR\n\
RRRRRRRRRRRRRRRR\n\
RRRRRRRRRRRRRRRR\n\
RRRRRRRRRRRRRRRR\n\
RRRRRRRRRRRRRRRR\n\
RRRRRRRRRRRRRRRR";
  reader->getStr("items", result);
  ASSERT_EQ(tempStr.compare(result), 0);
}

inline
void INIReaderTests::testGetFirstSectionNextSection()
{
  sxString firstSection = reader->setFirstSection();
  ASSERT_EQ(firstSection, "Boolean");

  sxString nextSection = reader->setNextSection();
  ASSERT_EQ(nextSection, "CommentTest");
}

inline
void INIReaderTests::testVec2Int()
{
  //ASSERT_EQ(reader->setSection("Vec2Int Test Values"), ReturnCodes::RC_SUCCESS,
  ASSERT_TRUE(reader->setSection("Vec2Int Test Values"));
  sxVec2Int result;  
  reader->getVec2Int("My Integer Vector", result);
  ASSERT_EQ(result.x, 66656);
  ASSERT_EQ(result.y, 2498799);
  
}

inline
void INIReaderTests::testVec2Real()
{
  //ASSERT_EQ(reader->setSection("Vec2Real Test Values"), ReturnCodes::RC_SUCCESS,
  ASSERT_TRUE(reader->setSection("Vec2Real Test Values"));
  sxVec2Real result;
  reader->getVec2Real("My Real Vector", result);
  ASSERT(!(66656.7 > result.x || 66656.9 < result.x || 
           249879.1 > result.y || 249879.3 < result.y));
}

