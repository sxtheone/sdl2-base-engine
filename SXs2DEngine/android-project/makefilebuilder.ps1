#
# Makefile creator
# Usage: fill the variables with the correct paths
$baseAndroidPath = "D:\gameProjects\SDL2\SDL2_Base_Engine\SXs2DEngine\android-project\jni\src"
$enginePath = "D:\gameProjects\sdl2\SDL2_Base_Engine\SXs2DEngine"
$gameProjectPath = "D:\gameProjects\SDL2\SDL2_Base_Engine\ExampleProject"
$makeFile = "Android.mk"
$engineIncludeDir = "include"
$engineSourceDir = "src"


function writeList($DirListPar, $PathToAppend, $lastDontHaveBackslash)
{
  for($i=0; $i -lt $DirListPar.length; $i++)
  {
    $filename = $DirListPar[$i]
    if(($lastDontHaveBackslash -eq $true) -and ($i -eq $DirListPar.length-1)){
      $DirListPar[$i] = "${PathToAppend}/${filename}"
    }else{
      $DirListPar[$i] = "${PathToAppend}/${filename} \"
    }
  }
}

if(Test-Path $baseAndroidPath\$makeFile)
{
  Remove-Item $baseAndroidPath\$makeFile
  echo "*** Makefile found and deleted ***"
}

$DirList = (Get-ChildItem -Path $enginePath\$engineIncludeDir\* -Include *.h*, *.cpp | Select-Object -ExpandProperty Name)
$myPath = "`$(INCLUDE_PATH)"
$lastOne = $false
writeList $DirList $myPath $lastOne
$FullFileList = $DirList
# $DirList | out-file -Encoding "UTF8" $baseAndroidPath\INCLUDE_PATH.txt

$DirList = (Get-ChildItem -Path $enginePath\$engineSourceDir\* -Include *.h*, *.cpp | Select-Object -ExpandProperty Name)
$myPath = "`$(SOURCE_PATH)"
$lastOne = $false
writeList $DirList $myPath $lastOne
$FullFileList += $DirList
# $DirList | out-file -Encoding "UTF8" $baseAndroidPath\SOURCE_PATH.txt

$DirList = (Get-ChildItem -Path $gameProjectPath\* -Include *.h*, *.cpp | Select-Object -ExpandProperty Name)
$myPath = "`$(GAME_PATH)"
$lastOne = $true
writeList $DirList $myPath $lastOne
$FullFileList += $DirList
# $DirList | out-file -Encoding "UTF8" $baseAndroidPath\GAME_PATH.txt

$fileContent = Get-Content $baseAndroidPath\templateAndroid.mk
$fileContent = $fileContent -replace "---FILE_LIST---",($FullFileList | ForEach-Object {$_+"`r`n"})
$fileContent | out-file -Encoding "UTF8" $baseAndroidPath\$makeFile
 


