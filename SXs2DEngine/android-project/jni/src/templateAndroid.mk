LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := main

LOCAL_CPPFLAGS := -D__sdlANDROID__=1 -DDEVELOPER_HELPERS=1 -fexceptions

INCLUDE_PATH := ../../../include
SOURCE_PATH := ../../../src
GAME_PATH := ../../../../ExampleProject
SDL_PATH := ../SDL
SDL_IMAGE_PATH := ../SDL2_image-2.0.1
SDL_TTF_PATH := ../SDL2_ttf-2.0.14
SDL_MIXER_PATH := ../SDL2_mixer-2.0.1

LOCAL_C_INCLUDES := $(LOCAL_PATH)/$(SDL_PATH)/include \
$(LOCAL_PATH)/$(SDL_IMAGE_PATH) \
$(LOCAL_PATH)/$(SDL_TTF_PATH) \
$(LOCAL_PATH)/$(SDL_MIXER_PATH) \
$(LOCAL_PATH)/$(INCLUDE_PATH) \
$(LOCAL_PATH)/$(SOURCE_PATH) \
$(LOCAL_PATH)/$(GAME_PATH)

# Add your application source files here...
LOCAL_SRC_FILES := $(SDL_PATH)/src/main/android/SDL_android_main.c \
---FILE_LIST---

LOCAL_SHARED_LIBRARIES := SDL2 \
                          SDL2_image \
                          libSDL2_ttf \
                          SDL2_mixer

LOCAL_LDLIBS := -lGLESv1_CM -llog

include $(BUILD_SHARED_LIBRARY)
