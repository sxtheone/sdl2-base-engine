#include "SpritesFileLoader.h"
#include "HitboxesFileLoader.hpp"

ReturnCodes SpritesFileLoader::processFile(const sxString& filename)
{
  ReturnCodes result = createReader(filename);
  if(shouldExitFromLoader(result)){
    return result;
  }

  textureMapId.clear();
  textureMapId = processTextureMapDataSection();
  if(!textureMapId.empty()){
    loadAllSprites();
  }

  destroyReader();
  return ReturnCodes::RC_SUCCESS;
}

sxString SpritesFileLoader::processTextureMapDataSection()
{
  if(SDL_FALSE == reader->setSection(textureMapDataSectionName)){
    return sxString();
  }
  HitboxesFileLoader hitboxLoader;
  loadFileList("hitboxFiles", hitboxLoader);
  return loadTextureMap();
}

sxString SpritesFileLoader::loadTextureMap()
{
  sxString id;
  if(ReturnCodes::RC_SUCCESS == reader->getFileNameWithPath("filename", id)){
    sxBool permanentFlag = SDL_FALSE;
    sxBool streamingFlag = SDL_FALSE;
    reader->getBoolean("permanent", permanentFlag, SDL_FALSE);
    reader->getBoolean("streaming", streamingFlag, SDL_FALSE);
    getSpriteManager->loadTextureMap(id, permanentFlag, streamingFlag);
  }
  return id;
}

void SpritesFileLoader::loadAllSprites()
{
  currentSectionName = reader->setFirstSection();
  do{
    if(SDL_TRUE == isCommandSection(currentSectionName)){
      processCommand(currentSectionName);
    }else if(0 != currentSectionName.compare(textureMapDataSectionName)){
      loadOneSprite();
    }
    currentSectionName = reader->setNextSection();
  }while(!currentSectionName.empty());
}

void SpritesFileLoader::loadOneSprite()
{
  currentSprite = sxSprite();
  ancestorName.clear();

  fillSpriteRelatedData(currentSectionName);
  setSpriteWidthHeightIfInvalid();
  getSpriteManager->addSprite(currentSectionName, currentSprite);
}

void SpritesFileLoader::loadAncestorsDataIfAvailable()
{
  if(!ancestorName.empty()){
    if(NULL == getSpriteManager->getSprite(ancestorName)){
      sxLOG(LL_ERROR, "Sprite ancestor named: %s is not present for sprite named: %s."
            "Ancestors should have been defined before children!",
            ancestorName.c_str(), ancestorName.c_str());
    }else{
      // load the ancestor's data into the new sprite
      currentSprite = *(getSpriteManager->getSprite(ancestorName));
    }
  }
}

void SpritesFileLoader::fillSpriteRelatedData(const sxString& spriteName)
{
  currentSprite.setTexture(getSpriteManager->getTextureMap(textureMapId));
  substractAncestorFromCurrentSectionName();
  loadAncestorsDataIfAvailable();
  fillTopLeftRelatedData();
  fillWidthHeightRelatedData();
  createHitbox(spriteName);
}

void SpritesFileLoader::fillTopLeftRelatedData()
{
  sxVec2Int tempVec;
  if(ReturnCodes::RC_SUCCESS == reader->getVec2Int("topLeft", tempVec, SDL_FALSE)){
    currentSprite.setTopLeftPosition(tempVec);
  }
  reader->getInteger("topLeftX", currentSprite.size.x, SDL_FALSE);
  reader->getInteger("topLeftY", currentSprite.size.y, SDL_FALSE);
}

void SpritesFileLoader::fillWidthHeightRelatedData()
{
  sxVec2Int tempVec;
  if(ReturnCodes::RC_SUCCESS == reader->getVec2Int("widthHeight", tempVec, SDL_FALSE)){
    currentSprite.setWidthHeight(tempVec);
  }
}

void SpritesFileLoader::setSpriteWidthHeightIfInvalid()
{
  if( 0 == currentSprite.size.w ||
      0 == currentSprite.size.h )
  {
    currentSprite.setMaxWidthHeight();
  }
}

void SpritesFileLoader::createHitbox(const sxString& hitboxName)
{
  if(SDL_TRUE == reader->isItemExisting("hitbox")){
    processHitboxField();
  }else{
    createHitboxFromSpritesize(hitboxName);
  }  
}

void SpritesFileLoader::processHitboxField()
{
  sxString hitboxName;
  if(ReturnCodes::RC_SUCCESS == reader->getStr("hitbox", hitboxName, SDL_FALSE)){
    currentSprite.hitbox = getSpriteManager->getHitboxRef(hitboxName);
  }
}

void SpritesFileLoader::createHitboxFromSpritesize(const sxString& hitboxName)
{
  sxQuad hitbox;
  hitbox.bottomRight.x = static_cast<sxFloat>(currentSprite.size.w);
  hitbox.bottomRight.y = static_cast<sxFloat>(currentSprite.size.h);
  getSpriteManager->addHitbox(hitboxName, hitbox);
  currentSprite.hitbox = getSpriteManager->getHitboxRef(hitboxName);
}

void SpritesFileLoader::processCommand(const sxString& sectionName)
{
  sxString commandName = getCommandName(sectionName);
  if(commandName.empty()){
    sxLOG_E("Error while parsing command section: %s", sectionName.c_str());
    return;
  }
  if(0 == commandName.compare(commandSlice)){
    processSliceCommand(getCommandParameters(sectionName));
  }
}
//TODO: refactor processSliceCommand
void SpritesFileLoader::processSliceCommand(sxString spriteNameBase)
{
  if(spriteNameBase.empty()){
    return;
  }

  fillSpriteRelatedData(spriteNameBase);
  if(0 == currentSprite.size.w){
    sxLOG_E("Sprite width is 0. Sprite: %s", currentSectionName.c_str());
    return;
  }
  sxUInt32 numOfItems = 0;
  sxString spriteName;
  sxInt32 tempInt = numOfItems;
  sxVec2Int spacingBetweenSprites;
  sxVec2Int topLeft;
  topLeft.x = currentSprite.size.x;
  topLeft.y = currentSprite.size.y;

  reader->getInteger("numOfItems", tempInt);
  numOfItems = static_cast<sxUInt32>(tempInt);
  reader->getVec2Int("spacing", spacingBetweenSprites, SDL_FALSE);

  sxInt32 textureWidth = currentSprite.getTextureWidth();
  sxInt32 textureHeight = currentSprite.getTextureHeight();
  for(sxUInt32 i=0; i<numOfItems; ++i){
    if(topLeft.y >= textureHeight){
      sxLOG_E("Ran out of texture!");
      break;
    }
    currentSprite.size.x = topLeft.x;
    currentSprite.size.y = topLeft.y;
    spriteName = strFormatter("%s%03d", spriteNameBase.c_str(), i);
    getSpriteManager->addSprite(spriteName, currentSprite);

    topLeft.x += currentSprite.size.w + spacingBetweenSprites.x;
    if(topLeft.x >= textureWidth){
      topLeft.x = spacingBetweenSprites.x;
      topLeft.y += currentSprite.size.h + spacingBetweenSprites.y;
    }
  }
}
