#include "FileLoaderBase.h"

ReturnCodes FileLoaderBase::createReader(const sxString& fileToLoad, sxBool checkIfAlreadyLoaded)
{
  destroyReader();

  reader = new INIReader(fileToLoad, checkIfAlreadyLoaded);
  return reader->ParseError();
}

void FileLoaderBase::destroyReader()
{
  if(NULL != reader){
    delete reader;
    reader = NULL;
  }
}

sxBool FileLoaderBase::shouldExitFromLoader(ReturnCodes& result)
{
  if(ReturnCodes::RC_SUCCESS != result){
    destroyReader();
    if(ReturnCodes::RC_FILE_ALREADY_LOADED == result){
      // sometimes it's inevitable that a file is in the list for more than once
      result = ReturnCodes::RC_SUCCESS;
    }
    return SDL_TRUE;
  }
  return SDL_FALSE;
}

void FileLoaderBase::substractAncestorFromCurrentSectionName()
{
  sxString::size_type atIndex = currentSectionName.find('@');
  if(sxString::npos != atIndex){
    ancestorName.assign(currentSectionName.substr(atIndex+1));
    currentSectionName.assign(currentSectionName.substr(0, atIndex));
  }
}

ReturnCodes FileLoaderBase::loadFileList(const sxString& fieldName, FileLoaderBase& loader)
{
  std::vector<sxString> fileList;
  if(ReturnCodes::RC_SUCCESS != reader->getStrList(fieldName, fileList, SDL_FALSE)){
    return ReturnCodes::RC_SUCCESS;
  }

  ReturnCodes result;
  for(sxUInt32 i=0; i<fileList.size(); ++i){
    result = loader.processFile(fileList[i]);
    if(ReturnCodes::RC_SUCCESS != result){
      sxLOG_E("Something wrong, loading interrupted. File: %s", fileList[i].c_str());
      return result;
    }
  }
  return ReturnCodes::RC_SUCCESS;
}

sxBool FileLoaderBase::isCommandSection(const sxString& sectionName)
{
  if(sxString::npos == sectionName.find(commandSectionName)){
    return SDL_FALSE;
  }
  return SDL_TRUE;
}


sxString FileLoaderBase::getCommandName(const sxString& sectionName)
{
  sxString commandName = sectionName.substr(commandSectionName.size());
  commandName = commandName.substr(0, commandName.find(' '));
  trimLeadingSpaces(commandName);
  return commandName;
}

sxString FileLoaderBase::getCommandParameters(const sxString& sectionName)
{
  sxString commandParameters = sectionName.substr(commandSectionName.size());
  trimTrailingSpaces(commandParameters);
  commandParameters = commandParameters.substr(commandParameters.find(' '));
  trimLeadingSpaces(commandParameters);
  return commandParameters;
}
