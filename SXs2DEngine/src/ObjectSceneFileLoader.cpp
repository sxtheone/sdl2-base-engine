#include "ObjectSceneFileLoader.h"
#include "SoundsFileLoader.h"
#include "ObjectManager.h"
#include "SpritesFileLoader.h"
#include "ObjectDrawerNormal.hpp"
#include "ObjectDrawerPosOffset.hpp"
#include "ObjectDrawerOrdered.hpp"
#include "DialogWindowBuilder.h"

ObjectSceneFileLoader::ObjectSceneFileLoader(const sxUInt32 sceneID) : SceneFileLoaderBase(sceneID)
{
  drawer = NULL;
  objects = NULL;
  drawerIsOrdered = SDL_FALSE;
}

ObjectSceneFileLoader::~ObjectSceneFileLoader()
{
  destroyReader();
}

ReturnCodes ObjectSceneFileLoader::processFile(const sxString& filename)
{
  // don't check if file is already loaded. Scenes are allowed to be duplicated.
  ReturnCodes result = createReader(filename, SDL_FALSE);
  if(shouldExitFromLoader(result)){
    return result;
  }
  if(ReturnCodes::RC_SUCCESS != processSections()){
    sxLOG_E("Scene file: %s loading failed.", filename.c_str());
    return ReturnCodes::RC_FILE_PARSE_ERROR;
  }
  return ReturnCodes::RC_SUCCESS;
}

ReturnCodes ObjectSceneFileLoader::processSections()
{
  ReturnCodes result;
  result = processConfigSection();
  if(ReturnCodes::RC_SUCCESS != result){
    return result;
  }

  currentSectionName = reader->setFirstSection();
  do{
    if(0 != currentSectionName.compare("Config")){
      if(SDL_FALSE == isCommandSection(currentSectionName)){
        processOneObjectSection();
      }else{
        processCommand();
      }
    }
    currentSectionName = reader->setNextSection();
  }while(!currentSectionName.empty());

  if(SDL_TRUE == drawerIsOrdered){
    static_cast<ObjectDrawerOrdered*>(drawer)->syncObjectsWithOrderedDrawer();
  }

  return result;
}

ReturnCodes ObjectSceneFileLoader::processConfigSection()
{
  ReturnCodes result;
  currentSectionName = reader->setSection("Config");
  processSceneObjectDrawerSettings();
  processBasicSceneInfo();

  processSlidingInfo();
  SoundsFileLoader soundLoader;
  loadFileList("soundFiles", soundLoader);
  SpritesFileLoader spriteLoader;
  loadFileList("spriteFiles", spriteLoader);
  ObjectFileLoader objectLoader;
  loadFileList("objectFiles", objectLoader);

  return result;
}

void ObjectSceneFileLoader::processSlidingInfo()
{
  if(ReturnCodes::RC_SUCCESS == reader->getVec2Real("slidingSpeed", slidingSpeed, SDL_FALSE)){
    if(ObjectDrawerTypeEnum::NORMAL == drawer->type){
      sxLOG_E("Drawer set to NORMAL and sliding needs posOffset!");
      return;
    }
    reader->getVec2Real("slidingDistance", slidingDistance);
  }
}

void ObjectSceneFileLoader::processSceneObjectDrawerSettings()
{
  sxBool usePosOffset = SDL_FALSE;
  sxBool useRenderToTexture = SDL_FALSE;
  sxVec2Int posOffset;
  if(SDL_FALSE == processObjectOrderedObjectDrawer()){
    sxBool usePosOffset = reader->isItemExisting("positionOffset");
    reader->getBoolean("renderToTexture", useRenderToTexture, SDL_FALSE);
    if ( SDL_TRUE == usePosOffset ||
         SDL_TRUE == useRenderToTexture )
    {
      if (SDL_TRUE == usePosOffset){
        reader->getVec2Int("positionOffset", posOffset);
      }
      if(SDL_TRUE == useRenderToTexture){
        drawer = new ObjectDrawerRenderToTexture(posOffset);
        setupFade(static_cast<ObjectDrawerRenderToTexture*>(drawer));
      }else{
        drawer = new ObjectDrawerPosOffset(posOffset);
      }
    }
    else{
      drawer = new ObjectDrawerNormal();
    }
  }
  objects = &drawer->objects;
}

sxBool ObjectSceneFileLoader::processObjectOrderedObjectDrawer()
{
  sxBool orderedObjects = SDL_FALSE;
  reader->getBoolean("useOrderedObjects", orderedObjects, SDL_FALSE);
  if(SDL_TRUE == orderedObjects){
    drawerIsOrdered = SDL_TRUE;
    drawer = new ObjectDrawerOrdered();
    sxInt32 drawQueueId = 0;
    reader->getInteger("drawQueueId", drawQueueId);
    static_cast<ObjectDrawerOrdered*>(drawer)->setDrawQueueId(static_cast<sxUInt32>(drawQueueId));
  }
  return orderedObjects;
}

void ObjectSceneFileLoader::setupFade(ObjectDrawerRenderToTexture* drawer)
{
  std::vector<sxString> fadeInfoStr;
  if(ReturnCodes::RC_SUCCESS == reader->getStrList("fadeInfo", fadeInfoStr, SDL_FALSE, ' ')){
    sxInt32 fadeTime = 0;
    if( 2 == fadeInfoStr.size() &&
        ReturnCodes::RC_SUCCESS == strToInt(fadeInfoStr[1], fadeTime) )
    {
      if("IN" == fadeInfoStr[0]) drawer->setupFade(FadeInfo::FADE_IN, fadeTime);
      else if("OUT" == fadeInfoStr[0]) drawer->setupFade(FadeInfo::FADE_OUT, fadeTime);
      else sxLOG_E("Invalid fadeInfo values, IN or OUT is allowed only: %s", fadeInfoStr[0].c_str());
    }else{
      sxString fadeInfoValues;
      reader->getStr("fadeInfo", fadeInfoValues);
      sxLOG_E("Invalid fadeInfo values: %s", fadeInfoValues.c_str());
    }
  }
}

ReturnCodes ObjectSceneFileLoader::processOneObjectSection()
{
  ReturnCodes result;
  newObject = NULL;
  sxString objectName = createObject();

  if(NULL != newObject){
    result = processObject(objectName);
    newObject->objectProps.setSceneID(sceneIDinAct);
  }
  return result;
}

sxString ObjectSceneFileLoader::createObject()
{
  sxString objectName;
  objectName = getObjectName();
  if(ancestorName.empty()){
    // create object without ancestor info
    newObject = createObjectNoAncestor(objectName);
  }else{
    // create object with ancestor info and sets objectName also  
    newObject = createObjectFromAncestor(objectName);
  }
  return objectName;
}

sxString ObjectSceneFileLoader::getObjectName()
{
  sxString objectName;
  ancestorName.clear();
  substractAncestorFromCurrentSectionName();
  if(!ancestorName.empty()){
    // in case the object has an ancestor return with empty objectName
    // because it's easier to get the Object's name later
    return objectName;
  }
  reader->getStr("objectName", objectName);
  return objectName;
}

Object* ObjectSceneFileLoader::createObjectFromAncestor(sxString& outObjectName)
{
  std::map<sxString, AncestorInfo>::iterator ancestorIt = ancestorInfo.find(ancestorName);
  if(ancestorInfo.end() == ancestorIt){
    sxLOG_E("Ancestor not found: %s", ancestorName.c_str());
    return NULL;
  }
  outObjectName = ancestorIt->second.objectName; // ancestor object's name 
  Object* resultObject = static_cast<Object*>(getObjectManager->createObject(outObjectName));
  if(NULL == resultObject){
    sxLOG_E("This object should have been here: %s", ancestorIt->second.objectName.c_str());
    return NULL;
  }
  *resultObject = *(*objects)[ancestorIt->second.posInVector];
  return resultObject;
}

Object* ObjectSceneFileLoader::createObjectNoAncestor(const sxString& objectName)
{
  Object* resultObject = static_cast<Object*>(getObjectManager->createObject(objectName));
  return resultObject;
}

ReturnCodes ObjectSceneFileLoader::processObject(const sxString& objectName)
{
  ReturnCodes result;
  result = newObject->loadData(reader);
  if(ReturnCodes::RC_SUCCESS == result){
    result = loadObjectPosition();
  }
  if(ReturnCodes::RC_SUCCESS == result){
    objects->push_back(newObject);
    ancestorInfo[currentSectionName].posInVector = objects->size()-1;
    ancestorInfo[currentSectionName].objectName = objectName;
  }
  return result;
}

ReturnCodes ObjectSceneFileLoader::loadObjectPosition()
{
  sxVec2Real tempVec2Real;
  if(ReturnCodes::RC_SUCCESS != reader->getVec2Real("position", tempVec2Real)){
    return ReturnCodes::RC_FILE_PARSE_ERROR;
  }
  newObject->setPosition(tempVec2Real);
  return ReturnCodes::RC_SUCCESS;
}

void ObjectSceneFileLoader::processCommand()
{
  sxString commandName = getCommandName(currentSectionName);
  if(commandName.empty()){
    sxLOG_E("Error while parsing command section: %s", currentSectionName.c_str());
    return;
  }
  if(0 == commandName.compare(commandGenerate)){
    processGenerateCommand();
  }
  if(0 == commandName.compare(commandDialogWindow)){
    processDialogWindowCommand();
  }
}

void ObjectSceneFileLoader::processGenerateCommand()
{
  sxInt32 numberOfObjects;
  if(ReturnCodes::RC_SUCCESS != reader->getInteger("numberOfObjects", numberOfObjects)){
    return;
  }
  if(ReturnCodes::RC_SUCCESS != processOneObjectSection()){
    sxLOG_E("Something went wrong while generating objects. Check logs!");
    return;
  }
  sxUInt32 objPos = ancestorInfo[currentSectionName].posInVector;
  sxString objName = ancestorInfo[currentSectionName].objectName;
  for(sxInt32 i=1; i<numberOfObjects; i++){ // i=1 because one is already created
    newObject = static_cast<Object*>(getObjectManager->createObject(objName));
    *newObject = *(*objects)[objPos];
    objects->push_back(newObject);
  }
}

void ObjectSceneFileLoader::processDialogWindowCommand()
{  
  DialogWindowBuilder builder;
  builder.loadDialogWindow(reader, objects);
}
