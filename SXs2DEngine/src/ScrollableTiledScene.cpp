#include "ScrollableTiledScene.h"
#include "MainConfig.h"
#include "Profiler.h"

ScrollableTiledScene::ScrollableTiledScene()
{
  type = SceneTypeEnum::SCROLLABLE_TILED;
  init();
}

void ScrollableTiledScene::init()
{
  StaticTiledScene::init();
  velocity = sxVec2Int(0,0);
  scrollDirection.resetDirection();
}

ReturnCodes ScrollableTiledScene::processFileContent(const sxString& filename, TiledSceneFileLoader& loader)
{
  ReturnCodes result = StaticTiledScene::processFileContent(filename, loader);
  if(ReturnCodes::RC_SUCCESS == result){
    INIReader* reader = loader.getReaderObject();
    reader->getVec2Int("velocity", velocity);
  }
  setScrollDirection(velocity);
  return result;
}

void ScrollableTiledScene::draw()
{
  getProfiler->startCountingFor(strFormatter("0Draw"));
  if(SDL_TRUE == sceneProps.visible()){
    setStartingPosToDraw();
    mapHandler.resetRemainingDrawRepeats();

    storeOriginalRenderInfo();
    SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);
    changeRenderTargetTo(renderTexture);

    dealWithAllTheDrawingStuff();
    
    restoreOriginalRenderTargetAndRender();
    drawMethod = TiledSceneDrawMethodEnum::UPDATETILES;
    resetShouldRedrawValues();
  }
  getProfiler->stopCountingFor(strFormatter("0Draw"));
}

void ScrollableTiledScene::dealWithAllTheDrawingStuff()
{
  if(TiledSceneDrawMethodEnum::FULLREDRAW == drawMethod){
    SDL_RenderClear(renderer);
  }
  if(TiledSceneDrawMethodEnum::SCROLLHAPPENED == drawMethod){
    dealWithScrolling();
  }else{
    bottomRightToDraw = sceneCorners.bottomRight;
    prevPosToDraw = posToDraw;
    doDrawing();
  }
}

void ScrollableTiledScene::dealWithScrolling()
{
  sxBool doneNegativeScroll = SDL_FALSE; // if we done scrolling in negative direction, the grid update is not needed
  sxVec2Int shiftOffset = shiftRenderedTextureWithOffset();
  if(0 != shiftOffset.x){
    doneNegativeScroll = doHorizontalScrolling(shiftOffset);
    doDrawing();
    setStartingPosToDraw();
    mapHandler.resetRemainingDrawRepeats();
  }    
  if(0 != shiftOffset.y){
    sxBool temp = doVerticalScrolling(shiftOffset);
    doDrawing();
    setStartingPosToDraw();
    mapHandler.resetRemainingDrawRepeats();
    if(SDL_FALSE == doneNegativeScroll) doneNegativeScroll = temp;
  }
  if( SDL_FALSE == doneNegativeScroll &&
      NULL != grid )
  {
    drawMethod = TiledSceneDrawMethodEnum::UPDATE_TILES_AND_WHOLE_GRID;
    setStartingPosToDraw();
    mapHandler.resetRemainingDrawRepeats();
    bottomRightToDraw = sceneCorners.bottomRight;
    doDrawing();
  }
}

sxBool ScrollableTiledScene::doHorizontalScrolling(const sxVec2Int& shiftOffset)
{
  sxBool doneNegativeScroll = SDL_FALSE;
  if(0 < shiftOffset.x){
    drawMethod = TiledSceneDrawMethodEnum::SCROLLHAPPENED_POS_DIRECTION;
    bottomRightToDraw.x = shiftOffset.x;
    bottomRightToDraw.y = sceneCorners.bottomRight.y;
  }else{
    topLeftToDraw.x = sceneCorners.bottomRight.x + shiftOffset.x - tileSize.x;
    topLeftToDraw.y = posToDraw.y;
    bottomRightToDraw = sceneCorners.bottomRight;
    drawMethod = TiledSceneDrawMethodEnum::SCROLLHAPPENED_NEG_DIRECTION;
    doneNegativeScroll = SDL_TRUE;
  }
  return doneNegativeScroll;
}

sxBool ScrollableTiledScene::doVerticalScrolling(const sxVec2Int& shiftOffset)
{
  sxBool doneNegativeScroll = SDL_FALSE;
  if(0 < shiftOffset.y){
    drawMethod = TiledSceneDrawMethodEnum::SCROLLHAPPENED_POS_DIRECTION;
    bottomRightToDraw.x = sceneCorners.bottomRight.x;
    bottomRightToDraw.y = shiftOffset.y;
  }else{
    topLeftToDraw.x = posToDraw.x;
    topLeftToDraw.y = sceneCorners.bottomRight.y + shiftOffset.y - tileSize.y;
    bottomRightToDraw = sceneCorners.bottomRight;
    drawMethod = TiledSceneDrawMethodEnum::SCROLLHAPPENED_NEG_DIRECTION;
    doneNegativeScroll = SDL_TRUE;
  }
  return doneNegativeScroll;
}

void ScrollableTiledScene::drawOneRow()
{
//  getProfiler->startCountingFor(strFormatter("draw1row %d", yPos));
  if(TiledSceneDrawMethodEnum::FULLREDRAW == drawMethod){
    topLeftToDraw = posToDraw; // posToDraw different if the map is repeated
    drawWholeRow();
  }else if(TiledSceneDrawMethodEnum::SCROLLHAPPENED_POS_DIRECTION == drawMethod){
    topLeftToDraw = posToDraw; // posToDraw different if the map is repeated
    drawWholeRow();
  }else if(TiledSceneDrawMethodEnum::SCROLLHAPPENED_NEG_DIRECTION == drawMethod){
    drawWholeRow();
  }else if(TiledSceneDrawMethodEnum::UPDATE_TILES_AND_WHOLE_GRID == drawMethod){
    drawTilesWithShouldRedraw(true);
  }else if(TiledSceneDrawMethodEnum::UPDATETILES == drawMethod){
    drawTilesWithShouldRedraw(false);
  }

//  getProfiler->stopCountingFor(strFormatter("draw1row %d", yPos));
}

void ScrollableTiledScene::stepAnimation(const sxUInt32 ticksPassed)
{
  if(SDL_TRUE == sceneProps.enabled()){
    if(0 != velocity.x || 0 != velocity.y){
      setScrollDirection(velocity);
      drawMethod = TiledSceneDrawMethodEnum::SCROLLHAPPENED;
      sxFloat tickSec = ticksPassed/1000.0f;
      //offset.x += velocity.x * tickSec;
      //offset.y += velocity.y * tickSec;
      addToOffset(sxVec2Real(velocity.x * tickSec, velocity.y * tickSec));
      processOffsetChanges();
    }
    StaticTiledScene::stepAnimation(ticksPassed);
  }
}

void ScrollableTiledScene::processOffsetChanges()
{
  sxVec2Real newOffset = getOffset();
  if(SDL_TRUE == cutOffsetIfNeeded(tileSize.x, newOffset.x)){
    setOffset(newOffset);
    mapHandler.setNextStartingColumn(scrollDirection);
  }
  if(SDL_TRUE == cutOffsetIfNeeded(tileSize.y, newOffset.y)){
    setOffset(newOffset);
    mapHandler.setNextStartingRow(scrollDirection);
  }
}

sxBool ScrollableTiledScene::cutOffsetIfNeeded(const sxInt32 stepSize, sxFloat& currOffset)
{
  // offset can be between -tileSize..0
  if(currOffset < -stepSize){
    currOffset += stepSize;
    return SDL_TRUE;
  }else if(currOffset > 0){
    currOffset -= stepSize;
    return SDL_TRUE;
  }
  return SDL_FALSE;
}

sxVec2Int ScrollableTiledScene::shiftRenderedTextureWithOffset()
{
  if(scrollDirection.isInvalid()) return sxVec2Int();

  changeRenderTargetTo(bufferTexture);  
  SDL_RenderClear(renderer);

  sxVec2Int finalScrollOffset = calcScrollOffset();
  sxVec2Int logicalSize = getMainConfig->getWindowLogicalSize();
  sxRect dstRect;
  dstRect.x = finalScrollOffset.x;
  dstRect.y = finalScrollOffset.y;
  dstRect.w = logicalSize.x;
  dstRect.h = logicalSize.y;
  prevPosToDraw = posToDraw;
    
  SDL_RenderCopy(renderer, renderTexture, NULL, &dstRect);

  changeRenderTargetTo(renderTexture);
  SDL_RenderClear(renderer);

  SDL_RenderCopy(renderer, bufferTexture, NULL, NULL);
  
  return finalScrollOffset;
}

sxVec2Int ScrollableTiledScene::calcScrollOffset()
{
  sxVec2Int result;
  result.x = posToDraw.x - prevPosToDraw.x;
  result.y = posToDraw.y - prevPosToDraw.y;
  
  if(SDL_TRUE == scrollDirection.isLeftSet() && prevPosToDraw.x < posToDraw.x){
    result.x -= tileSize.x;
  }
  if(SDL_TRUE == scrollDirection.isUpSet() && prevPosToDraw.y < posToDraw.y){
    result.y -= tileSize.y;
  }
  if(SDL_TRUE == scrollDirection.isRightSet() && prevPosToDraw.x > posToDraw.x){
    result.x += tileSize.x;
  }
  if(SDL_TRUE == scrollDirection.isDownSet() && prevPosToDraw.y > posToDraw.y){
    result.y += tileSize.y;
  }
  return result;
}



