#include "ButtonObject.h"
#include "AudioManager.h"
#include "ObjectManager.h"
#include "FontManager.h"

ButtonObject::ButtonObject()
{
  type = ObjectTypes::ButtonObject;
  clickAnimPlaying = SDL_FALSE;
  label = NULL;
}

ButtonObject::~ButtonObject()
{
  if(NULL != label){
    delete label;
    label = NULL;
  }
}

void ButtonObject::assign(const ObjectBase& source)
{
  AnimObject::assign(source); // don't forget to call the ancestor's assign function
#ifdef _DEBUG
  const ButtonObject* sourceObj = dynamic_cast<const ButtonObject*>(&source);
#else
  const ButtonObject* sourceObj = static_cast<const ButtonObject*>(&source);
#endif
  defaultAnimName  = sourceObj->defaultAnimName;
  clickAnimName    = sourceObj->clickAnimName;
  clickAnimPlaying = sourceObj->clickAnimPlaying;
  clickSound       = sourceObj->clickSound;
  if(NULL != sourceObj->label){
    label            = static_cast<Object*>(getObjectManager->getObjectBuilder()->
                                               createInstance(sourceObj->label->type));
    if(NULL != label) *label = *sourceObj->label; // label can also be generated with setLabel() function
    else sxLOG_E("Missing implementation! You wanted to copy a label created in ButtonObject with SetLabel()");
  }
}

ReturnCodes ButtonObject::loadData(INIReader* reader)
{
  ReturnCodes result;
  if(animations.empty()){ /// won't load animations again
    result = loadButtonAnims(reader);
  }
  loadLabel(reader);
  Object::loadData(reader);
  processClickSoundField(reader);
  return result;
}
void ButtonObject::callDrawMethodAndModifiers(SDL_Renderer*& renderer)
{
  AnimObject::callDrawMethodAndModifiers(renderer);
  if(NULL != label){
    label->draw(renderer);
  }
}

ReturnCodes ButtonObject::loadButtonAnims(INIReader* reader)
{
  std::vector<sxString> animNames;
  if(ReturnCodes::RC_SUCCESS != reader->getStr("defaultAnim", defaultAnimName)){
    return ReturnCodes::RC_FIELD_NOT_FOUND;
  }
  animNames.push_back(defaultAnimName);
  if(ReturnCodes::RC_SUCCESS != reader->getStr("clickAnim", clickAnimName)){
    return ReturnCodes::RC_FIELD_NOT_FOUND;
  }
  animNames.push_back(clickAnimName);
  loadAnimSprites(animNames);
  return ReturnCodes::RC_SUCCESS;
}

void ButtonObject::loadLabel(INIReader* reader)
{
  sxString labelObjStr;
  reader->getStr("labelObject", labelObjStr, SDL_FALSE);
  if(!labelObjStr.empty()){
    label = static_cast<Object*>(getObjectManager->createObject(labelObjStr));
  }
}

void ButtonObject::setLabel(const sxString& btnLabel, const sxString& fontName)
{
  if(NULL != label){
    delete label;
    label = NULL;
  }
  if(!btnLabel.empty()){
    if( fontName.empty() ||
      ReturnCodes::RC_SUCCESS == getFontManager->setFontToRender(fontName) )
    {
      label = getFontManager->generateText(btnLabel);
      setPosition(getPosition());
    }
  }
}

void ButtonObject::processClickSoundField(INIReader* reader)
{
  reader->getStr("clickSound", clickSound, SDL_FALSE);
}

void ButtonObject::buttonHit()
{
  clickAnimPlaying = SDL_TRUE;
  setAnimation(clickAnimName);
  play(SDL_TRUE);
  if(!clickSound.empty()){
    getAudioManager->playSound(clickSound);
  }
}

void ButtonObject::stepAnimation(const sxUInt32 ticksPassed)
{
  if(SDL_TRUE == objectProps.objectEnabled()){
    AnimObject::stepAnimation(ticksPassed);
    if( SDL_FALSE == spriteChangerPlaying &&
        SDL_TRUE == clickAnimPlaying )
    {
      clickAnimPlaying = SDL_FALSE;
      setAnimation(defaultAnimName);
      play(SDL_TRUE);
    }
  }
}

void ButtonObject::setPosition(const sxVec2Real& newPos)
{
  AnimObject::setPosition(newPos);
  if(NULL != label){
    sxVec2Real pos = realPosition;
    pos.x += getWidth()/2 - label->getWidth()/2;
    pos.y += getHeight()/2 - label->getHeight()/2;
    label->setPosition(pos);
  }
}