#include "LogManager.h"
#include "SystemIO.h"

sxString LogManager::APPLICATION_VERSION = __DATE__ " " __TIME__;
sxBool LogManager::errorHappened = SDL_FALSE;
sxBool LogManager::warningHappened = SDL_FALSE;
sxString LogManager::logFileName = "dummyFile.log";
sxLogLevel  LogManager::globalLogLevel = LL_TRACE;
sxBool      LogManager::logToFile = SDL_TRUE;
sxString LogManager::errorLogMessages;
sxString LogManager::warningLogMessages;

void LogManager::init(sxBool shouldLogToFile)
{  
  sxString strTime = SystemIO::getFullCurrentTimeAsStr();
  logFileName.clear();
  logToFile = shouldLogToFile;
  if(SDL_FALSE == SystemIO::createDirInBasePath(LOGFILE_DIR, logFileName)){
    logToFile = SDL_FALSE;
  }

  logFileName += strFormatter(LOGFILE_NAME, strTime.c_str());
  SDL_Log("Application Version: %s", APPLICATION_VERSION.c_str());
  SDL_Log("Log file's place: %s", logFileName.c_str());
  if(shouldLogToFile){
    sxLOG(LL_TRACE, "Application Version: %s", APPLICATION_VERSION.c_str());
    sxLOG(LL_TRACE, "Log file's place: %s", logFileName.c_str());
  }
}

void LogManager::toFile(sxLogLevel logLevel, sxString msg)
{
  if(logLevel <= globalLogLevel){
//TODO: find out where to log on OSX
#ifndef __MACOSX__
    if(SDL_TRUE == logToFile){
      sxFile logFile(logFileName, "a");
      if(ReturnCodes::RC_SUCCESS == logFile.error){
        sxString resultMsg;
        resultMsg = SystemIO::getCurrentSystemTime();    
        resultMsg += strFormatter(" | %s\n", msg.c_str());
        logFile.write(resultMsg);
      }
    }
#endif
    if(LL_ERROR == logLevel){
      errorLogMessages += strFormatter("%s\n", msg.c_str());
      errorHappened = SDL_TRUE;
    }
    if(LL_WARNING == logLevel)
    {
      warningLogMessages += strFormatter("%s\n", msg.c_str());
      warningHappened = SDL_TRUE;
    }
  }
}
