#include "WindowScene.h"
#include "ActManager.h"
#include "MainConfig.h"
#include "SceneMsgTypes.hpp"
#include "SceneMsgSendString.hpp"
#include "SceneMsgCloseScene.hpp"

void WindowScene::draw()
{
  if(SDL_TRUE == sceneProps.visible()) drawer->drawAll(renderer);
}

sxBool WindowScene::handleMsgFromOtherScene(SceneMsg* msg)
{
  if(SceneMsgTypes::SceneMsgCloseScene == msg->type){
    sxInt32 sceneID = static_cast<SceneMsgCloseScene*>(msg)->sceneID;
    getActManager->removeSceneFromAct(static_cast<sxUInt32>(sceneID));
    return SDL_TRUE;
  }
  return SDL_FALSE;
}

sxBool WindowScene::processInputEvent(sxEvent& event)
{
  if(SDL_FALSE == sceneProps.enabled()) return SDL_FALSE;

  std::vector<Object*>::iterator objIt = drawer->objects.begin();
  while(objIt != drawer->objects.end()){
    if( SDL_TRUE == (*objIt)->objectProps.objectEnabled() &&
        SDL_TRUE == (*objIt)->objectProps.clickable() )
    {
      if(SDL_TRUE == objectHit(*objIt, event)){
        if( sxEventTypes::MOUSE_BUTTON_UP == event.type() ||
            sxEventTypes::TOUCH_FINGER_UP == event.type() )
        {
          (*objIt)->objectHit(event);
        }
        return SDL_TRUE;
      }
    }
    // the object's collisionEnabled field is not needed for mouse interaction
    ++objIt;
  }
  return sceneProps.catchAllInput();
}

sxBool WindowScene::objectHit(Object* obj, sxEvent& inputPressed)
{
  if( inputPressed.type() == sxEventTypes::MOUSE_BUTTON_UP ||
      inputPressed.type() == sxEventTypes::MOUSE_BUTTON_DOWN )
  {
    CollisionData cData = obj->getCollisionData();
    return collDetector.objectCollidesWith(drawer->posOffset, &cData,
                                           sxVec2Int(inputPressed.button().x, 
                                                     inputPressed.button().y));
  }else if( inputPressed.type() == sxEventTypes::TOUCH_FINGER_UP ||
            inputPressed.type() == sxEventTypes::TOUCH_FINGER_DOWN )
  {
    CollisionData cData = obj->getCollisionData();
    sxVec2Int pos = getMainConfig->getWindowLogicalSize();
    pos.x = static_cast<sxInt32>(pos.x * inputPressed.tfinger().x);
    pos.y = static_cast<sxInt32>(pos.y * inputPressed.tfinger().y);

    return collDetector.objectCollidesWith(drawer->posOffset, &cData,
                                           pos);
  }

  return SDL_FALSE;
}
