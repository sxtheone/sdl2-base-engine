#include "sxSprite.h"
#include "LogManager.h"

sxSprite::sxSprite()
{
  size.h = 0;
  size.w = 0;
  size.x = 0;
  size.y = 0;
  texture = NULL;
  hitbox = NULL;
}

void sxSprite::setTexture(SDL_Texture* sdlTexture, sxBool shouldDestroyOnExit)
{
  freeTexture();
  texture = sdlTexture;
  textureLoadedHere = shouldDestroyOnExit;
  setMaxWidthHeight();
}

void sxSprite::setTexture(sxTexture* otherTexture, sxBool shouldDestroyOnExit)
{
  freeTexture();
  texture = otherTexture->getTexture();
  textureLoadedHere = shouldDestroyOnExit;
  setMaxWidthHeight();
}

void sxSprite::setTopLeftPosition(sxVec2Int newPos)
{
  size.x = newPos.x;
  size.y = newPos.y;
}

void sxSprite::setWidthHeight(sxVec2Int newPos)
{
  size.w = newPos.x;
  size.h = newPos.y;
}

void sxSprite::setMaxWidthHeight()
{
  if(NULL == texture){
    sxLOG(LL_ERROR, "Texture is NULL!");
    return;
  }
  size.w = getTextureWidth();
  size.h = getTextureHeight();
}