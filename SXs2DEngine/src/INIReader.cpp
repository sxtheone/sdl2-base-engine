#include <algorithm>
#include "INIReader.h"
#include "LoadedFiles.h"
#include "INIParser.h"

INIReader::INIReader(const sxString& filename, sxBool checkIfFileAlreadyLoaded)
{
  sxString filenameWithPath;
  openedFile = filename;
  if(":::" == filename.substr(0, 3)){ // full path strats with :::
    filenameWithPath = filename.substr(3);
  }else{
    filenameWithPath = assetDirectory;
    filenameWithPath.append(filename);
  }
  if( SDL_TRUE == checkIfFileAlreadyLoaded &&
      SDL_FALSE == fileShouldBeProcessed(filenameWithPath) )
  {
    return;
  }
  INIParser parser;
  _error = parser.parseFile(filenameWithPath, iniData, sectionOrderInFile);
  currentSectionIdx = 0;
}

INIReader::~INIReader()
{
  sxLOG(LL_TRACE, "Destroying reader for file: %s", openedFile.c_str());
}

sxBool INIReader::fileShouldBeProcessed(const sxString& filename)
{
  if(SDL_TRUE == getLoadedFiles->isFileAlreadyLoaded(filename)){
    _error = ReturnCodes::RC_FILE_ALREADY_LOADED;
    return SDL_FALSE;
  }
  getLoadedFiles->addToLoadedFilesList(filename);
  return SDL_TRUE;
}

ReturnCodes INIReader::ParseError()
{
  return _error;
}

ReturnCodes INIReader::getVectorStrList(const sxString& name, std::vector<sxString>& vectorStrList,
                                        const sxBool mandatory)
{
  ReturnCodes result = getStrList(name, vectorStrList, mandatory);
  if(ReturnCodes::RC_SUCCESS != result){
    return result;
  }
  if(2 != vectorStrList.size()){
    sxLOG(LL_ERROR, "Needed 2 coords, found: %d for field named: %s",
      vectorStrList.size(), name.c_str());
    return ReturnCodes::RC_FILE_PARSE_ERROR;
  }
  return ReturnCodes::RC_SUCCESS;
}

ReturnCodes INIReader::getVec2Real(const sxString& name, sxVec2Real& value, const sxBool mandatory)
{
  std::vector<sxString> vectorStr;
  ReturnCodes result = getVectorStrList(name, vectorStr, mandatory);
  if(ReturnCodes::RC_SUCCESS != result){
    return result;
  }
  result = stringToReal(vectorStr[0], value.x);
  if(ReturnCodes::RC_SUCCESS == result){
    result = stringToReal(vectorStr[1], value.y);
  }
  return result;
}

ReturnCodes INIReader::getVec2Int(const sxString& name, sxVec2Int& value, const sxBool mandatory)
{
  std::vector<sxString> vectorStr;
  ReturnCodes result = getVectorStrList(name, vectorStr, mandatory);
  if(ReturnCodes::RC_SUCCESS != result){
    return result;
  }
  result = stringToInt(vectorStr[0], value.x);
  if(ReturnCodes::RC_SUCCESS == result){
    result = stringToInt(vectorStr[1], value.y);
  }
  return result;
}

ReturnCodes INIReader::getMatrix2D(const sxString& name, sxMatrix2D& value, const sxBool mandatory)
{
  sxVec2Int vecValue;
  ReturnCodes result = getVec2Int(name, vecValue, mandatory);
  if(ReturnCodes::RC_SUCCESS == result){
    value.row = vecValue.x;
    value.col = vecValue.y;
  }
  return result;
}

ReturnCodes INIReader::getColor(const sxString& name, sxColor& value, const sxBool mandatory)
{
  std::vector<sxString> colorStr;
  ReturnCodes result;

  result = getStrList(name, colorStr, mandatory, ' ');
  if(ReturnCodes::RC_SUCCESS != result){
    return result;
  }  
  if(4 != colorStr.size()){
    sxLOG_E("Needed 4 colors, found: %d for: %s / %s.",
      colorStr.size(), currentSection.c_str(), name.c_str());
    return ReturnCodes::RC_FILE_PARSE_ERROR;
  }
  convertStrToColor(colorStr, value);
  return ReturnCodes::RC_SUCCESS;
}

ReturnCodes INIReader::getColorList(const sxString& name, std::vector<sxColor>& outputList,
                                    const sxBool mandatory, const sxChar separator)
{
  std::vector<sxString> colorListStr;
  ReturnCodes result = getStrList(name, colorListStr, mandatory, ',');
  if(ReturnCodes::RC_SUCCESS != result){
    return result;
  }
  std::vector<sxString>::iterator it = colorListStr.begin();
  while(it != colorListStr.end()){
    std::vector<sxString> oneColorStr;
    sxColor resultColor;
    cutStrToStrListTemplate(*it, oneColorStr, ' ');
    convertStrToColor(oneColorStr, resultColor);
    outputList.push_back(resultColor);
    ++it;
  }
  return result;
}

void INIReader::convertStrToColor(const std::vector<sxString>& colorStr,
                                  sxColor& outColor)
{
  sxInt32 color;
  stringToInt(colorStr[0], color);
  outColor.r = static_cast<sxUInt8>(color);
  stringToInt(colorStr[1], color);
  outColor.g = static_cast<sxUInt8>(color);
  stringToInt(colorStr[2], color);
  outColor.b = static_cast<sxUInt8>(color);
  stringToInt(colorStr[3], color);
  outColor.a = static_cast<sxUInt8>(color);
}

ReturnCodes INIReader::getFileNameWithPath(const sxString& name, sxString& value)
{
  ReturnCodes result = sectionAndFieldValid(name, SDL_TRUE);
  if(ReturnCodes::RC_SUCCESS != result){
    return result;
  }
  value = assetDirectory + iniData[currentSection][name];
  return ReturnCodes::RC_SUCCESS;
}

ReturnCodes INIReader::getInteger(const sxString& name, sxInt32& value, const sxBool mandatory)
{
  ReturnCodes result;
  sxString strValue;
  result = getStr(name, strValue, mandatory);
  if(ReturnCodes::RC_SUCCESS != result){
    return result;
  }
  return stringToInt(strValue, value);
}

ReturnCodes INIReader::getReal(const sxString& name, sxFloat& value, const sxBool mandatory)
{
  ReturnCodes result;
  sxString strValue;
  result = getStr(name, strValue, mandatory);
  if(ReturnCodes::RC_SUCCESS != result){
    return result;
  }
  return stringToReal(strValue, value);
}

ReturnCodes INIReader::getDouble(const sxString& name, sxDouble& value, const sxBool mandatory)
{
  ReturnCodes result;
  sxString strValue;
  result = getStr(name, strValue, mandatory);
  if(ReturnCodes::RC_SUCCESS != result){
    return result;
  }
  return stringToDouble(strValue, value);
}

ReturnCodes INIReader::getBoolean(const sxString& name, sxBool& value, const sxBool mandatory)
{
  sxString valstr;
  ReturnCodes result;

  result = getStr(name, valstr, mandatory);
  if(ReturnCodes::RC_SUCCESS != result){
    return result;
  }
  
  std::transform(valstr.begin(), valstr.end(), valstr.begin(), ::tolower);
  if( 0 == valstr.compare("true") || 
      0 == valstr.compare("yes") || 
      0 == valstr.compare("on") || 
      0 == valstr.compare("1") )
  {
    value = SDL_TRUE;
    return ReturnCodes::RC_SUCCESS;
  }

  if( 0 == valstr.compare("false") ||
      0 == valstr.compare("no") || 
      0 == valstr.compare("off") ||
      0 == valstr.compare("0") )
  {
    value = SDL_FALSE;
    return ReturnCodes::RC_SUCCESS;
  }

  sxLOG_E("Wrong value for bool: %s / %s: %s", currentSection.c_str(), name.c_str(),
          valstr.c_str());
  return ReturnCodes::RC_FILE_PARSE_ERROR;
}

sxBool INIReader::setSection(const sxString section, const sxBool mandatory)
{
  if(iniData.count(section)){
    setFirstSection();
    while(section != currentSection){
      ++currentSectionIdx;
      if(currentSectionIdx < sectionOrderInFile.size()){
        currentSection = sectionOrderInFile[currentSectionIdx];
      }else{
        sxLOG_E("Section should have been found: %s", section.c_str());
        return SDL_FALSE;
      }
    }
  }else{
    currentSection.clear();
    if(SDL_TRUE == mandatory){
      sxLOG_E("section not found: %s, file: %s", section.c_str(), openedFile.c_str());
    }
    return SDL_FALSE;
  }

  return SDL_TRUE;
}

sxString INIReader::setFirstSection()
{
  currentSectionIdx = 0;
  if(iniData.empty()){
    sxLOG(LL_ERROR, "No sections loaded.");
    currentSection.clear();
  }else{
    currentSection = sectionOrderInFile[0];
  }
  return currentSection;
}

sxString INIReader::setNextSection()
{
  if(ReturnCodes::RC_SUCCESS == selectedSectionValid()){
    ++currentSectionIdx;
    if(currentSectionIdx >= sectionOrderInFile.size()){ // current section is the last
      currentSection.clear();
      currentSectionIdx = sectionOrderInFile.size(); // don't give a chance to overflow
    }else{
      currentSection = sectionOrderInFile[currentSectionIdx];
    }    
  }else{
    currentSection.clear();
  }  
  return currentSection;
}

sxBool INIReader::isItemExisting(sxString itemName)
{
  if(currentSection.empty()){
    return SDL_FALSE;
  }
  if( iniData.find(currentSection)->second.end() !=
      iniData.find(currentSection)->second.find(itemName))
  {
    return SDL_TRUE;
  }
  return SDL_FALSE;
}

ReturnCodes INIReader::selectedSectionValid()
{
  if( currentSection.empty() ||
      iniData.end() == iniData.find(currentSection) )
  {
    sxLOG_E("Selected section is empty/not exists: '%s'", currentSection.c_str());
    return ReturnCodes::RC_SECTION_NOT_FOUND;
  }
  return ReturnCodes::RC_SUCCESS;
}

ReturnCodes INIReader::fieldValid(const sxString& fieldName, const sxBool mandatory)
{
  if(0 == iniData[currentSection].count(fieldName)){
    if(SDL_TRUE == mandatory){
      sxLOG_E("Requested field not exists: %s / %s", currentSection.c_str(), fieldName.c_str());
    }
    return ReturnCodes::RC_FIELD_NOT_FOUND;
  }
  return ReturnCodes::RC_SUCCESS;
}

ReturnCodes INIReader::sectionAndFieldValid(const sxString& fieldName, sxBool fieldMandatory)
{
  ReturnCodes result = selectedSectionValid();
  if(ReturnCodes::RC_SUCCESS == result){
    result = fieldValid(fieldName, fieldMandatory);
  }
  return result;
}

ReturnCodes INIReader::stringToDouble(const sxString& strValue, sxDouble& doubleValue)
{
  ReturnCodes result = strToDouble(strValue, doubleValue);
  if(ReturnCodes::RC_SUCCESS != result){
    sxLOG_E("conversion to double failed: %s", strValue.c_str());
  }
  return result;
}

ReturnCodes INIReader::stringToReal(const sxString& strValue, sxFloat& realValue)
{
  ReturnCodes result = strToReal(strValue, realValue);
  if(ReturnCodes::RC_SUCCESS != result){
    sxLOG_E("conversion to real failed: %s", strValue.c_str());
  }
  return result;
}

ReturnCodes INIReader::stringToInt(const sxString& strValue, sxInt32& intValue)
{
  ReturnCodes result = strToInt(strValue, intValue);
  if(ReturnCodes::RC_SUCCESS != result){
    sxLOG_E("conversion to Int failed: %s", strValue.c_str());
  }
  return result;
}

ReturnCodes INIReader::stringListToIntList(const std::vector<sxString>& strList,
                                           std::vector<sxInt32>& outList)
{
  ReturnCodes result = ReturnCodes::RC_SUCCESS;
  for(sxUInt32 i=0; i<strList.size(); ++i){
    sxInt32 value;
    if(ReturnCodes::RC_SUCCESS == stringToInt(strList[i], value)){
      outList.push_back(value);
    }else{
      sxLOG_E("Invalid value: %s", strList[i].c_str());
      result = ReturnCodes::RC_INVALID_VALUE;
    }
  }
  return result;
}

ReturnCodes INIReader::getIntList(const sxString& name, std::vector<sxInt32>& outputList,
                                    const sxBool mandatory, const sxChar separator)
{
  std::vector<sxString> intListStr;
  ReturnCodes result = getStrList(name, intListStr, mandatory, ',');
  if(ReturnCodes::RC_SUCCESS != result){
    return result;
  }
  return stringListToIntList(intListStr, outputList);
}

ReturnCodes INIReader::stringToIntList(const sxString& str, const sxChar separator,
                                         std::vector<sxInt32>& outVector)
{
  std::vector<sxString> intStr;
  cutString(str, separator, intStr);
  return stringListToIntList(intStr, outVector);
}