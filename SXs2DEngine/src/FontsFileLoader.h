#pragma once
#include "FileLoaderBase.h"
#include "sxFont.h"

class FontsFileLoader : public FileLoaderBase
{
public:
  FontsFileLoader() : thisIsTheSystemFontFile(SDL_FALSE) {}
  ReturnCodes processFile(const sxString& filename);
  sxBool thisIsTheSystemFontFile;

private:
  void processAllFontSection();
  void processOneFontSection();
  void processNormalFont();
  void processSystemFont();
  void processFont(sxFont* outFont);
  void dealWithAncestorIfPresent();
};
