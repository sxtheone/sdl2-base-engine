#include "StaticTiledScene.h"
#include "MainConfig.h"
#include "Profiler.h"

StaticTiledScene::StaticTiledScene()
{
  type = SceneTypeEnum::STATIC_TILED;
  init();
}

StaticTiledScene::~StaticTiledScene()
{
  destroyTexture(renderTexture);
  destroyTexture(bufferTexture);
  destroyTiles();
  delete grid;
  grid = NULL;
}

void StaticTiledScene::init()
{
  destroyTiles();
  sceneCorners.topLeft = sxVec2Int(0,0);
  sceneCorners.bottomRight = getMainConfig->getWindowLogicalSize();
  tileSize = sxVec2Int(1,1);
  posToDraw = sxVec2Int(0,0);
  offset = sxVec2Real(0,0);
  grid = NULL;
  renderTexture = NULL;
  bufferTexture = NULL;
  drawMethod = TiledSceneDrawMethodEnum::FULLREDRAW;
  createFullscreenTexture(renderTexture);
  createFullscreenTexture(bufferTexture);
}

void StaticTiledScene::setOffset(const sxVec2Real& newOffset)
{
  offset = newOffset;
}

void StaticTiledScene::addToOffset(const sxVec2Real& offsetToAdd)
{
  offset.x += offsetToAdd.x; 
  offset.y += offsetToAdd.y;
}

void StaticTiledScene::createFullscreenTexture(SDL_Texture*& texture)
{
  destroyTexture(texture);
  sxVec2Int logicalSize = getMainConfig->getWindowLogicalSize();
  texture = SDL_CreateTexture(renderer, 
                              SDL_PIXELFORMAT_RGBA8888,
                              SDL_TEXTUREACCESS_TARGET,
                              logicalSize.x, 
                              logicalSize.y);
  SDL_SetTextureBlendMode(texture, SDL_BLENDMODE_BLEND);
}

void StaticTiledScene::destroyTiles()
{
  for(sxUInt32 i=0; i<tiles.size(); ++i){
    delete tiles[i];
  }
  tiles.clear();
  objectTiles.clear();
}


ReturnCodes StaticTiledScene::loadFile(const sxString& filename)
{
  init();
  TiledSceneFileLoader loader(id, tiles, objectTiles, mapHandler);
  ReturnCodes result = processFileContent(filename, loader);
  if(ReturnCodes::RC_SUCCESS == result){
    name = loader.getSceneName();
    sceneProps = loader.getSceneProps();
    if(SDL_TRUE == loader.isCollisionInfoPresent()){
      grid = new ScreenGrid();
      setupGridSize(loader.getSizeOfBiggestHitbox());
    }
  }
  // shrink vector capacity so no extra, empty items are reserved
  std::vector<ObjectBase*>(tiles).swap(tiles);
  mapHandler.setFirstStartingPoint();
  return result;
}

CollisionData StaticTiledScene::getCollisionData(const sxUInt32 objectIndex)
{
  GridElement* elem = grid->getGridElement(objectIndex);
  CollisionData result;
  result.objectIndex = elem->itemIdx;
  result.position = elem->position;
  result.object = tiles[result.objectIndex];
  return result;
}

sxUInt32 StaticTiledScene::getNumOfCollisionObjects()
{
  if(NULL != grid){
    return grid->getNumOfItemsInSearchGrid();
  }else{
    sxLOG_E("No ScreenGrid present!");
    return 0;
  }
}

void StaticTiledScene::draw()
{
  getProfiler->startCountingFor(strFormatter("Draw"));
  if(SDL_TRUE == sceneProps.visible()){
    if(!mapHandler.areMapsLoaded()) return;

    setStartingPosToDraw();
    mapHandler.resetRemainingDrawRepeats();

    storeOriginalRenderInfo();
    SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);
    changeRenderTargetTo(renderTexture);

    bottomRightToDraw = sceneCorners.bottomRight;
    doDrawing();
    
    restoreOriginalRenderTargetAndRender();
    drawMethod = TiledSceneDrawMethodEnum::UPDATETILES;
    resetShouldRedrawValues();
  }
  getProfiler->stopCountingFor(strFormatter("Draw"));
}

void StaticTiledScene::doDrawing()
{
  sxInt32 startDrawingPosX = posToDraw.x;
  sxBool moreToDraw = SDL_TRUE;
  sxBool drawingFirstRowOfMaps = SDL_TRUE;
  
  while( bottomRightToDraw.y > posToDraw.y &&
          SDL_TRUE == moreToDraw )
  {
    // every row starts with the original start X position
    posToDraw.x = startDrawingPosX;
    mapHandler.getBackToStartingMap();
    drawTheMaps(drawingFirstRowOfMaps);
    posToDraw.y += tileSize.y;
    moreToDraw = mapHandler.shouldRepeatRow();
    drawingFirstRowOfMaps = SDL_FALSE;
  }
}


void StaticTiledScene::drawTheMaps(const sxBool drawingFirstRowOfMaps)
{
  sxBool drawingFirstMap = SDL_TRUE;
  sxInt32 startingPosY = posToDraw.y;
  while(bottomRightToDraw.x > posToDraw.x){
    if(SDL_TRUE == drawingFirstRowOfMaps){
      mapHandler.setRowToStartingRow();
    }
    posToDraw.y = startingPosY;
    drawOneMap(drawingFirstMap); 
    mapHandler.setNextMapToDraw();
    drawingFirstMap = SDL_FALSE;
  }
}


void StaticTiledScene::drawOneMap(const sxBool drawingFirstMap)
{
  sxBool finished = SDL_FALSE;
  sxInt32 startingPosX = posToDraw.x;
  sxUInt32 yPos = 0;
  while(SDL_FALSE == finished){
    if(SDL_TRUE == drawingFirstMap){
      mapHandler.setColToStartingCol();
    }
    drawOneRow();
    if(SDL_TRUE == moveToTheNextRowIfCan()){
      posToDraw.x = startingPosX;
    }else{
      finished = SDL_TRUE;
    }
    ++yPos;
  }
}

sxBool StaticTiledScene::moveToTheNextRowIfCan()
{
  if(SDL_TRUE == mapHandler.moveToNextRowToDraw()){
    posToDraw.y += tileSize.y;
    if(bottomRightToDraw.y > posToDraw.y){
      return SDL_TRUE;
    }
  }
  return SDL_FALSE;
}

void StaticTiledScene::drawOneRow()
{
  if(TiledSceneDrawMethodEnum::FULLREDRAW == drawMethod){
    topLeftToDraw = posToDraw;
    drawWholeRow();
  }else if(TiledSceneDrawMethodEnum::UPDATETILES == drawMethod){
    drawTilesWithShouldRedraw(false);
  }
}

//NOTE: there are a lot of 'if's here. If performance drops, check this code!
void StaticTiledScene::drawWholeRow()
{
  sxUInt32 tileIdxToDraw = 0;
  sxUInt32 prevTileIdxToDraw = UINT_MAX;
  ObjectBase* obj = NULL;
  while(bottomRightToDraw.x > posToDraw.x){
    tileIdxToDraw = mapHandler.getTileIndexToDraw();
    getTileIndexFromMapHandler(tileIdxToDraw, prevTileIdxToDraw, obj);
    // we are always starting at the beginning of the row (X is around 0 position)
    // no matter what is the topLeftToDraw.x. Because of this, the function also updates
    // the animated objects in the map even they are outside of the
    // topLeftToDraw - bottomRightToDraw rectangle.
    if( SDL_TRUE == obj->objectProps.shouldRedraw() ||
        (posToDraw.x >= topLeftToDraw.x && posToDraw.y >= topLeftToDraw.y) )
    {
      obj->draw(renderer, posToDraw);
    }
    if(NULL != grid){
      GridElement gridElem(tileIdxToDraw,
                           sxVec2Real(static_cast<sxFloat>(posToDraw.x), static_cast<sxFloat>(posToDraw.y)));
      grid->setElement(gridElem);
      drawDebugBoxes(tileIdxToDraw);
    }
    posToDraw.x += tileSize.x;
    if(SDL_FALSE == mapHandler.moveToNextTileIndexToDraw()){
      break;
    }
  }
}

void StaticTiledScene::drawTilesWithShouldRedraw(const bool updateTheWholeGrid)
{
  sxUInt32 tileIdxToDraw = 0;
  sxUInt32 prevTileIdxToDraw = UINT_MAX; // the start value should differ from tileIdxToDraw
  ObjectBase* obj = NULL;
  while(bottomRightToDraw.x > posToDraw.x)
  {
    tileIdxToDraw = mapHandler.getTileIndexToDraw();
    getTileIndexFromMapHandler(tileIdxToDraw, prevTileIdxToDraw, obj);

    if(SDL_TRUE == obj->objectProps.shouldRedraw()){
      fillWithTransparent(posToDraw, obj->getWidth(), obj->getHeight());
      obj->draw(renderer, posToDraw);
      GridElement gridElem(tileIdxToDraw,
                            sxVec2Real(static_cast<sxFloat>(posToDraw.x), static_cast<sxFloat>(posToDraw.y)));
      grid->setElement(gridElem);
    }else if(updateTheWholeGrid && NULL != grid){
      GridElement gridElem(tileIdxToDraw,
                           sxVec2Real(static_cast<sxFloat>(posToDraw.x), static_cast<sxFloat>(posToDraw.y)));
      grid->setElement(gridElem);
	  }
    drawDebugBoxes(tileIdxToDraw);

    posToDraw.x += tileSize.x;
    if(SDL_FALSE == mapHandler.moveToNextTileIndexToDraw()){
      break;
    }
  }
}

void StaticTiledScene::getTileIndexFromMapHandler(const sxUInt32 tileIdxToDraw,
                                                  sxUInt32& prevTileIdxToDraw, ObjectBase*& obj)
{
  if(prevTileIdxToDraw != tileIdxToDraw){
    obj = tiles[tileIdxToDraw];
    prevTileIdxToDraw = tileIdxToDraw;
  }
}

void StaticTiledScene::fillWithTransparent(const sxVec2Int& pos, 
                                           const sxInt32 width, const sxInt32 height)
{
  sxRect rect;
  rect.x = pos.x;
  rect.y = pos.y;
  rect.w = width;
  rect.h = height;
//TODO: set if we need blend these mode settings for transparent textures
//  SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_NONE); // don't blend when clearing texture
  SDL_RenderFillRect(renderer, &rect);
//  SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);

}

void StaticTiledScene::stepAnimation(const sxUInt32 ticksPassed)
{
  if(SDL_TRUE == sceneProps.enabled()){
    sxUInt32 numOfObjs = objectTiles.size();
    for(sxUInt32 i=0; i<numOfObjs; ++i){
      objectTiles[i]->stepAnimation(ticksPassed);
    }
  }
}

ReturnCodes StaticTiledScene::processFileContent(const sxString& filename, 
                                                 TiledSceneFileLoader& loader)
{
  ReturnCodes result = loader.processFile(filename);
  if(ReturnCodes::RC_SUCCESS == result){
    INIReader* reader = loader.getReaderObject();
    fillTileSize();
    reader->setSection("Config");
    reader->getVec2Int("topLeft", sceneCorners.topLeft, SDL_FALSE);
    reader->getVec2Int("bottomRight", sceneCorners.bottomRight, SDL_FALSE);
    reader->getVec2Int("tileSize", tileSize, SDL_FALSE);
  }
  return result;
}

void StaticTiledScene::fillTileSize()
{
  if(tiles.empty()){
    sxLOG_E("Tiles vector is empty. This scene probably fail.");
    return;
  }
  tileSize = static_cast<TileElement*>(tiles[0])->getSpriteSize();
}

void StaticTiledScene::setupGridSize(const sxVec2Int& biggestHitbox)
{
  sxVec2Int windowSize = getMainConfig->getWindowLogicalSize();
  sxVec2Int gridSize(1,1); // one more item is needed because 
                           // the mapper will draw it when scrolling
  if(0 != windowSize.x%tileSize.x){
    ++gridSize.x;
  }
  if(0 != windowSize.y%tileSize.y){
    ++gridSize.y;
  }
  gridSize.x += windowSize.x/tileSize.x;
  gridSize.y += windowSize.y/tileSize.y;
  grid->setGridSize(gridSize, tileSize, biggestHitbox);
}

void StaticTiledScene::setSceneSize(const sxVec2Int& size)
{
  sceneSize = size;
  updateBottomRightPosition();
}

void StaticTiledScene::updateBottomRightPosition()
{
  sceneCorners.bottomRight.x = sceneCorners.topLeft.x + sceneSize.x; 
  sceneCorners.bottomRight.y = sceneCorners.topLeft.y + sceneSize.y;
}

void StaticTiledScene::setTopLeftPosition(const sxVec2Int& pos)
{
  sceneCorners.topLeft = pos;
  updateBottomRightPosition();
}

void StaticTiledScene::setSceneWidth(const sxInt32 width)
{
  sceneSize.x = width; 
  sceneCorners.bottomRight.x = sceneCorners.topLeft.x + sceneSize.x;
}

void StaticTiledScene::setSceneHeight(const sxInt32 height)
{
  sceneSize.y = height; 
  sceneCorners.bottomRight.y = sceneCorners.topLeft.y + sceneSize.y;
}

void StaticTiledScene::setSearchRectangle(const sxVec2Real& pixelPos, const sxVec2Int& size)
{
  std::vector< std::vector<GridElement> > searchGrid;
  searchGrid = mapHandler.getSearchGrid(offset, pixelPos, size, tileSize);
  grid->setSearchRectNew(searchGrid);
  //  grid->setSearchRectangle(pixelPos, size);
  //TODO: fill grid
}

void StaticTiledScene::setBottomRightPosition(const sxVec2Int& bottomRightPos)
{
  sceneCorners.bottomRight = bottomRightPos;
  sceneSize.x = sceneCorners.bottomRight.x - sceneCorners.topLeft.x;
  sceneSize.y = sceneCorners.bottomRight.y - sceneCorners.topLeft.y;
}

void StaticTiledScene::storeOriginalRenderInfo()
{
  origRenderTexture = SDL_GetRenderTarget(renderer);
  SDL_GetRenderDrawBlendMode(renderer, &origBlendMode);
  SDL_GetRenderDrawColor(renderer, &origDrawColor.r, &origDrawColor.g, &origDrawColor.b,
                         &origDrawColor.a);
}

void StaticTiledScene::setStartingPosToDraw()
{
  posToDraw.x = sceneCorners.topLeft.x + static_cast<sxInt32>(offset.x);
  posToDraw.y = sceneCorners.topLeft.y + static_cast<sxInt32>(offset.y);
  if(NULL != grid){
    grid->setGrid00pos(posToDraw);
  }
}

void StaticTiledScene::destroyTexture(SDL_Texture*& texture)
{
  if(NULL != texture){
    SDL_DestroyTexture(texture);
    texture = NULL;
  }
}

void StaticTiledScene::changeRenderTargetTo(SDL_Texture* texture)
{
  SDL_SetRenderTarget(renderer, texture);
  SDL_SetRenderDrawColor(renderer, 128, 128, 128, 0); // clear with transparent
  if(TiledSceneDrawMethodEnum::FULLREDRAW == drawMethod){
    SDL_RenderClear(renderer);
  }
}

void StaticTiledScene::restoreOriginalRenderTargetAndRender()
{
  SDL_SetRenderTarget(renderer, origRenderTexture);
  /*
  // render only the scene size
  SDL_Rect r;
  r.x = sceneCorners.topLeft.x; r.y = sceneCorners.topLeft.y;
  r.w = sceneSize.x; r.h = sceneSize.y;
  SDL_RenderCopy(renderer, renderTexture, &r, &r);
  */
  SDL_RenderCopy(renderer, renderTexture, NULL, NULL);
  SDL_SetRenderDrawBlendMode(renderer, origBlendMode);
  SDL_SetRenderDrawColor(renderer, origDrawColor.r, origDrawColor.g, origDrawColor.b,
                         origDrawColor.a);
}

void StaticTiledScene::resetShouldRedrawValues()
{
  // currently only items in objectTiles vector has shouldRedraw switched
  std::vector<Object*>::iterator it = objectTiles.begin();
  while(it != objectTiles.end())
  {
    (*it)->objectProps.setShouldRedraw(SDL_FALSE);
    ++it;
  }
}

void StaticTiledScene::drawDebugBoxes(const sxUInt32 tileIdxToDraw)
{
#ifdef DEVELOPER_HELPERS
  if(SDL_TRUE == getProfiler->drawDebugBoxesActive()){
    if(NULL != tiles[tileIdxToDraw]->collisionProps){
      sxVec2Real posReal;
      posReal.x = static_cast<sxFloat>(posToDraw.x);
      posReal.y = static_cast<sxFloat>(posToDraw.y);
      getProfiler->addToDrawDebugHitboxes(posReal, 
                    *tiles[tileIdxToDraw]->collisionProps->hitbox,
                    Color_Red);
    }
  }
#endif
}