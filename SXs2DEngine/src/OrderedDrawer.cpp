#include "OrderedDrawer.h"

OrderedDrawer* OrderedDrawer::orderedDrawer = NULL;

void OrderedDrawer::createManager()
{
  if (NULL != orderedDrawer){
    sxLOG_W("OrderedDrawer was already instantinated.");
    delete orderedDrawer;
  }
  orderedDrawer = new OrderedDrawer();
}

OrderedDrawer* OrderedDrawer::getInstance()
{
  if (NULL == orderedDrawer){
    sxLOG_E("Not yet created!");
    return NULL;
  }
  return orderedDrawer;
}

OrderedDrawer::OrderedDrawer()
{
  sxLOG_T("OrderedDrawer created.");
}

OrderedDrawer::~OrderedDrawer()
{
  sxLOG_T("OrderedDrawer destroyed.");
}

void OrderedDrawer::destroyManager()
{
  sxLOG_T("Destroying OrderedDrawer...");
  if (NULL == orderedDrawer){
    sxLOG_E("OrderedDrawer instance is NULL, but how?");
    return;
  }
  delete orderedDrawer;
  orderedDrawer = NULL;
  sxLOG_T("OrderedDrawer destroyed.");
}

void OrderedDrawer::frameStarts()
{
  currQueueIt = drawQueues.begin();
  while(currQueueIt != drawQueues.end()){
    currQueueIt->drawFinished = SDL_FALSE;
    ++currQueueIt;
  }
}

void OrderedDrawer::addToQueue(const sxUInt32 queueId, Object* object)
{
  if(queueId >= drawQueues.size()){
    drawQueues.resize(queueId+1);
  }
  addObjectToQueue(queueId, object);
}

//NOTE: if more than one renderer is present, we only render with the first
//scene's renderer. This may cause problems..
void OrderedDrawer::draw(/*const sxInt32 drawQueueId, */SDL_Renderer* renderer)
{
  currQueueIt = drawQueues.begin();
  while(currQueueIt != drawQueues.end()){
    listIt = currQueueIt->queue.begin();
    if(SDL_FALSE == currQueueIt->drawFinished){
      while(listIt != currQueueIt->queue.end()){
        (*listIt)->draw(renderer);
        ++listIt;
      }
      currQueueIt->drawFinished = SDL_TRUE;
    }
    ++currQueueIt;
  }
}

void OrderedDrawer::addObjectToQueue(const sxUInt32 queueId, Object* object)
{
  currQueueIt = drawQueues.begin() + queueId;
  listIt = currQueueIt->queue.begin();
  sxFloat objPosY = object->getPosition().y;

  while(listIt != currQueueIt->queue.end()){
    if((*listIt)->getPosition().y > objPosY){
      currQueueIt->queue.insert(listIt, object);
      return;
    }
    ++listIt;
  }
  if(currQueueIt->queue.empty()){
    currQueueIt->queue.push_front(object);
  }else{
    currQueueIt->queue.insert(listIt, object);
  }
}

void OrderedDrawer::objectMoved(const sxUInt32 queueId, Object* object)
{
  currQueueIt = drawQueues.begin() + queueId;

  std::list<Object*>::iterator insertPlace = currQueueIt->queue.end();
  std::list<Object*>::iterator deletePlace = currQueueIt->queue.end();
  
  if(SDL_TRUE == findObjectPrevAndCurrentPlace(object, deletePlace, insertPlace)){
    listIt = deletePlace;
    if(++listIt != insertPlace){
      currQueueIt->queue.insert(insertPlace, object);
      currQueueIt->queue.erase(deletePlace);
    }
  }else{
    sxLOG_E("Not all the task have been finished!");
  }
}

sxBool OrderedDrawer::findObjectPrevAndCurrentPlace(Object* object,
                                                  std::list<Object*>::iterator& oldPlace,
                                                  std::list<Object*>::iterator& newPlace)
{
#define ADD_DONE 1
#define DELETE_DONE 2
#define ALL_FINISHED 3
  sxInt32 tasksDone = 0;
  sxFloat objPosY = object->getPosition().y;
  listIt = currQueueIt->queue.begin();
  //NOTE: empty list is not handled here
  while(listIt != currQueueIt->queue.end()){
    if( !(tasksDone & ADD_DONE) &&
        (*listIt)->getPosition().y > objPosY )
    {
      newPlace = listIt;
      tasksDone |= ADD_DONE;
    }
    if((*listIt) == object){
      oldPlace = listIt;
      tasksDone |= DELETE_DONE;
    }
    ++listIt;

    if(ALL_FINISHED == tasksDone){
      return SDL_TRUE;
    }
  }

  if(DELETE_DONE == tasksDone){
    newPlace = currQueueIt->queue.end();
    tasksDone |= ADD_DONE;
  }

  if(ALL_FINISHED == tasksDone){
    return SDL_TRUE;
  }
  return SDL_FALSE;
}

void OrderedDrawer::objectDeleted(const sxUInt32 drawQueue, const sxInt32 objectId)
{
  sxLOG_E("IMPLEMENT MEEEEE!");
}