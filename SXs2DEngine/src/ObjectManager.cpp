#include "ObjectManager.h"

ObjectManager* ObjectManager::objectManager = NULL;

ObjectManager::ObjectManager()
{
  sxLOG(LL_TRACE, "Created.");
}

ObjectManager::~ObjectManager()
{
  destroyAllObjects();
  sxLOG(LL_TRACE, "Deleted.");
}

void ObjectManager::createManager()
{
  if(NULL != objectManager){
    delete objectManager;
  }
  objectManager = new ObjectManager();
}

ObjectManager* ObjectManager::getInstance()
{
  if(NULL == objectManager){
    sxLOG(LL_ERROR, "Object Manager is not yet created.");
    return NULL;
  }
  return objectManager;
}

void ObjectManager::destroyManager()
{
  sxLOG(LL_TRACE, "Destroying ObjectManager...");
  if(NULL == objectManager){
    sxLOG(LL_ERROR, "Object Manager instance is NULL, but how?");
    return;
  }
  delete objectManager;
  objectManager = NULL;
  sxLOG(LL_TRACE, "ObjectManager destroyed.");
}

/// basic types registered automatically
void ObjectManager::registerObjectType(const sxCharStr& id, AbstractFactory<ObjectBase>* type)
{
  objectBuilder.registerFactory(id, type);
}

ReturnCodes ObjectManager::addObject(const sxString& objectName, ObjectBase* object)
{
  objectsIterator = objects.find(objectName);
  if(objects.end() != objectsIterator){
    sxLOG_T("Object already loaded, this one will be skipped: %s", objectName.c_str());
    destroyObject(object);
    return ReturnCodes::RC_OBJECT_ALREADY_LOADED;
  }
  objects[objectName] = object;
  return ReturnCodes::RC_SUCCESS;
}

void ObjectManager::destroyAllObjects()
{
  objectsIterator = objects.begin();
  while(objects.end() != objectsIterator){
    destroyObject(objectsIterator->second);
    ++objectsIterator;
  }
  objects.clear();
}

void ObjectManager::destroyObject(ObjectBase* obj)
{
  if(NULL != objectsIterator->second->collisionProps){
    delete obj->collisionProps;
    obj->collisionProps = NULL;
  }
  delete obj;
  obj = NULL;
}

sxBool ObjectManager::setObjectIteratorTo(const sxString& objectName)
{
  objectsIterator = objects.find(objectName);
  if(objects.end() == objectsIterator){
    sxLOG_E("Couldn't find object named: %s", objectName.c_str());
    return SDL_FALSE;
  }
  return SDL_TRUE;
}

sxCharStr ObjectManager::getObjectType(const sxString& objectName)
{
  objectsIterator = objects.find(objectName);
  if(objects.end() == objectsIterator){
    sxLOG_E("No object found with name: %s", objectName.c_str());
    return sxCharStr();
  }
  return objectsIterator->second->type;
}

ObjectBase* ObjectManager::createObject(const sxString& objectName)
{
  if(SDL_FALSE == setObjectIteratorTo(objectName)){
    return NULL;
  }
  ObjectBase* outObject = createObjectInstance();
  if(NULL == outObject){
    return NULL;
  }
  *outObject = *objectsIterator->second;
  //objectsIterator->second->copyEverythingTo(outObject);
  return outObject;
}

ObjectBase* ObjectManager::createObjectInstance()
{
  if(objectsIterator->second->type.empty()){
    return NULL;
  }
  ObjectBase* outObject;
  outObject = objectBuilder.createInstance(objectsIterator->second->type);
  if(NULL == outObject){
    sxLOG_E("Couldn't create object: %s with type: %s because the type is not registered.",
      objectsIterator->first.c_str(), objectsIterator->second->type.c_str());
  }
  return outObject;
}

void ObjectManager::setObjectCollisionProps(const sxString& objName, CollisionProps& collProps)
{
  objectsIterator = objects.find(objName);
  if(objects.end() == objectsIterator){
    sxLOG_E("No object found with name: %s", objName.c_str());
    return;
  }
  if(NULL == objects[objName]->collisionProps){
    sxLOG_E("No collision objectProps for object: %s", objName.c_str());
    return;
  }
  *objectsIterator->second->collisionProps = collProps;
}
