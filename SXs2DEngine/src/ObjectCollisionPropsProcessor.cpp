#include "ObjectCollisionPropsProcessor.h"
#include "ObjectManager.h"

ObjectCollisionPropsProcessor* ObjectCollisionPropsProcessor::objCollisionPropsProcessor = NULL;


ObjectCollisionPropsProcessor::ObjectCollisionPropsProcessor()
{
  sxLOG(LL_TRACE, "Created.");
}

ObjectCollisionPropsProcessor::~ObjectCollisionPropsProcessor()
{
  clearCollisionProperties();
  sxLOG(LL_TRACE, "Deleted.");
}

void ObjectCollisionPropsProcessor::createManager()
{
  if(NULL != objCollisionPropsProcessor){
    delete objCollisionPropsProcessor;
  }
  objCollisionPropsProcessor = new ObjectCollisionPropsProcessor();
}

ObjectCollisionPropsProcessor* ObjectCollisionPropsProcessor::getInstance()
{
  if(NULL == objCollisionPropsProcessor){
    sxLOG(LL_ERROR, "collision Detector is not yet created.");
    return NULL;
  }
  return objCollisionPropsProcessor;
}

void ObjectCollisionPropsProcessor::destroyManager()
{
  sxLOG(LL_TRACE, "Destroying...");
  if(NULL == objCollisionPropsProcessor){
    sxLOG(LL_ERROR, "Instance is NULL, but how?");
    return;
  }
  delete objCollisionPropsProcessor;
  objCollisionPropsProcessor = NULL;
  sxLOG(LL_TRACE, "destroyed.");
}

void ObjectCollisionPropsProcessor::addToObjectCollisionMap(const sxCharStr& collisionID,
                                                const sxCharStr& objectName,
                                                const std::vector<sxCharStr>& collidesWith)
{
  RawCollisionData collData;
  collData.collisionID = collisionID;
  collData.collidesWith = collidesWith;
  std::pair<sxCharStr, RawCollisionData> objRawInfo(objectName, collData);

  if(objectSettings.rawMap.insert(objRawInfo).second){
    // set the collision ID string without the final number
    objectSettings.finalCollisionIDs[collisionID] = 0;
  }else{
    // no error log because this can happen when adding an Act to 
    // the top what has common items with the older Act
    //sxLOG_E("Object is already present in the collision map: %s", collisionID.c_str());
  }
}

void ObjectCollisionPropsProcessor::processCollisionSettings()
{
  processObjectsCollisionSettings();
  addCollisionPropsToObjects();
}

void ObjectCollisionPropsProcessor::processObjectsCollisionSettings()
{
  generateFinalCollisionIDs();
  fillCollisionIDFields();
  fillCollidesWithFields();
}

void ObjectCollisionPropsProcessor::generateFinalCollisionIDs()
{
  if(0 == objectSettings.nextCollisionID.collisionID){
    objectSettings.nextCollisionID.collisionID = 1;
  }
  std::map<sxCharStr, sxUInt32>::iterator collIt = objectSettings.finalCollisionIDs.begin();
  while(objectSettings.finalCollisionIDs.end() != collIt){
    collIt->second = objectSettings.nextCollisionID.collisionID;
    objectSettings.nextCollisionID.collisionID = objectSettings.nextCollisionID.collisionID << 1;
    if(0 == objectSettings.nextCollisionID.collisionID){
      sxLOG_E("Collision IDs overflowed. You are adding too much acts top of each other!");
    }
    ++collIt;
  }
}

void ObjectCollisionPropsProcessor::fillCollisionIDFields()
{
  rawIt = objectSettings.rawMap.begin();
  while(objectSettings.rawMap.end() != rawIt){
    if(objectSettings.finalMap.end() == objectSettings.finalMap.find(rawIt->first)){
      objectSettings.finalMap[rawIt->first].collisionID = 
                         objectSettings.finalCollisionIDs[rawIt->second.collisionID];
    }
    ++rawIt;
  }
}

void ObjectCollisionPropsProcessor::fillCollidesWithFields()
{
  // fill the CollidesWith fields with the already calculated collisionIDs
  rawIt = objectSettings.rawMap.begin();
  while(objectSettings.rawMap.end() != rawIt){
    for(sxUInt32 i=0; i<rawIt->second.collidesWith.size(); ++i){
      objectSettings.finalMap[rawIt->first].collidesWith |= 
        objectSettings.finalCollisionIDs[rawIt->second.collidesWith[i]];
    }
    ++rawIt;
  }
}

void ObjectCollisionPropsProcessor::addCollisionPropsToObjects()
{
  // fill ObjectManager::objects with the data from the already filled finalMap 
  propsIt = objectSettings.finalMap.begin();
  while(objectSettings.finalMap.end() != propsIt){
    sxString str = (char*)propsIt->first.c_str();
    getObjectManager->setObjectCollisionProps(str, propsIt->second);
    ++propsIt;
  }
}

void ObjectCollisionPropsProcessor::clearCollisionProperties()
{
  if(NULL != objCollisionPropsProcessor){
    objectSettings.init();
  }
}
