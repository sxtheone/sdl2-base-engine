#include "ObjectsFileLoader.h"
#include "ObjectManager.h"
#include "SpritesFileLoader.h"
#include "AnimsFileLoader.h"
#include "SoundsFileLoader.h"
#include "ObjectCollisionPropsProcessor.h"
#include "FontsFileLoader.h"
#include "FontManager.h"
#include "SpriteManager.h"
#include "TileElement.h"


ObjectFileLoader::~ObjectFileLoader()
{
  destroyReader();
}

ReturnCodes ObjectFileLoader::processFile(const sxString& filename)
{
  loadedObjectsNames.clear();
  ReturnCodes result = createReader(filename, checkIfFileAlreadyLoaded);
  if(shouldExitFromLoader(result)){
    return result;
  }

  if(ReturnCodes::RC_SUCCESS != processSections()){
    sxLOG(LL_ERROR, "Object file: %s loading failed.", filename.c_str());
    destroyReader();
    return ReturnCodes::RC_FILE_PARSE_ERROR;
  }

  destroyReader();
  return ReturnCodes::RC_SUCCESS;
}

ReturnCodes ObjectFileLoader::processSections()
{
  ReturnCodes result;
  objectBuilder = getObjectManager->getObjectBuilder();
  result = processConfigSection();
  if(ReturnCodes::RC_SUCCESS != result){
    return result;
  }
  currentSectionName = reader->setFirstSection();
  if(currentSectionName.empty()){
    return result;
  }
  processObjectSections();

  return result;
}

ReturnCodes ObjectFileLoader::processConfigSection()
{
  if(SDL_FALSE == reader->setSection("Config")){
    return ReturnCodes::RC_SECTION_NOT_FOUND;
  }
  SpritesFileLoader spriteLoader;
  loadFileList("spriteFiles", spriteLoader);

  AnimsFileLoader animsLoader;
  loadFileList("animationFiles", animsLoader);

  SoundsFileLoader soundLoader;
  loadFileList("soundFiles", soundLoader);

  FontsFileLoader fontsLoader;
  loadFileList("fontFiles", fontsLoader);

  return ReturnCodes::RC_SUCCESS;
}

void ObjectFileLoader::processObjectSections()
{
  currentSectionName = reader->setFirstSection();
  do{
    if(0 != currentSectionName.compare("Config")){
      processOneObjectSection();
    }
    currentSectionName = reader->setNextSection();
  }while(!currentSectionName.empty());
}

void ObjectFileLoader::processOneObjectSection()
{
  if(SDL_FALSE == isCommandSection(currentSectionName)){
    dealWithAncestorIfPresent();
    if(ReturnCodes::RC_SUCCESS != loadAnObjectFromFile()){
      sxLOG_E("Object not loaded: %s", currentSectionName.c_str());
      return;
    }
    loadCollisionRelatedCommonFields();
    addObjectToObjectManager(currentSectionName, newObject);
  }else{
    processCommand(); // also adds the object(s)
  }
}

void ObjectFileLoader::dealWithAncestorIfPresent()
{
  ancestorName.clear();
  substractAncestorFromCurrentSectionName();
  if(!ancestorName.empty()){
    sxLOG(LL_ERROR, "Inheritance for .objects files is not yet implemented.");
  }
}

ReturnCodes ObjectFileLoader::loadAnObjectFromFile()
{
  ReturnCodes result;
  newObject = NULL;
  result = createObject();
  if(ReturnCodes::RC_SUCCESS != result){
    return result;
  }
  result = loadObjectData();
  if(ReturnCodes::RC_SUCCESS != result){
    delete newObject;
    newObject = NULL;
    return result;
  }
  return result;
}

void ObjectFileLoader::addObjectToObjectManager(const sxString& objectName, ObjectBase* object)
{
  getObjectManager->addObject(objectName, newObject);
  loadedObjectsNames.push_back(objectName);
}

ReturnCodes ObjectFileLoader::createObject()
{
  if(SDL_TRUE == reader->isItemExisting("fontName")){
    newObject = createLabelObject();
  }else{
    newObject = createOrdinaryObject();
  }
  if(NULL == newObject){
    return ReturnCodes::RC_FILE_PARSE_ERROR;    
  }
  return ReturnCodes::RC_SUCCESS;
}

ObjectBase* ObjectFileLoader::createOrdinaryObject()
{
  ObjectBase* resultObject = NULL;
  sxString typeString;
  if(ReturnCodes::RC_SUCCESS != reader->getStr("type", typeString)){
    sxLOG_E("Object's type field is missing! Object name: %s", currentSectionName.c_str());
    return NULL;
  }
  resultObject = objectBuilder->createInstance(typeString);
  if(NULL == resultObject){
    sxLOG_E("Object type is invalid. Type: %s, Object: %s", 
            typeString.c_str(), currentSectionName.c_str());
  }
  return resultObject;
}

void ObjectFileLoader::loadCollisionRelatedCommonFields()
{
  createCollisionProps();
  setClickable();
  loadCollidesWith();
  loadIdleTimeAfterCollision();
}

void ObjectFileLoader::createCollisionProps()
{
  sxBool createCollProps = SDL_FALSE;
  reader->getBoolean("collisionPossible", createCollProps, SDL_FALSE);
  if(SDL_TRUE == reader->isItemExisting("collidesWith")){
    createCollProps = SDL_TRUE;
  }
  if(ReturnCodes::RC_SUCCESS != reader->getStr("collisionGroupID", collisionGroupID, SDL_FALSE)){
    if(0 == currentSectionName.find("command:")){
      sxLOG_E("collisionGroupID is mandatory for objects/TileElements made "
        "with 'command:' argument. Command name: %s", currentSectionName.c_str());
      return;
    }
    collisionGroupID = currentSectionName;
    sxLOG_T("No collisionGroupID, using name for collisions: %s", collisionGroupID.c_str());
  }
  newObject->setupCollisionRelatedData(createCollProps);
  createHitboxForObjectIfNotExistButShould();
}

void ObjectFileLoader::setClickable()
{
  sxBool clickable = SDL_FALSE;
  reader->getBoolean("clickable", clickable, SDL_FALSE);
  newObject->objectProps.setClickable(clickable);
  if(SDL_TRUE == clickable){
    if(NULL == newObject->collisionProps){
      // setClickable is called after createCollisionProps so
      // it must be set already in newObject
      newObject->setupCollisionRelatedData(newObject->objectProps.collisionPossible());
      createHitboxForObjectIfNotExistButShould();
    }
  }
}

void ObjectFileLoader::loadCollidesWith()
{
  if(SDL_TRUE == newObject->objectProps.collisionPossible()){
    std::vector<sxCharStr> collidesWith;
    reader->getStrList("collidesWith", collidesWith, SDL_FALSE);
    // after loading all objects, object::objectProps::collisionID and collidesWith
    // fields are generated from this vector
    getObjectCollisionPropsProcessor->addToObjectCollisionMap(collisionGroupID,
                                                    currentSectionName, collidesWith);
  }
}

void ObjectFileLoader::loadIdleTimeAfterCollision()
{
  if(SDL_TRUE == newObject->objectProps.collisionPossible()){
    sxInt32 idleTimeAfterCollision = 0;
    reader->getInteger("idleTimeAfterCollision", idleTimeAfterCollision, SDL_FALSE);
    newObject->collisionProps->idleAfterCollision = static_cast<sxUInt32>(idleTimeAfterCollision);
  }
}

ReturnCodes ObjectFileLoader::loadObjectData()
{
  ReturnCodes result = newObject->loadData(reader);
  if(ReturnCodes::RC_SUCCESS != result){
    sxLOG_E("Error loading object data. Object: %s", currentSectionName.c_str());
  }
  return result;
}

ObjectBase* ObjectFileLoader::createLabelObject()
{
  sxString label, fontName;
  if( ReturnCodes::RC_SUCCESS != reader->getStr("label", label) ||
      ReturnCodes::RC_SUCCESS != reader->getStr("fontName", fontName) )
  {
    return NULL;
  }
  return generateFontObject(fontName, label);
}

ObjectBase* ObjectFileLoader::generateFontObject(const sxString& fontName, 
                                                 const sxString& text)
{
  if(ReturnCodes::RC_SUCCESS == getFontManager->setFontToRender(fontName)){
    sxInt32 lineWidth = 0;
    reader->getInteger("labelWidth", lineWidth, SDL_FALSE);
    return getFontManager->generateText(text, static_cast<sxUInt32>(lineWidth));
  }
  sxLOG_E("Couldn't find %s font for object: %s. Loading text with system font.",
    fontName.c_str(), currentSectionName.c_str());
  return getFontManager->generateSystemText(text);
}

void ObjectFileLoader::createHitboxForObjectIfNotExistButShould()
{
  //TODO: clickable, collisionPossible and collisionEnabled is bad naming and
  // logic flaw is that with false collPoss and false collEnab, clickable can be true
  // collisionPossible -> objectCollision ?
  // collisionEnabled could be used for both clickable and objectCollision. Of ObjectEnabled is good instead?
  if( (SDL_TRUE == newObject->objectProps.collisionPossible() ||
       SDL_TRUE == newObject->objectProps.clickable()) &&
      NULL == newObject->collisionProps->hitbox )
  {
    sxQuad hitbox;
    hitbox.bottomRight.x = static_cast<sxFloat>(newObject->getSpriteSize().x);
    hitbox.bottomRight.y = static_cast<sxFloat>(newObject->getSpriteSize().y);
    getSpriteManager->addHitbox(currentSectionName, hitbox);
    sxQuad* storedHitbox = getSpriteManager->getHitboxRef(currentSectionName);
    newObject->setNewHitbox(storedHitbox);
    sxLOG_T("Hitbox generated for: %s", currentSectionName.c_str());
  }
}

void ObjectFileLoader::processCommand()
{
  sxString commandName = getCommandName(currentSectionName);
  if(commandName.empty()){
    sxLOG_E("Error while parsing command section: %s", currentSectionName.c_str());
    return;
  }
  if(0 == commandName.compare(commandMakeTiles)){
    processMakeTilesCommand();
  }
}

void ObjectFileLoader::processMakeTilesCommand()
{
  std::vector<sxString> spriteNameList;
  if(ReturnCodes::RC_SUCCESS != reader->getStrList("sprites", spriteNameList)){
    sxLOG_E("No 'sprites' field found!");
    return;
  }
  loadAllTiles(spriteNameList);
}

void ObjectFileLoader::loadAllTiles(std::vector<sxString>& spriteNameList)
{
  if(ReturnCodes::RC_SUCCESS != loadAnObjectFromFile()){
    sxLOG_E("Object not loaded: %s", currentSectionName.c_str());
    return;
  }
  // create the first object and add it to ObjectManager
  sxSprite* tileSprite = getSpriteManager->getSprite(spriteNameList[0]);
  static_cast<TileElement*>(newObject)->setSprite(tileSprite);
  loadCollisionRelatedCommonFieldsForTileElement(spriteNameList[0]);
  addObjectToObjectManager(spriteNameList[0], newObject);
  // make it a reference Object and create the rest from it
  ObjectBase* referenceObject = newObject;
  spriteNameList.erase(spriteNameList.begin());
  createTilesWithSpriteList(referenceObject, spriteNameList);
}

/// creates TileElement objects with the reference object and a different sprite
void ObjectFileLoader::createTilesWithSpriteList(const ObjectBase* referenceObject,
                                                 const std::vector<sxString>& spriteNameList)
{
  sxSprite* tileSprite;
  for(sxUInt32 i=0; i<spriteNameList.size(); i++){
    newObject = objectBuilder->createInstance(ObjectTypes::TileElement);
    *newObject = *referenceObject;
    newObject->collisionProps = NULL;
    loadCollisionRelatedCommonFieldsForTileElement(spriteNameList[i]);
    tileSprite = getSpriteManager->getSprite(spriteNameList[i]);
    static_cast<TileElement*>(newObject)->setSprite(tileSprite);
    addObjectToObjectManager(spriteNameList[i], newObject);
  }  
}

void ObjectFileLoader::loadCollisionRelatedCommonFieldsForTileElement(const sxString& sprName)
{
  // it's easier to temporary change the currentSectionName than copy-paste and change the code
  sxString tempStr = currentSectionName;
  currentSectionName = sprName;
  loadCollisionRelatedCommonFields();
  currentSectionName = tempStr;
}
