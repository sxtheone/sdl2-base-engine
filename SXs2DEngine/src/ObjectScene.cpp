#pragma once
#include "ObjectScene.h"
#include "ObjectSceneFileLoader.h"


ReturnCodes ObjectScene::loadFile(const sxString& filename)
{
  ObjectSceneFileLoader loader(id);
  ReturnCodes result = loader.processFile(filename);
  drawer = loader.getFilledObjectDrawer();
  objects = &drawer->objects;
  name = loader.getSceneName();
  sceneProps = loader.getSceneProps();
  setupSliding(loader.getSlidingSpeed(), loader.getSlidingDistance());

  return result;
}

void ObjectScene::switchObjectWithIdTagTo(sxUInt16 idTag, Object* newObject)
{
    if (newObject != nullptr)
    {
        sxBool objFound = SDL_FALSE;
        for (size_t i = 0; i < objects->size(); ++i)
        {
            if ((*objects)[i]->idTag == idTag)
            {
                objFound = SDL_TRUE;
                delete (*objects)[i];
                (*objects)[i] = newObject;
                break;
            }
        }

        if (SDL_FALSE == objFound) sxLOG_E("Obj not found. idTag: %d", idTag);
    }
    else sxLOG_E("New object is null, you should delete the object from objects instead!");
}

void ObjectScene::setupSliding(const sxVec2Real& speed, const sxVec2Real& distance)
{
  slidingInfo.setSpeed(speed);
  if(SDL_TRUE == slidingInfo.isSpeedSet()){
    slidingInfo.setTargetPos(drawer->posOffset);
    slidingInfo.setDistance(distance);
    slidingInfo.setToStart();
    drawer->posOffset = slidingInfo.getCurrentPos();
  }
}

void ObjectScene::destroyObjects()
{
  if(NULL == drawer){
    return;
  }
  drawer->destroyObjects();
}

void ObjectScene::draw()
{
  if(SDL_TRUE == sceneProps.visible()) drawer->drawAll(renderer);
}

void ObjectScene::stepAnimation(const sxUInt32 ticksPassed)
{    
  stepSliding(ticksPassed);
  if(SDL_TRUE == sceneProps.enabled()){
    sxVec2Real prevPos;
    sxUInt32 numOfObjs = objects->size();
    for(sxUInt32 i=0; i<numOfObjs; ++i){
      prevPos = (*objects)[i]->getPosition();
      (*objects)[i]->stepAnimation(ticksPassed);
      if(prevPos.y != (*objects)[i]->getPosition().y){
        drawer->objectMoved((*objects)[i]);
      }
    }
  }
}

void ObjectScene::stepSliding(const sxUInt32& ticksPassed)
{
  if(SDL_FALSE == slidingInfo.isFinished()){
    sxFloat secondPerTicksPassed = ticksPassed/1000.0f;
    slidingInfo.step(secondPerTicksPassed);
    drawer->posOffset = slidingInfo.getCurrentPos();
  }
}

CollisionData ObjectScene::getCollisionData(const sxUInt32 objectIndex)
{
  CollisionData result = (*objects)[objectIndex]->getCollisionData();
  result.position.x = result.position.x + drawer->posOffset.x;
  result.position.y = result.position.y + drawer->posOffset.y;
  result.objectIndex = objectIndex;
  return result;
}
