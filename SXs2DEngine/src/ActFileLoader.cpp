#include "ActFileLoader.h"

ActFileLoader::~ActFileLoader()
{
  destroyReader();
}

ReturnCodes ActFileLoader::processFile(const sxString& filename)
{
  ReturnCodes result = createReader(filename, SDL_FALSE); //don't check if file already loaded
  if(shouldExitFromLoader(result)){
    return result;
  }
  
  result = processSections();
  return result;
}

ReturnCodes ActFileLoader::processSections()
{
  ReturnCodes result = processConfigSection();
  if(ReturnCodes::RC_SUCCESS != result){
    return result;
  }
  result = processScenes();
  return result;
}

ReturnCodes ActFileLoader::processConfigSection()
{
  ReturnCodes result;
  if(SDL_FALSE == reader->setSection("Config")){
    return ReturnCodes::RC_SECTION_NOT_FOUND;
  }
  if(ReturnCodes::RC_SUCCESS == result){
    result = reader->getStr("name", actName);
  }
  if(ReturnCodes::RC_SUCCESS == result){
    result = reader->getStrList("orderOfScenes", sceneOrder);
  }
  if (ReturnCodes::RC_SUCCESS == result) {
      sxDouble opacity = 1;
      result = reader->getDouble("windowOpacity", opacity);
      windowOpacity = static_cast<sxFloat>(opacity);
  }
  return result;
}

ReturnCodes ActFileLoader::processScenes()
{
  if(sceneOrder.empty()){
    return ReturnCodes::RC_FILE_PARSE_ERROR;
  }
  for(sxUInt32 i=0; i<sceneOrder.size(); ++i){
    if(SDL_TRUE == reader->setSection(sceneOrder[i])){
      currentSectionName = sceneOrder[i];
      processOneSceneSection();
    }else{
      sxLOG_E("scene listed in orderOfScenes not found: %s", sceneOrder[i].c_str());
    }
  }
  return ReturnCodes::RC_SUCCESS;
}

ReturnCodes ActFileLoader::processOneSceneSection()
{
  ReturnCodes result;
  SceneLoadInfo loadInfo;

  result = reader->getStr("sceneType", loadInfo.sceneType);
  if(ReturnCodes::RC_SUCCESS == result){
    result = reader->getStr("filename", loadInfo.filename);
  }
  if(ReturnCodes::RC_SUCCESS == result){
    loadInfo.sceneName = currentSectionName;
    sceneLoadInfo.push_back(loadInfo);
  }
  return result;
}
