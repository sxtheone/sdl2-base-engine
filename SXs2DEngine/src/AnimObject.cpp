#include "AnimObject.h"
#include "SpriteManager.h"
#include "Profiler.h"

AnimObject::AnimObject()
{
  type = ObjectTypes::AnimObject;
  spriteChangerPlaying = SDL_FALSE;
  currentFrame = NULL;
  timePassed = 0;
  currentAnimIt = animations.end();
}

void AnimObject::assign(const ObjectBase& source)
{
  Object::assign(source); // don't forget to call the ancestor's assign function
#ifdef _DEBUG
  const AnimObject* sourceObj = dynamic_cast<const AnimObject*>(&source);
#else
  const AnimObject* sourceObj = static_cast<const AnimObject*>(&source);
#endif
  animations = sourceObj->animations;
  setAnimation(sourceObj->currentAnimIt->first);
  spriteChangerPlaying = sourceObj->spriteChangerPlaying;
  tintPlaying = sourceObj->tintPlaying;
  timePassed = sourceObj->timePassed;
}

void AnimObject::addAnimation(sxString animName, sxAnimSprite* animSprite)
{
  //if(animations.end() != animations.find(animName)){
  if(false == animations.insert(std::pair<sxString, sxAnimContainer>(animName, animSprite)).second){
    sxLOG_W("Animated sprite already loaded, will be overwritten: %s", animName.c_str());
    animations[animName].animSprite = animSprite;
  }  
}

ReturnCodes AnimObject::setAnimation(sxString animName)
{
  currentAnimIt = animations.find(animName);
  if(animations.end() == currentAnimIt){
    sxLOG_E("Invalid animation name: %s", animName.c_str());
    return ReturnCodes::RC_INVALID_ANIMATION_NAME;
  }
  resetAnimationPosition();
  return ReturnCodes::RC_SUCCESS;
}

void AnimObject::resetAnimationPosition()
{
  if(animations.end() == currentAnimIt){
    sxLOG_E("No animation loaded!");
    return;
  }
  timePassed = 0;
  currentFrame = currentAnimIt->second.animSprite->getFrame(0);
  activeSprite = currentAnimIt->second.animSprite->getSprite(currentFrame->spriteNumber);
  setDisplaySizeForCurrentSprite();

  tintChangePerTick[0] = TINT_TIME_INVALID;
  tintChangePerTick[1] = 0;
  tintChangePerTick[2] = 0;
  tintChangePerTick[3] = 0;

  objectProps.setShouldRedraw(SDL_TRUE);
}

sxBool AnimObject::isPlaying()
{
  return (SDL_TRUE == spriteChangerPlaying || SDL_TRUE == tintPlaying) ? SDL_TRUE : SDL_FALSE;
}

void AnimObject::callDrawMethodAndModifiers(SDL_Renderer*& renderer)
{
  activateColorModifierChanges();
  sxInt32 result;
  result = SDL_RenderCopyEx(renderer,
                            activeSprite->getTexture(),
                            &activeSprite->size,
                            &destinationRect,
                            rotationInDegree,
                            rotationCenter,
                            spriteFlip);
  if(0 != result){
    sxLOG_E("Error while rendering texture. ErrorCode: %d, Position: %d:%d",
      result, destinationRect.x, destinationRect.y);
  }
  deactivateColorModifierChanger();
}

void AnimObject::stepAnimation(const sxUInt32 ticksPassed)
{
  if(SDL_TRUE == objectProps.objectEnabled()){
    Object::stepAnimation(ticksPassed);
    // sprite changer
    if(SDL_TRUE == spriteChangerPlaying){
      timePassed += ticksPassed;
      spriteChangerPlaying = stepSpriteChangerAnim();
    }
    // color tinting
    if(SDL_TRUE == tintPlaying){
      if(SDL_TRUE == currentAnimIt->second.colorTintingSet()){
        if(TINT_TIME_INVALID == tintChangePerTick[0]){
          setupColorTintAnim();
        }
        tintPlaying = stepColorTintAnim(ticksPassed);
      }else{
        tintPlaying = SDL_FALSE;
      }
    }
  }
}

sxBool AnimObject::stepSpriteChangerAnim()
{
  sxBool result = SDL_TRUE;
  while( timePassed >= currentFrame->time &&
          SDL_TRUE == spriteChangerPlaying )
  {
    objectProps.setShouldRedraw(SDL_TRUE);
    if(currentAnimIt->second.animSprite->isFrameValid(currentFrame->nextAnimFrame)){
      timePassed = timePassed - currentFrame->time;
      currentFrame = currentAnimIt->second.animSprite->getFrame(currentFrame->nextAnimFrame);
      activeSprite = currentAnimIt->second.animSprite->getSprite(currentFrame->spriteNumber);
      setHitbox();
    }else{
      result = SDL_FALSE;
      break;
    }
    //TODO: if different sized animations enabled, call setDisplaySizeForCurrentSprite() here
  }
  return result;
}

void AnimObject::setupColorTintAnim()
{
  calculateTintChangeTicks();
  preciseColorTint[0] = colorModifier.r;
  preciseColorTint[1] = colorModifier.g;
  preciseColorTint[2] = colorModifier.b;
  preciseColorTint[3] = colorModifier.a;

  targetColorTint[0] = currentAnimIt->second.animSprite->targetColorTint.r;
  targetColorTint[1] = currentAnimIt->second.animSprite->targetColorTint.g;
  targetColorTint[2] = currentAnimIt->second.animSprite->targetColorTint.b;
  targetColorTint[3] = currentAnimIt->second.animSprite->targetColorTint.a;
}

inline
void AnimObject::calculateTintChangeTicks()
{
  sxColor targetColors;
  targetColors = currentAnimIt->second.animSprite->targetColorTint;
  
  for(sxUInt32 i=0; i<4; ++i){
    if(0 == currentAnimIt->second.animSprite->tintTimes[i]){
      tintChangePerTick[i] = 0;
      continue;
    }
    sxFloat tintTime = static_cast<sxFloat>(currentAnimIt->second.animSprite->tintTimes[i]);
    if(0 == i) tintChangePerTick[i] = (targetColors.r - colorModifier.r)/tintTime;
    if(1 == i) tintChangePerTick[i] = (targetColors.g - colorModifier.g)/tintTime;
    if(2 == i) tintChangePerTick[i] = (targetColors.b - colorModifier.b)/tintTime;
    if(3 == i) tintChangePerTick[i] = (targetColors.a - colorModifier.a)/tintTime;
  }
}

inline
sxBool AnimObject::stepColorTintAnim(const sxUInt32 ticksPassed)
{
  sxBool result = SDL_FALSE;
  for(sxUInt32 i=0; i<4; ++i){
    if(0 != tintChangePerTick[i]){
      preciseColorTint[i] += tintChangePerTick[i] * ticksPassed;
      if(0 > tintChangePerTick[i]){
        if(targetColorTint[i] > preciseColorTint[i]){
          preciseColorTint[i] = targetColorTint[i];
          tintChangePerTick[i] = 0; // tinting finished for this color
          continue;
        }
      }else if(targetColorTint[i] < preciseColorTint[i]){
        preciseColorTint[i] = targetColorTint[i];
        tintChangePerTick[i] = 0; // tinting finished for this color
        continue;
      }
      result = SDL_TRUE;
    }
  }
  sxColor finalColor;
  finalColor.r = static_cast<sxUInt8>(preciseColorTint[0]);
  finalColor.g = static_cast<sxUInt8>(preciseColorTint[1]);
  finalColor.b = static_cast<sxUInt8>(preciseColorTint[2]);
  finalColor.a = static_cast<sxUInt8>(preciseColorTint[3]);
  setColorModifier(finalColor);
  return result;
}

inline
void AnimObject::setDisplaySizeForCurrentSprite()
{
  if(NULL != activeSprite){
    setScale(spriteScale);
  }else{
    sxLOG(LL_ERROR, "Current Sprite is NULL");
  }
}

ReturnCodes AnimObject::loadData(INIReader* reader)
{
  std::vector<sxString> animNames;
  if(animations.empty()){ /// won't load again animations
    if(ReturnCodes::RC_SUCCESS != reader->getStrList("animations", animNames)){
      return ReturnCodes::RC_FIELD_NOT_FOUND;
    }
    loadAnimSprites(animNames);
  }
  loadAnimationSettings(reader);
  Object::loadData(reader);

  return ReturnCodes::RC_SUCCESS;
}

void AnimObject::loadAnimSprites(const std::vector<sxString>& animNames)
{
  sxAnimSprite* animSprite = NULL;
  for(sxUInt32 i=0; i<animNames.size(); ++i){
    animSprite = getSpriteManager->getAnimSprite(animNames[i]);
    if(NULL == animSprite){
      sxLOG_E("Couldn't load animation named: '%s'", 
            animNames[i].c_str());
    }
    addAnimation(animNames[i], animSprite);
  }
  setAnimation(animNames[0]); // set a default animation
}

void AnimObject::loadAnimationSettings(INIReader* reader)
{
  sxString animToPlay;
  if(ReturnCodes::RC_SUCCESS == reader->getStr("setAnimationToPlay", animToPlay, SDL_FALSE)){
    setAnimation(animToPlay);
  }
  sxBool playAnim = SDL_TRUE;
  if(ReturnCodes::RC_SUCCESS == reader->getBoolean("playAnimation", playAnim, SDL_FALSE)){
    play(playAnim);
  }
}

sxString AnimObject::getActiveAnimName()
{
  if(animations.end() == currentAnimIt){
    return sxString();
  }
  return currentAnimIt->first;
}

void AnimObject::getAnimationNames(std::vector<sxString>& animNames)
{
  std::map<sxString, sxAnimContainer>::iterator it = animations.begin();
  while(animations.end() != it){
    animNames.push_back(it->first);
    ++it;
  }
}

void AnimObject::skipCurrentAnim()
{
  sxUInt32 maxLength = MAX_ANIM_LENGTH; 
  // because static const is not working as a reference with NDK compilers
  stepAnimation(maxLength);
}