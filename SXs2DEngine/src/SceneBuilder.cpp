#include "SceneBuilder.h"
#include "ObjectScene.h"
#include "ScrollableTiledScene.h"
#include "WindowScene.h"

SceneBuilder::SceneBuilder()
{
  registerBasicTypes();
}

void SceneBuilder::registerBasicTypes()
{
  registerFactory("StaticTiledScene", new SceneFactory<StaticTiledScene>());
  registerFactory("ScrollableTiledScene", new SceneFactory<ScrollableTiledScene>());
  registerFactory("ObjectScene", new SceneFactory<ObjectScene>());
  registerFactory("WindowScene", new SceneFactory<WindowScene>());
}