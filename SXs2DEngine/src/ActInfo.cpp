#include "ActInfo.h"
#include "LogManager.h"

SceneIDs* ActInfo::getTopmostAct()
{
  if(acts.empty()){
    return NULL;
  }
  return &acts.back();
}

void ActInfo::removeSceneID(const sxUInt32 sceneID)
{
  sxUInt32 actIdx = 0;
  std::vector<sxUInt32>::iterator it;
  while(actIdx < acts.size()){
    it = std::find(acts[actIdx].sceneIDs.begin(), acts[actIdx].sceneIDs.end(), sceneID);
    if(acts[actIdx].sceneIDs.end() != it){
      acts[actIdx].sceneIDs.erase(it);
      return;
    }
    ++actIdx;
  }
  sxLOG_E("SceneID not found in any act: %d", sceneID);
}

SceneIDs* ActInfo::getActOfScene(const sxUInt32 sceneID)
{
  sxUInt32 actIdx = 0;
  std::vector<sxUInt32>::iterator it;
  while(actIdx < acts.size()){
    it = std::find(acts[actIdx].sceneIDs.begin(), acts[actIdx].sceneIDs.end(), sceneID);
    if(acts[actIdx].sceneIDs.end() != it){
      return &acts[actIdx];
    }
    ++actIdx;
  }
  sxLOG_E("SceneID not found in any act: %d", sceneID);
  return NULL;
}

void ActInfo::pushAct(SceneIDs& act)
{
  if(ReturnCodes::RC_SUCCESS == checkSceneIDIntegrity(act)){
    acts.push_back(act);
  }
}

void ActInfo::popAct()
{
  if(acts.empty()){
    sxLOG_E("No Act to pop, vector is empty.");
    return;
  }
  acts.pop_back();
}

void ActInfo::destroyAllActInfo()
{
  acts.clear();
}

ReturnCodes ActInfo::checkSceneIDIntegrity(SceneIDs& act)
{
  ReturnCodes result;
  sxUInt32 numOfScenes = act.sceneIDs.size();
  sxUInt32 lastSceneID = nextSceneID;

  for(sxUInt32 i=0; i<numOfScenes; ++i){
    sxUInt32 currentSceneID = lastSceneID - numOfScenes + i;
    if(act.sceneIDs[i] != currentSceneID){
      sxLOG_E("SceneID is invalid: %d should be %d, for act: %s",
        act.sceneIDs[i], currentSceneID, act.actName.c_str());
      result = ReturnCodes::RC_ACT_SCENE_ID_INVALID;
    }
  }
  return result;
}

