#include "SceneFileLoaderBase.h"
#include "SceneCollisionPropsProcessor.h"


INIReader* SceneFileLoaderBase::getReaderObject()
{
  if(NULL == reader){
    sxLOG_E("Requested reader object is NULL.");
  }
  return reader;
}

void SceneFileLoaderBase::processBasicSceneInfo()
{
  processSceneProps();
  processSceneCollisionInfo();
}

void SceneFileLoaderBase::processSceneProps()
{
  sxBool value;
  if(ReturnCodes::RC_SUCCESS == reader->getBoolean("enabled", value, SDL_FALSE)){
    sceneProps.setEnabled(value);
  }
  if(ReturnCodes::RC_SUCCESS == reader->getBoolean("visible", value, SDL_FALSE)){
    sceneProps.setVisibility(value);
  }
  if(ReturnCodes::RC_SUCCESS == reader->getBoolean("catchAllInput", value, SDL_FALSE)){
    sceneProps.setCatchAllInput(value);
  }
}

void SceneFileLoaderBase::processSceneCollisionInfo()
{
  if(ReturnCodes::RC_SUCCESS != reader->getStr("collisionID", sceneName, SDL_FALSE)){
    sxLOG_T("No collision ID for scene, collision disabled."); 
    return;
  }
  std::vector<sxCharStr> collidesWith;
  if(reader->isItemExisting("collidesWith")){
    reader->getStrList("collidesWith", collidesWith);
  }
  getSceneCollisionPropsProcessor->addToSceneCollisionMap(sceneName, collidesWith);
  hasCollisionInfo = SDL_TRUE;
}


