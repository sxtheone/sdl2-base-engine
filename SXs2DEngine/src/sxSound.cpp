#include "sxSound.h"

sxSound::sxSound(const sxCharStr& filenameToSet)
{
  filename = filenameToSet;
  sound = NULL;
  loops = SOUND_LOOPS_NONE;
  volume = SOUND_VOLUME_MAX;
  permanent = SDL_FALSE;
  channel = -1;
}

sxSound::~sxSound()
{
  destroySound();
}

void sxSound::loadSoundToMemory()
{
  if(NULL != sound){
    sxLOG_E("Sound already loaded: %s", filename.c_str());
    return;
  }

  sxLOG_T("Loading sound: %s", filename.c_str());
  sound = Mix_LoadWAV(filename.c_str());
  if(NULL == sound){
    sxLOG_E("Couldn't find sound sample: %s", filename.c_str());
    return;
  }
}

void sxSound::unloadSoundFromMemory()
{
  if(NULL != sound){
    Mix_FreeChunk(sound);
    sound = NULL;
  }else{
    sxLOG_PW("Sound wasn't loaded to memory: %s", filename.c_str());
  }
}

sxInt32 sxSound::getVolume() const
{
  if(NULL != sound){
    return sound->volume;
  }
  return SOUND_VOLUME_INVALID;
}

void sxSound::setVolume(sxInt32 newVolume)
{
  checkAndSetVolume(newVolume);
  if(NULL != sound){
    Mix_VolumeChunk(sound, volume);
  }
}

void sxSound::checkAndSetVolume(sxUInt8 incomingVolume)
{
  if(SOUND_VOLUME_MAX < incomingVolume){
    volume = SOUND_VOLUME_MAX;
    sxLOG_E("Too big volume for sound: %s", filename.c_str());
  }else{
    volume = incomingVolume;
  }
}

void sxSound::setNumberOfLoops(sxInt32 numOfloops)
{
  if(numOfloops < SOUND_LOOPS_INFINITE){
    sxLOG_W("Trying to set invalid loop count: %d, setting it to infinite!", numOfloops);
    loops = SOUND_LOOPS_INFINITE;
  }else{
    loops = numOfloops;
  }
}

void sxSound::play()
{
  if(NULL == sound){
    sxLOG_PW("Sound wasn't loaded to memory: %s", filename.c_str());
    loadSoundToMemory();
  }
  if(NULL == sound){
    return;
  }
  Mix_VolumeChunk(sound, volume);
  channel = Mix_PlayChannel(-1, sound, loops);
}

sxBool sxSound::isPlaying()
{
  if(-1 == channel){
    return SDL_FALSE;
  }
  if(0 == Mix_Playing(channel)){
    channel = -1;
    return SDL_FALSE;
  }
  return SDL_TRUE;
}

void sxSound::destroySound()
{
  if(NULL != sound){
#if(_DEBUG)
    sxLOG_T("Destroying sound: %s", filename.c_str());
#endif
    filename.clear();
    Mix_FreeChunk(sound);
    sound = NULL;
  }
}
