#include "ObjectDrawerRenderToTexture.h"
#include "MainConfig.h"
#include "Profiler.h"

ObjectDrawerRenderToTexture::ObjectDrawerRenderToTexture()
{
  init();
  posOffsetUsed = SDL_FALSE;
}

ObjectDrawerRenderToTexture::ObjectDrawerRenderToTexture(const sxVec2Int& offset)
{
  init();
  posOffset = offset;
  if( 0 != posOffset.x ||
      0 != posOffset.y )
  {
    posOffsetUsed = SDL_TRUE;
  }else{
    posOffsetUsed = SDL_FALSE;
  }
}

ObjectDrawerRenderToTexture::~ObjectDrawerRenderToTexture()
{
  destroyTexture(renderTexture);
}

void ObjectDrawerRenderToTexture::drawAll(SDL_Renderer* renderer)
{
  if(NULL == renderTexture){
    createTextures(renderer);
  }
  if(objects.empty()) return;
  renderingPrepared = SDL_FALSE;

  doDrawing(renderer);

  if(SDL_TRUE == renderingPrepared){
    changeRenderTargetTo(renderer, origRenderTexture);
  }else{
    storeOriginalRenderInfo(renderer);
    setRenderInfoForRenderToTexture(renderer);
  }

  activateColorModifications();
  SDL_RenderCopy(renderer, renderTexture, NULL, NULL);
  resetColorModifications();

  restoreOriginalRenderInfo(renderer);
  drawMethod = ObjectDrawerDrawMethodEnum::UPDATE_NO_MOVEMENT;
}

void ObjectDrawerRenderToTexture::prepareForDrawing(SDL_Renderer*& renderer)
{
  origRenderTexture = SDL_GetRenderTarget(renderer);
  storeOriginalRenderInfo(renderer);
  setRenderInfoForRenderToTexture(renderer);
  changeRenderTargetTo(renderer, renderTexture);
  renderingPrepared = SDL_TRUE;
}

void ObjectDrawerRenderToTexture::doDrawing(SDL_Renderer*& renderer)
{
  if(ObjectDrawerDrawMethodEnum::FULLREDRAW == drawMethod){
    prepareForDrawing(renderer);
    doFullRedraw(renderer);
  }else if(ObjectDrawerDrawMethodEnum::UPDATE_NO_MOVEMENT == drawMethod){
    processObjsToClearVector(renderer);
    doUpdateNoMovementDraw(renderer);
  }
}

void ObjectDrawerRenderToTexture::doFullRedraw(SDL_Renderer*& renderer)
{
  SDL_RenderClear(renderer);
 
  sxUInt32 numOfObjs = objects.size();
  for(sxUInt32 i=0; i<numOfObjs; ++i){
      if(SDL_TRUE == posOffsetUsed){
        objects[i]->drawOffset(renderer, posOffset);
      }else{
        objects[i]->draw(renderer);
      }
      objects[i]->objectProps.setShouldRedraw(SDL_FALSE);
  }
}

void ObjectDrawerRenderToTexture::processObjsToClearVector(SDL_Renderer*& renderer)
{
  sxUInt32 numOfObjs = objsToClear.size();
  if(0 == numOfObjs) return;

//getProfiler->startCountingFor("rp");
  if(SDL_FALSE == renderingPrepared){
    prepareForDrawing(renderer);
  }
//getProfiler->stopCountingFor("rp");

//sxString str = strFormatter("c*%d*", numOfObjs);
//getProfiler->startCountingFor(str);
  for(sxUInt32 i=0; i<numOfObjs; ++i){
    fillWithTransparent(renderer, objsToClear[i]);
  }
  objsToClear.clear();
//getProfiler->stopCountingFor(str);

}

void ObjectDrawerRenderToTexture::doUpdateNoMovementDraw(SDL_Renderer*& renderer)
{
  std::vector<sxUInt32> objIndexes;
  sxUInt32 numOfObjs = objects.size();
  // clear the texture and store the objects what moved
  for(sxUInt32 i=0; i<numOfObjs; ++i){
    if(SDL_TRUE == objects[i]->objectProps.shouldRedraw()){
      if(SDL_FALSE == renderingPrepared){
        prepareForDrawing(renderer);
      }
      fillWithTransparent(renderer, objects[i]);
      objIndexes.push_back(i);
    }
  }
  // draw the objects what moved
  numOfObjs = objIndexes.size();
  for(sxUInt32 i=0; i<numOfObjs; ++i){
    if(SDL_TRUE == posOffsetUsed){
      objects[objIndexes[i]]->drawOffset(renderer, posOffset);
    }else{
      objects[objIndexes[i]]->draw(renderer);
    }
    objects[objIndexes[i]]->objectProps.setShouldRedraw(SDL_FALSE);
  }
}

void ObjectDrawerRenderToTexture::addToClearSpaceVector(const sxRect& objHitbox)
{
  // objHitbox.bottomRight contains the object sprite's width and height, without any offset!
  if(!objsToClear.empty()){
    mergeObjsToClearVectorItems(); // first, check the already stored items if they can be merged
    sxRect result;
    sxUInt32 numOfObjs = objsToClear.size();
    for(sxUInt32 i=0; i<numOfObjs; ++i){
      if(SDL_TRUE == objectsCanBeMerged(objsToClear[i], objHitbox, result)){
        objsToClear[i] = result;
        return;
      }
    }
  }
  objsToClear.push_back(objHitbox);
}

void ObjectDrawerRenderToTexture::mergeObjsToClearVectorItems()
{
  sxRect result;
  for(sxUInt32 actIdx=0; actIdx<objsToClear.size(); ++actIdx){
    sxUInt32 remainingNumOfObjs = objsToClear.size();
    for(sxUInt32 i=actIdx+1; i<remainingNumOfObjs; ++i){
      if(SDL_TRUE == objectsCanBeMerged(objsToClear[actIdx], objsToClear[i], result)){
        objsToClear[actIdx] = result;
        objsToClear.erase(objsToClear.begin()+i);
        break;
      }
    }
  }
}

sxBool ObjectDrawerRenderToTexture::objectsCanBeMerged(const sxRect& hb1,
                                                       const sxRect& hb2,
                                                       sxRect& result)
{
  if(SDL_FALSE == objectContainedByOther(hb1, hb2, result)){
    if(SDL_TRUE == objectContainedByOther(hb2, hb1, result)) return SDL_TRUE;
  }else return SDL_TRUE;

  if(hb1.y == hb2.y && hb1.h == hb2.h){
    return objectsCanBeMergedYAligned(hb1, hb2, result);
  }else if(hb1.x == hb2.x && hb1.w == hb2.w){
    return objectsCanBeMergedXAligned(hb1, hb2, result);
  }
  return SDL_FALSE;
}

sxBool ObjectDrawerRenderToTexture::objectsCanBeMergedXAligned(const sxRect& hb1,
                                                               const sxRect& hb2,
                                                               sxRect& result)
{
  sxInt32 hb1BottomLeftY = hb1.y + hb1.h;
  sxInt32 hb2BottomLeftY = hb2.y + hb2.h;
  if( hb1.y <= hb2BottomLeftY &&
      hb1BottomLeftY >= hb2.y )
  {
    result.x = hb1.x;
    result.w = hb1.w;

    if(hb1.y < hb2.y){
      result.y = hb1.y;
      result.h = hb2.y - hb1.y + hb2.h;
    }else{
      result.y = hb2.y;
      result.h = hb1.y - hb2.y + hb1.h;
    }
    return SDL_TRUE;
  }
  return SDL_FALSE;
}

sxBool ObjectDrawerRenderToTexture::objectsCanBeMergedYAligned(const sxRect& hb1,
                                                               const sxRect& hb2,
                                                               sxRect& result)
{
  sxInt32 hb1TopRightX = hb1.x + hb1.w;
  sxInt32 hb2TopRightX = hb2.x + hb2.w;
  if( hb1.x <= hb2TopRightX &&
      hb1TopRightX >= hb2.x )
  {
    result.y = hb1.y;
    result.h = hb1.h;
    if(hb1.x < hb2.x){
      result.x = hb1.x;
      result.w = hb2.x - hb1.x + hb2.w;
    }else{
      result.x = hb2.x;
      result.w = hb1.x - hb2.x + hb1.w;
    }
    return SDL_TRUE;
  }
  return SDL_FALSE;
}

sxBool ObjectDrawerRenderToTexture::objectContainedByOther(const sxRect& hb1,
                                                           const sxRect& hb2,
                                                           sxRect& result)
{
  sxVec2Int hb1BottomRight;
  hb1BottomRight.x = hb1.x + hb1.w;
  hb1BottomRight.y = hb1.y + hb1.h;

  sxVec2Int hb2BottomRight;
  hb2BottomRight.x = hb2.x + hb2.w;
  hb2BottomRight.y = hb2.y + hb2.h;
  // one is containing the other
  if( hb1.x <= hb2.x &&
      hb1.y <= hb2.y  &&
      hb1BottomRight.x >= hb2BottomRight.x && 
      hb1BottomRight.y >= hb2BottomRight.y )
  {
    result = hb1;
    return SDL_TRUE;
  }
  return SDL_FALSE;
}

void ObjectDrawerRenderToTexture::init()
{
  type = ObjectDrawerTypeEnum::RENDER_TO_TEXTURE;
  posOffset = sxVec2Int(0,0);
  drawMethod = ObjectDrawerDrawMethodEnum::FULLREDRAW;
  origRenderTexture = NULL;
  renderTexture = NULL;
  colorModifier.r = colorModifier.g = colorModifier.b = colorModifier.a = 255;
}

void ObjectDrawerRenderToTexture::createTextures(SDL_Renderer*& renderer)
{
  createFullscreenTexture(renderer, renderTexture);
}

void ObjectDrawerRenderToTexture::storeOriginalRenderInfo(SDL_Renderer*& renderer)
{
  SDL_GetRenderDrawBlendMode(renderer, &origBlendMode);
  SDL_GetRenderDrawColor(renderer, &origDrawColor.r, &origDrawColor.g, &origDrawColor.b,
                         &origDrawColor.a);
}

void ObjectDrawerRenderToTexture::setRenderInfoForRenderToTexture(SDL_Renderer*& renderer)
{
  SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_NONE);
  SDL_SetRenderDrawColor(renderer, 255, 255, 255, 0);
}

void ObjectDrawerRenderToTexture::changeRenderTargetTo(SDL_Renderer*& renderer,
                                                       SDL_Texture* texture)
{
  SDL_SetRenderTarget(renderer, texture);
}

void ObjectDrawerRenderToTexture::restoreOriginalRenderInfo(SDL_Renderer*& renderer)
{
  SDL_SetRenderDrawBlendMode(renderer, origBlendMode);
  SDL_SetRenderDrawColor(renderer, origDrawColor.r, origDrawColor.g, origDrawColor.b,
                         origDrawColor.a);
}

void ObjectDrawerRenderToTexture::fillWithTransparent(SDL_Renderer*& renderer, Object*& obj)
{
  sxRect rect;
  rect.x = posOffset.x + static_cast<sxInt32>(obj->getPosition().x);
  rect.y = posOffset.y + static_cast<sxInt32>(obj->getPosition().y);
  rect.w = obj->getWidth();
  rect.h = obj->getHeight();

  // SDL_BlendMode should be SDL_BLENDMODE_NONE when clearing
  SDL_RenderFillRect(renderer, &rect);
}

void ObjectDrawerRenderToTexture::fillWithTransparent(SDL_Renderer*& renderer,
                                                      const sxRect& objRect)
{
  sxRect rect = objRect;
  rect.x += posOffset.x;
  rect.y += posOffset.y;

  // SDL_BlendMode should be SDL_BLENDMODE_NONE when clearing
  SDL_RenderFillRect(renderer, &rect);
}

void ObjectDrawerRenderToTexture::createFullscreenTexture(SDL_Renderer*& renderer,
                                                          SDL_Texture*& texture)
{
  destroyTexture(texture);
  sxVec2Int logicalSize = getMainConfig->getWindowLogicalSize();
  texture = SDL_CreateTexture(renderer, 
                              SDL_PIXELFORMAT_RGBA8888,
                              SDL_TEXTUREACCESS_TARGET,
                              logicalSize.x, 
                              logicalSize.y);
  SDL_SetTextureBlendMode(texture, SDL_BLENDMODE_BLEND);
}

void ObjectDrawerRenderToTexture::destroyTexture(SDL_Texture*& texture)
{
  if(NULL != texture){
    SDL_DestroyTexture(texture);
    texture = NULL;
  }
}

void ObjectDrawerRenderToTexture::activateColorModifications()
{
  if(0 != SDL_SetTextureColorMod(renderTexture, colorModifier.r,
                                 colorModifier.g, colorModifier.b))
  {
    sxLOG_E("Couldn't set color modifications: r/g/b: %d/%d/%d",
      colorModifier.r, colorModifier.g, colorModifier.b);
  }
  if(0 != SDL_SetTextureAlphaMod(renderTexture, colorModifier.a)){
    sxLOG_E("Couldn't set color alphamodification: %d", colorModifier.a);
  }
}

void ObjectDrawerRenderToTexture::resetColorModifications()
{
  SDL_SetTextureColorMod(renderTexture,
                          255, 255, 255);
  SDL_SetTextureAlphaMod(renderTexture, 255);
}