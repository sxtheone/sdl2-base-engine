#include "ResourceReleaser.h"
#include "ObjectManager.h"
#include "AudioManager.h"
#include "LoadedFiles.h"
#include "FileExtensions.h"
#include "SpriteManager.h"
#include "FontManager.h"

void ResourceReleaser::release(const WhatToRelease toRelease)
{
  if(WhatToReleaseEnum::INVALID <= toRelease){
    sxLOG_E("Invalid value to release: %d", toRelease);
    return;
  }
  removeObjectsRelated(setTypesToRelease(toRelease));
  if(WhatToReleaseEnum::SPRITES_ALSO <= toRelease){
    removeSpritesRelated();
  }
  removeTextureRelated(toRelease);
}

void ResourceReleaser::removeObjectsRelated(const TypesToRelease typesToRelease)
{
  getObjectManager->destroyAllObjects();
  getAudioManager->destroyAllSounds(typesToRelease);
  getLoadedFiles->removeFilesContaining(FileExtensions::objectsFile);
  getLoadedFiles->removeFilesContaining(FileExtensions::soundsFile);
  //TODO: separate deletion for music when it's implemented?
}

void ResourceReleaser::removeSpritesRelated()
{
  getSpriteManager->destroyAllSpritesHitboxes();
  getFontManager->destroyAllFonts();
  getLoadedFiles->removeFilesContaining(FileExtensions::spritesFile);
  getLoadedFiles->removeFilesContaining(FileExtensions::hitboxesFile);
  getLoadedFiles->removeFilesContaining(FileExtensions::fontsFile);
  getLoadedFiles->removeFilesContaining(FileExtensions::animsFile);
}

void ResourceReleaser::removeTextureRelated(const WhatToRelease toRelease)
{
  if(WhatToReleaseEnum::TEXTURES_ALSO == toRelease){
    getSpriteManager->destroyAllTextures(TypesToReleaseEnum::NORMAL_ITEMS_ONLY);
  }else if(WhatToReleaseEnum::PERMANENT_ALSO == toRelease){
    getSpriteManager->destroyAllTextures(TypesToReleaseEnum::PERMANENT_ITEMS_ALSO);
  }
}

TypesToRelease ResourceReleaser::setTypesToRelease(const WhatToRelease toRelease)
{
  TypesToRelease typesToRelease = TypesToReleaseEnum::NORMAL_ITEMS_ONLY;
  if(WhatToReleaseEnum::PERMANENT_ALSO == toRelease){
    typesToRelease = TypesToReleaseEnum::PERMANENT_ITEMS_ALSO;
  }
  return typesToRelease;
}
