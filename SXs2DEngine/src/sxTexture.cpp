#include "sxTexture.h"
#include "LogManager.h"

sxTexture::sxTexture()
{
  texture = NULL;
  textureLoadedHere = SDL_FALSE;
  permanent = SDL_FALSE;
}

sxTexture::~sxTexture()
{
  freeTexture();
}

void sxTexture::loadTexture(SDL_Renderer*& renderer,
                            const sxString& filename, sxBool setPermanent)
{
  freeTexture();
  texture = IMG_LoadTexture_RW(renderer,
                               SDL_RWFromFile(filename.c_str(), "rb"),
                               1);
  if(NULL == texture){
    sxLOG(LL_ERROR, "Couldn't find texture: %s", filename.c_str());
  }else{
    textureLoadedHere = SDL_TRUE;
    permanent = setPermanent;
  }
}

void sxTexture::loadStreamingTexture(SDL_Renderer*& renderer,
                                     const sxString& filename, sxBool setPermanent)
{
  
  SDL_Surface* surface = IMG_Load_RW(SDL_RWFromFile(filename.c_str(), "rb"), 1);
  if(NULL == surface){
    sxLOG_E("Couldn't load texture: %s", filename.c_str());
    return;
  }
  freeTexture();
  texture = SDL_CreateTexture(renderer, 
                              SDL_PIXELFORMAT_RGBA8888,
                              SDL_TEXTUREACCESS_STREAMING,
                              surface->w,
                              surface->h);
  if(NULL == texture){
    sxLOG(LL_ERROR, "Couldn't find texture: %s", filename.c_str());
  }else{
    copySurfaceToTexture(surface);
    textureLoadedHere = SDL_TRUE;
    permanent = setPermanent;
  }
  SDL_FreeSurface(surface);
}

void sxTexture::copySurfaceToTexture(SDL_Surface* surface)
{
	void* mPixels;
	sxInt32 mPitch;
	SDL_LockTexture(texture, NULL, &mPixels, &mPitch);
	memcpy(mPixels, surface->pixels, surface->pitch*surface->h);
	SDL_UnlockTexture(texture);
}

sxInt32 sxTexture::getTextureWidth()
{
  sxInt32 result;
  SDL_QueryTexture(texture, NULL, NULL, &result, NULL);
  return result;
}

sxInt32 sxTexture::getTextureHeight()
{
  sxInt32 result;
  SDL_QueryTexture(texture, NULL, NULL, NULL, &result);
  return result;
}

void sxTexture::freeTexture()
{
  if( SDL_TRUE == textureLoadedHere &&
      NULL != texture )
  {
    SDL_DestroyTexture(texture);
  }
  textureLoadedHere = SDL_FALSE;
}
