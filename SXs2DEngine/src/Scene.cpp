#include "Scene.h"
#include "ActManager.h"
#include "SceneMsgSendString.hpp"

Scene::~Scene()
{
  if(NULL != collisionProps){
    delete collisionProps;
  }
  while(!messagesToSend.empty()){
    delete messagesToSend.back();
    messagesToSend.pop();
  }
}

void Scene::setRenderer(SDL_Renderer* _renderer)
{
  if(NULL == _renderer){
    sxLOG(LL_ERROR, "The received renderer is NULL!");
    return;
  }
  renderer = _renderer;
}

SceneMsg* Scene::msgToOtherScenes()
{
  if(messagesToSend.empty()){
    return NULL;
  }
  SceneMsg* result = messagesToSend.front();
  messagesToSend.pop();
  return result;
}

sxBool Scene::handleMsgFromOtherScene(SceneMsg* msg)
{
  //sxLOG_T("Not interested in any scene messages.");
  return SDL_FALSE;
}

void Scene::addSceneMsgToQueue(SceneMsg* msg)
{
  messagesToSend.push(msg);
}

void Scene::addStringSceneMsgToQueue(const sxString& strMsg, const sxBool broadcast)
{
  addStringSceneMsgToQueue(strMsg, SceneMsgSendString::DEFAULT_ID, broadcast);
}
  
void Scene::addStringSceneMsgToQueue(const sxString& strMsg, const sxInt32 id, const sxBool broadcast)
{
  SceneMsg* msg = getActManager->createSceneMsg(SceneMsgTypes::SceneMsgSendString);
  static_cast<SceneMsgSendString*>(msg)->value = strMsg;
  static_cast<SceneMsgSendString*>(msg)->id = id;
  msg->broadcast = broadcast;
  addSceneMsgToQueue(msg);
}
