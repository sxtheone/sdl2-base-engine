#include "Profiler.h"
#include "FontManager.h"
#include "GfxPrimitives.h"
#include "MainConfig.h"

Profiler* Profiler::profiler = NULL;
#define PROFILER_TEXT_MAX_LENGTH (getMainConfig->getWindowLogicalSize().x * 0.24f)

Profiler* Profiler::getInstance()
{
  if(NULL == profiler){
    sxLOG_E("createProfiler() should be called before using the Profiler!");
    return NULL;
  }
  return profiler;
}

void Profiler::createProfiler()
{
  if(NULL != profiler){
    sxLOG_E("Profiler already created.");
    return;
  }
  profiler = new Profiler();
}

void Profiler::destroyProfiler()
{
  if(NULL == profiler){
    sxLOG_E("Profiler already destroyed/not yet created!");
    return;
  }
  delete profiler;
  profiler = NULL;
}

#ifdef DEVELOPER_HELPERS

Profiler::Profiler()
{
  debugTextEnabled = SDL_TRUE;
  showTextsOnScreen = SDL_FALSE;
  writeLog = SDL_TRUE;
  renderer = NULL;
  fpsToDraw = NULL;
  cyclesPassed = 0;
  drawDebugBoxes = SDL_FALSE;
  profilerTextXcoord = getMainConfig->getWindowLogicalSize().x - PROFILER_TEXT_MAX_LENGTH;
}


Profiler::~Profiler()
{
  if(NULL != fpsToDraw){
    delete fpsToDraw;
  }
}


void Profiler::initialize(SDL_Renderer* mainRenderer)
{
  renderer = mainRenderer;
}

void Profiler::startCountingFor(const sxString& name, const PerfCounter::PCState state)
{
  if(SDL_FALSE == debugTextEnabled) return;

  objsIt = objsToDraw.find(name);
  if(objsToDraw.end() == objsIt){
    PerfCounter newObj(state);
    newObj.startTick = getTicks();
    objsToDraw[name] = newObj;
  }else{
    objsIt->second.startTick = getTicks();
    objsIt->second.stopTick = 0;
    objsIt->second.state = state;
  }
}

sxString Profiler::stopCountingFor(const sxString& name)
{
  sxString tempStr;
  if( SDL_TRUE == debugTextEnabled &&
      objsToDraw.end() != objsToDraw.find(name) )
  {
    PerfCounter* obj;
    obj = &objsToDraw[name];
    if(0 == obj->stopTick){ // if we already have rendered text, stop called twice after start
      obj->stopTick = getTicks();
    }
    sxUInt64 passedTicks = obj->stopTick - obj->startTick;
    tempStr = strFormatter(" %s: %.04f ", name.c_str(),
                            getPassedTicksInMillisec(passedTicks));
    if(SDL_TRUE == showTextsOnScreen){
      fillObject(obj->obj, tempStr);
    }else{
      logString += tempStr;
    }
  }
  return tempStr;
}

sxBool Profiler::counterExists(const sxString name)
{
  return (objsToDraw.end() == objsToDraw.find(name)) ? SDL_FALSE : SDL_TRUE;
}

void Profiler::deleteCounter(const sxString& name)
{
  std::map<sxString, PerfCounter>::iterator it;
  it = objsToDraw.find(name);
  if(objsToDraw.end() != it){
    objsToDraw.erase(it);
  }
}

void Profiler::writeToScreen(const sxString& text, const sxBool forced)
{
  if( SDL_FALSE == forced && 
      SDL_FALSE == debugTextEnabled ) return;

  objsIt = objsToDraw.find(text);
  if(objsToDraw.end() == objsIt){
    PerfCounter newObj;
    newObj.startTick = 0;
    objsToDraw[text] = newObj;
    // generate the object once because it won't change
    if(SDL_TRUE == showTextsOnScreen){
      objsToDraw[text].obj = getFontManager->generateSystemText(text);
    }
  }else{
    objsIt->second.startTick = 0;
    if(PerfCounter::TO_DELETE == objsIt->second.state){
      objsIt->second.state = PerfCounter::NORMAL;
    }
  }

  if(SDL_FALSE == showTextsOnScreen){
    logString += text;
  }
}

void Profiler::fillObject(BasicObject*& obj, const sxString& text)
{
  if(SDL_TRUE == shouldDraw()){
    if(NULL != obj){
      delete obj;
      obj = NULL;
    }
    obj = getFontManager->generateSystemText(text);
  }
  logString += text;
}

sxUInt64 Profiler::getTicks()
{
  //return SDL_GetTicks();
  return SDL_GetPerformanceCounter();
}


sxFloat Profiler::getPassedTicksInMillisec(const sxUInt64& passedTicks)
{
  sxFloat result = static_cast<sxFloat>(passedTicks)/SDL_GetPerformanceFrequency();
  return 1000.0f*result;
}


void Profiler::setFPStoDraw(sxUInt32 timePassed)
{
  if( 0 == timePassed ||
      fpsCounterEnabled == SDL_FALSE )
  {
    return;
  }

  fpsValues.push_back(timePassed);

  if(2 == fpsValues.size()){
    sxUInt32 avgFps = 0;
    for(sxUInt32 i=0; i<fpsValues.size();++i){
      avgFps += fpsValues[i]; 
    }
    avgFps /= fpsValues.size();
    fpsValues.clear();
    sxString tempStr = strFormatter("A_FPS: %d", 1000/avgFps);
    fillObject(fpsToDraw, tempStr);
  }
}

void Profiler::draw()
{
  sxColor backColor;
  SDL_GetRenderDrawColor(renderer, &backColor.r,  &backColor.g, &backColor.b, &backColor.a);
  
  posToDraw.x = profilerTextXcoord;
  posToDraw.y = 0;

  drawObject(fpsToDraw);
  if(SDL_FALSE == showTextsOnScreen){
    return;
  }

  objsIt = objsToDraw.begin();
  while(objsToDraw.end() != objsIt){
    drawObject(objsIt->second.obj);
    ++objsIt;
  }

  if(SDL_TRUE == drawDebugBoxes){
    std::vector<DebugDrawItem>::iterator ddIt = debugDraws.begin();
    while(debugDraws.end() != ddIt){
      if(0 == ddIt->solid){
        GfxPrimitives::rectangleRGBA(renderer, ddIt->rect, ddIt->color);
      }else{
        sxColor c = ddIt->color;
        c.a = ddIt->solid; // value of color is the alpha value
        GfxPrimitives::boxRGBA(renderer, ddIt->rect, c);
      }
      ++ddIt;
    }
  }
  debugDraws.clear();

  SDL_SetRenderDrawColor(renderer, backColor.r, backColor.g, backColor.b, backColor.a);
}

void Profiler::cycleEnded()
{
  ++cyclesPassed;
  if(!logString.empty()){
    logString += strFormatter(" Whole Cycle Ticks: %.04f ", 
                              getPassedTicksInMillisec(getTicks() - cycleStartTick));
    if(SDL_TRUE == writeLog){
      sxLOG(LL_TRACE, "%s", logString.c_str());
    }
    logString.clear();
  }
  if(SDL_TRUE == shouldDraw()){
    clearObjsToDraw();
  }
  // and set the next cycle's start time here
  cycleStartTick = getTicks();
}

sxBool Profiler::shouldDraw()
{
  if(0 == cyclesPassed % 13)
  {
    return SDL_TRUE;
  }
  return SDL_FALSE;
}

void Profiler::clearObjsToDraw()
{
  objsIt = objsToDraw.begin();
  while(objsToDraw.end() != objsIt){
    if(PerfCounter::TO_DELETE == objsIt->second.state){
      eraseObjectAndSetObjsItToNextObject();
    }else{
      if(PerfCounter::NORMAL == objsIt->second.state){
        objsIt->second.state = PerfCounter::TO_DELETE;
      }
      ++objsIt;
    }    
  }  
  debugDraws.clear();
}

void Profiler::eraseObjectAndSetObjsItToNextObject()
{
  std::map<sxString, PerfCounter>::iterator it = objsIt;
  ++it;
  objsToDraw.erase(objsIt);
  objsIt = it;
}

void Profiler::drawObject(BasicObject* obj)
{
  if(NULL == obj){
    return;
  }
  obj->setPosition(posToDraw);
  obj->simpleDraw(renderer);
  posToDraw.y += obj->getHeight();
}

void Profiler::addToDrawDebugHitboxes(const sxRect& destinationRect, 
                                      const sxQuad& hitbox,
                                      const sxColor& color,
                                      const sxUInt8 solid)
{
  if(SDL_TRUE == drawDebugBoxes){
    sxQuadInt rect;
    rect.topLeft.x = static_cast<sxInt32>(destinationRect.x + hitbox.topLeft.x);
    rect.topLeft.y = static_cast<sxInt32>(destinationRect.y + hitbox.topLeft.y);
    rect.bottomRight.x = static_cast<sxInt32>(destinationRect.x + hitbox.bottomRight.x);
    rect.bottomRight.y = static_cast<sxInt32>(destinationRect.y + hitbox.bottomRight.y);
    debugDraws.push_back(DebugDrawItem(rect, color, solid));
  }
}

void Profiler::addToDrawDebugHitboxes(const sxVec2Real& position,
                                      const sxQuad& hitbox,
                                      const sxColor& color,
                                      const sxUInt8 solid)
{
  if(SDL_TRUE == drawDebugBoxes){
    sxQuadInt rect;
    rect.topLeft.x = static_cast<sxInt32>(position.x + hitbox.topLeft.x);
    rect.topLeft.y = static_cast<sxInt32>(position.y + hitbox.topLeft.y);
    rect.bottomRight.x = static_cast<sxInt32>(position.x + hitbox.bottomRight.x);
    rect.bottomRight.y = static_cast<sxInt32>(position.y + hitbox.bottomRight.y);
    debugDraws.push_back(DebugDrawItem(rect, color, solid));
  }
}

#endif // DEVELOPER_HELPERS