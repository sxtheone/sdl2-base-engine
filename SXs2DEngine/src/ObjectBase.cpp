#include "ObjectBase.h"
#include "Profiler.h"

ObjectBase::ObjectBase()
{
  destinationRect.x = 0;
  destinationRect.y = 0;
  destinationRect.w = 0;
  destinationRect.h = 0;
  activeSprite = NULL;
  collisionProps = NULL;
  timePassedSinceLastCollision = 0;
  colorModifier.r = colorModifier.g = colorModifier.b = colorModifier.a = 255;
  rotationCenter = NULL;
  rotationInDegree = 0.0f;
  idTag = std::numeric_limits<sxUInt16>::max();
}

ObjectBase::~ObjectBase()
{
  if(NULL != rotationCenter){
    delete rotationCenter;
  }
}

ObjectBase& ObjectBase::operator=(const ObjectBase& other){
  if(SDL_TRUE == objectTypeIs(other.type, type)){
    assign(other); // don't forget to define assign() for every Object type
  }
  return *this;
}

void ObjectBase::assign(const ObjectBase& source)
{
  idTag           = source.idTag;
  activeSprite    = source.activeSprite;
  destinationRect = source.destinationRect;
  objectProps     = source.objectProps;
  collisionProps  = source.collisionProps;
  colorModifier   = source.colorModifier;
  timePassedSinceLastCollision = source.timePassedSinceLastCollision;
  if(NULL != rotationCenter){
    delete rotationCenter;
    rotationCenter = NULL;
  }
  if(NULL != source.rotationCenter)
  {
    rotationCenter  = new SDL_Point();
    *rotationCenter = *source.rotationCenter;
  }  
  rotationInDegree = source.rotationInDegree;
}

void ObjectBase::setHitbox()
{
  if(NULL != collisionProps){
    collisionProps->hitbox = activeSprite->hitbox;
  }
}

void ObjectBase::setColorModifier(const sxColor& colorMod)
{
  if( colorModifier.a != colorMod.a ||
      colorModifier.r != colorMod.r ||
      colorModifier.g != colorMod.g ||
      colorModifier.b != colorMod.b )
  {
    objectProps.setShouldRedraw(SDL_TRUE);
  }
  colorModifier = colorMod;
}

sxBool ObjectBase::objectTypeIs(const sxCharStr& typeToCheck, sxCharStr desiredType)
{
  if(desiredType != typeToCheck){
    sxLOG_E("Type: %s can't accept type: %s. "
      "Did you forget to define assignment( = ) operator?",
      type.c_str(), typeToCheck.c_str());
    return SDL_FALSE;
  }
  return SDL_TRUE;
}

void ObjectBase::setupCollisionRelatedData(sxBool collisionPossible)
{
  objectProps.setCollisionPossible(collisionPossible);
  if( SDL_TRUE == collisionPossible ||
      SDL_TRUE == objectProps.clickable() )
  {
    collisionProps = new ObjCollisionProps();
    objectProps.setCollisionEnabled(collisionPossible);
    setHitbox();
  }
}

void ObjectBase::setSize(const sxVec2Int size)
{
  if( destinationRect.w != size.x ||
      destinationRect.h != size.y )
  {
    objectProps.setShouldRedraw(SDL_TRUE);
    destinationRect.w = size.x;
    destinationRect.h = size.y;
  }
}

void ObjectBase::setNewHitbox(sxQuad*& newHitbox)
{
  if(NULL == collisionProps){
    sxLOG_E("CollisionProps is NULL. Object Type: %s", type.c_str());
    return;
  }
  collisionProps->hitbox = newHitbox;
}

sxVec2Int ObjectBase::getHitboxSize() const
{
  sxVec2Int result;
  if(NULL != collisionProps){
    result = collisionProps->hitbox->getSize();
  }
  return result;
}

void ObjectBase::incIdleTimePassed(sxUInt32 timeToAdd)
{
  if(timePassedSinceLastCollision <= collisionProps->idleAfterCollision){
    timePassedSinceLastCollision += timeToAdd;
  }
}

sxBool ObjectBase::idleTimePassed() const
{
  if(timePassedSinceLastCollision >= collisionProps->idleAfterCollision){
    return SDL_TRUE;
  }
  return SDL_FALSE;
}

void ObjectBase::activateColorModifierChanges()
{
  if(0 != SDL_SetTextureColorMod(activeSprite->getTexture(), colorModifier.r,
                                 colorModifier.g, colorModifier.b))
  {
    sxLOG_E("Couldn't set color modifications: r/g/b: %d/%d/%d",
      colorModifier.r, colorModifier.g, colorModifier.b);
  }
  if(0 != SDL_SetTextureAlphaMod(activeSprite->getTexture(), colorModifier.a)){
    sxLOG_E("Couldn't set color alphamodification: %d", colorModifier.a);
  }
}

void ObjectBase::deactivateColorModifierChanger()
{
  SDL_SetTextureColorMod(activeSprite->getTexture(),
                          255, 255, 255);
  SDL_SetTextureAlphaMod(activeSprite->getTexture(), 255);
}

sxBool ObjectBase::drawingEnabled() const
{
  if(SDL_TRUE == objectProps.objectVisible()){
    return SDL_TRUE;
  }
  return SDL_FALSE;
}

void ObjectBase::draw(SDL_Renderer* renderer, const sxVec2Int& position)
{
  if(SDL_TRUE == objectProps.objectVisible()){
    if(NULL != activeSprite){
      destinationRect.x = position.x;
      destinationRect.y = position.y;
      activateColorModifierChanges();
      int result;
      result = SDL_RenderCopy(renderer, activeSprite->getTexture(), &activeSprite->size, &destinationRect);
      if(0 != result){
        sxLOG_E("Error while rendering texture. ErrorCode: %d", result);
      }
      deactivateColorModifierChanger();
    }else{
      sxLOG_E("No sprite to render!");
    }
  }
}

ReturnCodes ObjectBase::loadData(INIReader* reader)
{
  sxBool tempBool = SDL_FALSE;
  sxInt32 tempInt = 0;
  if(ReturnCodes::RC_SUCCESS == reader->getInteger("idTag", tempInt, SDL_FALSE)){
    idTag = static_cast<sxUInt16>(tempInt);
    if(idTag == std::numeric_limits<sxUInt16>::max()){
      idTag -= 1;
      sxLOG_E("Don't set idTag to %d, that's reserved for the uninitialized value! "
            "Changed your idTag to %d.", std::numeric_limits<sxUInt16>::max(),
            idTag);
    }
  }
  if(ReturnCodes::RC_SUCCESS == reader->getBoolean("objectEnabled", tempBool, SDL_FALSE)){
    objectProps.setObjectEnabled(tempBool);
  }
  if(ReturnCodes::RC_SUCCESS == reader->getBoolean("collisionEnabled", tempBool, SDL_FALSE)){
    objectProps.setCollisionEnabled(tempBool);
  }
  if(ReturnCodes::RC_SUCCESS == reader->getBoolean("visible", tempBool, SDL_FALSE)){
    objectProps.setVisibility(tempBool);
  }
  sxColor colorTint;
  if(ReturnCodes::RC_SUCCESS == reader->getColor("colorTint", colorTint, SDL_FALSE)){
    colorModifier = colorTint;
  }
  reader->getDouble("rotation", rotationInDegree, SDL_FALSE);
  
  return ReturnCodes::RC_SUCCESS;
}

CollisionData ObjectBase::getCollisionData()
{
  CollisionData result;
  result.object = this;
  return result;
}

sxQuad ObjectBase::getHitBoxWithOffset(const sxVec2Real& posOffset, 
                                       const sxVec2Int& additionalOffset)
{
  sxVec2Real objPosition = posOffset;
  objPosition.x += additionalOffset.x;
  objPosition.y += additionalOffset.y;
  
  sxQuad result;
  if(NULL != collisionProps){
    result = *collisionProps->hitbox;
    result.topLeft.x += objPosition.x;
    result.topLeft.y += objPosition.y;
    result.bottomRight.x += objPosition.x;
    result.bottomRight.y += objPosition.y;
  }else{
    sxLOG_E("No collisionProps here: type: %s, idTag: %d, pos: %.04f, %.04f", 
      type.c_str(), idTag, objPosition.x, objPosition.y);
  }
  return result;
}