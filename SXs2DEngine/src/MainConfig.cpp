#include "MainConfig.h"

MainConfig* MainConfig::mainConfig = NULL;
ResolutionChanger MainConfig::resolutionChanger;

void MainConfig::setResolutionChange(const sxVec2Int& screenSize, const sxVec2Int& logicalSize)
{
  resolutionChanger.resolutionChangeSet = SDL_TRUE;
  resolutionChanger.screenSize = screenSize;
  resolutionChanger.logicalSize = logicalSize;
}

void MainConfig::createManager(const sxString& filename)
{
  if(NULL != mainConfig){
    delete mainConfig;
  }
  mainConfig = new MainConfig(filename);
}

MainConfig* MainConfig::getInstance()
{
  if(NULL == mainConfig){
    sxLOG(LL_ERROR, "MainConfig is not yet created.");
    return NULL;
  }
  return mainConfig;
}

void MainConfig::destroyManager()
{
  sxLOG(LL_TRACE, "Destroying MainConfig...");
  if(NULL == mainConfig){
    sxLOG(LL_ERROR, "MainConfig instance is NULL, but how?");
    return;
  }
  delete mainConfig;
  mainConfig = NULL;
  sxLOG(LL_TRACE, "MainConfig destroyed.");
}

MainConfig::MainConfig(const sxString& filename)
{
  fillDefaultValues();
  ConfigFileLoader loader;
  if(ReturnCodes::RC_SUCCESS == loader.processFile(filename)){
    getBaseConfigValues(loader);
    getAudioConfigValues(loader);
    getStartupConfigValues(loader);
  }
}

void MainConfig::getBaseConfigValues(ConfigFileLoader& loader)
{
  loader.setBaseConfigSection();
  loader.getBackgroundColor(backgroundColor);
  setResolution(loader);
  loader.getDesiredFPS(desiredFPS);
  defaultDesiredFPS = desiredFPS;
  desiredFrameStep = 1000.0f/desiredFPS;
  loader.getWindowType(windowType);
  loader.getLetterboxInfo(letterbox);
  loader.getWindowName(windowName);
  loader.getSpriteScalingMethod(spriteScalingMethod);
}

void MainConfig::setResolution(ConfigFileLoader& loader)
{
  if(SDL_FALSE == resolutionChanger.resolutionChangeSet){
    loader.getWindowLogicalSize(windowLogicalSize);
    loader.getWindowScreenSize(windowScreenSize);
  }else{
    windowLogicalSize = resolutionChanger.logicalSize;
    windowScreenSize = resolutionChanger.screenSize;
    resolutionChanger.resolutionChangeSet = SDL_FALSE;
  }
}

void MainConfig::setDesiredFPS(sxUInt16 fps)
{
  desiredFPS = fps;
  desiredFrameStep = 1000.0f / desiredFPS;
  sxLOG_T("Desired FPS set to: %d", desiredFPS);
}

void MainConfig::getAudioConfigValues(ConfigFileLoader& loader)
{
  loader.setAudioConfigSection();
  loader.getAudioFrequency(frequency);
  loader.getAudioNumberOfChannels(channels);
  loader.getAudioChunkSize(chunkSize);
}

void MainConfig::getStartupConfigValues(ConfigFileLoader& loader)
{
  loader.setStartupSection();
  loader.getStartingActFilename(startingActFilename);
}

void MainConfig::fillDefaultValues()
{
  backgroundColor.r = 128;
  backgroundColor.g = 128;
  backgroundColor.b = 128;
  backgroundColor.a = 255;
  windowLogicalSize = DEFAULT_LOGICAL_WINDOW_SIZE;
  windowScreenSize = DEFAULT_REAL_WINDOW_SIZE;
  desiredFPS = DEFAULT_DESIRED_FRAMES_PER_SECONDS;
  defaultDesiredFPS = desiredFPS;
  desiredFrameStep = 1000.0f/desiredFPS;
  windowType = DEFAULT_WINDOW_TYPE;
  letterbox = DEFAULT_LETTERBOX;
  windowName = DEFAULT_WINDOW_NAME;
  spriteScalingMethod = DEFAULT_SPRITE_SCALING_METHOD;

  frequency = DEFAULT_AUDIO_FREQUENCY;
  channels = DEFAULT_AUDIO_CHANNELS;
  chunkSize = DEFAULT_AUDIO_CHUNKSIZE;
}

