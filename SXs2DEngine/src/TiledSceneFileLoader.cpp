#include "TiledSceneFileLoader.h"
#include "ObjectManager.h"
#include "ObjectsFileLoader.h"
#include "SoundsFileLoader.h"
#include "SpritesFileLoader.h"

TiledSceneFileLoader::TiledSceneFileLoader(const sxUInt32 sceneID,
                                           std::vector<ObjectBase*>& _tiles,
                                           std::vector<Object*>& _objectTiles,
                                           TileMapHandler& _mapHandler)
                       : SceneFileLoaderBase(sceneID)
{
  tiles = &_tiles;
  objectTiles = &_objectTiles;
  mapHandler = &_mapHandler;
}

ReturnCodes TiledSceneFileLoader::processFile(const sxString& filename)
{
  ReturnCodes result = createReader(filename, SDL_FALSE);
  if(shouldExitFromLoader(result)){
    return result;
  }
  
  result = processSections();
  return result;
}

ReturnCodes TiledSceneFileLoader::processSections()
{
  ReturnCodes result = processConfigSection();
  if(ReturnCodes::RC_SUCCESS != result){
    return result;
  }
  result = processTilesSection();
  if(ReturnCodes::RC_SUCCESS != result){
    return result;
  }

  result = processMapSections();
  return result;
}
//TODO: config section is soo similar in many of the files. A common loader maybe?
ReturnCodes TiledSceneFileLoader::processConfigSection()
{
  ReturnCodes result;
  if(SDL_FALSE == reader->setSection("Config")){
    return ReturnCodes::RC_SECTION_NOT_FOUND;
  }
  processBasicSceneInfo();

  SoundsFileLoader soundLoader;
  loadFileList("soundFiles", soundLoader);
  SpritesFileLoader spriteLoader;
  loadFileList("spriteFiles", spriteLoader);
  ObjectFileLoader objectLoader;
  loadFileList("objectFiles", objectLoader);
  //TODO: these literals like "objectFiles" suck
  //and also I sucked because of them. Make a namespace for them!
  if(ReturnCodes::RC_SUCCESS == result){
    result = reader->getStrList("orderOfMaps", mapOrder);
  }
  return result;
  //TODO: CollisionID, collidesWith for collision handling
}

ReturnCodes TiledSceneFileLoader::processTilesSection()
{
  if(SDL_FALSE == reader->setSection("Tiles")){
    return ReturnCodes::RC_SECTION_NOT_FOUND;
  }
  std::vector<sxString> tileSpriteList;
  reader->getStrList("tileSprites", tileSpriteList);
  for(sxUInt32 i=0; i<tileSpriteList.size(); ++i){
    processOneTileSprite(tileSpriteList[i]);
  }
  loadTileSprites();
  return ReturnCodes::RC_SUCCESS;
}

void TiledSceneFileLoader::processOneTileSprite(const sxString& tileSpriteInfo)
{
  sxUInt16 spriteNumber;
  sxString spriteName;
  if(SDL_FALSE == getSpriteNumber(tileSpriteInfo, spriteNumber)){
    return;
  }
  if(SDL_FALSE == getSpriteName(tileSpriteInfo, spriteName)){
    return;
  }
  addToTileSpritesData(spriteName, spriteNumber);
}

void TiledSceneFileLoader::loadTileSprites()
{
  std::vector<sxInt16> checkVector;
  tiles->clear();
  tiles->resize(tileSpriteData.size());
  resizeAndInitToZero(tileSpriteData.size(), checkVector);

  std::map<sxString, sxUInt16>::iterator it;
  for(it = tileSpriteData.begin(); it != tileSpriteData.end(); ++it){
    if(SDL_TRUE == putSpriteToTilesVector(it->first, it->second)){
      ++checkVector[it->second];
    }
  }
  checkIfEveryItemEqualsTo(checkVector, 1);
}

void TiledSceneFileLoader::resizeAndInitToZero(const sxUInt32 desiredSize,
                                               std::vector<sxInt16>& vector)
{
  vector.resize(desiredSize); //TODO: vector.resize(desiredSize, 0) would do the init..?
  for(sxUInt32 i=0; i<vector.size(); ++i){
    vector[i] = 0;
  }

}

sxBool TiledSceneFileLoader::putSpriteToTilesVector(const sxString& spriteName,
                                                    const sxUInt16 spriteNumber)
{
  if(spriteNumber < tileSpriteData.size()){
    addToTilesVector(spriteName, spriteNumber);
    return SDL_TRUE;
  }
  sxLOG_E("tile sprite number is expected to be smaller. "
    "Max expected number is: %d, received: %d", tileSpriteData.size()-1, spriteNumber);
  return SDL_FALSE;
}

void TiledSceneFileLoader::addToTilesVector(const sxString& spriteName,
                                            const sxUInt16 spriteNumber)
{
  (*tiles)[spriteNumber] = getObjectManager->createObject(spriteName);
  (*tiles)[spriteNumber]->objectProps.setSceneID(sceneIDinAct);
  putObjectToObjectTilesIfNeeded((*tiles)[spriteNumber]);
  updateBiggestHitboxIfNeeded((*tiles)[spriteNumber]);
}

void TiledSceneFileLoader::putObjectToObjectTilesIfNeeded(ObjectBase* object)
{
  if(ObjectTypes::TileElement != object->type){
    objectTiles->push_back(static_cast<Object*>(object));
  }
}

void TiledSceneFileLoader::updateBiggestHitboxIfNeeded(ObjectBase* object)
{
  if(SDL_TRUE == object->objectProps.collisionPossible()){
    setBiggestHitbox(object->collisionProps->hitbox);
  }
}

void TiledSceneFileLoader::checkIfEveryItemEqualsTo(const std::vector<sxInt16>& vector, 
                                                    const sxInt16 value)
{
  for(sxUInt32 i=0; i<vector.size(); ++i){
    if(value != vector[i]){
      sxLOG_E("Something is wrong with the tile sprites. Number: %d is used for %d times.",
        i, vector[i]);
    }
  }
}

sxBool TiledSceneFileLoader::getSpriteNumber(const sxString& tileSpriteInfo, sxUInt16& spriteNumber)
{
  sxInt32 spriteNumber32bit;
  sxString strNumber = tileSpriteInfo.substr(0, tileSpriteInfo.find_first_of(' '));
  if(ReturnCodes::RC_SUCCESS != strToInt(strNumber, spriteNumber32bit)){
    sxLOG_E("Invalid sprite number. Whole line: %s", tileSpriteInfo.c_str());
    return SDL_FALSE;
  }
  if( 65535 < spriteNumber32bit ||
      0 > spriteNumber32bit )
  {
    sxLOG_E("Sprite number is too big, 65535 is the maximum. Whole line: %s",
      tileSpriteInfo.c_str());
    return SDL_FALSE;
  }
  spriteNumber = static_cast<sxUInt16>(spriteNumber32bit);
  return SDL_TRUE;
}

sxBool TiledSceneFileLoader::getSpriteName(const sxString& tileSpriteInfo, sxString& spriteName)
{
  spriteName = tileSpriteInfo.substr(tileSpriteInfo.find_first_of(' ')+1);
  trimLeadingSpaces(spriteName);
  trimTrailingSpaces(spriteName);
  if(spriteName.empty()){
    sxLOG_E("Invalid sprite name. Whole line: %s", tileSpriteInfo.c_str());
    return SDL_FALSE;
  }
  return SDL_TRUE;
}

void TiledSceneFileLoader::addToTileSpritesData(const sxString& spriteName,
                                                const sxUInt16 spriteNumber)
{
  if(tileSpriteData.end() != tileSpriteData.find(spriteName)){
    sxLOG_E("Item is already in the tileSprites map: %s, %d", 
      spriteName.c_str(), spriteNumber);
    return;
  }
  tileSpriteData[spriteName] = spriteNumber; //static_cast<sxUInt16>(spriteNumber);
}

ReturnCodes TiledSceneFileLoader::processMapSections()
{
  if(mapOrder.empty()){
    return ReturnCodes::RC_FILE_PARSE_ERROR;
  }
  for(sxUInt32 i=0; i<mapOrder.size(); ++i){
    if(SDL_TRUE == reader->setSection(mapOrder[i])){
      currentMap.clearMap();
      processOneMapSection();
    }else{
      sxLOG_E("Map listed in orderOfMaps not found: %s", mapOrder[i].c_str());
    }
  }
  return ReturnCodes::RC_SUCCESS;
}

void TiledSceneFileLoader::processOneMapSection()
{
  if(ReturnCodes::RC_SUCCESS != reader->getMatrix2D("numberOfRowsColumns", sizeOfCurrentMap)){
    return;
  }
  buildMap(sizeOfCurrentMap.col);
  if(SDL_TRUE != mapSizeOK()){
    return;
  }
  mapHandler->addMap(currentMap);
  fillNumOfRepeatsField();
}

void TiledSceneFileLoader::buildMap(const sxUInt32 numOfItemsInRow)
{
  std::vector<sxUInt32> row;
  std::vector<sxString> mapStr;
  if(ReturnCodes::RC_SUCCESS != reader->getStrList("map", mapStr)){
    return;
  }
  for(sxUInt32 i=0; i<mapStr.size(); ++i){
    putItemToMap(mapStr[i], row);
    if(row.size() == numOfItemsInRow){
      currentMap.addRow(row);
      row.clear();
    }
  }
}

void TiledSceneFileLoader::putItemToMap(const sxString& valueString, std::vector<sxUInt32>& row)
{
  sxInt32 value;
  if(ReturnCodes::RC_SUCCESS == strToInt(valueString, value)){
    if(static_cast<sxUInt32>(value) < tiles->size()){
      row.push_back(value);
    }else{
      sxLOG_E("Value is too big. Map name: %s, value: %d",
        currentSectionName.c_str(), value);
    }
  }else{
    sxLOG_E("Error while processing map. Map name: %s, value: %s",
      currentSectionName.c_str(), valueString.c_str());
  }
}

sxBool TiledSceneFileLoader::mapSizeOK()
{
  if( sizeOfCurrentMap.row != currentMap.numberOfRows() ||
      sizeOfCurrentMap.col != currentMap.numberOfItemsInARow() )
  {
    sxLOG_E("Map don't have the indicated size. Should be (r,c): %d, %d, but size is %d, %d",
      sizeOfCurrentMap.row, sizeOfCurrentMap.col,
      currentMap.numberOfRows(), currentMap.numberOfItemsInARow());
    return SDL_FALSE;
  }

  return SDL_TRUE;
}

void TiledSceneFileLoader::fillNumOfRepeatsField()
{
  sxInt32 numOfRepeats = 0;
  reader->getInteger("numberOfRepeats", numOfRepeats, SDL_FALSE);
  mapHandler->setNumberOfRepeats(static_cast<sxInt8>(numOfRepeats));
}

void TiledSceneFileLoader::setBiggestHitbox(const sxQuad* hitBox)
{
  sxVec2Int newBiggestHitbox = hitBox->getSize();

  if(newBiggestHitbox.x > biggestHitbox.x){
    biggestHitbox.x = newBiggestHitbox.x;
  }
  if(newBiggestHitbox.y > biggestHitbox.y){
    biggestHitbox.y = newBiggestHitbox.y;
  }
}


