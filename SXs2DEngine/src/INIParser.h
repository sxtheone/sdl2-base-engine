#pragma once
#include <map>
#include <vector>
#include "sxFile.h"

class INIParser
{
public:
  ReturnCodes parseFile(const sxString& filename, 
                        std::map<sxString, std::map<sxString, sxString> >& outData,
                        std::vector<sxString>& sectionOrder);

private:
  std::map<sxString, std::map<sxString, sxString> >* iniData;
  std::vector<sxString>* sectionOrderInFile;
  sxBool multiline;
  sxString currentSection;
  sxString currentField;
  sxString currentLine;

  void init();
  ReturnCodes doParsing(const sxString& filename);
  ReturnCodes processFile(sxFile* file);
  ReturnCodes parseCurrentLine();
  ReturnCodes prepareCurrentLine();
  ReturnCodes processCurrentLine();
  ReturnCodes processAndSetCurrentSectionName();
  ReturnCodes processValueInCurrentLine();
  ReturnCodes processMultilineValue();
  sxBool currentFieldHasElements();
  ReturnCodes processSingleValue(sxInt32 equalSignPos);
  sxBool isCurrentLineASectionHeader();
  sxBool currentLineCommentedOutOrEmpty();
  void removeCurrentLineComments();  
  ReturnCodes putValueToMap(const sxString& value);
  void removeEmptySectionFromSectionOrder();
};
