#include "FontManager.h"

FontManager* FontManager::fontManager = NULL;


void FontManager::createManager()
{
  if(NULL != fontManager){
    sxLOG(LL_WARNING, "SpriteManager was already instantinated.");
    delete fontManager;
  }
  fontManager = new FontManager();
}

FontManager* FontManager::getInstance()
{
  if(NULL == fontManager){
    sxLOG_E("Not yet created!");
    return NULL;
  }
  return fontManager;
}


FontManager::FontManager()
{
  mainRenderer = NULL;
  renderFont = NULL;
  lineWidthInPixels = 0;
  sxLOG_T("created.");
}

FontManager::~FontManager()
{
  // NOTE: don't delete mainRenderer, it's not created inside this class!
  sxLOG_T("destroyed.");
}

ReturnCodes FontManager::setRenderer(SDL_Renderer* renderer)
{
  if(NULL == renderer){
    sxLOG_E("Incoming renderer is NULL. Returning with error.");
    return ReturnCodes::RC_RENDERER_IS_NULL;
  }
  mainRenderer = renderer;

  return ReturnCodes::RC_SUCCESS;
}

void FontManager::destroyManager()
{
  sxLOG_T("Destroying...");
  if(NULL == fontManager){
    sxLOG_E("Instance is NULL, but how?");
    return;
  }
  delete fontManager;
  fontManager = NULL;
  sxLOG_T("destroyed.");
}

void FontManager::addFont(const sxString& fontName, sxFont& font)
{
  if(fonts.end() != fonts.find(fontName)){
    sxLOG_W("Font already loaded: %s", fontName.c_str());
  }
  fonts[fontName] = font;
}

sxFont* FontManager::getFont(const sxString& fontName)
{
  fontIt = fonts.find(fontName);
  if(fonts.end() == fontIt){
    sxLOG_E("Font not found: %s", fontName.c_str());
    return NULL;
  }
  return &fontIt->second;
}

ReturnCodes FontManager::setFontToRender(const sxString& fontName)
{
  sxFont* tempFont = getFont(fontName);
  if(NULL == tempFont){
    return ReturnCodes::RC_FONT_NOT_FOUND;
  }
  renderFont = tempFont;
  return ReturnCodes::RC_SUCCESS;
}

BasicObject* FontManager::generateSystemText(const sxString& text)
{
  return generateTextWithFont(text, systemFont);
}

BasicObject* FontManager::generateText(const sxString& text, const sxUInt32 widthInPixel)
{
  lineWidthInPixels = widthInPixel;
  return generateTextWithFont(text, *renderFont);
}

BasicObject* FontManager::generateText(const sxString& text, sxFont* font,
                                       const sxUInt32 widthInPixel)
{
  lineWidthInPixels = widthInPixel;
  return generateTextWithFont(text, *font);
}

BasicObject* FontManager::generateText(const sxString& text, const sxColor& color,
                                       const sxUInt32 widthInPixel)
{
  lineWidthInPixels = widthInPixel;
  sxColor origColor = renderFont->fontColor;
  renderFont->fontColor = color;
  BasicObject* generatedObject = generateTextWithFont(text, *renderFont);
  renderFont->fontColor = origColor;
  return generatedObject;
}

BasicObject* FontManager::generateTextWithFont(const sxString& text, sxFont& font)
{
  sxSprite* sprite;
  BasicObject* newObject = new BasicObject();

  sprite = renderTextToSprite(text, font);
  newObject->setSprite(sprite, SDL_TRUE);
  return newObject;
}

sxSprite* FontManager::renderTextToSprite(const sxString& text, sxFont& font)
{
  sxSprite* sprite = new sxSprite();
  SDL_Texture* generatedTexture;

  generatedTexture = renderTextToSDLTexture(text, font);
  sprite->setTexture(generatedTexture, SDL_TRUE); // texture should be destroyed on exit
  if(NULL == sprite->getTexture()){
     sxLOG_E("Texture not created from text: %s, Error: %s", text.c_str(), SDL_GetError());
  }
  return sprite;
}

SDL_Texture* FontManager::renderTextToSDLTexture(const sxString& text, sxFont& font)
{
  if(NULL == mainRenderer){
     sxLOG_E("Renderer is NULL.");
     return NULL;
  }
  if(NULL == font.font){
    sxLOG_PW("Font was not loaded: %s", font.filename.c_str());
    font.loadFont();
  }
  SDL_Surface* renderedSurface = renderTextToSDLSurface(text, font);
  SDL_Texture* outTexture = SDL_CreateTextureFromSurface(mainRenderer, renderedSurface);
  SDL_FreeSurface(renderedSurface);
  return outTexture;
}

SDL_Surface* FontManager::renderTextToSDLSurface(const sxString& text, sxFont& font)
{
  if(sxFontQualityEnum::NICE == font.quality){
    if(0 == lineWidthInPixels){
      return TTF_RenderUTF8_Blended(font.font, text.c_str(), font.fontColor);
    }else{
      return TTF_RenderUTF8_Blended_Wrapped(font.font, text.c_str(), font.fontColor,
                                            lineWidthInPixels);
    }
  }
  if(sxFontQualityEnum::NICE_WITH_BOX == font.quality){
    // NICE_WITH_BOX may have rendering problem on some android devices
    return TTF_RenderUTF8_Shaded(font.font, text.c_str(), 
                                 font.fontColor, font.backgroundColor);
  }
  if(sxFontQualityEnum::FAST != font.quality){
    sxLOG_E("Font Quality setting is invalid: %d. Rendering with 'FAST' quality.",
            font.quality);  
  }

  return TTF_RenderUTF8_Solid(font.font, text.c_str(), font.fontColor);
}

