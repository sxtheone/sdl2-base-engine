#include "SoundsFileLoader.h"

ReturnCodes SoundsFileLoader::processFile(const sxString& filename)
{
  ReturnCodes result = createReader(filename);
  if(shouldExitFromLoader(result)){
    return result;
  }

  currentSectionName = reader->setFirstSection();
  while(!currentSectionName.empty()){
    processSoundSection();
    currentSectionName = reader->setNextSection();
  }
  destroyReader();

  return ReturnCodes::RC_SUCCESS;
}

void SoundsFileLoader::processSoundSection()
{
  sxString sndFilename;
  if(ReturnCodes::RC_SUCCESS != reader->getFileNameWithPath("filename", sndFilename)){
    return;
  }
  newSound = new sxSound(sndFilename);
  setSoundLoops();
  setSoundVolume();
  setPermanentFlag();
  loadSoundToMemoryIfIndicated();
  getAudioManager->addSound(currentSectionName, newSound);
}

void SoundsFileLoader::setSoundLoops()
{
  sxInt32 loops = 0;
  reader->getInteger("loops", loops, SDL_FALSE);
  newSound->setNumberOfLoops(loops);
}

void SoundsFileLoader::setSoundVolume()
{
  sxInt32 volume = SOUND_VOLUME_MAX;
  reader->getInteger("volume", volume, SDL_FALSE);
  newSound->setVolume(volume);
}

void SoundsFileLoader::setPermanentFlag()
{
  sxBool permanentFlag = SDL_FALSE;
  reader->getBoolean("permanent", permanentFlag, SDL_FALSE);
  newSound->setPermanentFlag(permanentFlag);
}

void SoundsFileLoader::loadSoundToMemoryIfIndicated()
{
  sxBool loadNow = SDL_FALSE;
  reader->getBoolean("loadNow", loadNow, SDL_FALSE);
  if(SDL_TRUE == loadNow){
    newSound->loadSoundToMemory();
  }
}

