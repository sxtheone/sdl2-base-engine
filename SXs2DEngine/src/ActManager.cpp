#include "ActManager.h"
#include "MainConfig.h"
#include "EventManager.h"
#include "SceneCollisionPropsProcessor.h"
#include "ObjectCollisionPropsProcessor.h"
#include "OrderedDrawer.h"
#include "Profiler.h"
#include "SceneMsgSendString.hpp"

ActManager* ActManager::actManager = NULL;

ActManager::ActManager()
{
  initialize();
  sxLOG_T("ActManager Created.");
}

ActManager::~ActManager()
{
  sxLOG_T("Destroying ActManager...");
  destroyAllScenes();
  destroyAllSceneMsgs();  

  if(NULL != collDetector){
    delete collDetector;
    collDetector = NULL;
  }
  sxLOG_T("ActManager Destroyed.");
}

void ActManager::createManager()
{
  if(NULL != actManager){
    delete actManager;
  }
  actManager = new ActManager();
}

ActManager* ActManager::getInstance()
{
  if(NULL == actManager){
    sxLOG_E("Act Manager is not yet created.");
    return NULL;
  }
  return actManager;
}

void ActManager::destroyManager()
{
  sxLOG_T("Destroying ActManager...");
  if(NULL == actManager){
    sxLOG_E("ActManager instance is NULL, but how?");
    return;
  }
  delete actManager;
  actManager = NULL;
  sxLOG_T("ActManager destroyed.");
}

void ActManager::initialize()
{
  actLoader = NULL;
  objectCollisionDetEnabled = SDL_TRUE;
  collDetector = new ObjectCollisionDetector(&scenes);
}

void ActManager::loadAct(const sxString& filename,
                           SDL_Renderer* renderer)
{
  loadActAndGetReader(filename, renderer);
  deleteActLoader();
}

INIReader* ActManager::loadActAndGetReader(const sxString& filename,
                                           SDL_Renderer* renderer)
{
  sxUInt32 startSceneIdx = scenes.size();
  if(NULL != actLoader){delete actLoader;}
  actLoader = new ActFileLoader();
  actLoader->processFile(filename);
  processLoadedAct(*actLoader, renderer);
  collDetector->setEndSceneIndex(startSceneIdx);/// collision detector active in the last act
  finalizeCollisionSettings();
  return actLoader->getReaderObject();
}

void ActManager::deleteActLoader()
{
  if(NULL != actLoader){
    delete actLoader;
    actLoader = NULL;
  }
}

void ActManager::processLoadedAct(ActFileLoader& loader, SDL_Renderer* renderer)
{
  SceneIDs actPair;
  sxLOG_T("Processing Act: %s", loader.actName.c_str());
  actPair.actName = loader.actName;
  PushWindowOpacityToEventQueue(loader);

  for(sxUInt32 i=0; i<loader.sceneLoadInfo.size(); ++i){
    sxLOG_T("Processing Scene: %s", loader.sceneLoadInfo[i].sceneName.c_str());
    scenes.push_back(processScene(renderer, loader.sceneLoadInfo[i], actPair));
  }
  actInfo.pushAct(actPair);
  sxLOG_T("Act Processing finished for: %s", loader.actName.c_str());
}

void ActManager::PushWindowOpacityToEventQueue(ActFileLoader& loader)
{
  sxEvent opacity;
  opacity.type() = sxEventTypes::WINDOW_OPACITY;
  opacity.windowOpacity().value = loader.windowOpacity;
  getEventManager->pushEventToQueue(opacity);
}

Scene* ActManager::processScene(SDL_Renderer* renderer, const SceneLoadInfo& loadInfo,
                              SceneIDs& actPair)
{
  Scene* newScene = sceneBuilder.createInstance(loadInfo.sceneType);
  if(NULL != newScene){
    newScene->setRenderer(renderer);
    if(ReturnCodes::RC_SUCCESS == loadScene(loadInfo, newScene, actPair)){
      return newScene;
    }
    delete newScene;
  }
  sxLOG_E("Scene type is not registered: %s. Scene filename: %s, Scene name: %s",
    loadInfo.sceneType.c_str(), loadInfo.filename.c_str(), loadInfo.sceneName.c_str());
  return NULL;
}

ReturnCodes ActManager::loadScene(const SceneLoadInfo& loadInfo, Scene* newScene,
                                  SceneIDs& actPair)
{
  newScene->id = actInfo.getNextSceneID();
  ReturnCodes result = newScene->loadFile(loadInfo.filename);
  if(ReturnCodes::RC_SUCCESS == result){
    actPair.sceneIDs.push_back(newScene->id);
  }else{
    sxLOG_E("Couldn't register scene: %s, Type: %s, filename: %s",
      loadInfo.sceneName.c_str(), loadInfo.sceneType.c_str(), loadInfo.filename.c_str());
  }
  return result;
}

void ActManager::registerSceneMsgType(const SceneMsgID& id, AbstractFactory<SceneMsg>* type)
{
  sxLOG_T("Registering Scene Message Type: id: %d, name: %s", id.id, id.idStr.c_str());
  sceneMsgBuilder.registerFactory(id, type);
}

void ActManager::registerSceneType(const sxString& id, AbstractFactory<Scene>* type)
{
  sceneBuilder.registerFactory(id, type);
}

void ActManager::finalizeCollisionSettings()
{
  getSceneCollisionPropsProcessor->processCollisionSettings();
  getObjectCollisionPropsProcessor->processCollisionSettings();
  fillSceneCollisionProperties();
}

void ActManager::fillSceneCollisionProperties()
{
  for(sxUInt32 i=0; i<scenes.size(); ++i){
    if( scenes[i]->name.empty() ||
        NULL != scenes[i]->collisionProps )
    {
      continue;
    }
    CollisionProps props = getSceneCollisionPropsProcessor->getCollisionPropsForScene(scenes[i]->name);
    scenes[i]->collisionProps = new SceneCollisionProps(props);
  }
}

void ActManager::cleanSceneAndCollisionInfo()
{
  destroyAllScenes();
  getSceneCollisionPropsProcessor->clearCollisionProperties();
  getObjectCollisionPropsProcessor->clearCollisionProperties();
}

void ActManager::draw()
{
  sxUInt32 numOfScenes = scenes.size();
  for(sxUInt32 i=0; i<numOfScenes; ++i){
    scenes[i]->draw();
  }
}

void ActManager::frameStart()
{
  if(SDL_TRUE == OrderedDrawer::isCreated()){
    getOrderedDrawer->frameStarts();
  }
}

void ActManager::stepAnim(const sxUInt32& ticksPassed)
{
  for(sxInt32 i=scenes.size()-1; i>=0; --i){
    scenes[i]->stepAnimation(ticksPassed);
  }
}

void ActManager::collisionDetectionChecks(const sxUInt32& ticksPassed)
{
  if(SDL_TRUE == objectCollisionDetEnabled) collDetector->doCollisionDetectionChecks(ticksPassed);
}

void ActManager::frameEnd()
{
  if(SDL_TRUE == OrderedDrawer::isCreated()){
    getOrderedDrawer->frameEnds();
  }
}

void ActManager::processFrameStep(const sxUInt32& ticksPassed)
{
  frameStart();
  processDelayedSceneMsgs(); // first because if delay is 0 it should be processed
  processSceneMessages();
  processSceneMessagesFromOutside();
  stepAnim(ticksPassed);
  collisionDetectionChecks(ticksPassed);
  frameEnd();
}

void ActManager::pushSceneMsgToQueue(SceneMsg* msg)
{
  sceneMsgs.push(msg);
}

void ActManager::pushDelayedSceneMsgToQueue(SceneMsg* msg, const sxUInt32 framesToDelay)
{
  delayedSceneMsgs.push_back(DelayedSceneMsg(msg, framesToDelay));
}

void ActManager::pushSceneMsgSendStringToQueue(const sxString& str, const sxBool broadcast)
{
  pushSceneMsgToQueue(createStringMsg(str, SceneMsgSendString::DEFAULT_ID, broadcast));
}

void ActManager::pushSceneMsgSendStringToQueue(const sxString& str, const sxInt32 id,
                                               const sxBool broadcast)
{
  pushSceneMsgToQueue(createStringMsg(str, id, broadcast));
}

void ActManager::pushDelayedSceneMsgSendStringToQueue(const sxString& str,
                                                      const sxUInt32 framesToDelay,
                                                      const sxBool broadcast)
{
  pushDelayedSceneMsgToQueue(createStringMsg(str, SceneMsgSendString::DEFAULT_ID, broadcast), framesToDelay);
}

void ActManager::pushDelayedSceneMsgSendStringToQueue(const sxString& str,
                                                      const sxInt32 id,
                                                      const sxUInt32 framesToDelay,
                                                      const sxBool broadcast)
{
  pushDelayedSceneMsgToQueue(createStringMsg(str, id, broadcast), framesToDelay);
}

void ActManager::processInputEvents()
{
//  getProfiler->startCountingFor("EventHandling");
  sxInt32 i=0;
  sxEvent event;
  getEventManager->getNextEvent(sxEventTypes::ANY_KIND_OF_USER_INPUT, event);
  while(sxEventTypes::INVALID != event.type()){
    i=scenes.size();
    do{
      --i;
    }while(i>=0 && SDL_FALSE == scenes[i]->processInputEvent(event));
    event.destroyEvent();
    getEventManager->getNextEvent(sxEventTypes::ANY_KIND_OF_USER_INPUT, event);
  }
//  getProfiler->stopCountingFor("EventHandling");
}

void ActManager::processSceneMessages()
{
//  getProfiler->startCountingFor("SceneMsgs");
  // the SceneMsgs should be deleted here, after the scenes handled them
  SceneMsg* msg = NULL;
  for(sxInt32 i=scenes.size()-1; i>=0; --i){
    do{
      msg = scenes[i]->msgToOtherScenes();
      if(NULL == msg){
        break;
      }
      sendOutMsgToOtherScenes(i, msg);
      delete msg;
    }while(1);
  }
//  getProfiler->stopCountingFor("SceneMsgs");
}

void ActManager::sendOutMsgToOtherScenes(const sxInt32& senderID, SceneMsg*& msg)
{
  for(sxInt32 j=scenes.size()-1; j>=0; --j){
    if(j != senderID){
      if( SDL_TRUE == scenes[j]->handleMsgFromOtherScene(msg) &&
          SDL_FALSE == msg->broadcast )
      {
        break;
      }
    }
  }
}

void ActManager::processSceneMessagesFromOutside()
{
  while(!sceneMsgs.empty()){
    sendOutMsgToOtherScenes(-1, sceneMsgs.front());
    delete sceneMsgs.front();
    sceneMsgs.pop();
  }
}

void ActManager::addSceneOnTop(SDL_Renderer* renderer, const sxUInt32 sceneID,
                               const SceneLoadInfo& info)
{
  SceneIDs* sceneIDs = actInfo.getActOfScene(sceneID);
  if(NULL != sceneIDs){
    // find the last scene id of the act in the scenes vector
    sxUInt32 idx = 0;
    while( idx < scenes.size() &&
           sceneIDs->sceneIDs.back() != scenes[idx]->id )
    {
      ++idx;
    }
    Scene* newScene = processScene(renderer, info, *sceneIDs);
    if(idx+1 == scenes.size()) scenes.push_back(newScene);
    else scenes.insert(scenes.begin()+idx+1, newScene);
    return;
  }
  sxLOG_E("Scene ID not found: %d", sceneID);
}

void ActManager::addSceneOnTop(SDL_Renderer* renderer, const sxUInt32 sceneID,
                               const sxString& filename, const sxString& type)
{
  SceneLoadInfo info;
  info.filename = filename;
  info.sceneName = filename;
  info.sceneType = type;
  addSceneOnTop(renderer, sceneID, info);
}

SceneIDs* ActManager::removeSceneFromAct(const sxUInt32 sceneID)
{
  SceneIDs* sceneIDs = actInfo.getActOfScene(sceneID);
  if(NULL != sceneIDs){
    sxUInt32 i=0;
    while(sceneID != scenes[i]->id){
      ++i;
    }
    delete scenes[i];
    scenes.erase(scenes.begin()+i);
    actInfo.removeSceneID(sceneID);
  }
  return sceneIDs;
}

void ActManager::removeTopmostAct()
{
  if(1 >= actInfo.getNumberOfActs()){
    sxLOG_E("Won't remove the last Act. Use cleanSceneAndCollisionInfo().");
    return;
  }
  SceneIDs* actToRemove = actInfo.getTopmostAct();
  removeScenes(&actToRemove->sceneIDs, actToRemove->actName);
  actInfo.popAct();
  setCollisionDetectorStartingSceneIndex();
}

void ActManager::removeScenes(const std::vector<sxUInt32>* sceneIDs,
                              const sxString& actName)
{
  // go backwards because currently we only delete the topmost act
  sxUInt32 idIdx = sceneIDs->size();
  while(0 < idIdx){
    --idIdx;
    sxUInt32 sceneIdx;
    if(SDL_TRUE == isSceneIDPresentInScenesVector((*sceneIDs)[idIdx], sceneIdx)){
        delete *(scenes.begin()+sceneIdx);
        scenes.erase(scenes.begin()+sceneIdx);
    }else{
      sxLOG_E("Scene ID not found: %d for Act: %s", (*sceneIDs)[idIdx], actName.c_str());
    }
  }
}

sxBool ActManager::isSceneIDPresentInScenesVector(const sxUInt32 sceneID, sxUInt32& itemIdx)
{
  itemIdx = scenes.size();
  do{
    --itemIdx;
    if(sceneID == scenes[itemIdx]->id){
      return SDL_TRUE;
    }
  }while(0 < itemIdx);
  return SDL_FALSE;
}

void ActManager::setCollisionDetectorStartingSceneIndex()
{
  sxUInt32 numOfScenes = 0;
  SceneIDs* topAct = actInfo.getTopmostAct();
  if(NULL != topAct){
    numOfScenes = topAct->sceneIDs.size();
  }
  collDetector->setEndSceneIndex(scenes.size()-numOfScenes);
}

void ActManager::destroyAllScenes()
{
  for(sxUInt32 i=0; i<scenes.size(); ++i){
    if(NULL != scenes[i]){
      delete scenes[i];
      scenes[i] = NULL;
    }
  }
  scenes.clear();
  actInfo.destroyAllActInfo();
}
void ActManager::destroyAllSceneMsgs()
{
  while(!sceneMsgs.empty()){
    delete sceneMsgs.front();
    sceneMsgs.pop();
  }

  std::list<DelayedSceneMsg>::iterator it = delayedSceneMsgs.begin();
  while(delayedSceneMsgs.end() != it){
    delete (*it).msg;
    ++it;
  }
}

SceneMsg* ActManager::createStringMsg(const sxString& str, const sxInt32 id, const sxBool broadcast)
{
  SceneMsgSendString* msg;
  msg = static_cast<SceneMsgSendString*>(createSceneMsg(SceneMsgTypes::SceneMsgSendString));
  msg->value = str;
  msg->id = id;
  msg->broadcast = broadcast;
  return msg;
}

void ActManager::processDelayedSceneMsgs()
{
  std::list<DelayedSceneMsg>::iterator it = delayedSceneMsgs.begin();
  while(delayedSceneMsgs.end() != it){
    if(0 < it->framesToDelay){
      --it->framesToDelay;
    }else{
      sceneMsgs.push(it->msg);
      it = delayedSceneMsgs.erase(it);
      continue;
    }
    ++it;
  }
}

/* I hope this won't be necessary because it smells like a hack
Scene* ActManager::getScene(const sxString& sceneName)
{
  sxInt32 numOfScenes = static_cast<sxInt32>(scenes.size());
  for(sxInt32 i=numOfScenes-1; i>=0; --i){
    if(sceneName == scenes[i]->name){
      return scenes[i];
    }
  }
  return NULL;
}*/