#include "ConfigFileLoader.h"
#include "FontsFileLoader.h"

ReturnCodes ConfigFileLoader::processFile(const sxString& filename)
{
  if(ReturnCodes::RC_SUCCESS == createReader(filename)){
    return parseConfigFile(filename);
  }
  sxLOG_E("Couldn't load Main Config file: %s", filename.c_str());
  return ReturnCodes::RC_FILE_PARSE_ERROR;
}

ConfigFileLoader::~ConfigFileLoader()
{
  destroyReader();
}

ReturnCodes ConfigFileLoader::parseConfigFile(const sxString& filename)
{
  if(SDL_FALSE == setBaseConfigSection()){
    sxLOG(LL_ERROR, "Defaults will be loaded. Error code: %d", reader->ParseError());
    destroyReader();
    return ReturnCodes::RC_FILE_PARSE_ERROR;
  }
  processSystemFont();
  return ReturnCodes::RC_SUCCESS;
}

void ConfigFileLoader::processSystemFont()
{
  FontsFileLoader fontsLoader;
  sxString fontFilename;
  reader->getStr("System Font", fontFilename);
  fontsLoader.thisIsTheSystemFontFile = SDL_TRUE;
  fontsLoader.processFile(fontFilename);
}

sxInt32 ConfigFileLoader::getInt(const sxString& name, sxInt32 default_value,  sxBool mandatory)
{
  sxInt32 value = default_value;
  reader->getInteger(name, value, mandatory);
  return value;
}

void ConfigFileLoader::getWindowLogicalSize(sxVec2Int& windowLogicalSize)
{
  reader->getVec2Int("Logical Size", windowLogicalSize);
}

void ConfigFileLoader::getWindowScreenSize(sxVec2Int& windowScreenSize)
{
  reader->getVec2Int("Screen Size", windowScreenSize);
}

void ConfigFileLoader::getDesiredFPS(sxUInt16& desiredFPS)
{
  desiredFPS = static_cast<sxUInt16>(getInt("Desired FPS", desiredFPS));
}

void ConfigFileLoader::getWindowType(sxUInt32& windowType)
{
    std::vector<sxCharStr> settingsList;
    reader->getStrList("Window Type", settingsList, SDL_FALSE);

    sxUInt32 tempType = 0;
    for (size_t i = 0; i < settingsList.size(); ++i)
    {
        if(settingsList[i] == "fullscreen") tempType |= SDL_WINDOW_FULLSCREEN;
        if(settingsList[i] == "borderless") tempType |= SDL_WINDOW_BORDERLESS;
        if(settingsList[i] == "fullscreen desktop res") tempType |= SDL_WINDOW_FULLSCREEN_DESKTOP;
        if(settingsList[i] == "resizable") tempType |= SDL_WINDOW_RESIZABLE;
        if(settingsList[i] == "minimized") tempType |= SDL_WINDOW_MINIMIZED;
        if (settingsList[i] == "capture mouse"){
            tempType |= SDL_WINDOW_MOUSE_CAPTURE | SDL_WINDOW_INPUT_GRABBED;
        }
        if(settingsList[i] == "highdpi") tempType |= SDL_WINDOW_ALLOW_HIGHDPI;
        if(settingsList[i] == "always on top") tempType |= SDL_WINDOW_ALWAYS_ON_TOP;
        if (settingsList[i] == "skip taskbar") tempType |= SDL_WINDOW_SKIP_TASKBAR;
    }

    if (tempType != 0) windowType = tempType;
}

void ConfigFileLoader::getLetterboxInfo(sxBool& letterbox)
{
  reader->getBoolean("Letterbox", letterbox);
}

void ConfigFileLoader::getWindowName(sxString& windowName)
{
  reader->getStr("Window Name", windowName);
}

void ConfigFileLoader::getSpriteScalingMethod(sxString& spriteScalingMethod)
{
  sxString tempName;
  reader->getStr("Sprite Scaling", tempName);
  if( !tempName.empty() &&
      (0 == tempName.compare("nearest") ||
       0 == tempName.compare("linear") ||
       0 == tempName.compare("best")) )
  {
    spriteScalingMethod = tempName;
  }
#ifdef __sdlANDROID__
  reader->getStr("Sprite Scaling Android", tempName, SDL_FALSE);
  if( !tempName.empty() &&
      (0 == tempName.compare("nearest") ||
       0 == tempName.compare("linear") ||
       0 == tempName.compare("best")) )
  {
    spriteScalingMethod = tempName;
  }
#endif
}

void ConfigFileLoader::getBackgroundColor(sxColor& backgroundColor)
{
  reader->getColor("Background Color", backgroundColor);
}

void ConfigFileLoader::getAudioFrequency(sxUInt16& frequency)
{
  frequency = static_cast<sxUInt16>(getInt("Frequency", frequency));  
}

void ConfigFileLoader::getAudioNumberOfChannels(sxUInt8& channels)
{
  channels = static_cast<sxUInt8>(getInt("Channels", channels));
}

void ConfigFileLoader::getAudioChunkSize(sxUInt16& chunksize)
{
  chunksize = static_cast<sxUInt16>(getInt("Chunksize", chunksize));
}

void ConfigFileLoader::getStartingActFilename(sxString& actFilename)
{
  reader->getStr("Starting Act", actFilename);
}
