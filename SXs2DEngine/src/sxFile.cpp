#include "sxFile.h"

sxFile::sxFile(sxString name, sxString mode)
{
  file = NULL;
  fileContent = NULL;
  error = ReturnCodes::RC_SUCCESS;
  currentPosition = 0;
  fileSize = 0;
  openFile(name, mode);
}

sxFile::~sxFile()
{
  closeFile();
}

void sxFile::openFile(sxString name, sxString mode)
{
  file = SDL_RWFromFile(name.c_str(), mode.c_str());
  if(NULL == file){
    error = ReturnCodes::RC_FILE_CANT_BE_OPENED;
  }
}

void sxFile::closeFile()
{
  if(NULL != file){
    SDL_RWclose(file);
    file = NULL;
  }
  if(NULL != fileContent){
    delete []fileContent;
    fileContent = NULL;
  }
}

ReturnCodes sxFile::readFileToBuffer()
{
  fileSize = SDL_RWseek(file, 0, RW_SEEK_END);
  if(0 > fileSize){
    return ReturnCodes::RC_FILE_READ_ERROR;
  }
  SDL_RWseek(file, 0, RW_SEEK_SET);

  fileContent = new char[static_cast<sxUInt32>(fileSize+1)];
  if(fileSize != SDL_RWread(file, fileContent, 1, static_cast<size_t>(fileSize))){
    delete[] fileContent;
    fileContent = NULL;
    return ReturnCodes::RC_FILE_READ_ERROR;
  }
  fileContent[fileSize] = '\0';

  return ReturnCodes::RC_SUCCESS;
}

ReturnCodes sxFile::read(sxString& readLine)
{
  if(NULL == fileContent){
    error = readFileToBuffer();
    if(ReturnCodes::RC_SUCCESS != error){
      return error;
    }
  }
  if(fileSize <= currentPosition){
    return ReturnCodes::RC_END_OF_FILE;
  }

  readNextLine(readLine);
  return ReturnCodes::RC_SUCCESS;
}

void sxFile::readNextLine(sxString& readLine)
{
  sxInt64 startPosition = currentPosition;
  while( fileSize > currentPosition &&
         '\n' != fileContent[currentPosition] )
  {
    ++currentPosition;
  }
  if(fileSize > currentPosition){
    ++currentPosition;
  }
  readLine.assign(&fileContent[startPosition], static_cast<size_t>(currentPosition-startPosition));
}

void sxFile::write(sxString msg)
{
  SDL_RWwrite(file, msg.c_str(), msg.size(), 1);
}