#pragma once
#include "sxTypes.h"

namespace FileExtensions
{
  const sxString actFile = ".act";
  const sxString animsFile = ".anims";
  const sxString configFile = ".config";
  const sxString fontsFile = ".fonts";
  const sxString hitboxesFile = ".hitboxes";
  const sxString objectsFile = ".objects";
  const sxString scenesFile = ".scene";
  const sxString soundsFile = ".sounds";
  const sxString spritesFile = ".sprites";
  const sxString tiledSceneFile = ".tiledscene";
}