#include "ObjectBuilder.h"
#include "BasicObject.h"
#include "TileElement.h"
#include "MenuButton.h"

ObjectBuilder::ObjectBuilder()
{
  registerBasicTypes();
}

void ObjectBuilder::registerBasicTypes()
{
  registerFactory(ObjectTypes::BasicObject, new ObjectFactory<BasicObject>());
  registerFactory(ObjectTypes::AnimObject, new ObjectFactory<AnimObject>());
  registerFactory(ObjectTypes::TileElement, new ObjectFactory<TileElement>());
  registerFactory(ObjectTypes::ButtonObject, new ObjectFactory<ButtonObject>());
  registerFactory(ObjectTypes::MenuButton, new ObjectFactory<MenuButton>());
}