#include "sxAnimSprite.h"
#include "LogManager.h"

sxAnimSprite::sxAnimSprite()
{
  tintTimes[0] = TINT_TIME_INVALID;
  tintTimes[1] = 0;
  tintTimes[2] = 0;
  tintTimes[3] = 0;
}

sxAnimSprite::~sxAnimSprite()
{
  for(sxUInt32 i=0; i<animFrames.size(); ++i){
    delete animFrames[i];
  }
}

void sxAnimSprite::addSprite(sxSprite* sprite)
{
  sprites.push_back(sprite);
}

ReturnCodes sxAnimSprite::addFrame(sxAnimFrame* newFrame)
{
  if(sprites.size() <= newFrame->spriteNumber){
    sxLOG(LL_ERROR, "Sprite Number is invalid: %d", newFrame->spriteNumber);
    return ReturnCodes::RC_FILE_PARSE_ERROR;
  }
  animFrames.push_back(newFrame);
  return ReturnCodes::RC_SUCCESS;
}

sxBool sxAnimSprite::isFrameValid(sxUInt8 frameNum)
{
  if(animFrames.size() <= frameNum){
    return SDL_FALSE;
  }
  return SDL_TRUE;
}

sxSprite* sxAnimSprite::getSprite(sxUInt8 spriteNum)
{
  if(sprites.size() > spriteNum){
    return sprites[spriteNum];
  }
  return NULL;
}

sxAnimFrame* sxAnimSprite::getFrame(sxUInt8 frameNum)
{
  if(isFrameValid(frameNum)){
    return animFrames[frameNum];
  }
  return NULL;
}

void sxAnimSprite::setTintTimes(const sxInt32 r, const sxInt32 g,
                                const sxInt32 b, const sxInt32 a)
{
  tintTimes[0] = r;
  tintTimes[1] = g;
  tintTimes[2] = b;
  tintTimes[3] = a;
}