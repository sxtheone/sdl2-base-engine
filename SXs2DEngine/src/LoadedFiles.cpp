#include "LoadedFiles.h"
#include "LogManager.h"

LoadedFiles* LoadedFiles::loadedFiles = NULL;

LoadedFiles::LoadedFiles()
{
  sxLOG(LL_TRACE, "created.");
}

LoadedFiles::~LoadedFiles()
{
  sxLOG(LL_TRACE, "destroyed.");
}

LoadedFiles* LoadedFiles::getInstance()
{
  if(NULL == loadedFiles){
    sxLOG(LL_ERROR, "init() not called yet.");
    return NULL;
  }
  return loadedFiles;
}

void LoadedFiles::init()
{
  if(NULL == loadedFiles){
    loadedFiles = new LoadedFiles();
  }else{
    sxLOG(LL_ERROR, "Already exists.");
  }
}

void LoadedFiles::destroy()
{
  if(NULL == loadedFiles){
    sxLOG(LL_ERROR, "Already destroyed.");
  }else{
    delete loadedFiles;
    loadedFiles = NULL;
  }
}

sxBool LoadedFiles::isFileAlreadyLoaded(const sxString& filename)
{
  std::vector<std::string>::iterator it = filesLoaded.begin();
  while(filesLoaded.end() != it){
    if(0 == it->compare(filename)){
      sxLOG_T("File already loaded: %s", filename.c_str());
      return SDL_TRUE;
    }
    ++it;
  }
  return SDL_FALSE;
}

void LoadedFiles::removeFilesContaining(const sxString& stringPart)
{
  std::vector<sxString>::iterator it = filesLoaded.begin();
  while(filesLoaded.end() != it){
    if(sxString::npos != it->find(stringPart)){
      it = filesLoaded.erase(it);
    }else{
      ++it;
    }
  }
}

void LoadedFiles::addToLoadedFilesList(const sxString& filename)
{
  filesLoaded.push_back(filename);
  sxLOG(LL_TRACE, "File: %s loaded.", filename.c_str());
}

void LoadedFiles::resetLoadedFilesList()
{
  filesLoaded.clear();
}
