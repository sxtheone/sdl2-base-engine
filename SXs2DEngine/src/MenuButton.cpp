#include "MenuButton.h"
#include "ActManager.h"
#include "EventManager.h"
#include "SpriteManager.h"
#include "SceneMsgCloseScene.hpp"

MenuButton::~MenuButton()
{
  onClickEvent.destroyEvent();
  destroyMessage(onClickSceneMsg);
}

void MenuButton::assign(const ObjectBase& source)
{
  ButtonObject::assign(source); // don't forget to call the ancestor's assign function
#ifdef _DEBUG
  const MenuButton* sourceObj = dynamic_cast<const MenuButton*>(&source);
#else
  const MenuButton* sourceObj = static_cast<const MenuButton*>(&source);
#endif
  onClickEvent = sourceObj->onClickEvent;
  if(NULL != sourceObj->onClickSceneMsg){
    destroyMessage(onClickSceneMsg);
    onClickSceneMsg = getActManager->createSceneMsg(sourceObj->onClickSceneMsg->type);
    *onClickSceneMsg = *sourceObj->onClickSceneMsg;
  }
}

ReturnCodes MenuButton::loadData(INIReader* reader)
{
  ReturnCodes result = ButtonObject::loadData(reader);
  if(ReturnCodes::RC_SUCCESS == result){
    processEventFields(reader);
    processSceneMessageFields(reader);
  }
  return result;
}

void MenuButton::setHitbox()
{
  if(NULL != collisionProps){
    if(1.0 != spriteScale.x || 1.0 != spriteScale.y){
      scaleHitbox();
    }else{
      collisionProps->hitbox = activeSprite->hitbox;
    }
  }
}

void MenuButton::scaleHitbox()
{
  sxQuad hitbox;
  hitbox.bottomRight.x = static_cast<sxFloat>(getSpriteSize().x);
  hitbox.bottomRight.y = static_cast<sxFloat>(getSpriteSize().y);
  sxString name = strFormatter("generated_%.0f_%.0f",
                                hitbox.bottomRight.x, hitbox.bottomRight.y);
  if(SDL_FALSE == getSpriteManager->hitboxExists(name)){
    getSpriteManager->addHitbox(name, hitbox);
    sxLOG_T("Scaled hitbox generated: %s", name.c_str());
  }
  sxQuad* storedHitbox = getSpriteManager->getHitboxRef(name);
  setNewHitbox(storedHitbox);
}

void MenuButton::destroyMessage(SceneMsg* sceneMsgToDelete)
{
  if(NULL != sceneMsgToDelete){
    delete sceneMsgToDelete;
    sceneMsgToDelete = NULL;
  }
}

void MenuButton::processEventFields(INIReader* reader)
{
  sxString eventTypeStr;
  if(ReturnCodes::RC_SUCCESS != reader->getStr("eventType", eventTypeStr, SDL_FALSE)){
    return;
  }
  onClickEvent.destroyEvent();
  if(0 == eventTypeStr.compare("CHANGE_ACT")){
    onClickEvent.type() = sxEventTypes::CHANGE_ACT;
  }else if(0 == eventTypeStr.compare("QUIT")){
    onClickEvent.type() = sxEventTypes::QUIT;
  }
  processEventDataField(reader);
}

void MenuButton::processEventDataField(INIReader* reader)
{
  if(sxEventTypes::CHANGE_ACT == onClickEvent.type()){
    std::vector<sxString> eventData;
    if(ReturnCodes::RC_SUCCESS != reader->getStrList("eventData", eventData, SDL_FALSE)){
      return;
    }
    onClickEvent.actChange().changeType = getActChangeType(eventData[0]);
    if(1 < eventData.size()){
      onClickEvent.actChange().targetActName = eventData[1];
    }
  }
}

sxActChange::ChangeType MenuButton::getActChangeType(const sxString& changeType)
{
  sxActChange::ChangeType result = sxActChange::INVALID;
  if(0 == changeType.compare("DESTROY_OTHERS")){
    result = sxActChange::DESTROY_OTHERS;
  }else if(0 == changeType.compare("ADD_ON_TOP")){
    result = sxActChange::ADD_ON_TOP;
  }else if(0 == changeType.compare("REMOVE_FROM_TOP")){
    result = sxActChange::REMOVE_FROM_TOP;
  }
  return result;
}

void MenuButton::processSceneMessageFields(INIReader* reader)
{
  sxString sceneMsgTypeStr;
  if(ReturnCodes::RC_SUCCESS != reader->getStr("sceneMessageType", sceneMsgTypeStr, SDL_FALSE)){
    return;
  }
  destroyMessage(onClickSceneMsg);
  onClickSceneMsg = getActManager->createSceneMsg(sceneMsgTypeStr);
  reader->getBoolean("broadcast", onClickSceneMsg->broadcast, SDL_FALSE);
  if(ReturnCodes::RC_SUCCESS != onClickSceneMsg->loadData(reader)){
    sxLOG_E("Couldn't load sceneMsg data. SceneMsg: %s", sceneMsgTypeStr.c_str());
  }
}

void MenuButton::setSceneMessage(SceneMsg* msg)
{
  destroyMessage(onClickSceneMsg);
  onClickSceneMsg = msg;
}

void MenuButton::objectHit(sxEvent& eventHappened)
{
  ButtonObject::objectHit(eventHappened);
  /// not checking the trigger event now
  sendOutEvent();
  sendOutSceneMessage();
}

void MenuButton::sendOutEvent()
{
  /// don't use onClickEvent in pushEventToQueue() because it will 
  /// be destroyed after processed.
  if(sxEventTypes::INVALID != onClickEvent.type()){
    sxEvent clickEventToPush = onClickEvent;
    getEventManager->pushEventToQueue(clickEventToPush);
  }

}

void MenuButton::sendOutSceneMessage()
{
  /// don't use the SceneMsg stored here, because it will be
  /// deleted.
  if(NULL != onClickSceneMsg){
    SceneMsg* msg = getActManager->createSceneMsg(onClickSceneMsg->type);
    *msg = *onClickSceneMsg;
    // special case: the parent scene of the object is needed to be able to close it
    if(SceneMsgTypes::SceneMsgCloseScene == msg->type){
      static_cast<SceneMsgCloseScene*>(msg)->sceneID = objectProps.sceneID();
    }
    getActManager->pushSceneMsgToQueue(msg);
  }
}
