#include "SpriteManager.h"

SpriteManager* SpriteManager::spriteManager = NULL;


void SpriteManager::createManager()
{
  if(NULL != spriteManager){
    sxLOG(LL_WARNING, "SpriteManager was already instantinated.");
    delete spriteManager;
  }
  spriteManager = new SpriteManager();
}

SpriteManager* SpriteManager::getInstance()
{
  if(NULL == spriteManager){
    sxLOG(LL_ERROR, "Not yet created!");
    return NULL;
  }
  return spriteManager;
}

SpriteManager::SpriteManager()
{
  mainRenderer = NULL;
  sxLOG(LL_TRACE, "Texture Manager created.");
}

SpriteManager::~SpriteManager()
{
  destroyAnimSprites();
  // NOTE: don't delete mainRenderer, it's not created inside this class!
  sxLOG(LL_TRACE, "Texture Manager destroyed.");
}


void SpriteManager::destroyAnimSprites()
{
  std::map<sxString, sxAnimSprite*>::iterator spriteIterator;
  spriteIterator = animSprites.begin();
  while(animSprites.end() != spriteIterator){
    delete spriteIterator->second;
    ++spriteIterator;
  }
  animSprites.clear();
}

ReturnCodes SpriteManager::initialize(SDL_Renderer* renderer)
{
  if(NULL == renderer){
    sxLOG(LL_ERROR, "Incoming renderer is NULL. Returning with error.");
    return ReturnCodes::RC_RENDERER_IS_NULL;
  }
  mainRenderer = renderer;
  destroyAllSpritesHitboxes();
  destroyAllTextures(TypesToReleaseEnum::PERMANENT_ITEMS_ALSO);

  return ReturnCodes::RC_SUCCESS;
}

void SpriteManager::destroyManager()
{
  sxLOG(LL_TRACE, "Destroying SpriteManager...");
  if(NULL == spriteManager){
    sxLOG(LL_ERROR, "SpriteManager instance is NULL, but how?");
    return;
  }
  delete spriteManager;
  spriteManager = NULL;
  sxLOG(LL_TRACE, "SpriteManager destroyed.");
}

void SpriteManager::loadTextureMap(const sxString& filename, const sxBool permanent, const sxBool streaming)
{
  if(textureMaps.end() == textureMaps.find(filename)){
    if(SDL_FALSE == streaming){
      textureMaps[filename].loadTexture(mainRenderer, filename, permanent);
    }else{
      textureMaps[filename].loadStreamingTexture(mainRenderer, filename, permanent);
    }
  }
}

void SpriteManager::addSprite(const sxString& spriteName, sxSprite& sprite)
{
  if(sprites.end() != sprites.find(spriteName)){
    sxLOG(LL_WARNING, "Sprite already loaded: %s", spriteName.c_str());
  }
  sprites[spriteName] = sprite;
}

void SpriteManager::addAnimSprite(const sxString& animName, sxAnimSprite* animation)
{
  if(animSprites.end() != animSprites.find(animName)){
    sxLOG(LL_WARNING, "Anim already loaded: %s", animName.c_str());
    delete animSprites[animName];
  }
  animSprites[animName] = animation;
}

void SpriteManager::addHitbox(const sxString& hitboxName, sxQuad& hitbox)
{
  checkIfHitboxAlreadyInMap(hitboxName, hitbox);
  hitboxes[hitboxName] = hitbox;
}

void SpriteManager::checkIfHitboxAlreadyInMap(const sxString& hitboxName, const sxQuad& hitbox)
{
  std::map<sxString, sxQuad>::iterator hbIt = hitboxes.find(hitboxName);
  if(hitboxes.end() != hbIt){
//TODO: in case of error, don't overwrite. Keep the old data and drop the new
    sxLOG_E("Hitbox already loaded, will be overwritten: %s", hitboxName.c_str());
  }
  hbIt = hitboxes.begin();
  while(hitboxes.end() != hbIt){
    if(hitbox == hbIt->second){
      sxLOG_PW("%s is already in the list with name: %s",
        hitboxName.c_str(), hbIt->first.c_str());
      break;
    }
    ++hbIt;
  }
}

sxTexture* SpriteManager::getTextureMap(const sxString& id)
{
  std::map<sxString, sxTexture>::iterator textureMapIt;
  textureMapIt = textureMaps.find(id);
  if(textureMaps.end() == textureMapIt){
    sxLOG(LL_ERROR, "Couldn't find texture Map: %s", id.c_str());
    return NULL;
  }
  return &textureMapIt->second;
}

sxSprite* SpriteManager::getSprite(const sxString& id)
{
  std::map<sxString, sxSprite>::iterator spriteIterator;
  spriteIterator = sprites.find(id);
  if(sprites.end() == spriteIterator){
    sxLOG(LL_ERROR, "Sprite not found: %s", id.c_str());
    return NULL;
  }

  return &(spriteIterator->second);
}

sxAnimSprite* SpriteManager::getAnimSprite(const sxString& id)
{
  std::map<sxString, sxAnimSprite*>::iterator spriteIterator;
  spriteIterator = animSprites.find(id);
  if(animSprites.end() == spriteIterator){
    sxLOG(LL_ERROR, "AnimSprite not found: %s", id.c_str());
    return NULL;
  }

  return spriteIterator->second;
}

sxQuad* SpriteManager::getHitboxRef(const sxString& name)
{
  std::map<sxString, sxQuad>::iterator hbIt = hitboxes.find(name); 
  if(hitboxes.end() == hbIt){
    sxLOG_E("No Hitbox found with name: %s", name.c_str());
    return NULL;
  }
  return &hbIt->second;
}

sxBool SpriteManager::hitboxExists(const sxString& name)
{
  std::map<sxString, sxQuad>::iterator hbIt = hitboxes.find(name); 
  if(hitboxes.end() == hbIt){
    return SDL_FALSE;
  }
  return SDL_TRUE;
}

void SpriteManager::destroyAllSpritesHitboxes()
{
  sprites.clear();
  hitboxes.clear();
  destroyAnimSprites();
}

void SpriteManager::destroyAllTextures(const TypesToRelease textureType)
{
  switch(textureType){
  case TypesToReleaseEnum::NORMAL_ITEMS_ONLY: destroyNonPermanentTextures(); break;
  case TypesToReleaseEnum::PERMANENT_ITEMS_ALSO: textureMaps.clear(); break;
  default:
    sxLOG_E("Invalid type: %d", textureType);
    break;
  }
}

void SpriteManager::destroyNonPermanentTextures()
{
  std::map<sxString, sxTexture>::iterator it, helperIt;
  it = textureMaps.begin();
  while(textureMaps.end() != it){
    if(SDL_TRUE == it->second.isPermanent()){
      ++it;
    }else{
      helperIt = it;
      ++it;
      textureMaps.erase(helperIt);
    }
  }
}