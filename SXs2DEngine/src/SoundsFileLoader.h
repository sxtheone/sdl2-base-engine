#pragma once
#include <map>
#include "AudioManager.h"
#include "FileLoaderBase.h"

class SoundsFileLoader : public FileLoaderBase
{
public:
  ReturnCodes processFile(const sxString& filename);

private:
  sxSound* newSound;
  
  void processSoundSection();
  void setSoundLoops();
  void setSoundVolume();
  void setPermanentFlag();
  sxUInt8 checkAndCorrectVolume(sxInt32 incomingVolume);
  void loadSoundToMemoryIfIndicated();
};

