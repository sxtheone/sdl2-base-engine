#include "sxFont.h"

sxFont::sxFont()
{
  size = 10;
  fontColor.r = fontColor.g = fontColor.b = 0;
  fontColor.a = 255;
  backgroundColor = fontColor;
  backgroundColor.r = 255; // make it visible if it's not set
  font = NULL;
}

sxFont::~sxFont()
{
  destroyFont();
}

void sxFont::setQuality(const sxString& fontQuality)
{
  if(0 == fontQuality.compare("fast")){
    quality = sxFontQualityEnum::FAST;
  }else if(0 == fontQuality.compare("nice")){
    quality = sxFontQualityEnum::NICE;
  }else if(0 == fontQuality.compare("niceWithBox")){
    quality = sxFontQualityEnum::NICE_WITH_BOX;
  }else{
    quality = sxFontQualityEnum::FAST;
    sxLOG_E("Invalid incoming Font Quality: %s", fontQuality.c_str());
  }
}

sxBool sxFont::isFontLoaded()
{
  return (NULL == font) ? SDL_FALSE : SDL_TRUE;
}

ReturnCodes sxFont::loadFont()
{
  if(isFontLoaded()){
    sxLOG(LL_ERROR, "Font already loaded. Create another sxFont to hold an other font.");
    return ReturnCodes::RC_FONT_ALREADY_LOADED;
  }
  font =TTF_OpenFontRW(SDL_RWFromFile(filename.c_str(), "rb"),
                       1,
                       size);
  if(NULL == font){
    sxLOG(LL_ERROR, "Couldn't load font: %s", filename.c_str());
    return ReturnCodes::RC_FILE_READ_ERROR;
  }

  //TODO: make configurable style settings
  //TTF_SetFontStyle(font, TTF_STYLE_BOLD);
  /*
    TTF_STYLE_BOLD
    TTF_STYLE_ITALIC
    TTF_STYLE_UNDERLINE
    TTF_STYLE_STRIKETHROUGH
    */

  return ReturnCodes::RC_SUCCESS;
}

void sxFont::destroyFont()
{
  if(NULL != font){
    TTF_CloseFont(font);
    font = NULL;
  }
}
