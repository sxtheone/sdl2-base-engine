#include "SceneMsgBuilder.h"

SceneMsgBuilder::SceneMsgBuilder()
{
  registerBasicTypes();
}

void SceneMsgBuilder::registerBasicTypes()
{
  // no built-in types yet
}

SceneMsg* SceneMsgBuilder::createInstance(const SceneMsgID& id)
{
  SceneMsg* result = FactoryHandler::createInstance(id);
  if(NULL == result){
    sxLOG_E("Couldn't create Scene Message. ID: %d", id.id);
  }
  return result;
}

SceneMsg* SceneMsgBuilder::createInstance(const sxCharStr idStr)
{
  std::map<SceneMsgID, AbstractFactory<SceneMsg>* >::iterator it;

  for(it = factories.begin(); it != factories.end(); ++it){
    if(it->first.idStr == idStr){
      return it->second->CreateInstance();
    }
  }
  sxLOG_E("Couldn't find SceneMsgID: %s", idStr.c_str());
	return NULL;
}