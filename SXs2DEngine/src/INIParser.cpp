#include "INIParser.h"
#include "LogManager.h"

ReturnCodes INIParser::parseFile(const sxString& filename,
                                 std::map<sxString, std::map<sxString, sxString> >& outData,
                                 std::vector<sxString>& sectionOrder)
{
  sxLOG_T("Opening file: %s", filename.c_str());
  ReturnCodes result;
  iniData = &outData;
  sectionOrderInFile = &sectionOrder;
  result = doParsing(filename);
  if(ReturnCodes::RC_END_OF_FILE == result){
    result = ReturnCodes::RC_SUCCESS;
  }
  sxLOG_T("File closed: %s", filename.c_str());
  return result;
}

ReturnCodes INIParser::doParsing(const sxString& filename)
{
  ReturnCodes result;
  sxFile* file = NULL;
  file = new sxFile(filename, "r");
  if(ReturnCodes::RC_SUCCESS == file->error){
    init();
    result = processFile(file);
  }else{
    result = file->error;
    sxLOG_E("File opening error. File: %s, ReturnCode: %d", filename.c_str(), result);
  }
  delete file;
  return result;
}

void INIParser::init()
{
  currentSection.clear();
  currentField.clear();
  currentLine.clear();
  multiline = SDL_FALSE;
}

ReturnCodes INIParser::processFile(sxFile* file)
{
  sxString rawLine;
  ReturnCodes result = file->read(rawLine);
  while (ReturnCodes::RC_SUCCESS == result){
    currentLine = rawLine;
    if(ReturnCodes::RC_SUCCESS != parseCurrentLine()){
      sxLOG_E("Error in line: %s", rawLine.c_str());
    }
    result = file->read(rawLine);
  }
  removeEmptySectionFromSectionOrder();
  return result;
}

ReturnCodes INIParser::parseCurrentLine()
{
  ReturnCodes result = prepareCurrentLine();
  if(ReturnCodes::RC_LINE_EMPTY == result){
    return ReturnCodes::RC_SUCCESS;
  }
  return processCurrentLine();
}

ReturnCodes INIParser::prepareCurrentLine()
{
  trimLeadingSpaces(currentLine);
  if(SDL_TRUE == currentLineCommentedOutOrEmpty()){
    return ReturnCodes::RC_LINE_EMPTY;
  }
  removeCurrentLineComments();
  trimTrailingSpaces(currentLine);

  return ReturnCodes::RC_SUCCESS;
}

sxBool INIParser::currentLineCommentedOutOrEmpty()
{
  if( currentLine[0] == ';' || 
      //currentLine[0] == '#' || removed because it indicated a comment only at the start of the row
      currentLine[0] == '\n'||
      currentLine[0] == '\0' )
  {
    return SDL_TRUE;
  }
  return SDL_FALSE;
}

void INIParser::removeCurrentLineComments()
{
  currentLine = currentLine.substr(0, currentLine.find_first_of(";",0));
}

ReturnCodes INIParser::processCurrentLine()
{
  if(SDL_TRUE == isCurrentLineASectionHeader()){
    removeEmptySectionFromSectionOrder();
    multiline = SDL_FALSE;
    return processAndSetCurrentSectionName();
  }
  return processValueInCurrentLine();
}

sxBool INIParser::isCurrentLineASectionHeader()
{
  if(currentLine[0] == '['){
    return SDL_TRUE;
  }
  return SDL_FALSE;
}

ReturnCodes INIParser::processAndSetCurrentSectionName()
{ // "[ name ]" -> "name"
  sxString sectionName;
  sxString::size_type end = currentLine.find_first_of("]", 0);
  if(end != sxString::npos){
    sectionName = currentLine.substr(1, end-1);
    trimLeadingSpaces(sectionName);
    trimTrailingSpaces(sectionName);
    currentSection = sectionName;
    sectionOrderInFile->push_back(sectionName);
    return ReturnCodes::RC_SUCCESS;
  }
  return ReturnCodes::RC_FILE_PARSE_ERROR;
}

ReturnCodes INIParser::processValueInCurrentLine()
{
  sxString::size_type equalSign = currentLine.find_first_of("=", 0);
  if(sxString::npos == equalSign){
    return processMultilineValue();
  }
  multiline = SDL_FALSE;
  ReturnCodes result = processSingleValue(equalSign);
  multiline = SDL_TRUE;
  
  return result;
}

ReturnCodes INIParser::processMultilineValue()
{
  sxString value;
  if(multiline && !currentSection.empty() && !currentField.empty()){
    if(currentFieldHasElements()){
      value = "\n";
    }
    value += currentLine;
    return putValueToMap(value);
  }
  return ReturnCodes::RC_FILE_PARSE_ERROR;
}

sxBool INIParser::currentFieldHasElements()
{
  if((*iniData)[currentSection][currentField].empty()){
    return SDL_FALSE;
  }
  return SDL_TRUE;
}

ReturnCodes INIParser::processSingleValue(sxInt32 equalSignPos)
{
  sxString value;
  value = currentLine.substr(equalSignPos+1);
  trimLeadingSpaces(value);
  
  currentField = currentLine.substr(0, equalSignPos);
  trimTrailingSpaces(currentField);
  
  return putValueToMap(value);
}

ReturnCodes INIParser::putValueToMap(const sxString& value)
{
  if(!multiline && !(*iniData)[currentSection][currentField].empty()){
    sxLOG_E("Duplicated value: %s / %s", currentSection.c_str(), currentField.c_str());
    return ReturnCodes::RC_FILE_PARSE_ERROR;
  }else{
    (*iniData)[currentSection][currentField] += value;
  }
  return ReturnCodes::RC_SUCCESS;
}

void INIParser::removeEmptySectionFromSectionOrder()
{
  // if the previous section was empty, it won't be in iniData
  if( !sectionOrderInFile->empty() &&
      iniData->end() == iniData->find(sectionOrderInFile->back()) )
  {
    sectionOrderInFile->pop_back();
  }
}