#include "AnimsFileLoader.h"
#include "SpritesFileLoader.h"
#include "SpriteManager.h"


AnimsFileLoader::AnimsFileLoader()
{
  initialize();
}

void AnimsFileLoader::initialize()
{
  newAnimation = NULL;
  newFrame = NULL;
  prevAnimTime = 0;
}

ReturnCodes AnimsFileLoader::processFile(const sxString& filename)
{
  initialize();
  ReturnCodes result = createReader(filename);
  if(shouldExitFromLoader(result)){
    return result;
  }
  result = processSpriteSourceSection();
  if(ReturnCodes::RC_SUCCESS == result){
    result = processAllAnimations();
  }
  destroyReader();

  return result;
}

ReturnCodes AnimsFileLoader::processSpriteSourceSection()
{
  if(SDL_FALSE == reader->setSection("SpriteSource", SDL_FALSE)){
    sxLOG_T("No SpriteSource section found.");
    return ReturnCodes::RC_SUCCESS; // SpriteSource is not mandatory, sprites may be loaded elsewhere
  }
  sxString spriteFile;
  if(ReturnCodes::RC_SUCCESS != reader->getStr("filename", spriteFile)){
    sxLOG(LL_ERROR, "No filename section found.");
    return ReturnCodes::RC_FILE_PARSE_ERROR;
  }

  SpritesFileLoader sprLoader;//TODO: if loadFileList() used, more than one sprites file is possible.
  // If it's OK, check the possibility of identically named sprites and warn the user if it's not already done!
  return sprLoader.processFile(spriteFile);
}

ReturnCodes AnimsFileLoader::processAllAnimations()
{
  ReturnCodes result;
  currentSectionName = reader->setFirstSection();
  do{
    if(0 != currentSectionName.compare("SpriteSource")){
      result = processOneAnimation();
    }
    if(ReturnCodes::RC_SUCCESS != result){
      sxLOG(LL_ERROR, "Animation: %s is invalid.", currentSectionName.c_str());
    }
    currentSectionName = reader->setNextSection();
  }while(!currentSectionName.empty());

  return result;
}

ReturnCodes AnimsFileLoader::processOneAnimation()
{
  if(ReturnCodes::RC_SUCCESS == processSpritesSection()){
    processAllFrames();
    getSpriteManager->addAnimSprite(currentSectionName, newAnimation);
  }
  return ReturnCodes::RC_SUCCESS;
}

ReturnCodes AnimsFileLoader::processSpritesSection()
{
  std::vector<sxString> spriteNames;
  newAnimation = new sxAnimSprite();
  dealWithAncestorIfPresent();
 
  if(ReturnCodes::RC_SUCCESS != reader->getStrList("sprites", spriteNames)){
    sxLOG_E("sprites field is mandatory but missing! Section: '%s'",
      reader->getCurrentSectionName().c_str());
    return ReturnCodes::RC_FILE_PARSE_ERROR;
  }
  addSpritesToAnimation(spriteNames);

  return ReturnCodes::RC_SUCCESS;
}

ReturnCodes AnimsFileLoader::addSpritesToAnimation(std::vector<sxString>& spriteNames)
{
  sxSprite* spriteForAnim;
  for(sxUInt32 i=0; i<spriteNames.size(); ++i){
    spriteForAnim = getSpriteManager->getSprite(spriteNames[i]);
    if(NULL == spriteForAnim){
      sxLOG_E("Sprite can't be found: %s", spriteNames[i].c_str());
      delete newAnimation;
      return ReturnCodes::RC_FILE_PARSE_ERROR;
    }
    newAnimation->addSprite(spriteForAnim);
  }
  return ReturnCodes::RC_SUCCESS;
}

void AnimsFileLoader::dealWithAncestorIfPresent()
{
  ancestorName.clear();
  substractAncestorFromCurrentSectionName();
  if(!ancestorName.empty()){
    sxLOG(LL_ERROR, "Inheritance for .anims files is not implemented because it's working "
      "is not straightforward. Should it add to the ancestor's data or replace it?");
  }
}
//TODO: frames section can become a new class if more features put into it (like 1 .. 5)
void AnimsFileLoader::processAllFrames()
{
  std::vector<sxString> frameList;
  prevAnimTime = 0;
  sxUInt32 nextAnimFrame = 1;

  reader->getStrList("frames", frameList, SDL_FALSE);
  for(sxUInt32 i=0; i<frameList.size(); ++i){
    currentLine = frameList[i];
    if(sxString::npos != currentLine.find("goto")){
      processGoto();
    }else if(sxString::npos != currentLine.find("colorTint")){
      processColorTint();
    }else{
      processAnimFrame(nextAnimFrame);
      ++nextAnimFrame;
    }
  }
}

void AnimsFileLoader::processAnimFrame(const sxUInt32 nextAnimFrame)
{
  newFrame = new sxAnimFrame();
  newFrame->spriteNumber = atoi(currentLine.substr(0, currentLine.find(' ')).c_str());
  processFrameTime();
  newFrame->nextAnimFrame = nextAnimFrame;
  newAnimation->addFrame(newFrame);
}

void AnimsFileLoader::processFrameTime()
{
  if(sxString::npos != currentLine.find('t')){
    newFrame->time = atoi(currentLine.substr(currentLine.find('t')+1).c_str());
    prevAnimTime = newFrame->time;
  }else{
    setFrameTimeToPrevTime();
  }
}

void AnimsFileLoader::setFrameTimeToPrevTime()
{
  if(0 == prevAnimTime){
    sxLOG(LL_ERROR, "First animation frame should have time! Animation: %s",
      currentSectionName.c_str());
    prevAnimTime = 1;
  }
  newFrame->time = prevAnimTime;
}

void AnimsFileLoader::processGoto()
{
  if(NULL == newFrame){
    sxLOG(LL_ERROR, "GOTO used without having any data. "
      "GOTO can't be the first anim frame! Animation: %s", currentSectionName.c_str());
    return;
  }
  newFrame->nextAnimFrame = atoi(currentLine.substr(currentLine.find("goto")+4).c_str());
}

void AnimsFileLoader::processColorTint()
{
  sxString wholeLine = currentLine.substr(currentLine.find("colorTint")+10);

  std::vector<sxString> colorStr;
  sxColor color;
  reader->cutString(wholeLine.substr(0, wholeLine.find_first_of(':')), ' ', colorStr);
  reader->convertStrToColor(colorStr, color);
  newAnimation->setTargetTintColor(color);

  wholeLine = wholeLine.substr(wholeLine.find_first_of(':')+1);
  trimLeadingSpaces(wholeLine); //NOTE: if more data comes after this, trim trailing spaces also
  
  std::vector<sxInt32> times;
  reader->stringToIntList(wholeLine, ' ', times);
  if(4 != times.size()){
    sxLOG_E("colorTint times value parsing error. Found %d instead of 4.", times.size());
    return;
  }
  newAnimation->setTintTimes(times[0], times[1], times[2], times[3]);
}
