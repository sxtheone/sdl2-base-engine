#include "FontsFileLoader.h"
#include "FontManager.h"


ReturnCodes FontsFileLoader::processFile(const sxString& filename)
{
  ReturnCodes result = createReader(filename);
  if(!shouldExitFromLoader(result)){
    processAllFontSection();
    destroyReader();
  }
  return result;
}

void FontsFileLoader::processAllFontSection()
{
  currentSectionName = reader->setFirstSection();
  do{
    processOneFontSection();
    currentSectionName = reader->setNextSection();
  }while(SDL_FALSE == thisIsTheSystemFontFile && !currentSectionName.empty());
}

void FontsFileLoader::processOneFontSection()
{
  dealWithAncestorIfPresent();
  if(SDL_FALSE == thisIsTheSystemFontFile){
    processNormalFont();
  }else{
    processSystemFont();
  }
}

void FontsFileLoader::processNormalFont()
{
  //TODO: almost the same as processSystemFont
  sxFont font;
  processFont(&font);
  getFontManager->addFont(currentSectionName, font);
}

void FontsFileLoader::processSystemFont()
{
  sxFont font;
  processFont(&font);
  getFontManager->setSystemFont(font);
}

void FontsFileLoader::dealWithAncestorIfPresent()
{
  ancestorName.clear();
  substractAncestorFromCurrentSectionName();
  if(!ancestorName.empty()){
    sxLOG(LL_ERROR, "Inheritance is not implemented for fonts.");
  }
}

void FontsFileLoader::processFont(sxFont* outFont)
{
  sxString tempStr;
  reader->getFileNameWithPath("filename", outFont->filename);
  reader->getInteger("size", outFont->size);
  reader->getColor("font color", outFont->fontColor);
  reader->getColor("background color", outFont->backgroundColor, SDL_FALSE);
  if(ReturnCodes::RC_SUCCESS == reader->getStr("quality", tempStr, SDL_FALSE)){
    outFont->setQuality(tempStr);
  }
}

