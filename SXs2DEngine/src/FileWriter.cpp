#include "FileWriter.h"


FileWriter::~FileWriter()
{
  closeFile();
}

ReturnCodes FileWriter::createFileForWriting(const sxString& dir, const sxString& filename)
{
  return getFileForWriting(dir, filename, "w");
}

ReturnCodes FileWriter::openExistingFileForWriting(const sxString& dir, const sxString& filename)
{
  return getFileForWriting(dir, filename, "a");
}

ReturnCodes FileWriter::getFileForWriting(const sxString& dir, const sxString& filename,
                                          const sxString openMode)
{
  closeFile();

  this->filename = filename;
  sxString filenameWithPath;
  SystemIO::createDirInBasePath(dir, filenameWithPath);

  filenameWithPath += filename;
  file = new sxFile(filenameWithPath, openMode);

  if(ReturnCodes::RC_SUCCESS != file->error){
    sxLOG_E("Can't save file: %s, error code: %d", filenameWithPath.c_str(), file->error);
  }

  return file->error;
}

ReturnCodes FileWriter::saveFile(const sxBool clearDataFromMemAfterSaving)
{
  if( NULL == file || 
      ReturnCodes::RC_SUCCESS != file->error )
  {
    sxLOG_E("File to save not set: %s", filename.c_str());
    return ReturnCodes::RC_FILE_CANT_BE_OPENED;
  }
  file->write(getData());
  if(SDL_TRUE == clearDataFromMemAfterSaving) clearData();

  return ReturnCodes::RC_SUCCESS;
}

void FileWriter::closeFile()
{
  if(NULL != file){
    file->closeFile();
    delete file;
    file = NULL;

    filename.clear();
    iniData.clear();
    sectionOrderInFile.clear();

  sxString currentSection;
  }
}

sxString FileWriter::getData()
{
  sxString result;
  std::vector<sxString>::iterator sectionIt;
  std::map<sxString, sxString>::iterator fieldIt;
  
  sectionIt = sectionOrderInFile.begin();
  while(sectionOrderInFile.end() != sectionIt){
    if(iniData.end() != iniData.find(*sectionIt)){
      result += "[" + *sectionIt + "]\n";
      fieldIt = iniData[*sectionIt].begin();
      while(iniData[*sectionIt].end() != fieldIt){
        sxString fieldData = fieldIt->first + " = " + fieldIt->second;
        result += fieldData;
        ++fieldIt;
      }
      result += "\n";
    }
    ++sectionIt;
  }
  return result;
}

void FileWriter::createSection(const sxString& name)
{
  currentSection = name;
  sectionOrderInFile.push_back(name);
}

ReturnCodes FileWriter::setSection(const sxString& name)
{
  if(iniData.end() != iniData.find(name)){
    currentSection = name;
    return ReturnCodes::RC_SUCCESS;
  }
  if(currentSection == name) return ReturnCodes::RC_SUCCESS;

  return ReturnCodes::RC_SECTION_NOT_FOUND;
}

void FileWriter::addBool(const sxString fieldName, const sxBool value)
{
   iniData[currentSection][fieldName] = (SDL_TRUE == value) ? "true\n" : "false\n";
}

void FileWriter::addInt32(const sxString fieldName, const sxInt32 value)
{
  iniData[currentSection][fieldName] = strFormatter("%d\n", value);
}

void FileWriter::addInt64(const sxString fieldName, const sxInt64 value)
{
  iniData[currentSection][fieldName] = strFormatter("%d\n", value);
}

void FileWriter::addDouble(const sxString fieldName, const sxDouble value)
{
  iniData[currentSection][fieldName] = strFormatter("%f\n", value);
}

void FileWriter::addString(const sxString fieldName, const sxString& value)
{
  iniData[currentSection][fieldName] = value + "\n";
}

void FileWriter::addMatrix2D(const sxString fieldName, const sxMatrix2D& value)
{
  //INIReader reads the first value as row and the second as column
  iniData[currentSection][fieldName] = strFormatter("%d, %d\n", value.row, value.col);
}
