#include "SceneCollisionPropsProcessor.h"
#include "ObjectManager.h"

SceneCollisionPropsProcessor* SceneCollisionPropsProcessor::collisionPropsProcessor = NULL;


SceneCollisionPropsProcessor::SceneCollisionPropsProcessor()
{
  sxLOG(LL_TRACE, "Created.");
}

SceneCollisionPropsProcessor::~SceneCollisionPropsProcessor()
{
  clearCollisionProperties();
  sxLOG(LL_TRACE, "Deleted.");
}

void SceneCollisionPropsProcessor::createManager()
{
  if(NULL != collisionPropsProcessor){
    delete collisionPropsProcessor;
  }
  collisionPropsProcessor = new SceneCollisionPropsProcessor();
}

SceneCollisionPropsProcessor* SceneCollisionPropsProcessor::getInstance()
{
  if(NULL == collisionPropsProcessor){
    sxLOG(LL_ERROR, "collision Detector is not yet created.");
    return NULL;
  }
  return collisionPropsProcessor;
}

void SceneCollisionPropsProcessor::destroyManager()
{
  sxLOG(LL_TRACE, "Destroying...");
  if(NULL == collisionPropsProcessor){
    sxLOG(LL_ERROR, "Instance is NULL, but how?");
    return;
  }
  delete collisionPropsProcessor;
  collisionPropsProcessor = NULL;
  sxLOG(LL_TRACE, "destroyed.");
}

void SceneCollisionPropsProcessor::addToSceneCollisionMap(const sxCharStr& collisionID,
                                               const std::vector<sxCharStr>& collidesWith)
{
  std::pair<sxCharStr, std::vector<sxCharStr> > pair(collisionID, collidesWith);
  if(false == sceneSettings.rawMap.insert(pair).second){
    // no error log because this can happen when adding an Act to 
    // the top what has common items with the older Act
    //sxLOG_E("Object is already present in the collision map: %s", collisionID.c_str());
  }
}

void SceneCollisionPropsProcessor::processCollisionSettings()
{
  warningIfInvalidGroupNameInCollisionMap(sceneSettings.rawMap);
  if(SDL_TRUE == tooManyCollisionGroups(sceneSettings.rawMap.size())){
    return;
  }
  fillCollisionIDFields();
  fillCollidesWithFields();
}

sxBool SceneCollisionPropsProcessor::warningIfInvalidGroupNameInCollisionMap(std::map<sxCharStr, 
                                                          std::vector<sxCharStr> >& rawMap)
{
  // the map.second vector should only include names what are in the map.first 
  sxBool result = SDL_FALSE;
  rawIt = rawMap.begin();
  while(rawMap.end() != rawIt){
    for(sxUInt32 i=0; i<rawIt->second.size(); ++i){
      if(rawMap.end() == rawMap.find(rawIt->second[i])){
        sxLOG_E("Item in CollidesWith vector does not exists. Item: %s, collisionID: %s",
          rawIt->second[i].c_str(), rawIt->first.c_str());
        result = SDL_TRUE;
        rawIt->second.erase(rawIt->second.begin()+i);
        --i;
      }
    }
    ++rawIt;
  }
  return result;
}

sxBool SceneCollisionPropsProcessor::tooManyCollisionGroups(sxUInt32 vectorSize)
{
  CollisionProps testProps;
  if(sizeof(testProps.collisionID)*8 < vectorSize){
    sxLOG_E("Too many collision groups: %d Collision is disabled.", vectorSize);
    return SDL_TRUE;
  }
  return SDL_FALSE;
}

void SceneCollisionPropsProcessor::fillCollisionIDFields()
{
  rawIt = sceneSettings.rawMap.begin();
  if(0 == sceneSettings.nextCollisionID.collisionID){
    sceneSettings.nextCollisionID.collisionID = 1;
  }

  while(sceneSettings.rawMap.end() != rawIt){
    if(sceneSettings.finalMap.end() == sceneSettings.finalMap.find(rawIt->first)){
      sceneSettings.finalMap[rawIt->first].collisionID = sceneSettings.nextCollisionID.collisionID;
      sceneSettings.nextCollisionID.collisionID = sceneSettings.nextCollisionID.collisionID << 1;
    }
    ++rawIt;
  }
}

void SceneCollisionPropsProcessor::fillCollidesWithFields()
{
  // fill the CollidesWith fields with the already calculated collisionIDs
  rawIt = sceneSettings.rawMap.begin();
  while(sceneSettings.rawMap.end() != rawIt){
    for(sxUInt32 i=0; i<rawIt->second.size(); ++i){
      sceneSettings.finalMap[rawIt->first].collidesWith |= 
                                       sceneSettings.finalMap[rawIt->second[i]].collisionID;
    }
    ++rawIt;
  }
}

CollisionProps SceneCollisionPropsProcessor::getCollisionPropsForScene(const sxCharStr& sceneName)
{
  if(sceneSettings.finalMap.empty()){
    sxLOG_W("finalMap is empty. Did you forget to call processScenesCollisionSettings?");
    return CollisionProps();
  }
  propsIt = sceneSettings.finalMap.find(sceneName);
  if(sceneSettings.finalMap.end() == propsIt){
    sxLOG_E("No CollisionProps found for scene: %s", sceneName.c_str());
    return CollisionProps();
  }
  return propsIt->second;  
}

void SceneCollisionPropsProcessor::clearCollisionProperties()
{
  if(NULL != collisionPropsProcessor){
    sceneSettings.init();
  }
}
