#include "GfxPrimitives.h"

/*!
\brief Draw pixel with blending enabled if a<255.

\param renderer: The renderer to draw on.
\param coords: X (horizontal) coordinate of the pixel.
               Y (vertical) coordinate of the pixel.
\param color: r The red color value of the pixel to draw. 
              g The green color value of the pixel to draw.
              b The blue color value of the pixel to draw.
              a The alpha value of the pixel to draw.
\returns Returns RC_SUCCESS on success, RC_SERIOUS_ERROR on failure.
*/
ReturnCodes GfxPrimitives::pixelRGBA(SDL_Renderer *renderer, sxVec2Int& coords,
                                     sxColor& color)
{
	int result = 0;
	result |= SDL_SetRenderDrawBlendMode(renderer, 
                    (color.a == 255) ? SDL_BLENDMODE_NONE : SDL_BLENDMODE_BLEND);
	result |= SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, color.a);
	result |= SDL_RenderDrawPoint(renderer, coords.x, coords.y);
	if(0 == result){
    return ReturnCodes::RC_SUCCESS;
  }
  return ReturnCodes::RC_SERIOUS_ERROR;
}

/*!
\brief Draw horizontal line with blending.

\param renderer The renderer to draw on.
\param startCoord: X coordinate of the first point (i.e. left) of the line.
                   Y coordinate of the line.
\param endX: X coordinate of the second point (i.e. right) of the line.
\param color: r The red value of the line to draw. 
              g The green value of the line to draw. 
              b The blue value of the line to draw. 
              a The alpha value of the line to draw. 

\returns Returns RC_SUCCESS on success, RC_SERIOUS_ERROR on failure.
*/
ReturnCodes GfxPrimitives::hlineRGBA(SDL_Renderer *renderer, sxVec2Int& startCoord,
                                     sxInt16 endX, sxColor& color)
{
	int result = 0;
	result |= SDL_SetRenderDrawBlendMode(renderer, 
                            (color.a == 255) ? SDL_BLENDMODE_NONE : SDL_BLENDMODE_BLEND);
	result |= SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, color.a);
	result |= SDL_RenderDrawLine(renderer, startCoord.x, startCoord.y, endX, startCoord.y);

  if(0 == result){
    return ReturnCodes::RC_SUCCESS;
  }
  return ReturnCodes::RC_SERIOUS_ERROR;
}

/*!
\brief Draw vertical line with blending.

\param renderer The renderer to draw on.
\param startCoord: X coordinate of the points of the line.
                   Y coordinate of the first point (i.e. top) of the line.
\param endY: Y coordinate of the second point (i.e. bottom) of the line.
\param color: r The red value of the line to draw. 
              g The green value of the line to draw. 
              b The blue value of the line to draw. 
              a The alpha value of the line to draw.  

\returns Returns RC_SUCCESS on success, RC_SERIOUS_ERROR on failure.
*/
ReturnCodes GfxPrimitives::vlineRGBA(SDL_Renderer *renderer, sxVec2Int& startCoord,
                                     sxInt16 endY, sxColor& color)
{
	sxInt32 result = 0;
	result |= SDL_SetRenderDrawBlendMode(renderer, 
                            (color.a == 255) ? SDL_BLENDMODE_NONE : SDL_BLENDMODE_BLEND);
	result |= SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, color.a);
	result |= SDL_RenderDrawLine(renderer, startCoord.x, startCoord.y, startCoord.x, endY);
  if(0 == result){
    return ReturnCodes::RC_SUCCESS;
  }
  return ReturnCodes::RC_SERIOUS_ERROR;
}

/*!
\brief Draw rectangle with blending.

\param renderer The renderer to draw on.
\param x1 X coordinate of the first point (i.e. top right) of the rectangle.
\param y1 Y coordinate of the first point (i.e. top right) of the rectangle.
\param x2 X coordinate of the second point (i.e. bottom left) of the rectangle.
\param y2 Y coordinate of the second point (i.e. bottom left) of the rectangle.
\param r The red value of the rectangle to draw. 
\param g The green value of the rectangle to draw. 
\param b The blue value of the rectangle to draw. 
\param a The alpha value of the rectangle to draw. 

\returns Returns 0 on success, -1 on failure.
*/
ReturnCodes GfxPrimitives::rectangleRGBA(SDL_Renderer *renderer, sxQuadInt& rect, sxColor& color)
{
	ReturnCodes result;
	Sint16 tmp;

	/*
	* Test for special cases of straight lines or single point 
	*/
  if (rect.topLeft.x == rect.bottomRight.x) {
		if (rect.topLeft.y == rect.bottomRight.y) {
			return pixelRGBA(renderer, rect.topLeft, color);
		} else {
			return vlineRGBA(renderer, rect.topLeft, rect.bottomRight.y, color);
		}
	} else {
		if (rect.topLeft.y == rect.bottomRight.y) {
      sxVec2Int coords;
      coords.x = rect.topLeft.x;
      coords.y = rect.bottomRight.x;
			return hlineRGBA(renderer, coords, rect.topLeft.y, color);
		}
	}

	/*
	* Swap rect.topLeft.x, rect.bottomRight.x if required 
	*/
	if (rect.topLeft.x > rect.bottomRight.x) {
		tmp = rect.topLeft.x;
		rect.topLeft.x = rect.bottomRight.x;
		rect.bottomRight.x = tmp;
	}

	/*
	* Swap rect.topLeft.y, rect.bottomRight.y if required 
	*/
	if (rect.topLeft.y > rect.bottomRight.y) {
		tmp = rect.topLeft.y;
		rect.topLeft.y = rect.bottomRight.y;
		rect.bottomRight.y = tmp;
	}

	/* 
	* Create destination rect
	*/	
  sxRect outRect;
	outRect.x = rect.topLeft.x;
	outRect.y = rect.topLeft.y;
	outRect.w = rect.bottomRight.x - rect.topLeft.x;
	outRect.h = rect.bottomRight.y - rect.topLeft.y;
	
	/*
	* Draw
	*/
	sxInt32 SDLresult = 0;
	SDLresult |= SDL_SetRenderDrawBlendMode(renderer, 
                       (color.a == 255) ? SDL_BLENDMODE_NONE : SDL_BLENDMODE_BLEND);
	SDLresult |= SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, color.a);	
	SDLresult |= SDL_RenderDrawRect(renderer, &outRect);
  if(0 != SDLresult){
    result = ReturnCodes::RC_SERIOUS_ERROR;
  }
	return result;
}


/*!
\brief Draw box (filled rectangle) with blending.

\param renderer The renderer to draw on.
\param x1 X coordinate of the first point (i.e. top right) of the box.
\param y1 Y coordinate of the first point (i.e. top right) of the box.
\param x2 X coordinate of the second point (i.e. bottom left) of the box.
\param y2 Y coordinate of the second point (i.e. bottom left) of the box.
\param r The red value of the box to draw. 
\param g The green value of the box to draw. 
\param b The blue value of the box to draw. 
\param a The alpha value of the box to draw.

\returns Returns 0 on success, -1 on failure.
*/
ReturnCodes GfxPrimitives::boxRGBA(SDL_Renderer * renderer, sxQuadInt& rect, sxColor& color)
{
  ReturnCodes result;
	Sint16 tmp;

	/*
	* Test for special cases of straight lines or single point 
	*/
	if (rect.topLeft.x == rect.bottomRight.x) {
		if (rect.topLeft.y == rect.bottomRight.y) {
			return (pixelRGBA(renderer, rect.topLeft, color));
		} else {
			return (vlineRGBA(renderer, rect.topLeft, rect.bottomRight.y, color));
		}
	} else {
		if (rect.topLeft.y == rect.bottomRight.y) {
      sxVec2Int coords;
      coords.x = rect.topLeft.x;
      coords.y = rect.bottomRight.x;
			return (hlineRGBA(renderer, coords, rect.topLeft.y, color));
		}
	}

	/*
	* Swap rect.topLeft.x, rect.bottomRight.x if required 
	*/
	if (rect.topLeft.x > rect.bottomRight.x) {
		tmp = rect.topLeft.x;
		rect.topLeft.x = rect.bottomRight.x;
		rect.bottomRight.x = tmp;
	}

	/*
	* Swap rect.topLeft.y, rect.bottomRight.y if required 
	*/
	if (rect.topLeft.y > rect.bottomRight.y) {
		tmp = rect.topLeft.y;
		rect.topLeft.y = rect.bottomRight.y;
		rect.bottomRight.y = tmp;
	}

	/* 
	* Create destination rect
	*/	
  sxRect outRect;
	outRect.x = rect.topLeft.x;
	outRect.y = rect.topLeft.y;
	outRect.w = rect.bottomRight.x - rect.topLeft.x;
	outRect.h = rect.bottomRight.y - rect.topLeft.y;
	
	/*
	* Draw
	*/
	int SDLresult = 0;
	SDLresult |= SDL_SetRenderDrawBlendMode(renderer, (color.a == 255) ? SDL_BLENDMODE_NONE : SDL_BLENDMODE_BLEND);
	SDLresult |= SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, color.a);	
	SDLresult |= SDL_RenderFillRect(renderer, &outRect);

  if(0 != SDLresult){
    result = ReturnCodes::RC_SERIOUS_ERROR;
  }
	return result;
}