#include "MainLoop.h"
#include "SceneMsgTypes.hpp"
#include "SceneMsgSendString.hpp"
#include "SceneMsgCloseScene.hpp"
#include "MenuButton.h"
#include "ActManager.h"
#include "ObjectManager.h"
#include "FontManager.h"
#include "SceneCollisionPropsProcessor.h"
#include "ObjectCollisionPropsProcessor.h"
#include "SpriteManager.h"
#include "AudioManager.h"
#include "EventManager.h"
#include "LoadedFiles.h"
#include "OrderedDrawer.h"
#include "Profiler.h"

MainLoop::MainLoop()
{
  mainWindow = NULL;
  mainRenderer = NULL;
  renderTexture = NULL;
  fpsManager = NULL;
#ifdef _DEBUG
  checkObjectSizes();
#endif
}
/// just to indicate when new fields introduced without actualizing ::assign()
void MainLoop::checkObjectSizes()
{
  sxUInt32 desiredSize, realSize;
  {
    BasicObject basic;
    desiredSize = 112;
    realSize = sizeof(basic);
    if(desiredSize != realSize){
      sxLOG_E("Object size is: %d instead of: %d. Update the %s::assign() "
        "funcion and the expected object size if you modified the %s!",
        sizeof(basic), desiredSize, basic.type.c_str(), basic.type.c_str());
    }
  }
  {
    AnimObject anim;
    desiredSize = 200;
    realSize = sizeof(anim);
    if(desiredSize != realSize){
      sxLOG_E("Object size is: %d instead of: %d. Update the %s::assign() "
        "funcion and the expected object size if you modified the %s!",
        sizeof(anim), desiredSize, anim.type.c_str(), anim.type.c_str());
    }
  }
  {
    ButtonObject button;
    desiredSize = 272;
    realSize = sizeof(button);
    if(desiredSize != realSize){
      sxLOG_E("Object size is: %d instead of: %d. Update the %s::assign() "
        "funcion and the expected object size if you modified the %s!",
        sizeof(button), desiredSize, button.type.c_str(), button.type.c_str());
    }
  }
  {
    MenuButton button;
    desiredSize = 336;
    realSize = sizeof(button);
    if(desiredSize != realSize){
      sxLOG_E("Object size is: %d instead of: %d. Update the %s::assign() "
        "funcion and the expected object size if you modified the %s!",
        sizeof(button), desiredSize, button.type.c_str(), button.type.c_str());
    }
  }
}

MainLoop::~MainLoop()
{
  SDL_DestroyTexture(renderTexture);

  delete fpsManager;
  getOrderedDrawer->destroyManager();
  getActManager->destroyManager();
  getSceneCollisionPropsProcessor->destroyManager();
  getObjectCollisionPropsProcessor->destroyManager();
  getFontManager->destroyManager();
  getMainConfig->destroyManager();
  getAudioManager->destroyManager();
  getObjectManager->destroyManager();
  getSpriteManager->destroyManager();
  getEventManager->destroyManager();

  if(TTF_WasInit()){
    TTF_Quit();
  }

  deleteRenderer();
  deleteMainWindow();

  getLoadedFiles->destroy();
  getProfiler->destroyProfiler();

  SDL_Quit();
}

ReturnCodes MainLoop::init()
{
  ReturnCodes result;
#ifdef DEVELOPER_HELPERS
  sxLOG(LL_TRACE, "Developer Helper Tools ON");
#else
  sxLOG(LL_TRACE, "Developer Helper Tools OFF");
#endif
  LoadedFiles::init();
  EventManager::createManager();
  SpriteManager::createManager();
  ObjectManager::createManager();
  AudioManager::createManager();
  FontManager::createManager();
  MainConfig::createManager(MAIN_CONFIG_FILENAME);
  SceneCollisionPropsProcessor::createManager();
  ObjectCollisionPropsProcessor::createManager();
  ActManager::createManager();
  OrderedDrawer::createManager();

  result = initWindowAndRenderer();
  if(ReturnCodes::RC_SUCCESS != result){
    return result;
  }

  getFontManager->setRenderer(mainRenderer);

  result = getAudioManager->initialize();
  if(ReturnCodes::RC_SUCCESS != result){
    return result;
  }
  initTTFPlugin();
  getSpriteManager->initialize(mainRenderer);
  fpsManager = new SDLGFX_FramerateManager();

  registerSceneMsgs();

  Profiler::createProfiler();
  getProfiler->initialize(mainRenderer);
  getProfiler->showOnScreen(SDL_TRUE);
  getProfiler->setWriteLogs(SDL_FALSE);

  createRenderTexture();

  // MainLoop::stepAnimation uses these
  remainingTicks = 0;
  maxAllowedFrameStep = static_cast<sxUInt32>(getMainConfig->getDesiredFrameStep() * 2);

  return ReturnCodes::RC_SUCCESS;
}

void MainLoop::createRenderTexture()
{
  if(NULL != renderTexture) SDL_DestroyTexture(renderTexture);

  renderTexture = SDL_CreateTexture(mainRenderer, 
                                    SDL_PIXELFORMAT_RGBA8888,
                                    SDL_TEXTUREACCESS_TARGET,
                                    logicalSize.w, 
                                    logicalSize.h);
  SDL_SetTextureBlendMode(renderTexture, SDL_BLENDMODE_BLEND);
}

void MainLoop::registerSceneMsgs()
{
  getActManager->registerSceneMsgType(SceneMsgTypes::SceneMsgSendString,
                                      new SceneMsgFactory<SceneMsgSendString>());
  getActManager->registerSceneMsgType(SceneMsgTypes::SceneMsgCloseScene,
                                      new SceneMsgFactory<SceneMsgCloseScene>());
}

void MainLoop::initTTFPlugin()
{
  if(TTF_WasInit()){
    sxLOG(LL_WARNING, "TTF Plugin was already initialized.");
    TTF_Quit();
  }
  if(0 != TTF_Init()){
    sxLOG(LL_ERROR, "TTF Plugin can't be initialized. Error: %s", TTF_GetError());
  }
}

sxUInt32 MainLoop::setupWindowFlags()
{
  sxUInt32 windowFlags = SDL_WINDOW_INPUT_FOCUS;
  windowFlags = windowFlags | getMainConfig->getWindowType() | SDL_WINDOW_SHOWN;

  return windowFlags;
}

sxBool MainLoop::createGameWindow()
{
  deleteMainWindow();
  sxUInt32 windowFlags = setupWindowFlags();
  mainWindow = SDL_CreateWindow(getMainConfig->getWindowName().c_str(),
                                SDL_WINDOWPOS_CENTERED,
                                SDL_WINDOWPOS_CENTERED,
                                getMainConfig->getWindowScreenSize().x,
                                getMainConfig->getWindowScreenSize().y,
                                windowFlags);
  if(NULL == mainWindow){
    sxLOG(LL_ERROR, "Can't create Main Window.");
    return SDL_FALSE;
  }
#ifdef _WIN32
  // seems like SDL_GetWindowSurface() screws up sg. on Android. A renderer gets created here but only on
  // Android. If I call this after I created the renderer, it screws up the renderer..
  //UPDATE: window->surface_valid is NULL and window->surface is also NULL
  // the SDL_CreateWindowFramebuffer() is most probably called. The problem is there somewhere..
  getMainConfig->setWindowPixelFormatVariable(SDL_GetWindowSurface(mainWindow)->format);
#endif
  setScreenSizeConfig();
  return SDL_TRUE;
}

sxBool MainLoop::createRenderer()
{
  sxLOG_T("started.");
  deleteRenderer();
  mainRenderer = SDL_CreateRenderer(mainWindow,
                                    -1,
                                    SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
  if(NULL == mainRenderer){
    sxLOG_E("Can't create Renderer. SDL Error: %s", SDL_GetError());
	  return SDL_FALSE;
	}
  SDL_RendererInfo rendererInfo;
  SDL_GetRendererInfo(mainRenderer, &rendererInfo);
  sxLOG_T("Renderer created. Renderer flags: %d", rendererInfo.flags);
  return SDL_TRUE;
}

ReturnCodes MainLoop::initWindowAndRenderer()
{
  if(SDL_TRUE != initSDL()) return ReturnCodes::RC_SDL_INIT_FAILED;
  if(SDL_TRUE != createGameWindow()) return ReturnCodes::RC_CANT_CREATE_MAIN_WINDOW;
  if(SDL_TRUE != createRenderer()) return ReturnCodes::RC_CANT_CREATE_RENDERER;

  setRenderLogicalSize();
  SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, getMainConfig->getSpriteScalingMethod().c_str());
  SDL_SetHint(SDL_HINT_ACCELEROMETER_AS_JOYSTICK, "0"); // accelerometer usage can be switched inside EventManager

  sxColor backColor = getMainConfig->getBackroundColor();
  SDL_SetRenderDrawColor(mainRenderer, backColor.r, backColor.g, backColor.b, backColor.a);

  logicalSize.x = logicalSize.y = 0;
  logicalSize.w = getMainConfig->getWindowLogicalSize().x;
  logicalSize.h = getMainConfig->getWindowLogicalSize().y;
  screenSize.x = screenSize.y = 0;
  screenSize.w = getMainConfig->getWindowScreenSize().x;
  screenSize.h = getMainConfig->getWindowScreenSize().y;
  
  return ReturnCodes::RC_SUCCESS;
}

sxBool MainLoop::initSDL()
{
 	if(0 != SDL_Init(SDL_INIT_EVERYTHING)){
    sxLOG(LL_TRACE, "SDL_Init Error: ", SDL_GetError());
    return SDL_FALSE;
	}

  return SDL_TRUE;
}

void MainLoop::setRenderLogicalSize()
{
  //TODO: rework this..
  if(SDL_TRUE == getMainConfig->isLetterboxed()){
    SDL_RenderSetLogicalSize(mainRenderer, getMainConfig->getWindowLogicalSize().x,
                             getMainConfig->getWindowLogicalSize().y);
  }else{
//    SDL_RenderSetScale(mainRenderer, mousePositionCorrector.x,
//                                     mousePositionCorrector.y);
  }
  sxLOG_T("Window size is: %dx%d", getMainConfig->getWindowScreenSize().x, 
                                   getMainConfig->getWindowScreenSize().y);

  sxVec2Real mousePositionCorrector = calculateMousePosCorrector();
  getEventManager->setMousePositionCorrector(mousePositionCorrector);
}

sxVec2Real MainLoop::calculateMousePosCorrector()
{
  sxVec2Real mousePositionCorrector;
  mousePositionCorrector.x = static_cast<sxFloat>(getMainConfig->getWindowScreenSize().x);
  mousePositionCorrector.x /= static_cast<sxFloat>(getMainConfig->getWindowLogicalSize().x);
  mousePositionCorrector.y = static_cast<sxFloat>(getMainConfig->getWindowScreenSize().y);
  mousePositionCorrector.y /= static_cast<sxFloat>(getMainConfig->getWindowLogicalSize().y);
  return mousePositionCorrector;
}

void MainLoop::setScreenSizeConfig()
{
  // if the device not supporting the set screen size, correct the config value
  sxInt32 w,h;
  SDL_GetWindowSize(mainWindow, &w, &h);
  getMainConfig->setWindowScreenSize(sxVec2Int(w,h));
}

void MainLoop::loadStartingAct()
{
  loadAct(getMainConfig->getStartingActFilename());
}

void MainLoop::loadAct(const sxString& actName)
{
  sxLOG_T("=== Loading Act: %s ===", actName.c_str());
  getActManager->loadAct(actName, mainRenderer);
  handleWindowOpacity();
  sxLOG_T("=== FINISHED Loading Act: %s ===", actName.c_str());
}

void MainLoop::handleWindowEvents()
{
  sxEvent event;
  getEventManager->getNextEvent(sxEventTypes::WINDOW_EVENT, event);
  while(sxEventTypes::WINDOW_EVENT == event.type()){
    sxWindowEvent& window = event.window();
    switch (window.event){
      case SDL_WINDOWEVENT_SHOWN: sxLOG_T("Window %d shown", window.windowID); break;
      case SDL_WINDOWEVENT_HIDDEN: sxLOG_T("Window %d hidden", window.windowID); break;
      case SDL_WINDOWEVENT_EXPOSED: sxLOG_T("Window %d exposed", window.windowID); break;
      case SDL_WINDOWEVENT_MOVED: 
        sxLOG_T("Window %d moved to %d,%d", window.windowID, window.data1,
                                            window.data2);
      break;
      case SDL_WINDOWEVENT_RESIZED:
        sxLOG_T("Window %d resized to %dx%d", window.windowID, window.data1,
                                              window.data2);
      break;
      case SDL_WINDOWEVENT_SIZE_CHANGED:
        sxLOG_T("Window %d size changed to %dx%d",
                window.windowID, window.data1,
                window.data2);
      break;
      case SDL_WINDOWEVENT_MINIMIZED: sxLOG_T("Window %d minimized", window.windowID); break;
      case SDL_WINDOWEVENT_MAXIMIZED: sxLOG_T("Window %d maximized", window.windowID); break;
      case SDL_WINDOWEVENT_RESTORED: sxLOG_T("Window %d restored", window.windowID); break;
      case SDL_WINDOWEVENT_ENTER: sxLOG_T("Mouse entered Window %d", window.windowID); break;
      case SDL_WINDOWEVENT_LEAVE: sxLOG_T("Mouse left Window %d", window.windowID); break;
      case SDL_WINDOWEVENT_FOCUS_GAINED: sxLOG_T("Window %d gained keyboard focus", 
                                                 window.windowID);
      break;
      case SDL_WINDOWEVENT_FOCUS_LOST: sxLOG_T("Window %d lost keyboard focus", window.windowID);
      break;
      case SDL_WINDOWEVENT_CLOSE: sxLOG_T("Window %d closed", window.windowID); break;
      case SDL_WINDOWEVENT_TAKE_FOCUS: sxLOG_T("Window %d is offered a focus", window.windowID);
      break;
      case SDL_WINDOWEVENT_HIT_TEST: sxLOG_T("Window %d has a special hit test", window.windowID);
      break;
      default: sxLOG_T("Window %d got unknown event %d", window.windowID, window.event);
      break;
    }
    getEventManager->getNextEvent(sxEventTypes::WINDOW_EVENT, event);
  }
}

void MainLoop::handleWindowOpacity()
{
    if (SDL_TRUE != getEventManager->isEventActive(sxEventTypes::WINDOW_OPACITY)) {
        return;
    }
    sxEvent event;
    getEventManager->getNextEvent(sxEventTypes::WINDOW_OPACITY, event);
    sxWindowOpacity opacityData = event.windowOpacity();
    event.destroyEvent();
    SDL_SetWindowOpacity(mainWindow, opacityData.value);
}

void MainLoop::handleResizeWindow()
{
    if (SDL_TRUE != getEventManager->isEventActive(sxEventTypes::RESIZE_WINDOW)) {
        return;
    }
    sxEvent event;
    getEventManager->getNextEvent(sxEventTypes::RESIZE_WINDOW, event);
    sxResizeWindow windowData = event.resizeWindow();
    event.destroyEvent();
    resizeWindow(windowData.size, windowData.logicalSize, windowData.pos);
}

void MainLoop::draw()
{
  SDL_SetRenderTarget(mainRenderer, renderTexture);
  SDL_RenderClear(mainRenderer);

//  getProfiler->startCountingFor("Draw");
  getActManager->draw();  
//  getProfiler->stopCountingFor("Draw");
  getProfiler->draw();

//  getProfiler->startCountingFor("Last RendPres");
  SDL_SetRenderTarget(mainRenderer, NULL); //Detach the texture
  SDL_RenderCopy(mainRenderer, renderTexture, NULL, NULL); // render
  SDL_RenderPresent(mainRenderer);
//  getProfiler->stopCountingFor("Last RendPres");
}

void MainLoop::resizeWindow(sxVec2Int windowSize, sxVec2Int windowLogicalSize,
    sxVec2Int pos)
{
    // make sure window is not fullscreen
    sxUInt32 windowFlags = SDL_GetWindowFlags(mainWindow);
    if ((windowFlags & SDL_WINDOW_FULLSCREEN) == 0)
    {
        // update MainLoop's logicalSize and screenSize
        this->logicalSize.w = windowLogicalSize.x;
        this->logicalSize.h = windowLogicalSize.y;
        this->screenSize.w = windowSize.x;
        this->screenSize.h = windowSize.y;

        SDL_SetWindowSize(mainWindow, windowSize.x, windowSize.y);
        SDL_SetWindowPosition(mainWindow, pos.x, pos.y);
        SDL_RenderSetLogicalSize(mainRenderer, windowLogicalSize.x, windowLogicalSize.y);
        // create a new SDL_Surface for the window
        SDL_GetWindowSurface(mainWindow);
        SDL_UpdateWindowSurface(mainWindow);

        // generate the renderToTexture otherwise it's size data will overwrite the already set data
        // in the renderer. It uses MainLoop::logicalSize so that should be updated first!
        createRenderTexture();
    }
    else sxLOG_E("Can't resize a fullscreen window.");
}

#ifdef __sdlANDROID__
void MainLoop::androidFocusChangeChecks()
{
  if(getEventManager->isEventActive(sxEventTypes::APP_WILL_ENTER_BCKGRND)){
    sxEvent enterEvent;
    getEventManager->getNextEvent(sxEventTypes::APP_WILL_ENTER_BCKGRND, enterEvent);
    appWillEnterBackground();
    enterEvent.destroyEvent();
  }
  if(getEventManager->isEventActive(sxEventTypes::APP_DID_ENTER_BCKGRND)){
    sxEvent enterEvent;
    getEventManager->getNextEvent(sxEventTypes::APP_DID_ENTER_BCKGRND, enterEvent);
    appDidEnterBackground();
    enterEvent.destroyEvent();
  }
  if(getEventManager->isEventActive(sxEventTypes::APP_WILL_ENTER_FOREGRND)){
    sxEvent enterEvent;
    getEventManager->getNextEvent(sxEventTypes::APP_WILL_ENTER_FOREGRND, enterEvent);
    appWillEnterForeground();
    enterEvent.destroyEvent();
  }
  if(getEventManager->isEventActive(sxEventTypes::APP_DID_ENTER_FOREGRND)){
    sxEvent enterEvent;
    getEventManager->getNextEvent(sxEventTypes::APP_DID_ENTER_FOREGRND, enterEvent);
    appDidEnterForeground();
    enterEvent.destroyEvent();
  }
}

void MainLoop::appWillEnterBackground()
{
  getAudioManager->pauseAll();
  sxLOG_W("App Will Go Background!");
  sxLOG_W("Current Context Pointer: %d", SDL_GL_GetCurrentContext());
  sxLOG_W("MainRenderer Pointer: %d", mainRenderer);
}

void MainLoop::appWillEnterForeground()
{
  getAudioManager->resumeAll();
  sxLOG_W("App Will come back Foreground!");
  sxLOG_W("Current Context Pointer: %d", SDL_GL_GetCurrentContext());
  sxLOG_W("MainRenderer Pointer: %d", mainRenderer);
}

void MainLoop::appDidEnterBackground()
{
  sxLOG_W("App Gone to Background!");
  sxLOG_W("Current Context Pointer: %d", SDL_GL_GetCurrentContext());
  sxLOG_W("MainRenderer Pointer: %d", mainRenderer);
}

void MainLoop::appDidEnterForeground()
{
  sxLOG_W("App came back to Foreground!");
  sxLOG_W("Current Context Pointer: %d", SDL_GL_GetCurrentContext());
  sxLOG_W("MainRenderer Pointer: %d", mainRenderer);
}
#endif

sxBool MainLoop::run()
{
  //TODO: this function should be refactored after the input queue is implemented!!
  sxUInt32 timePassed = 1; // don't set to zero
  remainingTicks = 0;

  do
  {
#ifdef __sdlANDROID__
    androidFocusChangeChecks();
#endif
    handleWindowEvents();
    
    getEventManager->clearEventQueue();
    runInnerLoopWithSDLGFX_framerateManager(timePassed);
    //runInnerLoopMyFramerateManager(timePassed);
    handleActChange();
    handleResizeWindow();
    // to prevent big timejumps when stopping for debugging
    if(1000 < timePassed){
      timePassed = static_cast<sxUInt32>(getMainConfig->getDesiredFrameStep());
      sxLOG(LL_WARNING, "timePassed value corrected. Stopped for debugging?");
    }
  }while(!getEventManager->isEventActive(sxEventTypes::QUIT));

  sxLOG(LL_TRACE, "Exiting..");

  return SDL_FALSE;
}



void MainLoop::runInnerLoopWithSDLGFX_framerateManager(sxUInt32& timePassed)
{
  getEventManager->update();
  draw();
  getActManager->processInputEvents();
  stepAnimation(timePassed);
  getAudioManager->update();

  timePassed = fpsManager->SDL_framerateDelay();
  getProfiler->setFPStoDraw(timePassed);
  getProfiler->cycleEnded();
}

void MainLoop::runInnerLoopMyFramerateManager(sxUInt32& timePassed)
{
  // actualize time
  sxUInt32 currentTime = SDL_GetTicks();

  getEventManager->update();
  draw();
  getActManager->processInputEvents();
  stepAnimation(timePassed);
  getAudioManager->update();

  getProfiler->setFPStoDraw(timePassed);
  // sleep, if framerate would be too high
  timePassed = SDL_GetTicks() - currentTime;
  if(getMainConfig->getDesiredFrameStep() > timePassed){
    SDL_Delay(static_cast<sxUInt32>(getMainConfig->getDesiredFrameStep() - timePassed));
  }
  timePassed = SDL_GetTicks() - currentTime;
  getProfiler->cycleEnded();
}

void MainLoop::stepAnimation(const sxUInt32 timePassed)
{
  // the game can fall apart if logic stepping gets too much time to handle
  remainingTicks += timePassed;
  if(remainingTicks < maxAllowedFrameStep){
    getActManager->processFrameStep(timePassed);
    remainingTicks -= timePassed;
  }else{
    while (0 <= remainingTicks - getMainConfig->getDesiredFrameStep()){
      getActManager->processFrameStep(static_cast<sxUInt32>(getMainConfig->getDesiredFrameStep()));
      remainingTicks -= getMainConfig->getDesiredFrameStep();    
    }
    if(1 <= remainingTicks){
      getActManager->processFrameStep(static_cast<sxUInt32>(remainingTicks));
      remainingTicks = 0;
    }
//    getProfiler->writeToScreen("! SLOW FRAME !", SDL_TRUE);
  } 
}

void MainLoop::enableProfilerFPSCounter(sxBool enable)
{
    getProfiler->enableFPSCounter(enable);
}

void MainLoop::deleteMainWindow()
{
  if(NULL != mainWindow){
    deleteRenderer();
    SDL_DestroyWindow(mainWindow);
    mainWindow = NULL;
  }
}

void MainLoop::deleteRenderer()
{
  if(NULL != mainRenderer){
    SDL_DestroyRenderer(mainRenderer);
    mainRenderer = NULL;
  }
}

