#include "AudioManager.h"
#include "MainConfig.h"
#include "Profiler.h"

AudioManager* AudioManager::audioManager = NULL;

void AudioManager::createManager()
{
  if(NULL != audioManager){
    sxLOG_E("AudioManager was already instantinated!");
    return;
  }
  audioManager = new AudioManager();
}

AudioManager* AudioManager::getInstance()
{
  if(NULL == audioManager){
    sxLOG_E("AudioManager not yet created!");
    return NULL;
  }
  return audioManager;
}

void AudioManager::destroyManager()
{
  if(NULL == audioManager){
    sxLOG_E("AudioManager instance is NULL, but how?");
    return;
  }
  delete audioManager;
  audioManager = NULL;
  sxLOG_T("AudioManager destroyed.");
}

AudioManager::AudioManager()
{
  initialized = SDL_FALSE;

  sxLOG_T("AudioManager created.");
}

AudioManager::~AudioManager()
{
  destroyAllSounds(TypesToReleaseEnum::PERMANENT_ITEMS_ALSO);
  Mix_CloseAudio();
  sxLOG_T("AudioManager destroyed.");
}

void AudioManager::destroyAllSounds(TypesToRelease whatToRelease)
{
  if(SDL_FALSE == initialized){
    sxLOG_E("AudioManager wasn't initialized.");
    return;
  }
  soundsToPlay.clear();
  std::map<sxCharStr, sxSound*>::iterator helperIt;
  if(!sounds.empty()){
    soundsIterator = sounds.begin();
    while(sounds.end() != soundsIterator){
      if(SDL_TRUE == itemCanBeDeleted(whatToRelease, soundsIterator->second)){
        delete soundsIterator->second;
        helperIt = soundsIterator++;
        sounds.erase(helperIt);
      }else{
        ++soundsIterator;
      }
    }
  }
}

sxBool AudioManager::itemCanBeDeleted(const TypesToRelease whatToRelease,
                                      const sxSound* sound)
{
  if(SDL_FALSE == sound->isPermanent()){
    return SDL_TRUE;
  }
  if(TypesToReleaseEnum::PERMANENT_ITEMS_ALSO == whatToRelease){
    return SDL_TRUE;
  }
  return SDL_FALSE;
}

ReturnCodes AudioManager::initialize()
{
  if(AUDIO_OPEN_FAILED == Mix_OpenAudio(getMainConfig->getAudioFrequency(),
                                        DEFAULT_AUDIO_FORMAT,
                                        getMainConfig->getAudioNumberOfChannels(),
                                        getMainConfig->getAudioChunkSize()))
  {
    sxLOG_E("Mix_OpenAudio: %s\n", Mix_GetError());
    return ReturnCodes::RC_OPEN_AUDIO_FAILED;
  }
  initialized = SDL_TRUE;
  soundSystemPaused = SDL_FALSE;

  return ReturnCodes::RC_SUCCESS;
}

void AudioManager::addSound(const sxCharStr& name, sxSound*& sound)
{
  std::pair<std::map<sxCharStr, sxSound*>::iterator, bool> result;
  result = sounds.insert(std::pair<sxCharStr, sxSound*>(name, sound));
  if(false == result.second){
    sxLOG_W("Sound already loaded: %s", name.c_str());
    delete sound;
    sound = NULL;
  }
}

void AudioManager::playSound(const sxCharStr& name)
{
  soundsToPlay.insert(name);
}

sxBool AudioManager::isPlaying(const sxCharStr& name)
{
  soundsIterator = sounds.find(name);
  if(sounds.end() != soundsIterator){
    return soundsIterator->second->isPlaying();
  }
  return SDL_FALSE;
}

void AudioManager::update()
{
//  getProfiler->startCountingFor("Audio");
  if(SDL_FALSE == soundSystemPaused){
    std::set<sxCharStr>::iterator it = soundsToPlay.begin();
    while(soundsToPlay.end() != it){
      processPlayQueue(*it);
      ++it;
    }
    soundsToPlay.clear();
  }
//  getProfiler->stopCountingFor("Audio");
}

void AudioManager::pauseAll()
{
  soundSystemPaused = SDL_TRUE;
  Mix_Pause(-1);
}

void AudioManager::resumeAll()
{
  soundSystemPaused = SDL_FALSE;
  Mix_Resume(-1);
}

void AudioManager::unloadSound(const sxCharStr& name)
{
  soundsIterator = sounds.find(name);
  if(sounds.end() != soundsIterator){
    soundsIterator->second->unloadSoundFromMemory();
  }else{
    sxLOG_E("Sound not found: %s", name.c_str());
  }
}

void AudioManager::processPlayQueue(const sxCharStr& name)
{
  soundsIterator = sounds.find(name);
  if(sounds.end() != soundsIterator){
    soundsIterator->second->play();
  }else{
    sxLOG_E("Sound not found: %s", name.c_str());
  }
}
