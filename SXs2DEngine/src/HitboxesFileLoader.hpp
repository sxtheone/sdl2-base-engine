#pragma once
#include "FileLoaderBase.h"
#include "SpriteManager.h"

class HitboxesFileLoader : public FileLoaderBase
{
public:
  ReturnCodes processFile(const sxString& filename);

private:
  sxQuad newBox;

  void processHitbox();
};

inline
ReturnCodes HitboxesFileLoader::processFile(const sxString& filename)
{
  ReturnCodes result = createReader(filename);
  if(shouldExitFromLoader(result)){
    return result;
  }

  currentSectionName = reader->setFirstSection();
  while(!currentSectionName.empty()){
    processHitbox();
    currentSectionName = reader->setNextSection();
  }
  destroyReader();

  return ReturnCodes::RC_SUCCESS;
}

inline
void HitboxesFileLoader::processHitbox()
{
  newBox = sxQuad();
  reader->getVec2Real("topLeft", newBox.topLeft);
  reader->getVec2Real("bottomRight", newBox.bottomRight);
  getSpriteManager->addHitbox(currentSectionName, newBox);
}