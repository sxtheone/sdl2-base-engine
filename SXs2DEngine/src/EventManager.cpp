#include "EventManager.h"
#include "Profiler.h"
#include "MainConfig.h"

EventManager* EventManager::eventManager = NULL;
sxVec2Real EventManager::mousePositionCorrector = sxVec2Real(1.0f, 1.0f);
sxBool EventManager::touchToMouseConversion = SDL_FALSE;

EventManager* EventManager::getInstance()
{
  if(NULL == eventManager){
    sxLOG_E("Not created yet.");
    return NULL;
  }
  return eventManager;
}

EventManager::~EventManager()
{
  releaseAccelerometer();
}

void EventManager::createManager()
{
  if(NULL != eventManager){
    sxLOG_E("Event Manager is already present.");
    return;
  }
  eventManager = new EventManager();
  SDL_SetEventFilter(EventManager::filterAndCorrectEvents, NULL);
}

/*
This callback runs every time when the event queue gets updated. The unnecessary
inputs can be filtered out here and they won't make it to the queue.
*/
int EventManager::filterAndCorrectEvents(void* userdata, SDL_Event* event)
{
  if(SDL_TRUE == correctMouseRelatedCoordinates(event)){
    return 1;
  }
  if( SDL_TRUE == touchToMouseConversion &&
      SDL_TRUE == changeTouchInputToMouse(event) )
  {
    return 0;
  }

  return 1; 
  // return 1 -> event gets into the queue.
  // return 0 -> event gets discarded
}

sxBool EventManager::correctMouseRelatedCoordinates(SDL_Event*& event)
{
  if( event->type >= sxEventTypes::MOUSE_MOTION &&
      event->type <= sxEventTypes::MOUSE_BUTTON_UP )
// && event->button.which != SDL_TOUCH_MOUSEID removed because the laptop's touch
// screen receives touches doubled and it is unusable if the doubles are not corrected
  {
    // ->motion.x & .y are in the same place in the union, no need to re-do
    event->button.x = static_cast<sxInt32>(event->button.x / mousePositionCorrector.x);
    event->button.y = static_cast<sxInt32>(event->button.y / mousePositionCorrector.y);

    if(event->type == sxEventTypes::MOUSE_MOTION){
      event->motion.xrel = static_cast<sxInt32>(event->motion.xrel / mousePositionCorrector.x);
      event->motion.yrel = static_cast<sxInt32>(event->motion.yrel / mousePositionCorrector.y);
    }
    return SDL_TRUE;
  }
  return SDL_FALSE;
}

sxBool EventManager::changeTouchInputToMouse(SDL_Event*& event)
{
  if( event->type >= sxEventTypes::TOUCH_FINGER_DOWN &&
      event->type <= sxEventTypes::TOUCH_FINGER_MOTION )
  {
    // common fields. No matter if event->button or event->motion, it's a union
    sxEvent newEvent;
    newEvent.button().which = SDL_TOUCH_MOUSEID;
    newEvent.button().timestamp = event->tfinger.timestamp;
// newEvent.button().x = getMainConfig->getWindowScreenSize().x * event->tfinger.x;
// removed because the laptop's touch screen receives touches doubled and
// it is unusable if the doubles are not corrected
    newEvent.button().x = static_cast<Sint32>(getMainConfig->getWindowScreenSize().x * event->tfinger.x);
    newEvent.button().y = static_cast<Sint32>(getMainConfig->getWindowScreenSize().y * event->tfinger.y);

    if(event->type == sxEventTypes::TOUCH_FINGER_DOWN){
      newEvent.type() = sxEventTypes::MOUSE_BUTTON_DOWN;
      newEvent.button().button = SDL_BUTTON_LEFT;
      newEvent.button().state = SDL_PRESSED;
      newEvent.button().clicks = 1; // no double-click with touch currently supported
    }else if(event->type == sxEventTypes::TOUCH_FINGER_UP){
      newEvent.type() = sxEventTypes::MOUSE_BUTTON_UP;
      newEvent.button().button = SDL_BUTTON_LEFT;
      newEvent.button().state = SDL_RELEASED;
      newEvent.button().clicks = 1; // no double-click with touch currently supported
    }else if(event->type == sxEventTypes::TOUCH_FINGER_MOTION){
      newEvent.type() = sxEventTypes::MOUSE_MOTION;
      newEvent.motion().state = SDL_BUTTON_LMASK; //NOTE: I hope this means left button pressed
      newEvent.motion().xrel = static_cast<Sint32>(
          getMainConfig->getWindowScreenSize().x * event->tfinger.dx);
      newEvent.motion().yrel = static_cast<Sint32>(
          getMainConfig->getWindowScreenSize().y * event->tfinger.dy);
    }
    if(0 > SDL_PushEvent(newEvent)){
      sxLOG_E("Event queue is probably full! Converted touch event got lost.");
    }
    return SDL_TRUE;
  }
  return SDL_FALSE;
}

void EventManager::destroyManager()
{
  if(NULL == eventManager){
    sxLOG_E("Event Manager is already destroyed.");
    return;
  }
  delete eventManager;
  eventManager = NULL;
  sxLOG_T("EventManager destroyed.");
}

void EventManager::activateAccelerometerAsJoytickListening(const sxBool enable)
{
  if(SDL_FALSE == enable){
    releaseAccelerometer();
    SDL_SetHint(SDL_HINT_ACCELEROMETER_AS_JOYSTICK, "0");
  }else{
    SDL_SetHint(SDL_HINT_ACCELEROMETER_AS_JOYSTICK, "1");
    setupAccelerometer();
  }
}

void EventManager::setupAccelerometer()
{
  sxLOG_T("Num of joyticks found: %d", SDL_NumJoysticks());
  for (int i = 0; i < SDL_NumJoysticks(); ++i) {
    sxString name = SDL_JoystickNameForIndex(i);
    // Possible values: "Android Accelerometer", "iOS Accelerometer"
    if(sxString::npos != name.find(" Accelerometer")){
      sxLOG_T("Accelerometer Joystick %d: %s\n", i, name.c_str());
      accelerometer = SDL_JoystickOpen(i);
      if(NULL == accelerometer) {
        sxLOG_E("SDL_JoystickOpen(%d) failed: %s\n", i, SDL_GetError());
        continue;
      }
      break;
    }
  }
  sxLOG_T("No accelerometer found.");
}

void EventManager::releaseAccelerometer()
{
  if(NULL != accelerometer){
    SDL_JoystickClose(accelerometer);
    accelerometer = NULL;
  }
}

void EventManager::setMousePositionCorrector(const sxVec2Real& _mousePositionCorrector)
{
  if(0 >= _mousePositionCorrector.x){
    sxLOG_E("Received Mouse Position Corrector is invalid: %.02f, %.02f",
      _mousePositionCorrector.x, _mousePositionCorrector.y);
    return;
  }
  mousePositionCorrector = _mousePositionCorrector;
}

void EventManager::update()
{
//  getProfiler->startCountingFor("EventPoll");
  SDL_PumpEvents();
//  getProfiler->stopCountingFor("EventPoll");
}
void EventManager::clearEventQueue()
{
  //TODO: calling getNumOfEventsInQueue() can be a problem performance-wise
  getNumOfEventsInQueue();
  /// we don't flush user events because they can have special parts
  /// in void* user.data part. Input event also handled, so this is what remains:
  SDL_FlushEvents(sxEventTypes::LOCALECHANGED, sxEventTypes::SYS_WM_EVENT);
  SDL_FlushEvent(sxEventTypes::CLIPBOARDUPDATE);
  SDL_FlushEvent(sxEventTypes::DROPFILE);
  SDL_FlushEvents(sxEventTypes::AUDIODEVICEADDED, sxEventTypes::AUDIODEVICEREMOVED);

  //TODO: handle instead of flush!
  SDL_FlushEvent(sxEventTypes::RENDER_TARGETS_RESET);
  SDL_FlushEvent(sxEventTypes::RENDER_DEVICE_RESET);
}

sxBool EventManager::isEventActive(const sxEventTypes& event)
{
  switch(event){
  case sxEventTypes::ANY_KIND_OF_USER_INPUT:
    if(SDL_HasEvents(sxEventTypes::KEY_DOWN, 
                     sxEventTypes::TOUCH_MULTIGESTURE)){
      return SDL_TRUE;
    }
    break;
  default:
    if(SDL_HasEvent(event)){
      return SDL_TRUE;
    }
    break;
  }

  return SDL_FALSE;
}

void EventManager::getNextEvent(const sxEventTypes& eventType, sxEvent& outEvent)
{
  outEvent = sxEvent();
  sxEventTypes startEvent, endEvent;
  switch(eventType){
  case sxEventTypes::ANY_KIND_OF_USER_INPUT:
    startEvent = sxEventTypes::KEY_DOWN;
    endEvent = sxEventTypes::TOUCH_MULTIGESTURE;
    break;
  default:
    startEvent = endEvent = eventType;
    break;
  }
  SDL_PeepEvents(outEvent, 1, SDL_GETEVENT, startEvent,
                  endEvent);
}

void EventManager::pushEventToQueue(sxEvent& event)
{
  //TODO: give back some value what indicates success/queue full/filtered
  event.common().timestamp = SDL_GetTicks();
  switch(event.type()){
  case sxEventTypes::ANY_KIND_OF_USER_INPUT:
    sxLOG_E("ANY_KIND_OF_USER_INPUT can't be pushed to the event queue.");
    return;
    break;
  }

  int result = SDL_PushEvent(event);
  if(1 != result){
    sxLOG_E("Couldn't push event. Error: %d, Event: %d", result, event.type());
  }
}

sxUInt32 EventManager::getNumOfEventsInQueue()
{
  const sxInt32 numToGet = 10;
  SDL_Event events[numToGet];
  sxInt32 num = SDL_PeepEvents(events, numToGet, SDL_PEEKEVENT, 
                               sxEventTypes::INVALID, sxEventTypes::LASTEVENT);
  if(SDL_GetTicks() < 1000){
    return 0;
  }
  sxUInt32 currentTime = SDL_GetTicks()-1000;
  for(sxInt32 i=0; i<num; ++i){
    if(events[i].common.timestamp < currentTime){
      sxLOG_E("Pending Events[%d]: %d, timestamp: %d", i, events[i].type,
        events[i].common.timestamp);
    }
  }
  return num;
}

sxUInt32 EventManager::peekEventsInQueue(const sxUInt32 maxNumOfEvents, SDL_Event eventArray[])
{
  return peekEventsInQueue(sxEventTypes::INVALID, sxEventTypes::LASTEVENT, 
                           maxNumOfEvents, eventArray);
}

sxUInt32 EventManager::peekEventsInQueue(const sxUInt32 minEvent, const sxUInt32 maxEvent, 
                                         const sxUInt32 maxNumOfEvents, SDL_Event eventArray[])
{
  sxInt32 num = SDL_PeepEvents(eventArray, maxNumOfEvents, SDL_PEEKEVENT, 
                               minEvent, maxEvent);
  if(SDL_GetTicks() < 1000){ // don't give back event in the first second after program start
    return 0;
  }
  return num;
}