#include "SystemIO.h"

#ifdef WIN32
#include <direct.h>
#else
#include <sys/stat.h>
#endif

#ifdef __MACOSX__
#include <SDL2/SDL.h>
#else
#include <SDL.h>
#endif

#ifdef __sdlANDROID__
#include <jni.h>
#endif

#include "dirent.h" // directory related commands, cross-platform
#include "stringTools.h"
#include "LogManager.h"


ReturnCodes SystemIO::createDirectory(const sxString& nameWithPath)
{
  int nError = 0;
#ifdef WIN32
  nError = _mkdir(nameWithPath.c_str());
#else
  mode_t nMode = 0733; // UNIX style permissions
  nError = mkdir(nameWithPath.c_str(),nMode); // can be used on non-Windows
#endif
  if(0 == nError) return ReturnCodes::RC_SUCCESS;
  return ReturnCodes::RC_CANT_CREATE_DIRECTORY;
}

sxBool SystemIO::createDirInBasePath(const sxString dir, sxString& outDirPath)
{
#ifdef WIN32
  outDirPath = SDL_GetBasePath();
  outDirPath += dir;
  createDirectory(outDirPath);
#else
  if(NULL != SDL_AndroidGetExternalStoragePath()){
    outDirPath = SDL_AndroidGetExternalStoragePath();
    outDirPath += dir;
    createDirectory(outDirPath);
  }else{
    return SDL_FALSE;
  }
#endif
  return SDL_TRUE;
}

ReturnCodes SystemIO::getFileListInDir(const sxString& path, const sxString& extension,
                                       std::vector<sxString> &files)
{
  DIR *dp;
  if((dp  = opendir(path.c_str())) == NULL) {
    sxLOG_E("Error opening dir: %s", path.c_str());
    return ReturnCodes::RC_DIRECTORY_READ_ERROR;
  }
  if(extension.empty()){
    sxLOG_E("Extension must not be empty.");
    return ReturnCodes::RC_FILE_CANT_BE_OPENED;
  }
  
  sxUInt32 numOfDirs = 0; // directorys should be the first entries
  struct dirent *dirp;
  while ((dirp = readdir(dp)) != NULL) {
    sxString filename = sxString(dirp->d_name);
    if( 0 != filename.compare(".") &&
        (DT_DIR == dirp->d_type ||
         sxString::npos != filename.find(extension)) )
    {
      if(DT_DIR == dirp->d_type){
        if(numOfDirs < files.size()) files.insert(files.begin() + numOfDirs, filename);
        else files.push_back(filename);
        ++numOfDirs;
      }else files.push_back(filename);
    }
  }
  closedir(dp);
  return ReturnCodes::RC_SUCCESS;
}

#ifdef __sdlANDROID__
void SystemIO::callJavaMethod(sxString methodName)
{
  sxLOG_T("calling Java method: %s", methodName.c_str());
  // retrieve the JNI environment.
  JNIEnv* env = (JNIEnv*)SDL_AndroidGetJNIEnv();

  // retrieve the Java instance of the SDLActivity
  jobject activity = (jobject)SDL_AndroidGetActivity();

  // find the Java class of the activity. It should be SDLActivity or a subclass of it.
  jclass clazz(env->GetObjectClass(activity));

  // find the identifier of the method to call
  jmethodID method_id = env->GetMethodID(clazz, methodName.c_str(), "()V");

  // effectively call the Java method
  env->CallVoidMethod(activity, method_id);

  // clean up the local references.
  env->DeleteLocalRef(activity);
  env->DeleteLocalRef(clazz);
  sxLOG_T("Java method: %s called", methodName.c_str());
  // Warning (and discussion of implementation details of SDL for Android):
  // Local references are automatically deleted if a native function called
  // from Java side returns. For SDL this native function is main() itself.
  // Therefore references need to be manually deleted because otherwise the
  // references will first be cleaned if main() returns (application exit).
}
#endif

sxString SystemIO::getFullCurrentTimeAsStr()
{
  std::time_t currentTime = std::time(NULL);
  struct std::tm* now = std::localtime(&currentTime);

  return strFormatter("%d_%02d_%02d_%02d%02d%02d", now->tm_year+1900, now->tm_mon+1,
                      now->tm_mday, now->tm_hour, now->tm_min, now->tm_sec);
}

sxString SystemIO::getCurrentSystemTime()
{
  std::time_t currentTime = std::time(NULL);
  struct std::tm* now = std::localtime(&currentTime);
  return strFormatter("%02d:%02d:%02d", now->tm_hour, now->tm_min, now->tm_sec);
}
