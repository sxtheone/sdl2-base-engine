#include "ObjectCollisionDetector.h"
#include "ObjectScene.h"
#include "StaticTiledScene.h"
#include "Profiler.h"

void ObjectCollisionDetector::setEndSceneIndex(const sxUInt32 startIdx)
{
  endSceneIndex = static_cast<sxInt32>(startIdx);
  if(endSceneIndex >= static_cast<sxInt32>(scenes->size())){
    sxLOG_E("Invalid end scene index: %d", endSceneIndex);
    endSceneIndex = 0;
  }
}

void ObjectCollisionDetector::doCollisionDetectionChecks(sxUInt32 ticksPassed)
{
  sxInt32 startSceneIdx = static_cast<sxInt32>(scenes->size())-1;
  for(activeSceneIdx=startSceneIdx; activeSceneIdx>=endSceneIndex; --activeSceneIdx){
    activeScene = (*scenes)[activeSceneIdx];
    if( NULL == activeScene->collisionProps ||
        SDL_FALSE == activeScene->sceneProps.enabled() )
    {
      continue;
    }
    if(SceneTypeEnum::OBJECT == activeScene->type){
      sxUInt32 activeSceneNumOfObjects = activeScene->getNumOfCollisionObjects();
      for(activeObjectIdx=0; activeObjectIdx<activeSceneNumOfObjects; ++activeObjectIdx){
        actObjCollData = activeScene->getCollisionData(activeObjectIdx);
        if(SDL_TRUE == objectCanCollideNow(actObjCollData.object)){
          actObjCollData.object->incIdleTimePassed(ticksPassed);
          checkCollisionsForObject();
        }
      }
    }
  }
}

void ObjectCollisionDetector::checkCollisionsForObject()
{
  sxUInt32 objStartIdx = activeObjectIdx+1;
  for(sxInt32 currSceneIdx=activeSceneIdx; currSceneIdx >= endSceneIndex; --currSceneIdx){
    currScene = (*scenes)[currSceneIdx];
    if(NULL != currScene->collisionProps &&
       (activeScene->collisionProps->canCollideWith(currScene->collisionProps) ||
        currScene->collisionProps->canCollideWith(activeScene->collisionProps)) )
    {
      // object-object collision is handled one-by-one
      checkCollisionsInCurrScene(objStartIdx);    
      if( SceneTypeEnum::STATIC_TILED == currScene->type ||
          SceneTypeEnum::SCROLLABLE_TILED == currScene->type )
      {
        // object-tiledScene collision is handled by sending all the 
        // tiledScene collision info in collInfoToSend vector at once
        sendCollInfoToActiveScene();
      }
      activeScene->collisionDetectionFinishedForObject(activeObjectIdx);
    }

    objStartIdx = 0;
  }
}

void ObjectCollisionDetector::checkCollisionsInCurrScene(sxUInt32 objStartIdx)
{
  if( SceneTypeEnum::STATIC_TILED == currScene->type ||
      SceneTypeEnum::SCROLLABLE_TILED == currScene->type )
  {
    prepareSearchGridForTiledScene();
  }
  checkCollisionsInScene(objStartIdx);
}

void ObjectCollisionDetector::checkCollisionsInScene(sxUInt32 objStartIdx)
{
  CollisionData currCollData;
  collInfoToSend.clear();
  sxUInt32 numOfItems = currScene->getNumOfCollisionObjects();
  for(sxUInt32 currObjIdx=objStartIdx; currObjIdx < numOfItems; ++currObjIdx){
    currCollData = currScene->getCollisionData(currObjIdx);
    if(SDL_FALSE == objectCanCollideNow(currCollData.object)){
      drawDebugBox(currCollData);
      continue;
    }
    drawDebugBox(currCollData);
    dealWithCollisionBetweenObjects(&currCollData, &actObjCollData, currScene);
    if( SceneTypeEnum::STATIC_TILED == currScene->type ||
        SceneTypeEnum::SCROLLABLE_TILED == currScene->type )
    {
      dealWithTiledSceneCollision(&actObjCollData,&currCollData);
    }else{
      dealWithCollisionBetweenObjects(&actObjCollData, &currCollData, activeScene);      
    }
  }
}

void ObjectCollisionDetector::drawDebugBox(const CollisionData& collData)
{
#ifdef DEVELOPER_HELPERS
  if(SDL_TRUE == getProfiler->drawDebugBoxesActive()){
    sxQuad hitbox;
    if(NULL != collData.object->collisionProps){
      hitbox = *collData.object->collisionProps->hitbox;
    }else{
      hitbox.bottomRight.x = static_cast<sxFloat>(collData.object->getWidth());
      hitbox.bottomRight.y = static_cast<sxFloat>(collData.object->getHeight());
    }   
    getProfiler->addToDrawDebugHitboxes(collData.position, hitbox, Color_PaleYellow);
  }
#endif
}

void ObjectCollisionDetector::prepareSearchGridForTiledScene()
{
  sxVec2Int hitboxSize;
  hitboxSize = actObjCollData.object->getHitboxSize();
  sxVec2Real hitboxPos = actObjCollData.object->getHitBoxWithOffset(actObjCollData.position).topLeft;
  static_cast<StaticTiledScene*>(currScene)->setSearchRectangle(hitboxPos,/*actObjCollData.position,*/
                                                                hitboxSize);
}

void ObjectCollisionDetector::dealWithCollisionBetweenObjects(CollisionData* obj1,
                                                        CollisionData* obj2,
                                                        Scene* sceneWhatHandlesCollision)
{
  if(SDL_TRUE == objectCanCollideWithOther(obj1->object, obj2->object)){
    if(SDL_TRUE == areActuallyColliding(obj1, obj2)){
      CollisionInfo collInfo;
      fillCollisionInfo(obj1->objectIndex, obj2, collInfo);
      sceneWhatHandlesCollision->handleCollision(collInfo);
      obj1->object->setIdleTimePassed(0);
    }
  }
}

void ObjectCollisionDetector::dealWithTiledSceneCollision(CollisionData* obj1,
                                                          CollisionData* obj2)
{
  if(SDL_TRUE == objectCanCollideWithOther(obj1->object, obj2->object)){
    if(SDL_TRUE == areActuallyColliding(obj1, obj2)){
      CollisionInfo collInfo;
      fillCollisionInfo(obj1->objectIndex, obj2, collInfo);
      collInfoToSend.push_back(collInfo);      
    }
  }
}

void ObjectCollisionDetector::sendCollInfoToActiveScene()
{
  if(!collInfoToSend.empty()){
    activeScene->handleCollisionWithTiledScene(collInfoToSend);
    collInfoToSend.clear();
  }  
}
