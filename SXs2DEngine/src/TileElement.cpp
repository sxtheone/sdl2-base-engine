#include "TileElement.h"
#include "SpriteManager.h"


TileElement::TileElement()
{
  type = ObjectTypes::TileElement;
  objectProps.setShouldRedraw(SDL_FALSE); 
  // TileElement redraw is handled when doing the drawing in TiledScenes
}

TileElement::TileElement(sxSprite* newSprite)
{
  TileElement();
  setSprite(newSprite);
}

void TileElement::setSprite(sxSprite* newSprite)
{
  activeSprite = newSprite;
  destinationRect.w = activeSprite->size.w;
  destinationRect.h = activeSprite->size.h;
}

void TileElement::assign(const ObjectBase& source)
{
  ObjectBase::assign(source); // don't forget to call the ancestor's assign function
}

ReturnCodes TileElement::loadData(INIReader* reader)
{
  ReturnCodes result = ObjectBase::loadData(reader);
  if(ReturnCodes::RC_SUCCESS == result){
    sxString spriteName;
    /// not making sprite loading mandatory because of the command:MakeTiles
    if(ReturnCodes::RC_SUCCESS == reader->getStr("sprite", spriteName, SDL_FALSE)){
      sxSprite* newSprite = getSpriteManager->getSprite(spriteName);
      setSprite(newSprite);
    }
  }
  return result;
}
