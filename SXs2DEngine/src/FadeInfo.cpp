#include "FadeInfo.h"
#include "ObjectDrawerRenderToTexture.h"

void FadeInfo::setup(const FadeType type, const sxInt32 fadeTime)
{
  fadeType = type;
  timeStep = COLOR_MAX_VALUE/static_cast<sxFloat>(fadeTime);
  setupPreciseAlpha();
}

void FadeInfo::startFade()
{
  active = SDL_TRUE;
  sxLOG_T("Fade %s started", 
            (fadeType == FADE_IN) ? sxString("IN").c_str() : sxString("OUT").c_str());
}

void FadeInfo::stepFade(const sxUInt32 ticksPassed, ObjectDrawerBase* drawer)
{
  if( SDL_TRUE == active &&
      NULL != dynamic_cast<ObjectDrawerRenderToTexture*>(drawer) )
  {
    ObjectDrawerRenderToTexture* rttDrawer = static_cast<ObjectDrawerRenderToTexture*>(drawer);
    sxColor color = rttDrawer->getColorModification();

    if(FADE_IN == fadeType) stepFadeIn(ticksPassed);
    else stepFadeOut(ticksPassed);

    color.a = static_cast<sxUInt8>(preciseAlpha);
    rttDrawer->setColorModification(color);

    if(SDL_FALSE == active) setupPreciseAlpha();

  }else{
    active = SDL_FALSE;
  }
}

void FadeInfo::setupPreciseAlpha()
{
  if(FADE_IN == fadeType) preciseAlpha = 0;
  else preciseAlpha = COLOR_MAX_VALUE;
}

void FadeInfo::stepFadeIn(const sxUInt32 ticksPassed)
{
  preciseAlpha += timeStep * ticksPassed;
  if(COLOR_MAX_VALUE <= preciseAlpha){
    preciseAlpha = COLOR_MAX_VALUE;
    active = SDL_FALSE;
    sxLOG_T("Fade IN finished");
  }
}

void FadeInfo::stepFadeOut(const sxUInt32 ticksPassed)
{
  preciseAlpha -= timeStep * ticksPassed;
  if(0 >= preciseAlpha){
    preciseAlpha = 0;
    active = SDL_FALSE;
    sxLOG_T("Fade OUT finished");
  }
}
