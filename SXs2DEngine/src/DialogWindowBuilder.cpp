#include "DialogWindowBuilder.h"
#include "ObjectManager.h"

ReturnCodes DialogWindowBuilder::loadDialogWindow(INIReader* _reader, std::vector<Object*>*& _objects)
{
  reader = _reader;
  objects = _objects;
  ReturnCodes result = fillBasicVariables();
  if(ReturnCodes::RC_SUCCESS == result){
    std::vector<sxString> strs;
    result = reader->getStrList("straightObjects", strs);
    if (ReturnCodes::RC_SUCCESS == result) createDialogStraightElements(strs);
    else return result;

    sxString centerName;
    result = reader->getStr("centerObject", centerName);
    if (ReturnCodes::RC_SUCCESS == result) createDialogCenterElement(centerName);
    else return result;

    strs.clear();
    result = reader->getStrList("cornerObjects", strs);
    if (ReturnCodes::RC_SUCCESS == result) createDialogCornerElements(strs);
    else return result;
  }
  return result;
}

void DialogWindowBuilder::CreateDialogWindow(
    std::vector<Object*>*& _objects,
    sxVec2Real _windowPos,
    sxVec2Real _windowSize,
    sxColor _colorTint,
    sxBool _windowVisible,
    const std::vector<sxString>& straightObjects,
    const sxString centerObject,
    const std::vector<sxString>& cornerObjects)
{
  objects = _objects;
  windowPos = _windowPos;
  windowSize = _windowSize;
  colorTint = _colorTint;
  windowVisible = _windowVisible;

  createDialogStraightElements(straightObjects);
  createDialogCenterElement(centerObject);
  createDialogCornerElements(cornerObjects);
}

ReturnCodes DialogWindowBuilder::fillBasicVariables()
{
  if(ReturnCodes::RC_SUCCESS != reader->getVec2Real("centerAreaTopLeft", windowPos)){
    return ReturnCodes::RC_FIELD_NOT_FOUND;
  }
  if(ReturnCodes::RC_SUCCESS != reader->getVec2Real("centerAreaSize", windowSize)){
    return ReturnCodes::RC_FIELD_NOT_FOUND;
  }
  
  if(ReturnCodes::RC_SUCCESS != reader->getColor("colorTint", colorTint, SDL_FALSE)){
    colorTint = {255, 255, 255, 255};
  }

  if(ReturnCodes::RC_SUCCESS != reader->getBoolean("visible", windowVisible, SDL_FALSE)){
    windowVisible = SDL_TRUE;
  }
  return ReturnCodes::RC_SUCCESS;
}

Object* DialogWindowBuilder::createObject(const sxString& objectName)
{
  return static_cast<Object*>(getObjectManager->createObject(objectName));
}

void DialogWindowBuilder::createDialogStraightElements(const std::vector<sxString>& strs)
{
  if(4 != strs.size()){
    sxLOG_E("Invalid number of items in straightObjects: %d", strs.size());
    return;
  }

  newObject = createObject(strs[0]);
  fillAndPushDialogElement(sxVec2Real(windowPos.x - newObject->getWidth(), windowPos.y),
                           sxVec2Real(1, windowSize.y));

  newObject = createObject(strs[1]);
  fillAndPushDialogElement(sxVec2Real(windowPos.x + windowSize.x, windowPos.y),
                           sxVec2Real(1, windowSize.y));

  newObject = createObject(strs[2]);
  fillAndPushDialogElement(sxVec2Real(windowPos.x, windowPos.y - newObject->getHeight()),
                           sxVec2Real(windowSize.x, 1));

  newObject = createObject(strs[3]);
  fillAndPushDialogElement(sxVec2Real(windowPos.x, windowPos.y + windowSize.y),
                           sxVec2Real(windowSize.x, 1));
}

void DialogWindowBuilder::createDialogCenterElement(const sxString& centerName)
{
  newObject = createObject(centerName);
  fillAndPushDialogElement(windowPos, windowSize);
}

void DialogWindowBuilder::createDialogCornerElements(const std::vector<sxString>& strs)
{
  if(4 != strs.size()){
    sxLOG_E("Invalid number of items in cornerObjects: %d", strs.size());
    return;
  }

  newObject = createObject(strs[0]);
  fillAndPushDialogElement(sxVec2Real(windowPos.x - newObject->getWidth(), 
                           windowPos.y - newObject->getHeight()));

  newObject = createObject(strs[1]);
  fillAndPushDialogElement(sxVec2Real(windowPos.x + windowSize.x, 
                           windowPos.y - newObject->getHeight()));

  newObject = createObject(strs[2]);
  fillAndPushDialogElement(sxVec2Real(windowPos.x - newObject->getWidth(),
                           windowPos.y + windowSize.y));

  newObject = createObject(strs[3]);
  fillAndPushDialogElement(sxVec2Real(windowPos.x + windowSize.x, windowPos.y + windowSize.y));
}

void DialogWindowBuilder::fillAndPushDialogElement(const sxVec2Real& pos, const sxVec2Real& scale)
{
  newObject->setPosition(pos);
  newObject->setScale(scale);
  newObject->setColorModifier(colorTint);
  newObject->objectProps.setVisibility(windowVisible);
  objects->push_back(newObject);
}
