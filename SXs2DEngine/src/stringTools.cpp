#include "stringTools.h"

sxString stringTools::_strFormatter(const char *fmt, ...)
{
  va_list ap;
  va_start(ap, fmt);
  sxString buf = vformat(fmt, ap);
  va_end(ap);
  return buf;
}

void stringTools::_trimTrailingAndLeadingSpaces(sxString& line)
{
  _trimLeadingSpaces(line);
  _trimTrailingSpaces(line);
}

void stringTools::_trimLeadingSpaces(sxString& line, const sxUInt32 startIndex)
{
  sxUInt32 counter = startIndex;
  while(isspace(line[counter])){
    ++counter;
  }
  if(counter != startIndex){
    line = line.substr(counter);
  }
}

void stringTools::_trimTrailingSpaces(sxString& line, sxString::size_type endIndex)
{
  if(sxString::npos == endIndex) endIndex = line.size()-1;
  sxInt32 counter = endIndex;

  while(isspace(line[counter])){
    --counter;
  }

  if(counter != endIndex){
    line = line.substr(0, counter+1);
  }
}

ReturnCodes stringTools::_strToDouble(const sxString& strValue, sxDouble& doubleValue)
{
  char* end;
  sxDouble result = strtod(strValue.c_str(), &end);
  if(*end){
    return ReturnCodes::RC_FILE_PARSE_ERROR;
  }
  doubleValue = result;
  return ReturnCodes::RC_SUCCESS;
}

ReturnCodes stringTools::_strToReal(const sxString& strValue, sxFloat& realValue)
{
  char* end;
  sxFloat result = static_cast<sxFloat>(strtod(strValue.c_str(), &end));
  if(*end){
    return ReturnCodes::RC_FILE_PARSE_ERROR;
  }
  realValue = result;
  return ReturnCodes::RC_SUCCESS;
}

ReturnCodes stringTools::_strToInt(const sxString& strValue, sxInt32& intValue)
{
  char* end;
  sxInt32 result = static_cast<sxInt32>(strtol(strValue.c_str(), &end, 10));
  if(*end){
    return ReturnCodes::RC_FILE_PARSE_ERROR;
  }
  intValue = result;
  return ReturnCodes::RC_SUCCESS;
}


// intended to be just a helper function
sxString stringTools::vformat (const char *fmt, va_list ap)
{
  // Allocate a buffer on the stack that's big enough for us almost
  // all the time.  Be prepared to allocate dynamically if it doesn't fit.
  size_t size = 256;
  char stackbuf[256];
  std::vector<char> dynamicbuf;
  char *buf = &stackbuf[0];

  while (1) {
    // Try to vsnprintf into our buffer.
    int needed = vsnprintf(buf, size, fmt, ap);
    // NB. C99 (which modern Linux and OS X follow) says vsnprintf
    // failure returns the length it would have needed.  But older
    // glibc and current Windows return -1 for failure, i.e., not
    // telling us how much was needed.

    if (needed <= (int)size && needed >= 0) {
      // It fit fine so we're done.
      return sxString (buf, (size_t) needed);
    }

    // vsnprintf reported that it wanted to write more characters
    // than we allotted.  So try again using a dynamic buffer.  This
    // doesn't happen very often if we chose our initial size well.
    size = (needed > 0) ? (needed+1) : (size*2);
    dynamicbuf.resize (size);
    buf = &dynamicbuf[0];
  }
}

