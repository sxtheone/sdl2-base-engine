#include "Object.h"
#include "Profiler.h"
#include "SpriteManager.h"

Object::Object()
{
  spriteScale = sxVec2Real(1.0f, 1.0f);
  spriteFlip = SDL_FLIP_NONE;
  secondPerTicksPassed = 0;
}

Object& Object::operator=(const Object& other)
{
  if(SDL_TRUE == objectTypeIs(other.type, type)){
    assign(other);
  }
  return *this;
}

void Object::assign(const ObjectBase& source)
{
  ObjectBase::assign(source);
#ifdef _DEBUG
  const Object* sourceObj = dynamic_cast<const Object*>(&source);
#else
  const Object* sourceObj = static_cast<const Object*>(&source);
#endif 
  velocity        = sourceObj->velocity;
  realPosition    = sourceObj->realPosition;
  secondPerTicksPassed = sourceObj->secondPerTicksPassed;
  spriteFlip      = sourceObj->spriteFlip;
  spriteScale     = sourceObj->spriteScale;
}

void Object::setPosition(const sxVec2Real& newPos)
{
  realPosition.x = newPos.x;
  realPosition.y = newPos.y;
  fillDestinationRectPositionFromRealPosition();
}

void Object::addToPosition(const sxVec2Real& posToAdd)
{
  realPosition.x += posToAdd.x;
  realPosition.y += posToAdd.y;
  fillDestinationRectPositionFromRealPosition();
}

void Object::setVelocity(const sxVec2Real& newVelocity)
{
  velocity = newVelocity;
}

void Object::draw(SDL_Renderer* renderer)
{
  if(NULL != activeSprite){
    if(SDL_TRUE == drawingEnabled()){
      callDrawMethodAndModifiers(renderer);
    }
  }else{
    sxLOG_E("No current sprite to render!");
  }
#ifdef DEVELOPER_HELPERS
  if(SDL_TRUE == getProfiler->drawDebugBoxesActive()){
    if(NULL != collisionProps){
      getProfiler->addToDrawDebugHitboxes(destinationRect, *collisionProps->hitbox,
                                          Color_Blue);
    }
  }
#endif
}

void Object::simpleDraw(SDL_Renderer* renderer)
{
  ObjectBase::draw(renderer, sxVec2Int(destinationRect.x, destinationRect.y));
}

void Object::drawOffset(SDL_Renderer* renderer, const sxVec2Int& posOffset)
{
  sxVec2Real origPos = getPosition();
  setPosition(sxVec2Real(static_cast<sxFloat>(origPos.x+posOffset.x),
                         static_cast<sxFloat>(origPos.y+posOffset.y)));
  draw(renderer);
  setPosition(origPos);
}

void Object::stepAnimation(const sxUInt32 ticksPassed)
{
  if(SDL_TRUE == objectProps.objectEnabled()){
    secondPerTicksPassed = ticksPassed/1000.0f;
    realPosition.x += velocity.x * secondPerTicksPassed;
    realPosition.y += velocity.y * secondPerTicksPassed;

    fillDestinationRectPositionFromRealPosition();
  }
}

void Object::fillDestinationRectPositionFromRealPosition()
{
  if( destinationRect.x != static_cast<sxInt32>(realPosition.x) ||
      destinationRect.y != static_cast<sxInt32>(realPosition.y) )
  {
    objectProps.setShouldRedraw(SDL_TRUE);
  }
  destinationRect.x = static_cast<sxInt32>(realPosition.x);
  destinationRect.y = static_cast<sxInt32>(realPosition.y);
}

void Object::setSpriteFlip(SDL_RendererFlip newFlip)
{
  if(newFlip != spriteFlip){
    objectProps.setShouldRedraw(SDL_TRUE);
    spriteFlip = newFlip;
  }
}

void Object::setScale(const sxFloat newScale)
{
  if(0 < newScale){ // shouldRedraw is set in setSize()
    spriteScale.x = newScale;
    spriteScale.y = newScale;
    setSize(sxVec2Int(static_cast<sxInt32>(activeSprite->size.w*spriteScale.x),
                      static_cast<sxInt32>(activeSprite->size.h*spriteScale.y)));
  }
}

void Object::setScale(const sxVec2Real& newScale)
{
  if( 0 < newScale.x &&
      0 < newScale.y )
  { // shouldRedraw is set in setSize()
    spriteScale = newScale;
    setSize(sxVec2Int(static_cast<sxInt32>(activeSprite->size.w*spriteScale.x),
                      static_cast<sxInt32>(activeSprite->size.h*spriteScale.y)));
  }
}

ReturnCodes Object::loadData(INIReader* reader)
{
  ObjectBase::loadData(reader);
  sxVec2Real tempVecReal;
  sxFloat tempFloat;
  if(ReturnCodes::RC_SUCCESS == reader->getVec2Real("velocity", tempVecReal, SDL_FALSE)){
    setVelocity(tempVecReal);
  }
  if(ReturnCodes::RC_SUCCESS == reader->getVec2Real("spriteScale", tempVecReal, SDL_FALSE)){
    if(NULL != activeSprite){
      setScale(tempVecReal);
      setHitbox(); // if the original object already exists and we are loading from ObjectSceneFileLoader
    }else{
      spriteScale = tempVecReal;
    }
  }else if(ReturnCodes::RC_SUCCESS == reader->getReal("spriteScale", tempFloat, SDL_FALSE)){
    if(NULL != activeSprite) setScale(tempFloat);
    else spriteScale = sxVec2Real(tempFloat, tempFloat);
  }

  loadSpriteFlipFields(reader);
  return ReturnCodes::RC_SUCCESS;
}

void Object::loadSpriteFlipFields(INIReader* reader)
{
  sxBool flipBool = SDL_FALSE;
  SDL_RendererFlip newFlip = SDL_FLIP_NONE;
  if(ReturnCodes::RC_SUCCESS == reader->getBoolean("spriteFlipHorizontal", flipBool, SDL_FALSE)){
    if(SDL_TRUE == flipBool){
      newFlip = SDL_FLIP_HORIZONTAL;
    }
  }
  if(ReturnCodes::RC_SUCCESS == reader->getBoolean("spriteFlipVertical", flipBool, SDL_FALSE)){
    if(SDL_TRUE == flipBool){
      newFlip = static_cast<SDL_RendererFlip>(newFlip | SDL_FLIP_VERTICAL);
    }
  }
  setSpriteFlip(newFlip);
}

CollisionData Object::getCollisionData()
{
  CollisionData result;
  result = ObjectBase::getCollisionData();
  result.position = getPosition();
  return result;
}

sxQuad Object::getHitBoxWithPositionOffset(const sxVec2Int& additionalOffset)
{
  return getHitBoxWithOffset(realPosition, additionalOffset);
}
