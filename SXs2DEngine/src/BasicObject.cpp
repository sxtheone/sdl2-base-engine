#include "BasicObject.h"
#include "SpriteManager.h"

BasicObject::BasicObject()
{
  type = ObjectTypes::BasicObject;
  destroySprite = SDL_FALSE;
}

BasicObject::~BasicObject()
{
  destroyTheSprite();
}

void BasicObject::assign(const ObjectBase& source)
{
  Object::assign(source); // don't forget to call the ancestor's assign function
  destroySprite = SDL_FALSE; // only the owner should delete the activeSprite
}

void BasicObject::setSprite(sxSprite* newSprite, sxBool shouldDeleteSprite)
{
  destroyTheSprite();
  activeSprite = newSprite;
  setScale(spriteScale);
  destroySprite = shouldDeleteSprite;
}

sxBool BasicObject::isSpriteLoaded()
{
  return (NULL == activeSprite) ? SDL_FALSE : SDL_TRUE;
}

void BasicObject::callDrawMethodAndModifiers(SDL_Renderer*& renderer)
{
  activateColorModifierChanges();
  //SDL_RenderCopy(renderer, activeSprite->getTexture(), &activeSprite->size, &destinationRect);
  sxInt32 result;
  result = SDL_RenderCopyEx(renderer,
                            activeSprite->getTexture(),
                            &activeSprite->size,
                            &destinationRect,
                            rotationInDegree,
                            rotationCenter,
                            spriteFlip);
  if(0 != result){
    sxLOG_E("Error while rendering texture. ErrorCode: %d, Position: %d:%d",
      result, destinationRect.x, destinationRect.y);
  }
  deactivateColorModifierChanger();
}

void BasicObject::destroyTheSprite()
{
  if( SDL_TRUE == destroySprite &&
      NULL != activeSprite )
  {
    delete activeSprite;
    activeSprite = NULL;
  }
  destroySprite = SDL_FALSE;
}

ReturnCodes BasicObject::loadData(INIReader* reader)
{
  Object::loadData(reader);
  sxString spriteName;
  if(NULL != activeSprite){ /// Won't load again sprites
    return ReturnCodes::RC_SUCCESS;
  }
  reader->getStr("sprite", spriteName);
  sxSprite* newSprite = getSpriteManager->getSprite(spriteName);
  if(NULL == newSprite){
    return ReturnCodes::RC_CANT_CREATE_OBJECT;
  }
  setSprite(newSprite);
  return ReturnCodes::RC_SUCCESS;
}


