#pragma once
#include <string>
#ifdef __MACOSX__
#include <SDL2/SDL_stdinc.h>
#include <SDL2/SDL_pixels.h>
#include <SDL2/SDL_keyboard.h>
#else
#include <SDL_stdinc.h>
#include <SDL_pixels.h>
#include <SDL_keyboard.h>
#endif

typedef SDL_bool sxBool;
typedef float sxFloat;
typedef double sxDouble;

typedef Sint8 sxChar;
typedef Sint8 sxInt8;
typedef Uint8 sxUInt8;
typedef Sint16 sxInt16;
typedef Uint16 sxUInt16;
typedef Sint32 sxInt32;
typedef Uint32 sxUInt32;
typedef Sint64 sxInt64;
typedef Uint64 sxUInt64;
typedef std::string sxString;
typedef SDL_Color sxColor;
typedef SDL_Keysym sxKeySym;
typedef SDL_Rect sxRect;
typedef SDL_FRect sxFloatRect;
//TODO: MAX/MIN consts:
// signed MIN 1 << (sizeof(type) * 8 - 1) -> 10000 0000 ..
// signed MAX -1 >> 1 -> 0xEFFFFF...
// unsigned MAX -1
// problem: VS compiler duplicates the sign value (in case of negative, this means not zero)