#pragma once
#include "sxTypes.h"


class sxResizeWindow
{
public:
    sxVec2Int pos{ 0,0 };
    sxVec2Int size{ 0,0 };
    sxVec2Int logicalSize { 0,0 };
};