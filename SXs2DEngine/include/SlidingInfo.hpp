#include "sxTypes.h"

class SlidingInfo
{
public:
  void setDistance(const sxVec2Real& distance){slidingDistance = distance;}
  void setSpeed(const sxVec2Real& speed);
  void setTargetPos(const sxVec2Int& pos);
  void setToStart();
  sxBool isSpeedSet(){return (0 != slidingSpeed.x || 0 != slidingSpeed.y) ? SDL_TRUE : SDL_FALSE;}
  sxBool isFinished(){return (currSlidingPos == targetSlidingPos) ? SDL_TRUE : SDL_FALSE;}
  sxVec2Int getCurrentPos();
  void step(const sxFloat secondPerTicksPassed);

private:
  sxDirection direction;
  sxVec2Real slidingSpeed; // the scene slides in to the screen
  sxVec2Real slidingDistance;
  sxVec2Real targetSlidingPos;
  sxVec2Real currSlidingPos;  

  void correctSlidingDistance();
};

inline
void SlidingInfo::setSpeed(const sxVec2Real& speed)
{
  direction.resetDirection();
  slidingSpeed = speed;
  if(0 > speed.x) direction.addDirection(sxDirection::RIGHT); 
  else if(0 < speed.x) direction.addDirection(sxDirection::LEFT);
    
  if(0 > speed.y) direction.addDirection(sxDirection::UP);
  else if(0 < speed.y) direction.addDirection(sxDirection::DOWN);
}

inline
void SlidingInfo::setTargetPos(const sxVec2Int& pos)
{
  targetSlidingPos.x = static_cast<sxFloat>(pos.x);
  targetSlidingPos.y = static_cast<sxFloat>(pos.y);
}

inline
void SlidingInfo::setToStart()
{
  if(SDL_TRUE == isSpeedSet()){
    correctSlidingDistance();
    
    if(direction.isLeftSet()) currSlidingPos.x = targetSlidingPos.x - slidingDistance.x;
    else currSlidingPos.x = targetSlidingPos.x + slidingDistance.x;
    if(direction.isDownSet()) currSlidingPos.y = targetSlidingPos.y - slidingDistance.y;
    else currSlidingPos.y = targetSlidingPos.y + slidingDistance.y; 
  }else{
    currSlidingPos = targetSlidingPos; // if speed is not set, don't move
  }  
}

inline
void SlidingInfo::correctSlidingDistance()
{
  if(0 == slidingSpeed.x && 0 != slidingDistance.x){
    sxLOG_E("sliding distance X is not zero: %d, but sliding speed is!", slidingDistance.x);
    slidingDistance.x = 0;
  }
  if(0 == slidingSpeed.y && 0 != slidingDistance.y){
    sxLOG_E("sliding distance Y is not zero: %d, but sliding speed is!", slidingDistance.y);
    slidingDistance.y = 0;
  }
}

inline
sxVec2Int SlidingInfo::getCurrentPos()
{
  return sxVec2Int(static_cast<sxInt32>(currSlidingPos.x),
                    static_cast<sxInt32>(currSlidingPos.y));
}

inline
void SlidingInfo::step(const sxFloat secondPerTicksPassed)
{
  if(direction.isLeftSet()){
    currSlidingPos.x += slidingSpeed.x * secondPerTicksPassed;
    if(currSlidingPos.x > targetSlidingPos.x) currSlidingPos.x = targetSlidingPos.x;
  }else if(direction.isRightSet()){
    currSlidingPos.x += slidingSpeed.x * secondPerTicksPassed;
    if(currSlidingPos.x < targetSlidingPos.x) currSlidingPos.x = targetSlidingPos.x;
  }
  if(direction.isDownSet()){
    currSlidingPos.y += slidingSpeed.y * secondPerTicksPassed;
    if(currSlidingPos.y > targetSlidingPos.y) currSlidingPos.y = targetSlidingPos.y;
  }else if(direction.isUpSet()){
    currSlidingPos.y += slidingSpeed.y * secondPerTicksPassed;
    if(currSlidingPos.y < targetSlidingPos.y) currSlidingPos.y = targetSlidingPos.y;
  }
}
