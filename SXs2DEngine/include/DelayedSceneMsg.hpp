#pragma once
#include "SceneMsg.hpp"

class DelayedSceneMsg
{
public:
  SceneMsg* msg;
  sxUInt32 framesToDelay;

  DelayedSceneMsg(SceneMsg* msg, const sxUInt32 delay) : msg(msg), framesToDelay(delay) {}
};