#pragma once
#include "FileLoaderBase.h"

class SceneLoadInfo
{
public:
  sxString sceneName;
  sxString sceneType;
  sxString filename;
};

class ActFileLoader : public FileLoaderBase
{
public:
  /// actName, sceneLoadInfo and windowOpacity are output variables, filled up during loading
  sxString actName;
  std::vector<SceneLoadInfo> sceneLoadInfo;
  /// sets the window's opacity connected to this scene. Not usually used for games, 
  /// but apps can benefit from it
  sxFloat windowOpacity;

  ~ActFileLoader();
  ReturnCodes processFile(const sxString& filename);
  INIReader* getReaderObject(){return reader;}

private:
  std::vector<sxString> sceneOrder;

  ReturnCodes processSections();
  ReturnCodes processConfigSection();
  ReturnCodes processScenes();
  ReturnCodes processOneSceneSection();
};

