#pragma once

template <typename AncestorType>
class AbstractFactory
{
public:
	virtual ~AbstractFactory() { }
	virtual AncestorType * CreateInstance() = 0;
};
