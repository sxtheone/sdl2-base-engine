#pragma once
#ifdef __MACOSX__
#include <SDL2/SDL_render.h>
#include <SDL2_image/SDL_image.h>
#else
#include <SDL_render.h>
#include <SDL_image.h>
#endif
#include "sxTypeDefs.h"

class sxTexture
{
public:
  sxTexture();
  virtual ~sxTexture();
  void loadTexture(SDL_Renderer*& renderer, const sxString& filename,
                   sxBool setPermanent = SDL_FALSE);
  void loadStreamingTexture(SDL_Renderer*& renderer, const sxString& filename,
                            sxBool setPermanent = SDL_FALSE);
  SDL_Texture* getTexture(){return texture;}
  sxInt32 getTextureWidth();
  sxInt32 getTextureHeight();
  sxBool isPermanent(){return permanent;}
  void freeTexture(); // only frees the texture what was loaded to this sxTexture with 'loadTexture()'

protected:
  SDL_Texture* texture;
  sxBool textureLoadedHere;
  sxBool permanent;

  void copySurfaceToTexture(SDL_Surface* surface);
};
