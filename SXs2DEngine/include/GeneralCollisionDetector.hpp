#pragma once
#include "CollisionDetectorBase.hpp"

class GeneralCollisionDetector : public CollisionDetectorBase
{
public:
  sxBool objectCanCollideWith(const Object* obj, const Object* otherObj);
  sxBool objectCollidesWith(Object* obj, Object* otherObj);
  sxBool objectCollidesWith(const sxVec2Int sceneOffset, const CollisionData* obj,
                            const sxVec2Int& buttonPos);

  //sxBool objectAllowedToAndCollidingWith(const Object* obj, const Object* otherObj);

private:
};

inline
sxBool GeneralCollisionDetector::objectCanCollideWith(const Object* obj, const Object* otherObj)
{
  return objectCanCollideWithOther(obj, otherObj);
}

inline
sxBool GeneralCollisionDetector::objectCollidesWith(Object* obj, Object* otherObj)
{
  //NOTE: we don't have the object index here but probably won't need it
  CollisionData objCollData = obj->getCollisionData();
  CollisionData otherObjCollData = otherObj->getCollisionData();
  return areActuallyColliding(&objCollData, &otherObjCollData);
}

inline
sxBool GeneralCollisionDetector::objectCollidesWith(const sxVec2Int sceneOffset,
                                                    const CollisionData* obj, 
                                                    const sxVec2Int& buttonPos)
{
  sxQuad hitbox1, hitbox2;
  sxVec2Int pos(buttonPos.x-sceneOffset.x, buttonPos.y-sceneOffset.y);
  buildCollisionQuad(obj, hitbox1);
  buildCollisionQuad(pos, hitbox2);
  return areHitboxesColliding(hitbox1, hitbox2);
}

/*
inline
sxBool GeneralCollisionDetector::objectAllowedToAndCollidingWith(const Object* obj,
                                                                 const Object* otherObj)
{
  if(SDL_TRUE == objectCanCollideWithOther(obj, otherObj)){
    if(SDL_TRUE == areActuallyColliding(obj, otherObj)){  
      return SDL_TRUE;
    }
  }
  return SDL_FALSE;
}*/