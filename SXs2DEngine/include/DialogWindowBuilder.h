#pragma once
#include <INIReader.h>
#include <Object.h>

class DialogWindowBuilder
{
public:
  ReturnCodes loadDialogWindow(INIReader* _reader, std::vector<Object*>*& _objects);
  void CreateDialogWindow(
        std::vector<Object*>*& _objects,
        sxVec2Real windowPos,
        sxVec2Real windowSize,
        sxColor colorTint,
        sxBool windowVisible,
        const std::vector<sxString>& straightObjects,
        const sxString centerObject,
        const std::vector<sxString>& cornerObjects
      );

private:
  INIReader* reader;
  std::vector<Object*>* objects;

  Object* newObject;
  sxVec2Real windowPos;
  sxVec2Real windowSize;
  sxColor colorTint {255,255,255,255};
  sxBool windowVisible;

  ReturnCodes fillBasicVariables();
  Object* createObject(const sxString& objectName);
  void createDialogStraightElements(const std::vector<sxString>& strs);
  void createDialogCenterElement(const sxString& centerName);
  void createDialogCornerElements(const std::vector<sxString>& strs);
  void fillAndPushDialogElement(const sxVec2Real& pos,
                                const sxVec2Real& scale = sxVec2Real(1, 1));
};

