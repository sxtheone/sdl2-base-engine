#pragma once
#include <map>
#include "sxEnumClasses.hpp"
#include "sxAnimSprite.h"

#define getSpriteManager SpriteManager::getInstance()

class SpriteManager
{
public:
  static void createManager();
  static SpriteManager* getInstance();
  ReturnCodes initialize(SDL_Renderer* renderer);
  void destroyManager();
  void loadTextureMap(const sxString& filename, const sxBool permanent, const sxBool streaming);
  void addSprite(const sxString& spriteName, sxSprite& sprite);
  void addAnimSprite(const sxString& animName, sxAnimSprite* animation);
  void addHitbox(const sxString& hitboxName, sxQuad& hitbox);
  sxTexture* getTextureMap(const sxString& id);
  sxSprite* getSprite(const sxString& id);
  sxAnimSprite* getAnimSprite(const sxString& id);
  sxQuad* getHitboxRef(const sxString& name);
  sxBool hitboxExists(const sxString& name);
  void destroyAllSpritesHitboxes();
  /// always delete the sprites before calling this!
  void destroyAllTextures(const TypesToRelease textureType);

private:
  static SpriteManager* spriteManager;
  std::map<sxString, sxTexture> textureMaps;
  std::map<sxString, sxSprite> sprites;
  std::map<sxString, sxQuad> hitboxes;
  std::map<sxString, sxAnimSprite*> animSprites;

  SDL_Renderer* mainRenderer;

  SpriteManager();
  ~SpriteManager();
  void destroyAnimSprites();
  void destroyNonPermanentTextures();
  void checkIfHitboxAlreadyInMap(const sxString& hitboxName, const sxQuad& hitbox);
};
