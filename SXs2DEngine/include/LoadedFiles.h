#pragma once
#include <vector>
#include "sxTypeDefs.h"

#define getLoadedFiles LoadedFiles::getInstance()

class LoadedFiles
{
public:
  LoadedFiles();
  ~LoadedFiles();
  static void init();
  void destroy();
  static LoadedFiles* getInstance();
  sxBool isFileAlreadyLoaded(const sxString& filename);
  void addToLoadedFilesList(const sxString& filename);
  void resetLoadedFilesList();
  void removeFilesContaining(const sxString& stringPart);

private:
  std::vector<sxString> filesLoaded;
  static LoadedFiles* loadedFiles;
};

