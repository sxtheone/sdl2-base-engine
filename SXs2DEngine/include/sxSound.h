#pragma once
#ifdef __MACOSX__
#include <SDL2_mixer/SDL_mixer.h>
#else
#include <SDL_mixer.h>
#endif
#include "sxCharStr.hpp"

static const sxInt32 SOUND_VOLUME_INVALID = -1;
static const sxInt32 SOUND_VOLUME_SILENT = 0;
static const sxInt32 SOUND_VOLUME_MAX = MIX_MAX_VOLUME;

static const sxInt32 SOUND_LOOPS_NONE = 0;
static const sxInt32 SOUND_LOOPS_INFINITE = -1;

class sxSound
{
public:
  sxSound(const sxCharStr& filenameToSet);
  ~sxSound();
  void    loadSoundToMemory();
  void    unloadSoundFromMemory();
  sxCharStr getFilename() const {return filename;}
  sxInt32 getVolume() const;
  void    setVolume(sxInt32 newVolume);
  void    setNumberOfLoops(sxInt32 numOfloops);
  sxInt32 getNumberOfLoops() const {return loops;}
  void    setPermanentFlag(const sxBool permanentFlag){permanent = permanentFlag;}
  sxBool  isPermanent() const {return permanent;}
  void    play();
  sxBool  isPlaying();

private:
  Mix_Chunk* sound;
  sxCharStr filename;
  sxInt32 loops;
  sxUInt8 volume;
  sxBool permanent;
  sxInt32 channel; // if not playing, it is -1
  // volume variable is needed to be able to set starting volume 
  // for sounds what are not loaded immediately
  void destroySound();
  void checkAndSetVolume(sxUInt8 incomingVolume);  
};
