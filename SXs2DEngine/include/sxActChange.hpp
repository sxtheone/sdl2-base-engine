#pragma once
#include "sxTypes.h"


class sxActChange
{
public:
  enum ChangeType{
    ADD_ON_TOP = 0,
    REMOVE_FROM_TOP,
    DESTROY_OTHERS,
    INVALID
  };

  ChangeType changeType;
  sxString targetActName;

  sxActChange() : changeType(INVALID) {}
};