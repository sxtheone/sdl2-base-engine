#pragma once
#include "Object.h"
#include "sxEnumClasses.hpp"

class ObjectDrawerBase
{
public:
  ObjectDrawerType type;
  sxVec2Int posOffset;
  std::vector<Object*> objects;

  virtual ~ObjectDrawerBase(){}
  void destroyObjects();
  virtual void objectMoved(Object* object){} // used by ObjectDrawerOrdered
  virtual void objectDeleted(const sxInt32 objectId){} // used by ObjectDrawerOrdered
  virtual void drawAll(SDL_Renderer* renderer) = 0;
  virtual void draw(const sxUInt32 objIdx, SDL_Renderer* renderer) = 0;
};

inline
void ObjectDrawerBase::destroyObjects()
{
  for(sxUInt32 i=0; i<objects.size(); ++i){
    delete objects[i];    
  }
  objects.clear();  
}