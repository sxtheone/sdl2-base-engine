#pragma once
#include <vector>
#include "BasicObject.h"
#include "GfxPrimitives.h"

#define getProfiler (Profiler::getInstance())

#ifdef DEVELOPER_HELPERS
class PerfCounter
{
public:
  enum PCState {TO_DELETE, NORMAL, PERMANENT};

  PCState state;
  sxUInt64 startTick;
  sxUInt64 stopTick;
  BasicObject* obj;

  PerfCounter(PCState _state = NORMAL){
    obj = NULL;
    state = _state;
    startTick = 0;
    stopTick = 0;
  }
  ~PerfCounter(){
    if(NULL != obj){
      delete obj;
    }
  }
};

class DebugDrawItem
{
public:
  sxQuadInt rect;
  sxColor color;
  sxUInt8 solid;
  DebugDrawItem(const sxQuadInt& r, const sxColor& c, const sxUInt8 s) : rect(r), color(c), solid(s){}
};

static const sxColor Color_Red = {255, 0, 0, 255};
static const sxColor Color_Green = {0, 255, 0, 255};
static const sxColor Color_Blue = {0, 0, 255, 255};
static const sxColor Color_PaleYellow = {200, 200, 0, 255};

class Profiler
{
public:
  static Profiler* getInstance();
  static void createProfiler();
  void destroyProfiler();
  void initialize(SDL_Renderer* mainRenderer);
  void showOnScreen(const sxBool show){showTextsOnScreen = show;} // disabled -> log writing
  void enableDebugText(const sxBool enable){debugTextEnabled = enable;} // also disables log writing
  void enableFPSCounter(const sxBool enable) {fpsCounterEnabled = enable;}
  sxBool isDebugTextEnabled(){return debugTextEnabled;}
  void startCountingFor(const sxString& name,
                        const PerfCounter::PCState state = PerfCounter::NORMAL);
  sxString stopCountingFor(const sxString& name); // returns with text printed on screen
  sxBool counterExists(const sxString name);
  void deleteCounter(const sxString& name);
  void writeToScreen(const sxString& text, const sxBool forced = SDL_FALSE);
  void setFPStoDraw(sxUInt32 timePassed);
  void setWriteLogs(sxBool writeTheLogs){writeLog = writeTheLogs;}
  void draw();
  void cycleEnded();
  void addToDrawDebugHitboxes(const sxRect& destinationRect, 
                              const sxQuad& hitbox, const sxColor& color,
                              const sxUInt8 solid = 0);
  void addToDrawDebugHitboxes(const sxVec2Real& position,
                              const sxQuad& hitbox,
                              const sxColor& color,
                              const sxUInt8 solid = 0);
  void activateDebugBoxDrawing(const sxBool toActive){drawDebugBoxes = toActive;}
  sxBool drawDebugBoxesActive(){return drawDebugBoxes;}

private:
  static Profiler* profiler;
  SDL_Renderer *renderer;
  BasicObject  *fpsToDraw;
  std::vector<sxUInt32> fpsValues; // gather some values and calculate average
  std::map<sxString, PerfCounter> objsToDraw;
  std::map<sxString, PerfCounter>::iterator objsIt;
  std::vector<DebugDrawItem> debugDraws;
  sxFloat profilerTextXcoord;

  sxBool       debugTextEnabled; // disables all debug text except fps counter
  sxBool       showTextsOnScreen;
  sxBool       fpsCounterEnabled;
  sxBool       writeLog;
  sxUInt64     cycleStartTick;
  sxVec2Real   posToDraw;
  sxUInt64     cyclesPassed;
  sxString     logString;
  sxBool       drawDebugBoxes;

  Profiler();
  ~Profiler();
  sxBool shouldDraw();
  void clearObjsToDraw();
  void eraseObjectAndSetObjsItToNextObject();
  void drawObject(BasicObject* obj);
  sxUInt64 getTicks();
  sxFloat getPassedTicksInMillisec(const sxUInt64& passedTicks);
  void fillObject(BasicObject*& obj, const sxString& text);
};

#else

class Profiler
{
public:
  static Profiler* getInstance();
  static void createProfiler();
  void destroyProfiler();
  void initialize(SDL_Renderer* mainRenderer){}
  void showOnScreen(sxBool show){}
  void enableDebugText(const sxBool enable){} // also disables log writing
  void enableFPSCounter(const sxBool enable){}
  void startCountingFor(const sxString& name){}
  void stopCountingFor(const sxString& name){}
  void setFPStoDraw(sxUInt32 timePassed){}
  void setWriteLogs(sxBool writeTheLogs){}
  void draw(){}
  void cycleEnded(){}
  void writeToScreen(const sxString& text, const sxBool forced = SDL_FALSE){}
  void addToDrawDebugHitboxes(const sxRect& destinationRect, 
                              const sxQuad& hitbox, const sxColor& color,
                              const sxUInt8 solid = 0){}

  void addToDrawDebugHitboxes(const sxVec2Real& position,
                              const sxQuad& hitbox,
                              const sxColor& color,
                              const sxUInt8 solid = 0){}
  void activateDebugBoxDrawing(const sxBool toActive){}
  sxBool drawDebugBoxesActive(){return SDL_FALSE;}

private:
  static Profiler* profiler;
};
#endif