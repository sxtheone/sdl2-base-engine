// Read an INI file into easy-to-access name/value pairs.

// inih and INIReader are released under the New BSD license (see LICENSE.txt).
// Go to the project home page for more info:
//
// http://code.google.com/p/inih/

// NOTE: the original code is heavyly modified by me.

#pragma once
#include <map>
#include "sxCharStr.hpp"


static const sxString assetDirectory = "resources/";

class INIReader
{
public:
  // Construct INIReader and parse given filename
  INIReader(const sxString& filename, sxBool checkIfFileAlreadyLoaded = SDL_TRUE);
  ~INIReader();

  sxString getOpenedFileName(){return openedFile;}
  ReturnCodes ParseError();

  sxBool   setSection(const sxString section, const sxBool mandatory = SDL_TRUE);
  sxString setFirstSection();
  sxString setNextSection();

  sxString getCurrentSectionName(){return currentSection;}
  sxUInt16 getNumberOfItemsInSection(){return static_cast<sxUInt16>(iniData.find(currentSection)->second.size());}
  sxBool   isItemExisting(sxString itemName);
  

  ReturnCodes getStr(const sxString& name, sxString& value, const sxBool mandatory = SDL_TRUE)
  {return getStrTemplate(name, value, mandatory);}
  ReturnCodes getStr(const sxString& name, sxCharStr& value, const sxBool mandatory = SDL_TRUE)
  {return getStrTemplate(name, value, mandatory);}
  ReturnCodes getFileNameWithPath(const sxString& name, sxString& value);
  ReturnCodes getStrList(const sxString& name, std::vector<sxString>& outputList,
                         const sxBool mandatory = SDL_TRUE, const sxChar separator = ',')
  {return getStrListTemplate(name, outputList, separator, mandatory);}
  ReturnCodes getStrList(const sxString& name, std::vector<sxCharStr>& outputList,
                         const sxBool mandatory = SDL_TRUE, const sxChar separator = ',')
  {return getStrListTemplate(name, outputList, separator, mandatory);}
  ReturnCodes getVec2Real(const sxString& name, sxVec2Real& value, 
                          const sxBool mandatory = SDL_TRUE);
  ReturnCodes getVec2Int(const sxString& name, sxVec2Int& value, 
                         const sxBool mandatory = SDL_TRUE);
  ReturnCodes getMatrix2D(const sxString& name, sxMatrix2D& value, 
                         const sxBool mandatory = SDL_TRUE); // row, col
  ReturnCodes getBoolean(const sxString& name, sxBool& value, const sxBool mandatory = SDL_TRUE);
  ReturnCodes getInteger(const sxString& name, sxInt32& value, const sxBool mandatory = SDL_TRUE);
  ReturnCodes getDouble(const sxString& name, sxDouble& value, const sxBool mandatory = SDL_TRUE);
  ReturnCodes getReal(const sxString& name, sxFloat& value, const sxBool mandatory = SDL_TRUE);
  ReturnCodes getColor(const sxString& name, sxColor& value, const sxBool mandatory = SDL_TRUE);
  ReturnCodes getColorList(const sxString& name, std::vector<sxColor>& outputList,
                           const sxBool mandatory = SDL_TRUE, const sxChar separator = ' ');
  ReturnCodes getIntList(const sxString& name, std::vector<sxInt32>& outputList,
                         const sxBool mandatory = SDL_TRUE, const sxChar separator = ',');  
  void convertStrToColor(const std::vector<sxString>& colorStr, sxColor& outColor);    
  static void cutString(const sxString& inputStr, const sxChar separator, std::vector<sxString>& output)
  {cutStrToStrListTemplate(inputStr, output, separator);}
  ReturnCodes stringToIntList(const sxString& str, const sxChar separator,
                              std::vector<sxInt32>& outVector);

private:
  sxString openedFile; // NOTE: this is just to log the filename when destroying reader instance
  sxString currentSection;
  sxUInt32 currentSectionIdx;
  ReturnCodes _error;
  std::map<sxString, std::map<sxString, sxString> > iniData;
  std::vector<sxString> sectionOrderInFile;

  sxBool fileShouldBeProcessed(const sxString& filename);
  ReturnCodes getVectorStrList(const sxString& name, std::vector<sxString>& vectorStrList,
                               const sxBool mandatory);
  ReturnCodes stringToDouble(const sxString& strValue, sxDouble& doubleValue);
  ReturnCodes stringToReal(const sxString& strValue, sxFloat& realValue);
  ReturnCodes stringToInt(const sxString& strValue, sxInt32& intValue);

  ReturnCodes selectedSectionValid();
  ReturnCodes fieldValid(const sxString& fieldName, const sxBool mandatory);
  ReturnCodes sectionAndFieldValid(const sxString& fieldName, sxBool fieldMandatory);
  ReturnCodes stringListToIntList(const std::vector<sxString>& strList,
                                  std::vector<sxInt32>& outList);

  template<typename SomeString>
  ReturnCodes getStrTemplate(const sxString& name, SomeString& value,
                             const sxBool mandatory);
  template<typename SomeString>
  ReturnCodes getStrListTemplate(const sxString& name, std::vector<SomeString>& outputList,
                                 const sxChar separator,
                                 const sxBool mandatory);
  template<typename SomeString>
  static void cutStrToStrListTemplate(const sxString& inputStr, std::vector<SomeString>& outputList,
                               const sxChar separator);

};

template<typename SomeString>
ReturnCodes INIReader::getStrTemplate(const sxString& name,
                                      SomeString& value,
                                      const sxBool mandatory)
{
  ReturnCodes result = sectionAndFieldValid(name, mandatory);
  if(ReturnCodes::RC_SUCCESS == result){
    value = iniData[currentSection][name];
    return ReturnCodes::RC_SUCCESS;
  }
  return result;
}

template<typename SomeString>
ReturnCodes INIReader::getStrListTemplate(const sxString& name,
                                          std::vector<SomeString>& outputList,
                                          const sxChar separator,
                                          const sxBool mandatory)
{
  ReturnCodes result = sectionAndFieldValid(name, mandatory);
  if(ReturnCodes::RC_SUCCESS != result){
    return result;
  }
  const sxString& originalNames = iniData[currentSection][name];
  cutStrToStrListTemplate(originalNames, outputList, separator);
  return ReturnCodes::RC_SUCCESS;
}

template<typename SomeString>
void INIReader::cutStrToStrListTemplate(const sxString& inputStr,
                                        std::vector<SomeString>& outputList,
                                        const sxChar separator)
{
  sxString oneName;
  size_t startIdx = 0;
  while(startIdx < inputStr.size()){
    size_t endIdx = inputStr.find_first_of(separator, startIdx);
    oneName = inputStr.substr(startIdx, endIdx-startIdx);

    trimLeadingSpaces(oneName);
    trimTrailingSpaces(oneName);
    outputList.push_back(oneName);

    if(sxString::npos != endIdx){
      startIdx = endIdx + 1;
      continue;
    }
    break;
  }
}
