#pragma once
#include "INIReader.h"
#include "SceneMsgID.hpp"

class SceneMsg
{
public:
  SceneMsgID type;
  sxBool broadcast;

  SceneMsg() : broadcast(SDL_FALSE){}
  virtual ~SceneMsg(){}
  virtual ReturnCodes loadData(INIReader* reader) = 0; /// the SceneMsg should load itself
  virtual SceneMsg& operator=(const SceneMsg& other){
    type = other.type;
    broadcast = other.broadcast;
    return *this;
  }
};

