#pragma once
#include <map>
#include <set>
#include "sxSound.h"
#include "sxEnumClasses.hpp"

#define getAudioManager (AudioManager::getInstance())

static const sxInt32 AUDIO_OPEN_FAILED = -1;
static const sxInt32 AUDIO_CHANNELS_MONO = 1;
static const sxInt32 AUDIO_CHANNELS_STEREO = 2;
static const sxUInt16 DEFAULT_AUDIO_FORMAT    = MIX_DEFAULT_FORMAT; // AUDIO_S16SYS

class AudioManager
{
public:
  static void createManager();
  static AudioManager* getInstance();
  void destroyManager();

  ReturnCodes initialize();
  sxBool isInitialized() const {return initialized;};

  void addSound(const sxCharStr& name, sxSound*& sound);
  /* playSound() puts strings into a queue and will play it when update() called */
  void playSound(const sxCharStr& name);
  sxBool isPlaying(const sxCharStr& name);
  void update();
  void pauseAll();
  void resumeAll();
  void unloadSound(const sxCharStr& name);
  void destroyAllSounds(TypesToRelease whatToRelease);

private:
  sxBool soundSystemPaused;
  static AudioManager* audioManager;
  std::map<sxCharStr, sxSound*> sounds;
  std::map<sxCharStr, sxSound*>::iterator soundsIterator;
  std::set<sxCharStr> soundsToPlay;
  sxBool initialized;
  
  AudioManager();
  ~AudioManager();
  void processPlayQueue(const sxCharStr& name);
  sxBool itemCanBeDeleted(const TypesToRelease whatToRelease, const sxSound* sound);
};
