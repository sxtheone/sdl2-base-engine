#pragma once
#include <vector>
#include "ReturnCodes.hpp"

/***
 SceneIDs:
 Stores all the Scene::id values rendered for the act.
 These sceneIDs are used for identification when removing Acts.
 Scene*-s with the Scene::id stored in the sceneIDs vector will be removed.
 Removing happens in ActManager.
***/
class SceneIDs
{
public:
  sxString actName;
  std::vector<sxUInt32> sceneIDs;
};

class ActInfo
{
public:
  ActInfo() : nextSceneID(0) {}
  sxUInt32 getNextSceneID(){return nextSceneID++;} // Scene::id field, not an idx in vector
  void removeSceneID(const sxUInt32 sceneID);
  sxUInt32 getNumberOfActs(){return acts.size();}
  SceneIDs* getActOfScene(const sxUInt32 sceneID);
  void pushAct(SceneIDs& act); /// places as the topmost act 
  void popAct(); /// removes the topmost Act Info
  void destroyAllActInfo();
  SceneIDs* getTopmostAct();

private:
  sxUInt32 nextSceneID;
  std::vector<SceneIDs> acts;
  std::vector<SceneIDs>::iterator actsIt;

  ReturnCodes checkSceneIDIntegrity(SceneIDs& act);
};
