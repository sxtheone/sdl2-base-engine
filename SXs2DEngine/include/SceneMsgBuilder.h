#pragma once
#include "FactoryHandler.hpp"
#include "GenericFactory.hpp"
#include "SceneMsg.hpp"

template <typename TConcrete>
class SceneMsgFactory : public GenericFactory<SceneMsg, TConcrete> { };

class SceneMsgBuilder : public FactoryHandler<const SceneMsgID, SceneMsg>
{
public:
  SceneMsgBuilder();
  SceneMsg* createInstance(const SceneMsgID& id);
  SceneMsg* createInstance(const sxCharStr idStr);

private:
  void registerBasicTypes();
};
