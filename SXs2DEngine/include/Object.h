#pragma once
#ifdef __MACOSX__
#include <SDL2/SDL_render.h>
#else
#include <SDL_render.h>
#endif

#include "ObjectBase.h"

class Object : public ObjectBase
{
public:
  Object();
  virtual ~Object(){}
  virtual Object& operator=(const Object& other);
  virtual void setPosition(const sxVec2Real& newPos);
  virtual void addToPosition(const sxVec2Real& posToAdd);
  sxVec2Real  getPosition() const {return realPosition;}
  void       setVelocity(const sxVec2Real& newVelocity);
  void       addToVelocity(const sxVec2Real& velocityToAdd){velocity += velocityToAdd;}
  sxVec2Real getVelocity() const {return velocity;}
  void       setScale(const sxFloat newScale);
  void       setScale(const sxVec2Real& newScale);
  void       setSpriteFlip(SDL_RendererFlip newFlip);
  SDL_RendererFlip getSpriteFlip(){return spriteFlip;}
  virtual ReturnCodes loadData(INIReader* reader);
  virtual void draw(SDL_Renderer* renderer); // just error checking and call of callDrawMethodAndModifiers()
  void simpleDraw(SDL_Renderer* renderer); // uses SDL_RenderCopy
  virtual void drawOffset(SDL_Renderer* renderer, const sxVec2Int& posOffset); // draw with an offset "without" changing Object position
  virtual void stepAnimation(const sxUInt32 ticksPassed);
  virtual CollisionData getCollisionData();
  /// gives back hitbox+object position+additional offset
  sxQuad getHitBoxWithPositionOffset(const sxVec2Int& additionalOffset);

protected:
  sxVec2Real spriteScale;
  sxVec2Real velocity;
  sxVec2Real realPosition;
  sxFloat    secondPerTicksPassed;
  SDL_RendererFlip spriteFlip;

  virtual void assign(const ObjectBase& source);
  void fillDestinationRectPositionFromRealPosition();
  virtual void callDrawMethodAndModifiers(SDL_Renderer*& renderer) = 0;

private:
  void loadSpriteFlipFields(INIReader* reader);
};
