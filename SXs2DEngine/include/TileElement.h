#pragma once
#include "ObjectBase.h"

class TileElement : public ObjectBase
{
public:
  TileElement();
  TileElement(sxSprite* newSprite);
  virtual ~TileElement(){}
  void setSprite(sxSprite* newSprite);
  virtual void stepAnimation(const sxUInt32 ticksPassed){}
  virtual ReturnCodes loadData(INIReader* reader);
protected:
  virtual void assign(const ObjectBase& source);
};
