#pragma once
#include "FileLoaderBase.h"
#include "SceneProps.hpp"

class SceneFileLoaderBase : public FileLoaderBase
{
public:
  SceneFileLoaderBase(const sxUInt32 sceneID) : sceneIDinAct(sceneID), hasCollisionInfo(SDL_FALSE) {}
  sxString getSceneName(){return sceneName;}
  SceneProps getSceneProps(){return sceneProps;}
  INIReader* getReaderObject();
  void closeFile(){destroyReader();};
  sxBool isCollisionInfoPresent(){return hasCollisionInfo;}

protected:
  sxString sceneName;
  sxUInt32 sceneIDinAct;
  sxBool hasCollisionInfo;
  SceneProps sceneProps;

  void processBasicSceneInfo();

private:
  void processSceneCollisionInfo();
  void processSceneProps();
};
