#pragma once
#include <Object.h>
#include "SceneFileLoaderBase.h"
#include "TileElement.h"
#include "TileMapHandler.hpp"


class TiledSceneFileLoader : public SceneFileLoaderBase
{
public:
  TiledSceneFileLoader(const sxUInt32 sceneID,
                       std::vector<ObjectBase*>& _tiles, 
                       std::vector<Object*>& _objectTiles,
                       TileMapHandler& _mapHandler);
  ~TiledSceneFileLoader(){destroyReader();}
  virtual ReturnCodes processFile(const sxString& filename);
  sxVec2Int getSizeOfBiggestHitbox(){return biggestHitbox;}

protected:
  TiledSceneFileLoader(const sxUInt32 sceneID) : SceneFileLoaderBase(sceneID){} // just to let the child classes instantiate without setting tiles, objectTiles, mapHandler
  virtual ReturnCodes processSections();
  ReturnCodes processConfigSection();
  ReturnCodes processTilesSection();
  ReturnCodes processMapSections();
  void addToTilesVector(const sxString& spriteName, const sxUInt16 spriteNumber);

  std::vector<ObjectBase*>* tiles;
  std::vector<Object*>* objectTiles;
  TileMapHandler* mapHandler;

private:
  std::vector<sxString> mapOrder;
  std::map<sxString, sxUInt16> tileSpriteData; // name, number
  sxVec2Int biggestHitbox;
  RawTileMap currentMap;
  sxMatrix2D sizeOfCurrentMap;
  
  sxBool getSpriteNumber(const sxString& tileSpriteInfo, sxUInt16& spriteNumber);
  sxBool getSpriteName(const sxString& tileSpriteInfo, sxString& spriteName);
  void addToTileSpritesData(const sxString& spriteName, const sxUInt16 spriteNumber);
  void processOneTileSprite(const sxString& tileSpriteInfo);
  void loadTileSprites();
  void resizeAndInitToZero(const sxUInt32 desiredSize, std::vector<sxInt16>& vector);
  sxBool putSpriteToTilesVector(const sxString& spriteName, const sxUInt16 spriteNumber);
  void putObjectToObjectTilesIfNeeded(ObjectBase* object);
  void updateBiggestHitboxIfNeeded(ObjectBase* object);
  void checkIfEveryItemEqualsTo(const std::vector<sxInt16>& vector, const sxInt16 value);
  void processOneMapSection();
  void buildMap(const sxUInt32 numOfItemsInRow);
  void putItemToMap(const sxString& valueString, std::vector<sxUInt32>& row);
  sxBool mapSizeOK();
  void fillNumOfRepeatsField();
  void setBiggestHitbox(const sxQuad* hitBox);
};
