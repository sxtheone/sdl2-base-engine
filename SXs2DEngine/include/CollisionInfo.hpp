#pragma once
#include "ObjectTypes.hpp"


class ObjectBase;

class CollidingObject
{
public:
  ObjectBase* object{nullptr};
  sxVec2Real pixelPosition;
};

class CollisionInfo
{
public:
  sxUInt32 objectIndex{ std::numeric_limits<sxUInt32>::max() };
  CollidingObject otherObject;
};

class SmallCollisionInfo
{
public:
  sxUInt32 playerIdx;
  sxQuad otherObjHitbox;

  SmallCollisionInfo() : playerIdx(0) {}
  SmallCollisionInfo(const sxUInt32 idx, const sxQuad& hitbox) : playerIdx(idx),
                                                                 otherObjHitbox(hitbox) {}
};