/**
ObjectCollisionPropsProcessor:
- the Collision Detector works with bitmasks. ObjectCollisionPropsProcessor generates the bitmasks
- The base of the CollisionID generation can be the Object's name 
  or the CollisionGroupID give in the .objects file
- if you are absolutely sure that no other Acts will be loaded before deleting ALL the current Acts,
  call clearCollisionProperties(). This will release all the data and gives back memory but if you
  add an Act after calling it, collision detection will go nuts
**/
#pragma once
#include <map>
#include "sxCharStr.hpp"

#define getObjectCollisionPropsProcessor ObjectCollisionPropsProcessor::getInstance()

class RawCollisionData
{
public:
  sxCharStr collisionID;
  std::vector<sxCharStr> collidesWith;
};

class ObjectCollisionSettings
{
public:
  std::map<sxCharStr, RawCollisionData> rawMap;
  std::map<sxCharStr, CollisionProps> finalMap;
  std::map<sxCharStr, sxUInt32> finalCollisionIDs;
  CollisionProps nextCollisionID;

  void init(){
    rawMap.clear();
    finalMap.clear();
    finalCollisionIDs.clear();
    nextCollisionID.collisionID = 1;
  }
};

class ObjectCollisionPropsProcessor
{
public:
  static void createManager();
  static ObjectCollisionPropsProcessor* getInstance();
  void destroyManager();

  void addToObjectCollisionMap(const sxCharStr& collisionID,
                               const sxCharStr& objectName,
                               const std::vector<sxCharStr>& collidesWith);
  void processCollisionSettings(); // call before getCollisionPropsForScene()
  void clearCollisionProperties();

private:
  static ObjectCollisionPropsProcessor* objCollisionPropsProcessor;
  ObjectCollisionSettings objectSettings;
  std::map<sxCharStr, RawCollisionData>::iterator rawIt;
  std::map<sxCharStr, CollisionProps>::iterator propsIt;

  ObjectCollisionPropsProcessor();
  ~ObjectCollisionPropsProcessor();
  void generateFinalCollisionIDs();
  void fillCollisionIDFields();
  void fillCollidesWithFields();
  void addCollisionPropsToObjects();
  void processObjectsCollisionSettings();
};