#pragma once
#include "ObjectBuilder.h"


#define getObjectManager ObjectManager::getInstance()

class ObjectManager
{
public:
  static void createManager();
  static ObjectManager* getInstance();
  void destroyManager();
  
  void registerObjectType(const sxCharStr& id, AbstractFactory<ObjectBase>* type);
  ObjectBuilder* getObjectBuilder(){return &objectBuilder;}
  ReturnCodes addObject(const sxString& objectName, ObjectBase* object);
  void setObjectCollisionProps(const sxString& objName, CollisionProps& collProps);

  sxCharStr getObjectType(const sxString& objectName);
  ObjectBase* createObject(const sxString& objectName); /// receiver should delete the object!!
  void destroyAllObjects();

private:
  ObjectBuilder objectBuilder;
  static ObjectManager* objectManager;
  std::map<sxString, ObjectBase*> objects;
  std::map<sxString, ObjectBase*>::iterator objectsIterator;

  ObjectManager();
  ~ObjectManager();
  void destroyObject(ObjectBase* obj);
  sxBool setObjectIteratorTo(const sxString& objectName);
  ObjectBase* createObjectInstance(); /// use objectsIterator so set it before calling!
};

