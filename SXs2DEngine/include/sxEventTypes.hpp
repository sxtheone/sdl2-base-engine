#pragma once
#include "LogManager.h"

class sxEventTypes
{
public:
  // compatible with SDL_EventType enum
  enum EventTypes{
    INVALID,
    QUIT = 0x100,

    APP_TERMINATING = SDL_APP_TERMINATING,
    APP_LOW_MEMORY = SDL_APP_LOWMEMORY,
    APP_WILL_ENTER_BCKGRND = SDL_APP_WILLENTERBACKGROUND,
    APP_DID_ENTER_BCKGRND = SDL_APP_DIDENTERBACKGROUND,
    APP_WILL_ENTER_FOREGRND = SDL_APP_WILLENTERFOREGROUND,
    APP_DID_ENTER_FOREGRND = SDL_APP_DIDENTERFOREGROUND,

    /* user's locale preferences */
    LOCALECHANGED = SDL_LOCALECHANGED,
    
    /* Display events */
    DISPLAYEVENT = SDL_DISPLAYEVENT,
    
    /* Window events */
    WINDOW_EVENT = SDL_WINDOWEVENT,
    SYS_WM_EVENT = SDL_SYSWMEVENT,

    /* Keyboard events */
    KEY_DOWN = SDL_KEYDOWN,
    KEY_UP = SDL_KEYUP,
    TEXTEDITING = SDL_TEXTEDITING,
    TEXTINPUT = SDL_TEXTINPUT,

    /* Mouse events */
    MOUSE_MOTION = SDL_MOUSEMOTION,
    MOUSE_BUTTON_DOWN = SDL_MOUSEBUTTONDOWN,
    MOUSE_BUTTON_UP = SDL_MOUSEBUTTONUP, 
    MOUSE_WHEEL = SDL_MOUSEWHEEL,

    /* Joystick events */
    JOY_AXIS_MOTION = SDL_JOYAXISMOTION,
    JOY_BALL_MOTION = SDL_JOYBALLMOTION,
    JOY_HAT_MOTION = SDL_JOYHATMOTION,
    JOY_BUTTON_DOWN = SDL_JOYBUTTONDOWN,
    JOY_BUTTON_UP = SDL_JOYBUTTONUP,
    JOY_DEVICE_ADDED = SDL_JOYDEVICEADDED,
    JOY_DEVICE_REMOVED = SDL_JOYDEVICEREMOVED,

    /* Game controller events */
    CONTROLLER_AXIS_MOTION = SDL_CONTROLLERAXISMOTION,
    CONTROLLER_BUTTON_DOWN = SDL_CONTROLLERBUTTONDOWN,
    CONTROLLER_BUTTON_UP = SDL_CONTROLLERBUTTONUP,
    CONTROLLER_DEVICE_ADDED = SDL_CONTROLLERDEVICEADDED,
    CONTROLLER_DEVICE_REMOVED = SDL_CONTROLLERDEVICEREMOVED,
    CONTROLLER_DEVICE_REMAPPED = SDL_CONTROLLERDEVICEREMAPPED,

    /* Touch events */
    TOUCH_FINGER_DOWN = SDL_FINGERDOWN,
    TOUCH_FINGER_UP = SDL_FINGERUP,
    TOUCH_FINGER_MOTION = SDL_FINGERMOTION,

    /* Gesture events */
    TOUCH_DOLLAR_GESTURE = SDL_DOLLARGESTURE,
    TOUCH_DOLLAR_RECORD = SDL_DOLLARRECORD,
    TOUCH_MULTIGESTURE = SDL_MULTIGESTURE,

    /* Clipboard events */
    CLIPBOARDUPDATE = SDL_CLIPBOARDUPDATE, /**< The clipboard changed */
    /* Drag and drop events */
    DROPFILE = SDL_DROPFILE, /**< The system requests a file open */
    
    AUDIODEVICEADDED = SDL_AUDIODEVICEADDED, /**< A new audio device is available */
    AUDIODEVICEREMOVED = SDL_AUDIODEVICEREMOVED,

    RENDER_TARGETS_RESET = SDL_RENDER_TARGETS_RESET, /**< The render targets have been reset and their contents need to be updated */
    RENDER_DEVICE_RESET = SDL_RENDER_DEVICE_RESET, /**< The device has been reset and all textures need to be recreated */

    USER_EVENT = SDL_USEREVENT,
    
    /* own defines. Place for 100 events */
    /*TODO: if the SDL_RegisterEvent() gets implemented in EventManager,
    take care not to let these values overridden!*/
    ANY_KIND_OF_USER_INPUT = 0xFF9A,
    CHANGE_ACT,
    WINDOW_OPACITY,
    RESIZE_WINDOW,
    LASTEVENT    = 0xFFFF
  };

  sxEventTypes(){value = INVALID;}
  sxEventTypes(sxInt32 i){ assign(i); }
  operator sxInt32() const {return value;}
  sxEventTypes& operator=(const sxEventTypes& v) {
      value = v.value;
      return *this;
  }


private:
  EventTypes value;
  void assign(sxInt32 i)
  {
    if(i > INVALID && i < LASTEVENT){
      value = static_cast<EventTypes>(i);
    }else{
      sxLOG_E("Value out of bounds!");
    }
  }
};