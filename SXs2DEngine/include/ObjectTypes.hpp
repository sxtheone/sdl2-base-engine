#pragma once
#include "sxCharStr.hpp"

//TODO: shorter ObjectTypes strings. 'Object' is unnecessary. Correct them in the ini files also.
namespace ObjectTypes{
  const sxCharStr BasicObject("BasicObject");
  const sxCharStr AnimObject("AnimObject");
  const sxCharStr TileElement("TileElement");
  const sxCharStr ButtonObject("ButtonObject");
  const sxCharStr MenuButton("MenuButton");
};
