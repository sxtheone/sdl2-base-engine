#pragma once
#include "RawTileMap.hpp"

class TileMapInfo
{
public:
  RawTileMap* map;
  sxUInt32 mapIndex;
  sxUInt32 numOfItems;

  TileMapInfo(){init();}
  void setCurrItemIndex(const sxMatrix2D& newValue);
  void setCurrItemIndexRow(const sxUInt16 newValue);
  void setCurrItemIndexCol(const sxUInt16 newValue);
  sxBool incCurrItemIndexCol();
  sxBool incCurrItemIndexRow();

  sxUInt32 getCurrItem(){return *currItemIt;}//return map->getItem(currItemIndex);}
  sxUInt32 getNumberOfRows(){return map->numberOfRows();}
  sxUInt32 getNumberOfColumns(){return map->numberOfItemsInARow();}

private:
  sxMatrix2D currItemIndex;
  std::vector<sxUInt32>::iterator currItemIt;

  void init()
  {
    map = NULL;
    mapIndex = 0;
    numOfItems = 0;
    currItemIndex = sxMatrix2D();
    //currItemIt = map->getMapRowBeginIterator(currItemIndex.row);
  }
};

inline
void TileMapInfo::setCurrItemIndex(const sxMatrix2D& newValue)
{
  currItemIndex = newValue;
  currItemIt = map->getMapRowBeginIterator(currItemIndex.row);
  currItemIt += currItemIndex.col;
}

inline
void TileMapInfo::setCurrItemIndexRow(const sxUInt16 newValue)
{
  currItemIndex.row = newValue;
  currItemIt = map->getMapRowBeginIterator(currItemIndex.row);
  currItemIt += currItemIndex.col;
}

inline
void TileMapInfo::setCurrItemIndexCol(const sxUInt16 newValue)
{
  currItemIndex.col = newValue;
  currItemIt = map->getMapRowBeginIterator(currItemIndex.row);
  currItemIt += currItemIndex.col;
}

inline
sxBool TileMapInfo::incCurrItemIndexCol()
{
  ++currItemIndex.col;
  if(currItemIndex.col >= numOfItems){
    currItemIndex.col = 0;
    currItemIt = map->getMapRowBeginIterator(currItemIndex.row);
    return SDL_FALSE;
  }
  ++currItemIt;

  return SDL_TRUE;
}

inline
sxBool TileMapInfo::incCurrItemIndexRow()
{
  ++currItemIndex.row;
  currItemIndex.col = 0;
  if(map->numberOfRows() <= currItemIndex.row){
    currItemIndex.row = 0;
    currItemIt = map->getMapRowBeginIterator(currItemIndex.row);
    return SDL_FALSE;
  }
  currItemIt = map->getMapRowBeginIterator(currItemIndex.row);
  return SDL_TRUE;
}
