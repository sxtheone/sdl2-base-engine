#pragma once
#include "sxEnumClasses.hpp"

class ResourceReleaser
{
public:
  static void release(const WhatToRelease toRelease);

private:
  static void removeObjectsRelated(const TypesToRelease typesToRelease);
  static void removeSpritesRelated();
  static void removeTextureRelated(const WhatToRelease toRelease);
  static TypesToRelease setTypesToRelease(const WhatToRelease toRelease);
};
