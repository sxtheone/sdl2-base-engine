#pragma once
#ifdef __MACOSX__
#include <SDL2/SDL_render.h>
#else
#include <SDL_render.h>
#endif

#include "INIReader.h"
#include "ObjectProps.hpp"
#include "CollisionInfo.hpp"
#include "sxEvent.hpp"
#include "sxSprite.h"

class ObjectBase;

class CollisionData
{
public:
  sxUInt32 objectIndex;
  ObjectBase* object;
  sxVec2Real position;

  CollisionData() : object(NULL), objectIndex(-1) {}
};

class ObjectBase
{
public:
  sxCharStr type;
  sxUInt16 idTag; // an id for user for better identification 
  ObjectProps objectProps;
  ObjCollisionProps* collisionProps;

  ObjectBase();
  virtual ~ObjectBase();
  virtual ObjectBase& operator=(const ObjectBase& other);
  void setupCollisionRelatedData(sxBool collisionPossible);
  void setNewHitbox(sxQuad*& newHitbox); // newHitbox won't be deleted here!
  void setSize(const sxVec2Int size);
  sxInt32 getWidth() const {return destinationRect.w;}
  sxInt32 getHeight() const {return destinationRect.h;}
  sxVec2Int getSpriteSize(){return sxVec2Int(destinationRect.w, destinationRect.h);}
  sxVec2Int getHitboxSize() const;
  void incIdleTimePassed(sxUInt32 timeToAdd);
  sxBool idleTimePassed() const;
  void setIdleTimePassed(sxUInt32 newTime){timePassedSinceLastCollision = newTime;}
  void setColorModifier(const sxColor& colorMod); /// srcColor = srcColor * (color / 255)
  sxColor getColorModifier(){return colorModifier;}
  virtual ReturnCodes loadData(INIReader* reader);
  /// It is not mandatory to fill objectHit() functions
  //  If sg collides with object, objectHit() will be called
  virtual void objectHit(CollisionInfo& collInfo){}
  virtual void objectHit(SmallCollisionInfo& smallCollInfo){}
  virtual void objectHit(sxEvent& eventHappened){}
  virtual void draw(SDL_Renderer* renderer, const sxVec2Int& position);
  virtual CollisionData getCollisionData();
  sxQuad getHitBoxWithOffset(const sxVec2Real& posOffset, 
                             const sxVec2Int& additionalOffset = sxVec2Int());

protected:
  sxSprite*  activeSprite;
  sxRect     destinationRect;
  sxColor    colorModifier;
  sxUInt32   timePassedSinceLastCollision;
  SDL_Point* rotationCenter;
  sxDouble   rotationInDegree;

  virtual void assign(const ObjectBase& source);
  virtual void setHitbox();
  sxBool drawingEnabled() const;
  sxBool objectTypeIs(const sxCharStr& typeToCheck, sxCharStr desiredType);
  void activateColorModifierChanges(); // call before rendering in draw()
  void deactivateColorModifierChanger(); // call after rendering in draw()
};
