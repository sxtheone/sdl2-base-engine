#pragma once
#include "LogManager.h"

template<typename EnumInClass>
class EnumBasedType
{
public:
  EnumBasedType(){ value = EnumInClass::INVALID; }
  EnumBasedType(sxInt8 i) {
    value = EnumInClass::INVALID;
    assign(i); 
  }

  EnumBasedType& operator=(const EnumBasedType& v) {
      value = v.value;
      return *this;
  } 
  operator sxInt32() const { return value; }

private:
  sxUInt8 value;

  void assign(sxInt8 i)//TODO: sxUInt8
  {
    if(i <= EnumInClass::INVALID){
      value = static_cast<sxUInt8>(i);
    }else{
      sxLOG_E("Value out of Bounds: %d", i);
    }
  }
};

class sxFontQualityEnum
{
public:
  enum{
    FAST = 0,
    NICE,
    NICE_WITH_BOX,
    INVALID
  };
};
typedef EnumBasedType<sxFontQualityEnum> sxFontQuality;

class TypesToReleaseEnum
{
public:
  enum{
    NORMAL_ITEMS_ONLY = 0,
    PERMANENT_ITEMS_ALSO,
    INVALID
  };
};
typedef EnumBasedType<TypesToReleaseEnum> TypesToRelease;

class WhatToReleaseEnum
{
public:
  enum{
    ONLY_OBJECTS = 0,
    SPRITES_ALSO,
    TEXTURES_ALSO,
    PERMANENT_ALSO, // all the things above will be released
    INVALID
  };
};
typedef EnumBasedType<WhatToReleaseEnum> WhatToRelease;

class SceneTypeEnum
{
public:
  enum {
    OBJECT = 0,
    STATIC_TILED,
    SCROLLABLE_TILED,
    SIMPLE_SCENE,
    INVALID 
  };
};
typedef EnumBasedType<SceneTypeEnum> SceneType;

class TiledSceneDrawMethodEnum
{
public:
  enum {
    FULLREDRAW = 0,
    SCROLLHAPPENED,
    SCROLLHAPPENED_POS_DIRECTION,
    SCROLLHAPPENED_NEG_DIRECTION,
    UPDATE_TILES_AND_WHOLE_GRID,
    UPDATETILES,
    INVALID
  };
};
typedef EnumBasedType<TiledSceneDrawMethodEnum> TiledSceneDrawMethod;

class ObjectDrawerDrawMethodEnum
{
public:
  enum {
    FULLREDRAW = 0,
    UPDATE_NO_MOVEMENT, // don't use if objects move because prev pos is not checked
    INVALID
  };
};
typedef EnumBasedType<ObjectDrawerDrawMethodEnum> ObjectDrawerDrawMethod;

class ObjectDrawerTypeEnum
{
public:
  enum {
    NORMAL = 0,
    POS_OFFSET,
    RENDER_TO_TEXTURE,
    ORDERED,
    INVALID
  };
};
typedef EnumBasedType<ObjectDrawerTypeEnum> ObjectDrawerType;






