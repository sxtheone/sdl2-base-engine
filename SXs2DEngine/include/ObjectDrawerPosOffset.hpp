#pragma once
#include "ObjectDrawerBase.hpp"
#include "Profiler.h"

class ObjectDrawerPosOffset : public ObjectDrawerBase
{
public:
  ObjectDrawerPosOffset()
  {
    init();
  }
  
  ObjectDrawerPosOffset(const sxVec2Int& offset)
  {
    init();
    posOffset = offset;
  }

  void setPositionOffset(const sxVec2Int& offset){posOffset = offset;}

  virtual void drawAll(SDL_Renderer* renderer)
  {
    sxUInt32 numOfObjs = objects.size();
    for(sxUInt32 i=0; i<numOfObjs; ++i){
      objects[i]->drawOffset(renderer, posOffset);
    }
  }
  
  virtual void draw(const sxUInt32 objIdx, SDL_Renderer* renderer)
  {
    objects[objIdx]->drawOffset(renderer, posOffset);
  }

private:
  void init()
  {
    type = ObjectDrawerTypeEnum::POS_OFFSET;
    posOffset = sxVec2Int(0,0);
  }
};