#pragma once
#include "AbstractFactory.hpp"

template<typename AncestorType, typename ConcreteType>
class GenericFactory : public AbstractFactory<AncestorType>
{
public:
	virtual AncestorType* CreateInstance() { return new ConcreteType(); }
};
