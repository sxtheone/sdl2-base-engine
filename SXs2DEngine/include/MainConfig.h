#pragma once
#include "ConfigFileLoader.h"

#define getMainConfig MainConfig::getInstance()

static const sxString DEFAULT_WINDOW_NAME = "SX's 2D Engine Test";
static const sxVec2Int DEFAULT_LOGICAL_WINDOW_SIZE = sxVec2Int(1280, 720);
static const sxVec2Int DEFAULT_REAL_WINDOW_SIZE = sxVec2Int(640, 480);
static const sxUInt32 DEFAULT_WINDOW_TYPE = SDL_WINDOW_FULLSCREEN;
static const sxUInt16 DEFAULT_DESIRED_FRAMES_PER_SECONDS = 60;
static const sxString DEFAULT_SPRITE_SCALING_METHOD = "nearest";
static const sxUInt16 DEFAULT_AUDIO_FREQUENCY = 22050;
static const sxUInt8 DEFAULT_AUDIO_CHANNELS = 2;
static const sxUInt16 DEFAULT_AUDIO_CHUNKSIZE = 4096;
#ifdef __sdlANDROID__
static const sxBool DEFAULT_LETTERBOX = SDL_FALSE;
#else
static const sxBool DEFAULT_LETTERBOX = SDL_TRUE;
#endif

class ResolutionChanger
{
public:
  sxBool resolutionChangeSet;
  sxVec2Int logicalSize;
  sxVec2Int screenSize;
};

class MainConfig
{
public:
  static void setResolutionChange(const sxVec2Int& screenSize, const sxVec2Int& logicalSize);
  static void unsetResolutionChange(){resolutionChanger.resolutionChangeSet = SDL_FALSE;}
  static sxBool isResolutionChangeSet(){return resolutionChanger.resolutionChangeSet;}

  static void createManager(const sxString& filename);
  static MainConfig* getInstance();
  void destroyManager();

  sxColor getBackroundColor(){return backgroundColor;}
  sxVec2Int getWindowLogicalSize(){return windowLogicalSize;}
  sxVec2Int getWindowScreenSize(){return windowScreenSize;}
  void setWindowScreenSize(const sxVec2Int newScreenSize){windowScreenSize = newScreenSize;}
  sxUInt16  getDesiredFPS(){return desiredFPS;}
  sxBool    isDefaultDesiredFPSSet(){return desiredFPS == defaultDesiredFPS ? SDL_TRUE : SDL_FALSE;}
  void      setDesiredFPS(sxUInt16 fps);
  void      setDesiredFPSToDefault(){setDesiredFPS(defaultDesiredFPS);}
  sxDouble  getDesiredFrameStep(){return desiredFrameStep;}
  sxBool    isLetterboxed(){return letterbox;}
  sxString getWindowName(){return windowName;}
  sxString getSpriteScalingMethod(){return spriteScalingMethod;}
  sxUInt32 getWindowType() { return windowType; }
#ifdef _WIN32
  void setWindowPixelFormatVariable(SDL_PixelFormat* windowPF){windowPixelFormat = windowPF;}
  SDL_PixelFormat* getWindowPixelFormat(){return windowPixelFormat;}
#endif

  sxUInt16 getAudioFrequency(){return frequency;}
  sxUInt8 getAudioNumberOfChannels(){return channels;}
  sxUInt16 getAudioChunkSize(){return chunkSize;}

  sxString getStartingActFilename(){return startingActFilename;}

private:
  static MainConfig* mainConfig;
  static ResolutionChanger resolutionChanger;
  sxColor    backgroundColor;
  sxVec2Int  windowLogicalSize;
  sxVec2Int  windowScreenSize;
  sxUInt16   defaultDesiredFPS;
  sxUInt16   desiredFPS;
  sxDouble   desiredFrameStep;
  sxBool     letterbox;
  sxUInt32   windowType;
  sxString   windowName;//TODO: check if sxCharStr (with the conversions from/to sxString) worth the change
  sxString   spriteScalingMethod;
#ifdef _WIN32
  SDL_PixelFormat* windowPixelFormat;
#endif
  // audio config
  sxUInt16   frequency;
  sxUInt8    channels;
  sxUInt16   chunkSize;
  // startup
  sxString   startingActFilename;

  MainConfig(const sxString& filename);
  ~MainConfig(){}
  void fillDefaultValues();
  void getBaseConfigValues(ConfigFileLoader& loader);
  void setResolution(ConfigFileLoader& loader);
  void getAudioConfigValues(ConfigFileLoader& loader);
  void getStartupConfigValues(ConfigFileLoader& loader);
};
