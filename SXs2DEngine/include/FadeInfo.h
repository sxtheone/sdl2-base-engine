#pragma once
#include "ObjectDrawerBase.hpp"

class FadeInfo
{
public:
#define COLOR_MAX_VALUE 255
  enum FadeType{FADE_IN, FADE_OUT};

  FadeInfo() : fadeType(FADE_OUT), timeStep(0.05), active(SDL_FALSE), preciseAlpha(COLOR_MAX_VALUE) {}
  void setup(const FadeType type, const sxInt32 fadeTime);
  void startFade();
  sxBool fadeInProgress(){return active;}
  void stepFade(const sxUInt32 ticksPassed, ObjectDrawerBase* drawer);

private:
  FadeType fadeType;
  sxDouble timeStep;
  sxBool active;
  sxDouble preciseAlpha;

  void setupPreciseAlpha();
  void stepFadeIn(const sxUInt32 ticksPassed);
  void stepFadeOut(const sxUInt32 ticksPassed);
};
