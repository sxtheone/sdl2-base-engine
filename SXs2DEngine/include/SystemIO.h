#pragma once
#include <vector>
#include <ctime>
#include "ReturnCodes.hpp"


class SystemIO
{
public:
  static ReturnCodes createDirectory(const sxString& nameWithPath);
  static sxBool createDirInBasePath(const sxString dir, sxString& outDirPath);
  static ReturnCodes getFileListInDir(const sxString& path, const sxString& extension,
                                      std::vector<sxString> &files);

#ifdef __sdlANDROID__
  static void callJavaMethod(sxString methodName);
#endif
  // not sure this should be here
  static sxString getFullCurrentTimeAsStr(); // year_month_day_hour_min_sec
  static sxString getCurrentSystemTime();
  static std::time_t getCurrentSystemTimeInTime_t(){return std::time(NULL);}

};

