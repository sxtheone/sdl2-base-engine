#pragma once
#include "SceneMsgTypes.hpp"

class SceneMsgCloseScene: public SceneMsg
{
public:
  sxInt32 sceneID;

  SceneMsgCloseScene() : sceneID(-1){
    type = SceneMsgTypes::SceneMsgCloseScene;
  }

  virtual ReturnCodes loadData(INIReader* reader){
    // the scene id is dynamic inside the act, filled when sending the msg
    return ReturnCodes::RC_SUCCESS;
  }

  virtual SceneMsg& operator=(const SceneMsg& other){
    SceneMsg::operator=(other);
    const SceneMsgCloseScene* castedValue = static_cast<const SceneMsgCloseScene*>(&other);
    sceneID = castedValue->sceneID;
    return *this;
  }
};