#pragma once
#include <string>
#include <stdio.h>
#ifdef __MACOSX__
#include <SDL2/SDL.h>
#else
#include <SDL.h>
#endif
#include "sxTypes.h"

class sxFile
{
public:
  ReturnCodes error;
  
  sxFile(sxString name, sxString mode);
  ~sxFile();
  ReturnCodes read(sxString& readLine);
  void write(sxString msg);
  void closeFile();

private:
  SDL_RWops* file;
  char* fileContent;
  sxInt64 fileSize;
  sxInt64 currentPosition; // current position in the file
  
  void openFile(sxString name, sxString mode);
  ReturnCodes readFileToBuffer();
  void readNextLine(sxString& readLine);
};