#pragma once
#include <list>
#include "ActInfo.h"
#include "SceneBuilder.h"
#include "SceneMsgBuilder.h"
#include "ObjectCollisionDetector.h"
#include "ActFileLoader.h"
#include "DelayedSceneMsg.hpp"

#define getActManager ActManager::getInstance()

class ActManager
{
public:
  static void createManager();
  static ActManager* getInstance();
  void destroyManager();

  /// loadAct() deletes loader
  void loadAct(const sxString& filename, SDL_Renderer* renderer);
  /// loadActAndGetReader() won't delete loader, you have to, with deleteActLoader()
  INIReader* loadActAndGetReader(const sxString& filename, SDL_Renderer* renderer);
  void deleteActLoader();

  void enableObjectCollisionDetection(const sxBool enable){objectCollisionDetEnabled = enable;}

  void registerSceneMsgType(const SceneMsgID& id, AbstractFactory<SceneMsg>* type);
  SceneMsg* createSceneMsg(const sxCharStr& idStr){return sceneMsgBuilder.createInstance(idStr);}
  SceneMsg* createSceneMsg(const SceneMsgID& id){return sceneMsgBuilder.createInstance(id);}
  void registerSceneType(const sxString& id, AbstractFactory<Scene>* type);
  void pushSceneMsgToQueue(SceneMsg* msg);
  void pushDelayedSceneMsgToQueue(SceneMsg* msg, const sxUInt32 framesToDelay);
  void pushSceneMsgSendStringToQueue(const sxString& str, const sxBool broadcast);
  void pushSceneMsgSendStringToQueue(const sxString& str, const sxInt32 id, const sxBool broadcast);
  void pushDelayedSceneMsgSendStringToQueue(const sxString& str, 
                                            const sxUInt32 framesToDelay, const sxBool broadcast);
  void pushDelayedSceneMsgSendStringToQueue(const sxString& str,
                                            const sxInt32 id,
                                            const sxUInt32 framesToDelay,
                                            const sxBool broadcast);
  void draw();
  void processInputEvents();
  void processFrameStep(const sxUInt32& ticksPassed); // runs the functions from frameStart to frameEnd
  void frameStart();
  void processSceneMessages();
  void processSceneMessagesFromOutside();
  void stepAnim(const sxUInt32& ticksPassed);
  void collisionDetectionChecks(const sxUInt32& ticksPassed);
  void frameEnd();

  void cleanSceneAndCollisionInfo();
  // add on top of act which contains the sceneID
  void addSceneOnTop(SDL_Renderer* renderer, const sxUInt32 sceneID, const SceneLoadInfo& info);
  void addSceneOnTop(SDL_Renderer* renderer, const sxUInt32 sceneID,
                     const sxString& filename, const sxString& type);

  SceneIDs* removeSceneFromAct(const sxUInt32 sceneID);
  void removeTopmostAct();
  /// getScene(): if more scenes are there with the same name, gets the topmost
  //Scene* getScene(const sxString& sceneName);

private:
  ActFileLoader* actLoader;
  static ActManager* actManager;
  ActInfo actInfo;
  SceneMsgBuilder sceneMsgBuilder;
  SceneBuilder sceneBuilder;
  std::vector<Scene*> scenes;
  std::queue<SceneMsg*> sceneMsgs; /// scene messages registered with pushSceneMsgToQueue()
  std::list<DelayedSceneMsg> delayedSceneMsgs; // these will be handled in the next frame
  ObjectCollisionDetector* collDetector;
  sxBool objectCollisionDetEnabled;

  ActManager();
  ~ActManager();
  void initialize();
  void finalizeCollisionSettings();
  void fillSceneCollisionProperties();
  void sendOutMsgToOtherScenes(const sxInt32& senderID, SceneMsg*& msg);
  void processLoadedAct(ActFileLoader& loader, SDL_Renderer* renderer);
  void PushWindowOpacityToEventQueue(ActFileLoader& loader);
  Scene* processScene(SDL_Renderer* renderer, const SceneLoadInfo& loadInfo,
                      SceneIDs& actPair);
  ReturnCodes loadScene(const SceneLoadInfo& loadInfo, Scene* newScene,
                        SceneIDs& actPair);
  void removeScenes(const std::vector<sxUInt32>* sceneIDs, const sxString& actName);
  sxBool isSceneIDPresentInScenesVector(const sxUInt32 sceneID, sxUInt32& itemIdx);
  void setCollisionDetectorStartingSceneIndex();
  void destroyAllScenes();
  void destroyAllSceneMsgs();
  SceneMsg* createStringMsg(const sxString& str, const sxInt32 id, const sxBool broadcast);
  void processDelayedSceneMsgs();
};
