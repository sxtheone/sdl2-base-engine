#pragma once
#include "ButtonObject.h"
#include "SceneMsg.hpp"

class MenuButton : public ButtonObject
{
public:
  MenuButton(){type = ObjectTypes::MenuButton; onClickSceneMsg = NULL;}
  virtual ~MenuButton();
  virtual ReturnCodes loadData(INIReader* reader);
  virtual void objectHit(sxEvent& eventHappened);
  void setSceneMessage(SceneMsg* msg);

protected:
  SceneMsg* onClickSceneMsg;
  sxEvent onClickEvent;

  virtual void assign(const ObjectBase& source);
  virtual void processEventFields(INIReader* reader);
  virtual void processSceneMessageFields(INIReader* reader);
  void processEventDataField(INIReader* reader);
  virtual void sendOutEvent();
  virtual void sendOutSceneMessage();
  sxActChange::ChangeType getActChangeType(const sxString& changeType);
  void destroyMessage(SceneMsg* sceneMsgToDelete);

  // called every time the anim changes. 
  // If scaling is set, this will be slow, but OK for a button
  virtual void setHitbox();
  void scaleHitbox();
};
