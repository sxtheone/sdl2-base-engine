#pragma once
#include "TileMapVector.hpp"
#include "ScreenGrid.h" //TODO: only GridElement is needed


class TileMapHandler
{
public:
  TileMapHandler();
  void addMap(RawTileMap& newMap){tileMaps.add(newMap);}
  sxUInt32 getTileIndexToDraw();
  void setFirstStartingPoint();
  void resetRemainingDrawRepeats();
  void getBackToStartingMap();
  void setRowToStartingRow(){currentMap.setCurrItemIndexRow(tileMaps.getStartingRowIndex());}
  void setColToStartingCol(){currentMap.setCurrItemIndexCol(tileMaps.getStartingColIndex());}
  void setNextStartingColumn(const sxDirection directionX);//changes starting map if runs out of columns
  void setNextStartingRow(const sxDirection directionY);//not changes map
  void setNumberOfRepeats(const sxInt8 repeats){numOfRepeats = repeats;}
  sxBool moveToNextTileIndexToDraw(); // true if there is a next item to draw
  sxBool moveToNextRowToDraw();
  sxBool shouldRepeatRow();
  void setNextMapToDraw();// sets current map only, not touching starting map
  sxUInt32 getNumOfRowsForCurrentMap(){return currentMap.getNumberOfRows();}
  sxUInt32 getNumOfColsForCurrentMap(){return currentMap.getNumberOfColumns();}
  std::vector< std::vector<GridElement> > getSearchGrid(sxVec2Real mapOffset, sxVec2Real objPos,
                                                        sxVec2Int objHitboxSize, sxVec2Int tileSize);
  sxBool areMapsLoaded(){return tileMaps.areMapsLoaded();}

private:
  sxInt8 numOfRepeats;
  sxInt8 repeatsRemaining;
  TileMapVector tileMaps;
  TileMapInfo currentMap;
  
  void correctMapPosition(sxVec2Int& mapPos, sxVec2Int& mapSize);
};

inline
TileMapHandler::TileMapHandler()
{
  numOfRepeats = 0;
  repeatsRemaining = 0;
}

inline
sxUInt32 TileMapHandler::getTileIndexToDraw()
{
  return currentMap.getCurrItem();
}

inline
void TileMapHandler::setFirstStartingPoint()
{
  tileMaps.setFirstStartingPoint();
  getBackToStartingMap();
}

inline
void TileMapHandler::resetRemainingDrawRepeats()
{
  repeatsRemaining = numOfRepeats;
}

inline
void TileMapHandler::getBackToStartingMap()
{
  tileMaps.getStartingMap(currentMap);
}

inline
void TileMapHandler::setNextMapToDraw()
{
  tileMaps.cycleNextMapIndex(currentMap.mapIndex);
  tileMaps.getMap(currentMap.mapIndex, currentMap);
}

inline
void TileMapHandler::setNextStartingColumn(const sxDirection directionX)
{
  if(directionX.isLeftSet()){
    tileMaps.cycleNextStartingColumn();
  }else{
    tileMaps.cyclePreviousStartingColumn();
  }
}

inline
void TileMapHandler::setNextStartingRow(const sxDirection directionY)
{
  if(directionY.isUpSet()){
    tileMaps.cycleNextStartingRow();
  }else{
    tileMaps.cyclePreviousStartingRow();
  }
}

inline
sxBool TileMapHandler::moveToNextTileIndexToDraw()
{
  return currentMap.incCurrItemIndexCol();
}

inline
sxBool TileMapHandler::moveToNextRowToDraw()
{
  return currentMap.incCurrItemIndexRow();
}

inline
sxBool TileMapHandler::shouldRepeatRow()
{
  if(repeatsRemaining > 0){
    --repeatsRemaining;
    return SDL_TRUE;
  }
  return SDL_FALSE;
}

inline
std::vector< std::vector<GridElement> > TileMapHandler::getSearchGrid(sxVec2Real mapOffset, 
                                                                   sxVec2Real objPos, 
                                                                   sxVec2Int objHitboxSize,
                                                                   sxVec2Int tileSize)
{
  std::vector< std::vector<GridElement> > result;

  sxVec2Int mapPos;

  sxVec2Real mapOffSeeable;
  if(mapOffset.x != 0) mapOffSeeable.x = tileSize.x - abs(mapOffset.x);
  if(mapOffset.y != 0) mapOffSeeable.y = tileSize.y - abs(mapOffset.y);

  sxVec2Real objPosWithOffset;
  objPosWithOffset.x = objPos.x + abs(mapOffset.x);
  objPosWithOffset.y = objPos.y + abs(mapOffset.y);
  
  mapPos.x = static_cast<sxInt32>(floor(objPosWithOffset.x / tileSize.x));
  mapPos.y = static_cast<sxInt32>(floor(objPosWithOffset.y / tileSize.y));

  // now we have the map position but it's from the screen starting coord, 
  // not related to the map start
  sxVec2Int mapSize;
  correctMapPosition(mapPos, mapSize);

  sxVec2Real middleTilePos;

  middleTilePos.x = objPos.x - mapOffSeeable.x;
  middleTilePos.y = objPos.y - mapOffSeeable.y;

  middleTilePos.x = static_cast<sxFloat>(floor(middleTilePos.x / tileSize.x));
  middleTilePos.y = static_cast<sxFloat>(floor(middleTilePos.y / tileSize.y));

  middleTilePos.x *= tileSize.x;
  middleTilePos.y *= tileSize.y;

  middleTilePos.x += mapOffSeeable.x;
  middleTilePos.y += mapOffSeeable.y;

  /*
  // ezt azert, hogy 1 tile-nal nagyobb scrollozast is tudjon
  TileMapInfo startMap;
  tileMaps.getStartingMap(startMap);
  if(0 != tileMaps.getStartingRowIndex())
    middleTilePos.y -= (startMap.getNumberOfRows() - tileMaps.getStartingRowIndex()) * tileSize.y;
  */

  sxMatrix2D searchGridSize;
  sxVec2Int offsetInsideTile;
  offsetInsideTile.x = static_cast<sxInt32>(objPosWithOffset.x) % tileSize.x;
  offsetInsideTile.y = static_cast<sxInt32>(objPosWithOffset.y) % tileSize.y;
  searchGridSize.col =  static_cast<sxUInt16>(ceil((objHitboxSize.x + abs(offsetInsideTile.x)) / static_cast<sxFloat>(tileSize.x)));
  searchGridSize.row =  static_cast<sxUInt16>(ceil((objHitboxSize.y + abs(offsetInsideTile.y)) / static_cast<sxFloat>(tileSize.y)));
//BUG: collision detection: when moving offscreen, object tends to go through other objects what it doesn't do when onscreen
//  searchGridSize.col =  ceil((objHitboxSize.x + abs(offsetInsideTile.x)).. this may also cause the error
//  searchGridSize = sxMatrix2D(5, 5);

  result.resize(searchGridSize.col);
  for(int i=0; i<searchGridSize.col; ++i){
    result[i].resize(searchGridSize.row);
  }
  sxVec2Int currPosStart = mapPos;
  currPosStart.x -= 1;
  currPosStart.y -= 1;
  sxVec2Int currPos = currPosStart;

  for(int col=0; col<searchGridSize.col; ++col){
    ++currPos.x;
    if(currPos.x >= mapSize.x) currPos.x = 0;
    currPos.y = currPosStart.y;
    for(int row=0; row<searchGridSize.row; ++row){
      ++currPos.y;
      if(currPos.y >= mapSize.y) currPos.y = 0;
      result[col][row].itemIdx = tileMaps.getTileIndex(currPos);
      result[col][row].position.x = middleTilePos.x + col*tileSize.x;
      result[col][row].position.y = middleTilePos.y + row*tileSize.y;
    }
  }

  return result;
}

inline
void TileMapHandler::correctMapPosition(sxVec2Int& mapPos, sxVec2Int& mapSize)
{
  TileMapInfo currentMap;
  tileMaps.getStartingMap(currentMap);

  if(mapPos.x >= 0){
    // positive X
    sxInt32 remainingTiles = currentMap.getNumberOfColumns();
    if(0 != tileMaps.getStartingColIndex()){
      remainingTiles -= tileMaps.getStartingColIndex();
    }
    if(remainingTiles > mapPos.x){
      if(0 != tileMaps.getStartingColIndex()){
        mapPos.x += tileMaps.getStartingColIndex();
      }
    }else{
      sxInt32 result = mapPos.x - remainingTiles;
      // move to next map
      sxUInt32 nextMapIndex = currentMap.mapIndex;
      tileMaps.cycleNextMapIndex(nextMapIndex);
      tileMaps.getMap(nextMapIndex, currentMap);
      while(static_cast<sxInt32>(currentMap.getNumberOfColumns()) <= result){
        result -= currentMap.getNumberOfColumns();
        // move to next map
        sxUInt32 nextMapIndex = currentMap.mapIndex;
        tileMaps.cycleNextMapIndex(nextMapIndex);
        tileMaps.getMap(nextMapIndex, currentMap);
      }
      mapPos.x = result;
    }
  }else{
    // negative X
    if(tileMaps.getStartingColIndex() >= static_cast<sxUInt32>(abs(mapPos.x))){
      mapPos.x += tileMaps.getStartingColIndex();
    }else{
      sxInt32 result = mapPos.x + tileMaps.getStartingColIndex();
      // move to previous map
      sxUInt32 prevMapIndex = currentMap.mapIndex;
      tileMaps.cyclePreviousMapIndex(prevMapIndex);
      tileMaps.getMap(prevMapIndex, currentMap);
      while(currentMap.getNumberOfColumns() < static_cast<sxUInt32>(abs(result))){
        result += currentMap.getNumberOfColumns();
        // move to prevoius map
        sxUInt32 prevMapIndex = currentMap.mapIndex;
        tileMaps.cyclePreviousMapIndex(prevMapIndex);
        tileMaps.getMap(prevMapIndex, currentMap);
      }
      mapPos.x = currentMap.getNumberOfColumns() + result;
    }
  }

  mapPos.y += tileMaps.getStartingRowIndex();
  mapPos.y %= currentMap.getNumberOfRows();
  mapSize.x = currentMap.getNumberOfColumns();
  mapSize.y = currentMap.getNumberOfRows();
}