#pragma once
#include <map>
#include "LogManager.h"
#include "AbstractFactory.hpp"

template <typename IdentifierType, typename ItemType>
class FactoryHandler
{
public:
	virtual ~FactoryHandler()
  {
    clearFactories();
  }

	void registerFactory(IdentifierType id, AbstractFactory<ItemType>* factory)
	{
    if(factories.end() != factories.find(id)){
      sxLOG_E("Type already registered");
      delete factory;
      factory = NULL;
      return;
    }
		factories[id] = factory;
	}
	/*
	AbstractFactory<ItemType>* getFactory(IdentifierType id)
	{
		return factories[id];
	}
  */
	void deregisterFactory(IdentifierType id)
	{
    if(factories.end() == factories.find(id)){
      sxLOG_E("Type not found");
      return;
    }
		delete factories[id];
    factories.erase(id);
	}
  /*
  sxUInt32 getNumOfRegisteredFactories()
  {
    return factories.size();
  }
  */

	ItemType* createInstance(IdentifierType id)
	{
		AbstractFactory<ItemType>* factory = factories[id];
		return factory != NULL ? factory->CreateInstance() : NULL;
	}

protected:
	std::map<IdentifierType, AbstractFactory<ItemType> *> factories;

  void clearFactories()
  {
    typename std::map<IdentifierType, AbstractFactory<ItemType> *>::iterator it;
    it = factories.begin();
    while(factories.end() != it){
      delete it->second;
      ++it;
    }
    factories.clear();
  }
};
