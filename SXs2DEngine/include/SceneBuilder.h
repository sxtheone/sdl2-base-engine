#pragma once
#include "FactoryHandler.hpp"
#include "GenericFactory.hpp"
#include "Scene.h"

template <typename TConcrete>
class SceneFactory : public GenericFactory<Scene, TConcrete> { };

class SceneBuilder : public FactoryHandler<const sxString, Scene>
{
public:
  SceneBuilder();

private:
  void registerBasicTypes();
};
