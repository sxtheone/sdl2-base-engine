#pragma once
#include "sxTypeDefs.h"

static const sxUInt8 INVALID_SPRITE_NUMBER = 255;

class sxAnimFrame
{
public:
  sxUInt8  spriteNumber;
  sxUInt32 time;
  sxUInt8  nextAnimFrame;

  sxAnimFrame();
};

inline
sxAnimFrame::sxAnimFrame()
{
  spriteNumber = INVALID_SPRITE_NUMBER;
  time = 0;
  nextAnimFrame = 0;
}
