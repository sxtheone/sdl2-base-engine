#pragma once
#include "Scene.h"
#include "ObjectDrawerBase.hpp"
#include "SlidingInfo.hpp"

class ObjectScene : public Scene
{
public:
  ObjectScene(){type = SceneTypeEnum::OBJECT; drawer = NULL; objects = NULL;}
  virtual ~ObjectScene(){destroyObjects(); delete drawer;}
  virtual ReturnCodes loadFile(const sxString& filename);
  virtual void draw();
  virtual void stepAnimation(const sxUInt32 ticksPassed);
  virtual CollisionData getCollisionData(const sxUInt32 objectIndex);
  virtual sxUInt32 getNumOfCollisionObjects(){return objects->size();}

protected:
  ObjectDrawerBase* drawer;
  std::vector<Object*>* objects;
  SlidingInfo slidingInfo;

  void destroyObjects();
  void switchObjectWithIdTagTo(sxUInt16 idTag, Object* newObject);
  void setupSliding(const sxVec2Real& speed, const sxVec2Real& distance);
  void stepSliding(const sxUInt32& ticksPassed);
};
