#pragma once
#include "FactoryHandler.hpp"
#include "GenericFactory.hpp"
#include "ObjectBase.h"

template <typename TConcrete>
class ObjectFactory : public GenericFactory<ObjectBase, TConcrete> { };

class ObjectBuilder : public FactoryHandler<const sxCharStr, ObjectBase>
{
public:
  ObjectBuilder();

private:
  void registerBasicTypes();
};
