#pragma once
#include "TileMapInfo.hpp"

class TileMapVector
{
public:
  TileMapVector();
  void add(RawTileMap& newMap);
  void setFirstStartingPoint();
  void cycleNextStartingColumn();
  void cycleNextStartingRow();
  void cyclePreviousStartingColumn();
  void cyclePreviousStartingRow();
  void getStartingMap(TileMapInfo& outMap);
  void getMap(const sxUInt32 mapIndex, TileMapInfo& outMap);
  void cycleNextMapIndex(sxUInt32& mapIndex); // if index is the last, the first will be the next
  void cyclePreviousMapIndex(sxUInt32& index);
  sxUInt32 getStartingRowIndex(){return startItemIdx.row;}
  sxUInt32 getStartingColIndex(){return startItemIdx.col;}
  sxUInt32 getTileIndex(const sxVec2Int& tileCoord); /// finds corresponding tile in any map
  sxBool areMapsLoaded(){return tileMaps.empty() ? SDL_FALSE : SDL_TRUE;}

private:
  std::vector<RawTileMap> tileMaps;
  sxUInt32 numOfTileMaps; // tileMaps.size(). It's introduced for speedup
  sxUInt32 startMapIdx;    // drawing starts with this map
  sxMatrix2D startItemIdx;   // item index in the startMap
  
  void init();
  sxBool numberOfRowsInMapOK(const RawTileMap& map);
};

inline
TileMapVector::TileMapVector()
{
  init();
}

inline
void TileMapVector::init()
{
  tileMaps.clear();
  startMapIdx = 0;
  startItemIdx = sxMatrix2D();
  numOfTileMaps = 0;
}

inline
void TileMapVector::add(RawTileMap& newMap)
{
  if(SDL_TRUE == numberOfRowsInMapOK(newMap)){
    newMap.shrinkMapCapacityToSize();
    tileMaps.push_back(newMap);
    numOfTileMaps = tileMaps.size();
  }
}

inline
sxBool TileMapVector::numberOfRowsInMapOK(const RawTileMap& map)
{
  if( tileMaps.empty() ||
      tileMaps[0].numberOfRows() == map.numberOfRows() )
  {
    return SDL_TRUE;
  }
  sxLOG_E("Number of rows should be the same for all maps in a tiled scene."
    "Erroneous map has: %d rows. Should have: %d rows.", 
    map.numberOfRows(), tileMaps[0].numberOfRows());
  return SDL_FALSE;
}

inline
void TileMapVector::setFirstStartingPoint()
{
  startMapIdx = 0;
  startItemIdx = sxMatrix2D();
  numOfTileMaps = tileMaps.size();
  if(0 == numOfTileMaps){
    sxLOG_E("TileMaps vector is empty!");
  }
}

inline
void TileMapVector::cycleNextStartingColumn()
{
  ++startItemIdx.col;
  if(startItemIdx.col >= tileMaps[startMapIdx].numberOfItemsInARow()){
    cycleNextMapIndex(startMapIdx);
    startItemIdx.col = 0;
  }
}

inline
void TileMapVector::cycleNextStartingRow()
{
  ++startItemIdx.row;
  if(startItemIdx.row >= tileMaps[startMapIdx].numberOfRows()){
    startItemIdx.row = 0;
  }
}

inline
void TileMapVector::cyclePreviousStartingColumn()
{
  if(0 == startItemIdx.col){
    cyclePreviousMapIndex(startMapIdx);
    startItemIdx.col = tileMaps[startMapIdx].numberOfItemsInARow()-1;
  }else{
    --startItemIdx.col;
  }
}

inline
void TileMapVector::cyclePreviousStartingRow()
{
  if(0 == startItemIdx.row){
    startItemIdx.row = tileMaps[startMapIdx].numberOfRows()-1;
  }else{
    --startItemIdx.row;
  }
}

inline
void TileMapVector::getStartingMap(TileMapInfo& outMap)
{
  getMap(startMapIdx, outMap);
}

inline
void TileMapVector::getMap(const sxUInt32 mapIndex, TileMapInfo& outMap)
{
  outMap.mapIndex = mapIndex;
  outMap.map = &tileMaps[mapIndex];
  outMap.setCurrItemIndex(sxMatrix2D());
  // numOfItems won't be updated later. Every row should be same sized
  outMap.numOfItems = tileMaps[mapIndex].numberOfItemsInARow();
}

inline
void TileMapVector::cycleNextMapIndex(sxUInt32& index)
{
  ++index;
  if(numOfTileMaps <= index){
    index = 0;
  }
}

inline
void TileMapVector::cyclePreviousMapIndex(sxUInt32& index)
{
  if(0 == index){
    index = numOfTileMaps-1;
  }else{
    --index;
  }
}

inline
sxUInt32 TileMapVector::getTileIndex(const sxVec2Int& tileCoord)
{
  //TODO: incoming negative values!!!!

  sxMatrix2D finalCoord;
  finalCoord.col = tileCoord.x;
  finalCoord.row = tileCoord.y;
  sxInt32 i = 0;
  sxUInt32 numOfMaps = tileMaps.size();
  while(tileMaps[i].numberOfItemsInARow() <= finalCoord.col)
  {
    finalCoord.col -= tileMaps[i].numberOfItemsInARow();
    ++i;
    if(i == numOfMaps){
      i = 0;
    }
  }
  //NOTE: all maps should have the same height
  if(tileMaps[i].numberOfRows() <= finalCoord.row){
    finalCoord.row = tileMaps[i].numberOfRows() % finalCoord.row;
  }

  return tileMaps[i].getItem(finalCoord);
}