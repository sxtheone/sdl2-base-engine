#pragma once
#include "AnimObject.h"

class ButtonObject : public AnimObject
{
public:
  ButtonObject();
  virtual ~ButtonObject();
  virtual ReturnCodes loadData(INIReader* reader);
  virtual void stepAnimation(const sxUInt32 ticksPassed);
  virtual void setPosition(const sxVec2Real& newPos);
  // label set auto when loading from ini. Use only when creating button from code
  void setLabel(const sxString& btnLabel, const sxString& fontName);

  /// overload objectHit() functions and do logic what should be
  /// executed on collision
  virtual void objectHit(CollisionInfo& collInfo){buttonHit();}
  virtual void objectHit(sxEvent& eventHappened){buttonHit();}

protected:
  sxString defaultAnimName;
  sxString clickAnimName;
  sxBool clickAnimPlaying;
  sxCharStr clickSound;
  Object* label;

  virtual void assign(const ObjectBase& source);
  virtual void callDrawMethodAndModifiers(SDL_Renderer*& renderer);
  ReturnCodes loadButtonAnims(INIReader* reader);
  void loadLabel(INIReader* reader);
  void processClickSoundField(INIReader* reader);
  void buttonHit(); /// plays the click animation and executes doButtonHitActions()
};
