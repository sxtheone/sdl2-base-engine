#pragma once
#include "Object.h"

class CollisionDetectorBase
{
public:
  static void mergeNeighbouringHitboxes(std::vector<CollisionInfo>& collInfo, 
                                        std::vector<SmallCollisionInfo>& outVector);
  static sxBool hitboxesTouching(const sxQuad& hb1, const sxQuad& hb2);

protected:
  sxBool objectCanCollideNow(const ObjectBase* object) const;
  void fillCollisionInfo(sxUInt32 objIdx, CollisionData*& objCollData, CollisionInfo& collInfo);
  sxBool areActuallyColliding(const CollisionData* object1, const CollisionData* object2);
  void buildCollisionQuad(const CollisionData*& obj, sxQuad& outQuad);
  void buildCollisionQuad(sxVec2Int& pos, sxQuad& outQuad);
  sxBool objectCanCollideWithOther(const ObjectBase* obj, const ObjectBase* otherObj);
  sxBool areHitboxesColliding(const sxQuad& hitbox1, const sxQuad& hitbox2);
};

inline
sxBool CollisionDetectorBase::objectCanCollideNow(const ObjectBase* object) const
{
  if( SDL_TRUE == object->objectProps.objectEnabled() &&
      SDL_TRUE == object->objectProps.collisionPossible() &&
      SDL_TRUE == object->objectProps.collisionEnabled() )
  {
    return SDL_TRUE;
  }
  return SDL_FALSE;
}

inline
void CollisionDetectorBase::fillCollisionInfo(sxUInt32 objIdx, CollisionData*& objCollData, 
                                              CollisionInfo& collInfo)
{
  collInfo.objectIndex = objIdx;
  collInfo.otherObject.object = objCollData->object;
  collInfo.otherObject.pixelPosition = objCollData->position;
}

inline
sxBool CollisionDetectorBase::areActuallyColliding(const CollisionData* object1, 
                                                   const CollisionData* object2)
{
  sxQuad hitBox1, hitBox2;
  buildCollisionQuad(object1, hitBox1);
  buildCollisionQuad(object2, hitBox2);
  return areHitboxesColliding(hitBox1, hitBox2);
}

inline
void CollisionDetectorBase::buildCollisionQuad(const CollisionData*& obj, sxQuad& outQuad)
{
  outQuad = *(obj->object->collisionProps->hitbox);
  outQuad.topLeft.x += obj->position.x;
  outQuad.topLeft.y += obj->position.y;
  outQuad.bottomRight.x += obj->position.x;
  outQuad.bottomRight.y += obj->position.y;
}

inline
sxBool CollisionDetectorBase::areHitboxesColliding(const sxQuad& hitbox1, const sxQuad& hitbox2)
{
  // it's easier to allow objects touching with hitbox borders and give back no collision
  if( hitbox1.topLeft.x >= hitbox2.bottomRight.x ||
      hitbox1.topLeft.y >= hitbox2.bottomRight.y ||
      hitbox1.bottomRight.x <= hitbox2.topLeft.x ||
      hitbox1.bottomRight.y <= hitbox2.topLeft.y )
  {
    return SDL_FALSE;
  }
  return SDL_TRUE;
}

inline
void CollisionDetectorBase::buildCollisionQuad(sxVec2Int& pos, sxQuad& outQuad)
{
  outQuad.topLeft.x = static_cast<sxFloat>(pos.x);
  outQuad.topLeft.y = static_cast<sxFloat>(pos.y);
  outQuad.bottomRight.x = static_cast<sxFloat>(pos.x + 1);
  outQuad.bottomRight.y = static_cast<sxFloat>(pos.y + 1);
}

inline
sxBool CollisionDetectorBase::objectCanCollideWithOther(const ObjectBase* obj, const ObjectBase* otherObj)
{
  if( SDL_TRUE == obj->idleTimePassed() &&
      SDL_TRUE == obj->collisionProps->canCollideWith(otherObj->collisionProps) )
  {
    return SDL_TRUE;
  }
  return SDL_FALSE;
}

inline
void CollisionDetectorBase::mergeNeighbouringHitboxes(std::vector<CollisionInfo>& collInfo, 
                                                      std::vector<SmallCollisionInfo>& outVector)
{
  std::vector<CollisionInfo>::iterator actItemIt = collInfo.begin();
  //NOTE: this function is intentionally this big. I couldn't split it without making it slower..
  for(actItemIt=collInfo.begin(); collInfo.end() != actItemIt; ++actItemIt){
    if(NULL == actItemIt->otherObject.object) continue;

    std::vector<CollisionInfo>::iterator currItemIt;
    ObjectBase* actItemObj;
    sxQuad resultHitbox, currHitbox;

    actItemObj = actItemIt->otherObject.object;
    resultHitbox = actItemObj->getHitBoxWithOffset(actItemIt->otherObject.pixelPosition);

    for(currItemIt=actItemIt+1; collInfo.end() != currItemIt; ++currItemIt){
      if(NULL == currItemIt->otherObject.object) continue;
        
      ObjectBase*& currItemObj = currItemIt->otherObject.object;
      currHitbox = currItemObj->getHitBoxWithOffset(currItemIt->otherObject.pixelPosition);
      if(SDL_TRUE == hitboxesTouching(resultHitbox, currHitbox)){
        resultHitbox.bottomRight = currHitbox.bottomRight;
        currItemObj = NULL;
      }
    }
        
    SmallCollisionInfo collData(actItemIt->objectIndex, resultHitbox);
    outVector.push_back(collData);
    actItemObj = NULL;
  }
}

// hitboxes are touching but not overlapping at least in one point
inline
sxBool CollisionDetectorBase::hitboxesTouching(const sxQuad& hb1, const sxQuad& hb2)
{
  if( (hb1.topLeft.x == hb2.topLeft.x &&
        hb1.bottomRight.x == hb2.bottomRight.x) ||
      (hb1.topLeft.y == hb2.topLeft.y &&
        hb1.bottomRight.y == hb2.bottomRight.y) )
  {
    return SDL_TRUE;
  }
  return SDL_FALSE;
}
