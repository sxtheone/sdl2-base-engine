#pragma once
#include "Scene.h"
#include "TiledSceneFileLoader.h"
#include "ScreenGrid.h"


class StaticTiledScene : public Scene
{
public:
  std::vector<ObjectBase*> tiles;//TODO: resize capacity after add finished
  std::vector<Object*> objectTiles; /// pointers to the tiles vector's Object* items
  ScreenGrid* grid; // collision grid

  StaticTiledScene();
  ~StaticTiledScene();
  virtual void draw();
  virtual void stepAnimation(const sxUInt32 ticksPassed);
  virtual ReturnCodes loadFile(const sxString& filename);
  virtual CollisionData getCollisionData(const sxUInt32 objectIndex);
  virtual sxUInt32 getNumOfCollisionObjects();

  void setSceneSize(const sxVec2Int& size);
  sxVec2Int getSceneSize(){return sceneSize;}
  void setTopLeftPosition(const sxVec2Int& pos);
  sxVec2Int getTopLeftPosition(){return sceneCorners.topLeft;}
  void setBottomRightPosition(const sxVec2Int& bottomRightPos);
  sxVec2Int getBottomRightPosition(){return sceneCorners.bottomRight;}
  void setSceneWidth(const sxInt32 width);
  void setSceneHeight(const sxInt32 height);

  void setSearchRectangle(const sxVec2Real& pixelPos, const sxVec2Int& size);

protected:
  TileMapHandler mapHandler;
  sxVec2Int sceneSize;
  sxVec2Int tileSize;
  sxQuadInt sceneCorners; /// don't change this directly
  sxVec2Int posToDraw; // used in drawing: where to draw the next tile
  sxVec2Int topLeftToDraw;
  sxVec2Int bottomRightToDraw;
  TiledSceneDrawMethod drawMethod;
  
  SDL_Texture *origRenderTexture; // the one what is already set as render target
  SDL_Texture *renderTexture;
  SDL_Texture *bufferTexture;
  SDL_BlendMode origBlendMode;
  sxColor origDrawColor;

  virtual void init();
  virtual ReturnCodes processFileContent(const sxString& filename, TiledSceneFileLoader& loader);
  virtual void drawOneRow(); // returns with the number of columns drawn
  sxVec2Real getOffset() const {return offset;}
  void setOffset(const sxVec2Real& newOffset);
  void addToOffset(const sxVec2Real& offsetToAdd);

  void doDrawing();
  virtual void drawTheMaps(const sxBool drawingFirstRowOfMaps);
  void drawOneMap(const sxBool drawingFirstMap);  // returns with the number of columns drawn
  sxBool moveToTheNextRowIfCan();
  void drawWholeRow();
  void drawTilesWithShouldRedraw(const bool updateTheWholeGrid);

  void fillWithTransparent(const sxVec2Int& pos, 
                           const sxInt32 width, const sxInt32 height);
  void destroyTiles();
  void storeOriginalRenderInfo();
  void setStartingPosToDraw();
  void createFullscreenTexture(SDL_Texture*& texture);
  void destroyTexture(SDL_Texture*& texture);
  void changeRenderTargetTo(SDL_Texture* texture);
  void restoreOriginalRenderTargetAndRender();
  void drawDebugBoxes(const sxUInt32 tileIdxToDraw);
  void getTileIndexFromMapHandler(const sxUInt32 tileIdxToDraw, 
                                  sxUInt32& prevTileIdxToDraw, ObjectBase*& obj);
  void resetShouldRedrawValues();
  void setupGridSize(const sxVec2Int& biggestHitbox);

private:
  // DON'T set offset directly, only through changing velocity (see scrollable tiledscene class)
  // or writing a function what handles offset changes
  sxVec2Real offset;

  void fillTileSize();
  
  void updateBottomRightPosition();
};
