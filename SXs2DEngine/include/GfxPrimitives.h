/*
This class is based on SDL_Gfx library
*/
#pragma once
#include "LogManager.h"

class GfxPrimitives
{
public:
  static ReturnCodes pixelRGBA(SDL_Renderer *renderer, sxVec2Int& coords, sxColor& color);
  static ReturnCodes hlineRGBA(SDL_Renderer *renderer, sxVec2Int& startCoord,
                               sxInt16 endX, sxColor& color);
  static ReturnCodes vlineRGBA(SDL_Renderer *renderer, sxVec2Int& startCoord,
                               sxInt16 endY, sxColor& color);
	static ReturnCodes rectangleRGBA(SDL_Renderer *renderer, sxQuadInt& rect, sxColor& color);
  static ReturnCodes boxRGBA(SDL_Renderer * renderer, sxQuadInt& rect, sxColor& color);
};