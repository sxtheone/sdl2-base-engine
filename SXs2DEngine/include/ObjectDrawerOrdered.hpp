#pragma once
#include "ObjectDrawerBase.hpp"
#include "OrderedDrawer.h"

/*
This is the 'frontend' for the OrderedDrawer singleton.
The scenes what have ObjectDrawerOrdered typed drawer uses the drawQueueId to
identify the draw queue and they all put their objects into the same queue.
*/

class ObjectDrawerOrdered : public ObjectDrawerBase
{
public:
  ObjectDrawerOrdered();
  void setDrawQueueId(sxUInt32 id){drawQueueId = id;}
  virtual void objectMoved(Object* object);
  virtual void objectDeleted(const sxUInt32 objectId);
  virtual void drawAll(SDL_Renderer* renderer);
  virtual void draw(const sxUInt32 objIdx, SDL_Renderer* renderer){}
  void syncObjectsWithOrderedDrawer();

private:
  sxUInt32 drawQueueId;
};

ObjectDrawerOrdered::ObjectDrawerOrdered()
{
  type = ObjectDrawerTypeEnum::ORDERED;
  drawQueueId = 0;
}

void ObjectDrawerOrdered::drawAll(SDL_Renderer* renderer)
{
  getOrderedDrawer->draw(/*drawQueueId, */renderer);
}

/// Add all objects to OrderedDrawer. Slow, don't call! Used only when loading a level.
void ObjectDrawerOrdered::syncObjectsWithOrderedDrawer()
{
  std::vector<Object*>::iterator objIt = objects.begin();
  while(objects.end() != objIt){
    getOrderedDrawer->addToQueue(drawQueueId, *objIt);
    ++objIt;
  }
}

void ObjectDrawerOrdered::objectMoved(Object* object)
{
  getOrderedDrawer->objectMoved(drawQueueId, object);
}

void ObjectDrawerOrdered::objectDeleted(const sxUInt32 objectId)
{
  getOrderedDrawer->objectDeleted(drawQueueId, objectId);
}