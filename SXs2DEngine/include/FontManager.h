#pragma once
#include <map>
#include "sxFont.h"
#include "BasicObject.h"

#define getFontManager FontManager::getInstance()

class FontManager
{
public:
  static void createManager();
  static FontManager* getInstance();
  ReturnCodes setRenderer(SDL_Renderer* renderer);
  void destroyManager();
  void setSystemFont(sxFont& font){systemFont = font;}
  void addFont(const sxString& fontName, sxFont& font);
  void destroyAllFonts(){fonts.clear();}
  sxFont* getFont(const sxString& fontName);
  ReturnCodes setFontToRender(const sxString& fontName);
  BasicObject* generateSystemText(const sxString& text); // generates with system font
  BasicObject* generateText(const sxString& text, const sxUInt32 widthInPixel = 0);
  BasicObject* generateText(const sxString& text, sxFont* font, const sxUInt32 widthInPixel = 0);
  BasicObject* generateText(const sxString& text, const sxColor& color,
                            const sxUInt32 widthInPixel = 0);
  
private:
  static FontManager* fontManager;

  SDL_Renderer* mainRenderer;
  sxFont systemFont; // this font can't be deleted with destroyAllFonts()
  sxFont* renderFont; // pointer to the selected font in fonts map
  std::map<sxString, sxFont> fonts;
  std::map<sxString, sxFont>::iterator fontIt;
  sxUInt32 lineWidthInPixels; // for NICE rendering

  FontManager();
  ~FontManager();
  BasicObject* generateTextWithFont(const sxString& text, sxFont& font);
  sxSprite* renderTextToSprite(const sxString& text, sxFont& font);
  SDL_Texture* renderTextToSDLTexture(const sxString& text, sxFont& font);
  SDL_Surface* renderTextToSDLSurface(const sxString& text, sxFont& font);
};

