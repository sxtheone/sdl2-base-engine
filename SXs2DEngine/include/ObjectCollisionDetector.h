/**
Object Collision Detector:
- collision detection is enabled only for the topmost Act. No collision detection
  enabled for lower Acts. This restriction can be disabled by setting startSceneIdx/endSceneIdx
- when loading a second (third..) Act, on top of the current, it will bercome active
- NOTE: don't use the same name for two objects/scenes even in different acts! It will cause
  problems, because only one collisionID/collidesWith enabled for an object.
**/

#pragma once
#include "CollisionDetectorBase.hpp"
#include "Scene.h"

class ObjectCollisionDetector : public CollisionDetectorBase
{
public:
  ObjectCollisionDetector(std::vector<Scene*>* _scenes) : scenes(_scenes) {}
  /// setEndSceneIndex: collision check starts from the uppermost scene and goes backward
  void setEndSceneIndex(const sxUInt32 startIdx);
  void doCollisionDetectionChecks(sxUInt32 ticksPassed);

private:
  std::vector<CollisionInfo> collInfoToSend;
  sxInt32 endSceneIndex;
  std::vector<Scene*>* scenes;
  Scene* activeScene{nullptr};
  CollisionData actObjCollData;
  Scene* currScene{nullptr};
  sxInt32 activeSceneIdx{-1};
  sxUInt32 activeObjectIdx{ std::numeric_limits<sxUInt32>::max() };

  void checkCollisionsForObject();
  void checkCollisionsInCurrScene(sxUInt32 objStartIdx);
  void checkCollisionsInScene(sxUInt32 objStartIdx);
  void prepareSearchGridForTiledScene();
  void dealWithCollisionBetweenObjects(CollisionData* obj1,
                                       CollisionData* obj2,
                                       Scene* sceneWhatHandlesCollision);
  void dealWithTiledSceneCollision(CollisionData* obj1,
                                   CollisionData* obj2);
  void sendCollInfoToActiveScene();
  void drawDebugBox(const CollisionData& collData);
};