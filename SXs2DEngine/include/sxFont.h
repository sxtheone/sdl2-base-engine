#pragma once
#ifdef __MACOSX__
#include <SDL2_ttf/SDL_ttf.h>
#else
#include <SDL_ttf.h>
#endif
#include "sxEnumClasses.hpp"

class sxFont
{
public:
  TTF_Font*     font;
  sxString      filename;
  sxInt32       size;
  sxFontQuality quality;
  sxColor       fontColor;
  sxColor       backgroundColor;
 
  sxFont();
  ~sxFont();
  void setQuality(const sxString& fontQuality);
  sxBool isFontLoaded();
  ReturnCodes loadFont();
  void destroyFont();

private:

};
