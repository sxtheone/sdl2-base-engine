#pragma once
#include "sxEvent.hpp"

#define getEventManager EventManager::getInstance()

class EventManager
{
public:
  static EventManager* getInstance();
  static void createManager();
  void destroyManager();

  // this one is mainly for phones but uses parts of SDL what are commonly available so no compiler switch for now
  void activateAccelerometerAsJoytickListening(const sxBool enable);

  void setMousePositionCorrector(const sxVec2Real& _mousePositionCorrector);
  sxVec2Real getMousePositionCorrector(){return mousePositionCorrector;}

  // user have to set SDL_HINT_ANDROID_SEPARATE_MOUSE_AND_TOUCH to "1" if using this custom touch to mouse
  void setTouchToMouseConversion(const sxBool enable){touchToMouseConversion = enable;}
  sxBool isTouchToMouseEnabled(){return touchToMouseConversion;}

  void update(); // updates event queue. Should be called in every loop
  void clearEventQueue(); // deletes certain events from the queue. Call only in MainLoop
  sxBool isEventActive(const sxEventTypes& event);
  /// getNextEvent: gets and removes next event from queue.
  /// outEvent always gets initialized at the start of the function!
  void getNextEvent(const sxEventTypes& eventType, sxEvent& outEvent);
  void pushEventToQueue(sxEvent& event);
  sxUInt32 getNumOfEventsInQueue();
  sxUInt32 peekEventsInQueue(const sxUInt32 maxNumOfEvents,
                             SDL_Event eventArray[]); // returns with num of evens found
  sxUInt32 peekEventsInQueue(const sxUInt32 minEvent, const sxUInt32 maxEvent, 
                             const sxUInt32 maxNumOfEvents, SDL_Event eventArray[]);

private:
  static EventManager* eventManager;
  static sxVec2Real mousePositionCorrector;
  // do touch-to-mouse conversion instead of SDL2 because their solution can't handle multitouch
  static sxBool touchToMouseConversion;
  SDL_Joystick *accelerometer;

  EventManager() : accelerometer(NULL){}
  ~EventManager();
  static int filterAndCorrectEvents(void *userdata, SDL_Event * event);
  static sxBool correctMouseRelatedCoordinates(SDL_Event*& event);
  static sxBool changeTouchInputToMouse(SDL_Event*& event);
  void setupAccelerometer();
  void releaseAccelerometer();
};