#pragma once
#include "LogManager.h"

/// lightweight string class, with low memory consumption and few features
class sxCharStr
{
public:
  sxCharStr() : str(NULL), length(0) {}
  /// use the char* constructor only with NULL-terminated strings.
  /// There is also a 100 character limit!
  sxCharStr(const char* otherStr) : str(NULL), length(0){copy(otherStr);}
  sxCharStr(const sxCharStr& otherStr) : str(NULL), length(0){copy(otherStr);}
  sxCharStr(const sxString& otherStr) : str(NULL), length(0){copy(otherStr);}
  ~sxCharStr();
//  const sxChar* c_str() const {return str;}
  const char* c_str() const {return reinterpret_cast<char*>(str);}
  sxUInt32 getLength() const {return length;}
  sxBool empty(){return (0 == getLength() ? SDL_TRUE : SDL_FALSE);}
  void clear(){deleteStr();}
  sxCharStr& operator=(const sxCharStr& otherStr);
  sxCharStr& operator=(const sxString& otherStr);
  sxCharStr& operator=(const char* otherStr);
  bool operator==(const sxCharStr& otherStr) const;
  bool operator!=(const sxCharStr& otherStr) const;
  bool operator<(const sxCharStr& otherStr) const;

private:
  sxChar* str;
  sxUInt16 length;
  void copy(const sxCharStr& otherStr);
  void copy(const sxString& otherStr);
  void copy(const char* otherStr);

  void deleteStr();
  bool areEqual(const sxCharStr& otherStr) const;
};

inline
sxCharStr::~sxCharStr()
{
  deleteStr();
}

inline
void sxCharStr::deleteStr()
{
  if(NULL != str){
    delete[] str;
    str = NULL;
  }
  length = 0;
}

inline
sxCharStr& sxCharStr::operator=(const sxCharStr& otherStr){
  copy(otherStr);
  return *this;
}

inline
sxCharStr& sxCharStr::operator=(const sxString& otherStr){
  copy(otherStr);
  return *this;
}

inline
sxCharStr& sxCharStr::operator=(const char* otherStr){
  copy(otherStr);
  return *this;
}

inline
bool sxCharStr::operator==(const sxCharStr& otherStr) const
{
  return areEqual(otherStr);
}

inline
bool sxCharStr::operator!=(const sxCharStr& otherStr) const
{
  return (true == areEqual(otherStr) ? false : true);
}

inline
bool sxCharStr::operator<(const sxCharStr& otherStr) const
{
  return std::lexicographical_compare((char*)str, 
                                      (char*)(str+length),
                                      (char*)otherStr.str, 
                                      (char*)(otherStr.str+otherStr.length));
}

inline
bool sxCharStr::areEqual(const sxCharStr& otherStr) const
{
  if(length != otherStr.length){
    return false;
  }
  if(0 == length){
    return true;
  }
  sxUInt32 currPos = 0;
  while(currPos <= length){
    if(str[currPos] != otherStr.str[currPos]){
      return false;
    }
    ++currPos;
  }
  return true;  
}

inline
void sxCharStr::copy(const sxCharStr& otherStr)
{
  deleteStr();
  length = otherStr.length;
  if(0 != length){
    str = new sxChar[length+1]();
    std::memcpy(str, otherStr.str, length);
  }
}

inline
void sxCharStr::copy(const sxString& otherStr)
{
  deleteStr();
  length = static_cast<sxUInt16>(otherStr.size());
  if(otherStr.empty()){
    return;
  }
  str = new sxChar[length+1]();
  std::memcpy(str, otherStr.c_str(), length);
}

inline
void sxCharStr::copy(const char* otherStr)
{
  deleteStr();
  while( otherStr[length] != '\0' &&
         length < 100 )
  {
    ++length;
  }
  if(0 == length){
    return;
  }
  str = new sxChar[length+1]();
  std::memcpy(str, otherStr, length);
  if(100 == length){
    str[length-1] = '\0';
    sxLOG_E("Incoming char+ string was longer than 100 chars,"
      " it got truncated: %s", c_str());
  }
}
