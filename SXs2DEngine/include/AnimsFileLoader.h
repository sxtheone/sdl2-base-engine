#pragma once
#include "sxAnimSprite.h"
#include "FileLoaderBase.h"

//TODO: Anim/Sprite/ObjectFileLoader function names are not correlating (section/field processing naming)
class AnimsFileLoader : public FileLoaderBase
{
public:
  AnimsFileLoader();
  ReturnCodes processFile(const sxString& filename);
private:
  sxAnimSprite* newAnimation;
  sxAnimFrame* newFrame;
  sxUInt32 prevAnimTime;
  sxString currentLine;

  void initialize();
  ReturnCodes processSpriteSourceSection();
  ReturnCodes processAllAnimations();
  ReturnCodes processOneAnimation();
  ReturnCodes processSpritesSection();
  ReturnCodes addSpritesToAnimation(std::vector<sxString>& spriteNames);
  void dealWithAncestorIfPresent();
  void processAllFrames();
  void processAnimFrame(const sxUInt32 nextAnimFrame);
  void processFrameTime();
  void setFrameTimeToPrevTime();
  void processGoto();
  void processColorTint();
};
