#pragma once
#include <string>
#ifdef __MACOSX__
#include <SDL2/SDL.h>
#else
#include <SDL.h>
#endif
#include "stringTools.h"
#include "sxFile.h"

typedef enum
{
  LL_ERROR,
  LL_WARNING,
  LL_PERFWARN, // Performance Warning. Glitches can be avoided by 
               //loading resources to memory in load-time
  LL_TRACE  
} sxLogLevel;

#define LOGFILE_DIR "/logs/"
#define LOGFILE_NAME "log_sxe_%s.log"

#define sxLOG(LL, ...){ \
  sxString logMsg; \
  if(LL == LL_ERROR)  {logMsg = "!E! ";}; \
  if(LL == LL_WARNING){logMsg = "!W! ";}; \
  if(LL == LL_PERFWARN){logMsg = "!PW! ";}; \
  if(LL == LL_TRACE)  {logMsg = "!T! ";}; \
  logMsg += strFormatter("%s: ", __FUNCTION__) + \
            strFormatter(__VA_ARGS__); \
  if(LL != LL_TRACE && LL != LL_PERFWARN) \
    SDL_Log("%s", logMsg.c_str()); \
  LogManager::toFile(LL, logMsg.c_str()); \
}

#define sxLOG_E(...) sxLOG(LL_ERROR, __VA_ARGS__)
#define sxLOG_W(...) sxLOG(LL_WARNING, __VA_ARGS__)
#define sxLOG_PW(...) sxLOG(LL_PERFWARN, __VA_ARGS__)
#if(_DEBUG) // no need for gazillion unnecessary messages
#define sxLOG_T(...) sxLOG(LL_TRACE, __VA_ARGS__)
#else
#define sxLOG_T(...) sxLOG(LL_TRACE, __VA_ARGS__)
//#define sxLOG_T(...)
#endif

class LogManager
{
public:
  static sxBool errorHappened;
  static sxBool warningHappened;

  static void init(sxBool shouldLogToFile = SDL_TRUE);
  static void toFile(sxLogLevel logLevel, sxString msg);

#if(_DEBUG)
  static sxString getErrorMessages(){return errorLogMessages;}
  static sxString getWarningMessages(){return warningLogMessages;}
  static void clearErrorAndWarningMessages(){errorLogMessages.clear(); warningLogMessages.clear();}
#endif

private:
  static sxString APPLICATION_VERSION;
  static sxString logFileName;
  static sxLogLevel globalLogLevel;
  static sxBool     logToFile;
  static sxString errorLogMessages;
  static sxString warningLogMessages;
};
