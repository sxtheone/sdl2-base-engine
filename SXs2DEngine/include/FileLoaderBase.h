#pragma once
#include "INIReader.h"

static const sxString commandSectionName = "command:";

class FileLoaderBase
{
public:
  FileLoaderBase() : reader(NULL){}
  virtual ~FileLoaderBase(){}
  virtual ReturnCodes processFile(const sxString& filename) = 0;

protected:
  INIReader* reader;
  sxString ancestorName;
  sxString currentSectionName;

  ReturnCodes createReader(const sxString& fileToLoad, sxBool checkIfAlreadyLoaded = SDL_TRUE);
  void destroyReader();
  sxBool shouldExitFromLoader(ReturnCodes& result);
  void substractAncestorFromCurrentSectionName();
  virtual ReturnCodes loadFileList(const sxString& fieldName, FileLoaderBase& loader);
  sxBool isCommandSection(const sxString& sectionName);
  sxString getCommandName(const sxString& sectionName);
  sxString getCommandParameters(const sxString& sectionName);
};
