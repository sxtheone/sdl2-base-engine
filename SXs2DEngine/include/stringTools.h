// source:
// http://stackoverflow.com/questions/69738/c-how-to-get-fprintf-results-as-a-stdstring-w-o-sprintf#69911
// It's under BSD license.

#pragma once

#include <string>
#include <cstdarg>
#include <vector>
#include <string>
#include "ReturnCodes.hpp"

#define strFormatter stringTools::_strFormatter
#define trimTrailingAndLeadingSpaces stringTools::_trimTrailingAndLeadingSpaces
#define trimLeadingSpaces stringTools::_trimLeadingSpaces
#define trimTrailingSpaces stringTools::_trimTrailingSpaces
#define strToDouble stringTools::_strToDouble
#define strToReal stringTools::_strToReal
#define strToInt stringTools::_strToInt

class stringTools
{
public:
  static sxString _strFormatter(const char *fmt, ...);
  static void _trimTrailingAndLeadingSpaces(sxString& line);
  static void _trimLeadingSpaces(sxString& line, const sxUInt32 startIndex = 0);
  static void _trimTrailingSpaces(sxString& line, sxString::size_type endIndex = sxString::npos);
  static ReturnCodes _strToDouble(const sxString& strValue, sxDouble& doubleValue);
  static ReturnCodes _strToReal(const sxString& strValue, sxFloat& realValue);
  static ReturnCodes _strToInt(const sxString& strValue, sxInt32& intValue);

private:
// intended to be just a helper function
  static sxString vformat (const char *fmt, va_list ap);
};
