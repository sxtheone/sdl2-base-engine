#pragma once
#include "ObjectDrawerPosOffset.hpp"
#include "FadeInfo.h"

class ObjectDrawerRenderToTexture : public ObjectDrawerPosOffset
{
public:
  ObjectDrawerRenderToTexture();
  ObjectDrawerRenderToTexture(const sxVec2Int& offset);
  virtual ~ObjectDrawerRenderToTexture();
  void setColorModification(const sxColor newColorMod){colorModifier = newColorMod;}
  sxColor getColorModification(){return colorModifier;}
  void resetColorModification(){colorModifier = {255, 255, 255, 255};}
  virtual void drawAll(SDL_Renderer* renderer);
  void addToClearSpaceVector(const sxRect& objHitbox); /* adds info to a vector, will clear
   the render texture under this object before drawing anything else.
   objHitbox.bottomRight contains the object sprite's width and height, without any offset! */
  void forceFullRedraw(){drawMethod = ObjectDrawerDrawMethodEnum::FULLREDRAW;}
  
  void setupFade(const FadeInfo::FadeType type, const sxInt32 fadeTime){fadeInfo.setup(type, fadeTime);}
  void startFade(){fadeInfo.startFade();}
  void stepFade(const sxUInt32 ticksPassed){fadeInfo.stepFade(ticksPassed, this);}
  sxBool fadeInProgress(){return fadeInfo.fadeInProgress();}
  FadeInfo getFadeInfo(){return fadeInfo;}
  void setFadeInfo(const FadeInfo& fadeInfo){this->fadeInfo = fadeInfo;}

protected:
  sxBool renderingPrepared;
  sxBool posOffsetUsed;
  sxColor colorModifier;
  FadeInfo fadeInfo;

  virtual void doUpdateNoMovementDraw(SDL_Renderer*& renderer);
  void prepareForDrawing(SDL_Renderer*& renderer);
  void fillWithTransparent(SDL_Renderer*& renderer, Object*& obj);

private:
  sxColor origDrawColor;
  std::vector<sxRect> objsToClear; // clear the objects' sprite in the render texture

  SDL_Texture *origRenderTexture; // the one what is already set as render target
  SDL_Texture *renderTexture;
  SDL_BlendMode origBlendMode;
  ObjectDrawerDrawMethod drawMethod;

  void init();
  void createTextures(SDL_Renderer*& renderer);
  void storeOriginalRenderInfo(SDL_Renderer*& renderer);
  void changeRenderTargetTo(SDL_Renderer*& renderer, SDL_Texture* texture);
  void setRenderInfoForRenderToTexture(SDL_Renderer*& renderer);

  void doDrawing(SDL_Renderer*& renderer);
  void doFullRedraw(SDL_Renderer*& renderer);
  void processObjsToClearVector(SDL_Renderer*& renderer);
  
  void restoreOriginalRenderTargetAndRender(SDL_Renderer*& renderer);
  void restoreOriginalRenderInfo(SDL_Renderer*& renderer);
  void fillWithTransparent(SDL_Renderer*& renderer, const sxRect& objRect);

  void createFullscreenTexture(SDL_Renderer*& renderer, SDL_Texture*& texture);
  void destroyTexture(SDL_Texture*& texture);

  void mergeObjsToClearVectorItems();
  sxBool objectsCanBeMerged(const sxRect& hb1, const sxRect& hb2, sxRect& result);
  sxBool objectsCanBeMergedXAligned(const sxRect& hb1, const sxRect& hb2, sxRect& result);
  sxBool objectsCanBeMergedYAligned(const sxRect& hb1, const sxRect& hb2, sxRect& result);
  sxBool objectContainedByOther(const sxRect& hb1, const sxRect& hb2, sxRect& result);

  void activateColorModifications();
  void resetColorModifications();
};

