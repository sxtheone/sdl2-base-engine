#pragma once
#include "sxEventTypes.hpp"
#include "sxActChange.hpp"
#include "sxWindowOpacity.hpp"
#include "sxResizeWindow.hpp"

typedef SDL_CommonEvent           sxCommonEvent;
typedef SDL_WindowEvent           sxWindowEvent;
typedef SDL_KeyboardEvent         sxKeyboardEvent;
typedef SDL_TextEditingEvent      sxTextEditingEvent;
typedef SDL_TextInputEvent        sxTextInputEvent;
typedef SDL_MouseMotionEvent      sxMouseMotionEvent;
typedef SDL_MouseButtonEvent      sxMouseButtonEvent;
typedef SDL_MouseWheelEvent       sxMouseWheelEvent;
typedef SDL_JoyAxisEvent          sxJoyAxisEvent;
typedef SDL_JoyBallEvent          sxJoyBallEvent;
typedef SDL_JoyHatEvent           sxJoyHatEvent;
typedef SDL_JoyButtonEvent        sxJoyButtonEvent;
typedef SDL_JoyDeviceEvent        sxJoyDeviceEvent;
typedef SDL_ControllerAxisEvent   sxControllerAxisEvent;
typedef SDL_ControllerButtonEvent sxControllerButtonEvent;
typedef SDL_ControllerDeviceEvent sxControllerDeviceEvent;
typedef SDL_QuitEvent             sxQuitEvent;
typedef SDL_UserEvent             sxUserEvent;
typedef SDL_SysWMEvent            sxSysWMEvent;
typedef SDL_TouchFingerEvent      sxTouchFingerEvent;
typedef SDL_MultiGestureEvent     sxMultiGestureEvent;
typedef SDL_DollarGestureEvent    sxDollarGestureEvent;
typedef SDL_DropEvent             sxDropEvent;

class sxEvent
{
public:
  sxEvent();
  void destroyEvent(); /// don't forget to call when event is handled outside processInputEvent()
  /// don't forget to update the = operator if new sxEvenTypes introduced
  sxUInt32& type(){return event.type;}
  sxCommonEvent& common(){return event.common;}
  sxWindowEvent& window(){return event.window;}         
  sxKeyboardEvent& key(){return event.key;}          
  sxTextEditingEvent& edit(){return event.edit;}      
  sxTextInputEvent& text(){return event.text;}        
  sxMouseMotionEvent& motion(){return event.motion;}    
  sxMouseButtonEvent& button(){return event.button;}    
  sxMouseWheelEvent& wheel(){return event.wheel;}      
  sxJoyAxisEvent& jaxis(){return event.jaxis;}         
  sxJoyBallEvent& jball(){return event.jball;}         
  sxJoyHatEvent& jhat(){return event.jhat;}           
  sxJoyButtonEvent& jbutton(){return event.jbutton;}     
  sxJoyDeviceEvent& jdevice(){return event.jdevice;}     
  sxControllerAxisEvent& caxis(){return event.caxis;}    
  sxControllerButtonEvent& cbutton(){return event.cbutton;}
  sxControllerDeviceEvent& cdevice(){return event.cdevice;}
  sxQuitEvent& quit(){return event.quit;}             
  //sxUserEvent& user(){return event.user;} disabled until it is really needed  
  sxSysWMEvent& syswm(){return event.syswm;}           
  sxTouchFingerEvent& tfinger(){return event.tfinger;}   
  sxMultiGestureEvent& mgesture(){return event.mgesture;} 
  sxDollarGestureEvent& dgesture(){return event.dgesture;}
  sxDropEvent& drop(){return event.drop;}  
  sxActChange& actChange();
  sxWindowOpacity& windowOpacity();
  sxResizeWindow& resizeWindow();

  operator SDL_Event*()
  {
    return &event;
  }

  sxEvent(sxEvent& other)
  {
    event = SDL_Event();
    *this = other;
  }

  sxEvent& operator=(const sxEvent& other)
  {
    destroyEvent();
    if(sxEventTypes::CHANGE_ACT == other.event.type){
      event.type = other.event.type;
      actChange() = *static_cast<sxActChange*>(other.event.user.data1);
    }else if (sxEventTypes::WINDOW_OPACITY == other.event.type){
      event.type = other.event.type;
      windowOpacity() = *static_cast<sxWindowOpacity*>(other.event.user.data1);
    }else if (sxEventTypes::RESIZE_WINDOW == other.event.type) {
      event.type = other.event.type;
      resizeWindow() = *static_cast<sxResizeWindow*>(other.event.user.data1);
    }else{
      event = other.event;
    }
    return *this;
  }

  sxEvent& operator=(const SDL_Event& other)
  {
    destroyEvent();
    if (sxEventTypes::CHANGE_ACT == other.type){
      event.type = other.type;
      actChange() = *static_cast<sxActChange*>(other.user.data1);
    }else if (sxEventTypes::WINDOW_OPACITY == other.type){
      event.type = other.type;
      windowOpacity() = *static_cast<sxWindowOpacity*>(other.user.data1);
    }else if (sxEventTypes::RESIZE_WINDOW == other.type){
      event.type = other.type;
      resizeWindow() = *static_cast<sxResizeWindow*>(other.user.data1);
    }else{
      event = other;
    }
    return *this;
  }

private:
  SDL_Event event;
  
};

inline
sxEvent::sxEvent()
{
  event = SDL_Event();
}

inline
void sxEvent::destroyEvent()
{
  /// don't forget to update the = operator if new sxEvenTypes introduced
  if(NULL != event.user.data1){
    if(sxEventTypes::CHANGE_ACT == event.type){
      delete static_cast<sxActChange*>(event.user.data1);
      event.user.data1 = NULL;
    }else if (sxEventTypes::WINDOW_OPACITY == event.type){
      delete static_cast<sxWindowOpacity*>(event.user.data1);
      event.user.data1 = NULL;
    }else if (sxEventTypes::RESIZE_WINDOW == event.type) {
        delete static_cast<sxResizeWindow*>(event.user.data1);
        event.user.data1 = NULL;
    }
  }
}

inline
sxActChange& sxEvent::actChange()
{
  if(sxEventTypes::CHANGE_ACT != event.type){
    sxLOG_E("Invalid event type: %d. Set event type first and call functions after.", event.type);
    event.type = sxEventTypes::CHANGE_ACT;
  }
  if(NULL == event.user.data1){
    event.user.data1 = new sxActChange();
  }  
  return *static_cast<sxActChange*>(event.user.data1);
}

inline
sxWindowOpacity& sxEvent::windowOpacity()
{
    if (sxEventTypes::WINDOW_OPACITY != event.type) {
        sxLOG_E("Invalid event type: %d. Set event type first and call functions after.", event.type);
        event.type = sxEventTypes::WINDOW_OPACITY;
    }
    if (NULL == event.user.data1) {
        event.user.data1 = new sxWindowOpacity();
        static_cast<sxWindowOpacity*>(event.user.data1)->value = 1;
    }
    return *static_cast<sxWindowOpacity*>(event.user.data1);
}

inline
sxResizeWindow& sxEvent::resizeWindow()
{
    if (sxEventTypes::RESIZE_WINDOW != event.type) {
        sxLOG_E("Invalid event type: %d. Set event type first and call functions after.", event.type);
        event.type = sxEventTypes::RESIZE_WINDOW;
    }
    if (NULL == event.user.data1) {
        event.user.data1 = new sxResizeWindow();
    }
    return *static_cast<sxResizeWindow*>(event.user.data1);
}
