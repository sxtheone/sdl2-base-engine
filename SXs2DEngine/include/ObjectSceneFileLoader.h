#pragma once
#include "SceneFileLoaderBase.h"
#include "ObjectsFileLoader.h"
#include "Object.h"
#include "ObjectDrawerBase.hpp"
#include "ObjectDrawerRenderToTexture.h"

static const sxString commandGenerate = "Generate";
static const sxString commandDialogWindow = "DialogWindow";

class AncestorInfo
{
public:
  sxUInt32 posInVector;
  sxString objectName;
};

class ObjectSceneFileLoader : public SceneFileLoaderBase
{
public:
  ObjectSceneFileLoader(const sxUInt32 sceneID);
  ~ObjectSceneFileLoader();
  ReturnCodes processFile(const sxString& filename);
  ObjectDrawerBase* getFilledObjectDrawer(){return drawer;}
  INIReader* getReaderObject(){return reader;}
  sxVec2Real getSlidingSpeed(){return slidingSpeed;}
  sxVec2Real getSlidingDistance(){return slidingDistance;}

private:
  std::map<sxString, AncestorInfo> ancestorInfo; // object name, position in Scene::objects
  Object* newObject;
  sxString sceneName;
  ObjectDrawerBase* drawer;
  std::vector<Object*>* objects;
  sxBool drawerIsOrdered;
  sxVec2Real slidingSpeed;
  sxVec2Real slidingDistance;

  ReturnCodes processSections();
  ReturnCodes processConfigSection();
  ReturnCodes processOneObjectSection();
  void processSlidingInfo();
  sxString createObject();
  sxString getObjectName();
  Object* createObjectFromAncestor(sxString& outObjectName);
  Object* createObjectNoAncestor(const sxString& objectName);
  void fillNewObjectWithAncestorData(sxString& outAncestorObjectName);
  ReturnCodes processObject(const sxString& objectName);
  ReturnCodes loadObjectPosition();
  void processSceneObjectDrawerSettings();
  sxBool processObjectOrderedObjectDrawer();
  void setupFade(ObjectDrawerRenderToTexture* drawer);
  void processCommand();
  void processGenerateCommand();
  void processDialogWindowCommand();
};
