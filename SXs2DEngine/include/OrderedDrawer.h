#pragma once
#include <list>
#include "LogManager.h"
#include "Object.h"

#define getOrderedDrawer OrderedDrawer::getInstance()

class DrawQueue
{
public:
  std::list<Object*> queue;
  //NOTE: C++11: std::forward_list<Object*> queue; Consumes less memory.
  //      Check commits made before 10.03.2016
  sxBool drawFinished;
  DrawQueue() : drawFinished(SDL_FALSE){}
};

/*
This is a singleton helper for ObjectDrawerOrdered. 
The main idea is that we need a drawer what orders all the 
object in a draw queue regardless what scene are they in.
That's why a singleton is needed. 
*/
class OrderedDrawer
{
public:
  static sxBool isCreated(){return (orderedDrawer ? SDL_TRUE : SDL_FALSE);}
  static void createManager();
  static OrderedDrawer* getInstance();
  void destroyManager();
  void clearDrawQueues(){drawQueues.clear();}
  void frameStarts();
  void frameEnds(){}
  void addToQueue(const sxUInt32 queueId, Object* object);
  void objectMoved(const sxUInt32 queueId, Object* object);
  void objectDeleted(const sxUInt32 queueId, const sxInt32 objectId);
  void draw(/*const sxInt32 drawQueueId, */SDL_Renderer* renderer);

private:
  static OrderedDrawer* orderedDrawer;
  std::vector<DrawQueue> drawQueues;
  std::vector<DrawQueue>::iterator currQueueIt;
  std::list<Object*>::iterator listIt;

  OrderedDrawer();
  ~OrderedDrawer();
  void addObjectToQueue(const sxUInt32 queueId, Object* object);
  sxBool findObjectPrevAndCurrentPlace(Object* object,
                                       std::list<Object*>::iterator& oldPlace,
                                       std::list<Object*>::iterator& newPlace);
};

