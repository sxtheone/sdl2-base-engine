#pragma once
#include <vector>
#include "sxTypes.h"

class GridElement
{
public:
  sxUInt32 itemIdx;
  sxVec2Real position;
  GridElement() : itemIdx(0) {}
  GridElement(const sxUInt32 _itemIdx, const sxVec2Real& _position) : itemIdx(_itemIdx),
                                                                  position(_position) {}
};

class ScreenGrid
{
public:
  ScreenGrid() : fillingJustStarted(SDL_TRUE) {}
  void setGridSize(const sxVec2Int& size, const sxVec2Int& sizeOfOneTile,
                   const sxVec2Int& sizeOfTheBiggestHitbox);
  void setGrid00pos(const sxVec2Int& pos){grid00pos.x = static_cast<sxFloat>(pos.x); 
                                          grid00pos.y = static_cast<sxFloat>(pos.y);}
  void setElement(const GridElement& element);
  /// setSearchRectangle: pixelPos is not grid position!
  void setSearchRectangle(const sxVec2Real& pixelPos, const sxVec2Int& size);
  void setSearchRectNew(std::vector< std::vector<GridElement> >& sGrid);
  /// converts objectIndex to x,y to be inside the search grid and gives back data.
  /// Throw error log if index is out of bounds
  GridElement* getGridElement(const sxUInt32 objectIndex);
  sxUInt32 getNumOfItemsInSearchGrid();

  /// only for debugging purposes!
  sxVec2Int getGridSize(){return gridSize;}
  GridElement* getGridElement(const sxUInt32 col, const sxUInt32 row){return &grid[col][row];}

  /*
  void clearGrid()
  {
    for(sxUInt32 x=0; x<grid.size(); ++x){
      for(sxUInt32 y=0; y<grid[0].size(); ++y){
        grid[x][y].itemIdx = 0;
        grid[x][y].position = sxVec2Real();
      }
    }
  }
*/
private:
  sxVec2Real grid00pos;
  sxBool fillingJustStarted;
  sxVec2UInt8 currGridPos;
  sxVec2UInt8 searchRectTopLeft;
  sxVec2UInt8 searchRectBottomRight;
  sxVec2Int searchGridSize;
  sxVec2Int biggestTileExtraPixels;
  sxVec2Int gridSize;
  sxVec2Int tileSize;
  std::vector< std::vector<GridElement> > grid; // [x][y]

  sxVec2UInt8 getGridPos(const sxVec2Real& pixelPos);
  sxBool isGridPositionValid(const sxVec2UInt8& pos);
  void correctInvalidSearchRectangleSize(sxVec2UInt8& searchRect);
  sxVec2UInt8 convertIndexToGridCoord(const sxUInt32 objectIndex);
};
