/**
SceneCollisionPropsProcessor:
- the Collision Detector works with bitmasks. SceneCollisionPropsProcessor generates the bitmasks
- if you are absolutely sure that no other Acts will be loaded before deleting ALL the current Acts,
  call clearCollisionProperties(). This will release all the data and gives back memory but if you
  add an Act after calling it, collision detection will go nuts
**/
#pragma once
#include <map>
#include "sxCharStr.hpp"

#define getSceneCollisionPropsProcessor SceneCollisionPropsProcessor::getInstance()

class SceneCollisionSettings
{
public:
  std::map<sxCharStr, std::vector<sxCharStr> > rawMap;
  std::map<sxCharStr, CollisionProps> finalMap;
  CollisionProps nextCollisionID;

  void init(){
    rawMap.clear();
    finalMap.clear();
    nextCollisionID.collisionID = 1;
  }
};

class SceneCollisionPropsProcessor
{
public:
  static void createManager();
  static SceneCollisionPropsProcessor* getInstance();
  void destroyManager();

  void addToSceneCollisionMap(const sxCharStr& collisionID,
                              const std::vector<sxCharStr>& collidesWith);
  void processCollisionSettings(); // call before getCollisionPropsForScene()
  CollisionProps getCollisionPropsForScene(const sxCharStr& sceneName);
  void clearCollisionProperties();

private:
  static SceneCollisionPropsProcessor* collisionPropsProcessor;
  SceneCollisionSettings sceneSettings;
  std::map<sxCharStr, std::vector<sxCharStr> >::iterator rawIt;
  std::map<sxCharStr, CollisionProps>::iterator propsIt;

  SceneCollisionPropsProcessor();
  ~SceneCollisionPropsProcessor();
  sxBool warningIfInvalidGroupNameInCollisionMap(std::map<sxCharStr, std::vector<sxCharStr> >& rawMap);
  sxBool tooManyCollisionGroups(sxUInt32 vectorSize);
  void fillCollisionIDFields();
  void fillCollidesWithFields();
};