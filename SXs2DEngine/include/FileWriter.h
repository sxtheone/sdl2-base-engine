#pragma once
#include <map>
#include <vector>
#include "sxTypes.h"
#include "SystemIO.h"
#include "sxFile.h"
#include "LogManager.h"

class FileWriter
{
public:
  FileWriter() : file(NULL){}
  virtual ~FileWriter();
  ReturnCodes createFileForWriting(const sxString& dir, const sxString& filename);
  ReturnCodes openExistingFileForWriting(const sxString& dir, const sxString& filename);
  sxString getFilename(){return filename;}
  void clearData(){iniData.clear();} // clear all data collected
  virtual ReturnCodes saveFile(const sxBool clearDataFromMemAfterSaving = SDL_TRUE);
  void closeFile();
  sxString getData();

  void createSection(const sxString& name);
  // sets section as current. If section doesn't exist, RC_SECTION_NOT_FOUND is returned
  ReturnCodes setSection(const sxString& name);

  void addBool(const sxString fieldName, const sxBool value);
  void addInt32(const sxString fieldName, const sxInt32 value);
  void addInt64(const sxString fieldName, const sxInt64 value);
  void addDouble(const sxString fieldName, const sxDouble value);
  void addString(const sxString fieldName, const sxString& value);
  void addMatrix2D(const sxString fieldName, const sxMatrix2D& value);

private:
  sxFile* file;
  sxString filename;
  std::map<sxString, std::map<sxString, sxString> > iniData;
  std::vector<sxString> sectionOrderInFile;

  sxString currentSection;
  ReturnCodes getFileForWriting(const sxString& dir, const sxString& filename, const sxString openMode);
};
