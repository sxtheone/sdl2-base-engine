#pragma once
#include "sxAnimSprite.h"

class sxAnimContainer
{
public:
  sxAnimSprite* animSprite;

  sxAnimContainer(){init();}
  sxAnimContainer(sxAnimSprite* animSpr);
  sxBool colorTintingSet();

private:

  void init();
};

inline
sxAnimContainer::sxAnimContainer(sxAnimSprite* animSpr)
{
  init();
  animSprite = animSpr;
}

inline
sxBool sxAnimContainer::colorTintingSet()
{
  if(NULL == animSprite) return SDL_FALSE;
  return animSprite->colorTintingSet();
}

inline
void sxAnimContainer::init()
{
  animSprite = NULL;
}