#pragma once
#include "SceneMsgTypes.hpp"

class SceneMsgSendString : public SceneMsg
{
public:
  enum{ DEFAULT_ID = -1 };
  sxString value;
  sxInt32 id; // not mandatory but can be used to store some values used for identification

  SceneMsgSendString() : id(DEFAULT_ID){
    type = SceneMsgTypes::SceneMsgSendString;
  }

  virtual ReturnCodes loadData(INIReader* reader){
    std::vector<sxString> valueList;
    ReturnCodes result = reader->getStrList("sceneMessageData", valueList);
    if(ReturnCodes::RC_SUCCESS == result){
      value = valueList[0];
      if(valueList.size() > 1) result = strToInt(valueList[1], id);
    }
    return result;
  }

  virtual SceneMsg& operator=(const SceneMsg& other){
    SceneMsg::operator=(other);
    const SceneMsgSendString* castedValue = static_cast<const SceneMsgSendString*>(&other);
    value = castedValue->value;
    id = castedValue->id;
    return *this;
  }
};
