#pragma once
#ifdef __MACOSX__
#include <SDL2/SDL.h>
#else
#include <SDL.h>
#endif
#include <queue>
#include "sxEnumClasses.hpp"
#include "SceneMsg.hpp"
#include "sxEvent.hpp"
#include "CollisionInfo.hpp"
#include "Object.h"
#include "SceneProps.hpp"

class Scene
{
public:
  SceneType type;
  sxString name; /// used as the collisionID. It can be blank if no collision required
  sxUInt32 id; /// generated and used by ActInfo, to know what act is containing this scene
  SceneCollisionProps* collisionProps;
  SceneProps sceneProps;

  Scene() : renderer(NULL), collisionProps(NULL), id(-1) {}
  virtual ~Scene();
  void setRenderer(SDL_Renderer* _renderer);
  SceneMsg* msgToOtherScenes();
  virtual sxBool handleMsgFromOtherScene(SceneMsg* msg); // returns SDL_TRUE if msg was handled
  virtual sxBool processInputEvent(sxEvent& event){return SDL_FALSE;}
  virtual void handleCollision(CollisionInfo& info){}
  virtual void handleCollisionWithTiledScene(std::vector<CollisionInfo>& collInfo){}
  virtual ReturnCodes loadFile(const sxString& filename) = 0;
  virtual void draw() = 0;
  virtual void stepAnimation(const sxUInt32 ticksPassed) = 0;
  virtual CollisionData getCollisionData(const sxUInt32 objectIndex) = 0;
  virtual sxUInt32 getNumOfCollisionObjects() = 0;
  virtual void collisionDetectionFinishedForObject(const sxUInt32 objIdx){}

protected:
  SDL_Renderer* renderer;
  std::queue<SceneMsg*> messagesToSend; // send to other scenes

  void addSceneMsgToQueue(SceneMsg* msg);
  void addStringSceneMsgToQueue(const sxString& strMsg, const sxBool broadcast);
  void addStringSceneMsgToQueue(const sxString& strMsg, const sxInt32 id, const sxBool broadcast);
};
