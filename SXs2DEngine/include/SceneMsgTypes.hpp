#pragma once
#include "SceneMsg.hpp"

namespace SceneMsgTypes{
  const SceneMsgID SceneMsgSendString(0, sxCharStr("SendString"));
  const SceneMsgID SceneMsgCloseScene(1, sxCharStr("CloseScene"));
};

//#include "UserSceneMsgTypes.hpp"
