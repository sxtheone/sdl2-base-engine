#pragma once
#include "Object.h"

class BasicObject : public Object
{
public:
  BasicObject();
  virtual ~BasicObject();
  void setSprite(sxSprite* newSprite, sxBool shouldDeleteSprite = SDL_FALSE);
  sxBool isSpriteLoaded();
  virtual ReturnCodes loadData(INIReader* reader);

protected:
    sxBool destroySprite;

    virtual void assign(const ObjectBase& source);
    void destroyTheSprite();
    virtual void callDrawMethodAndModifiers(SDL_Renderer*& renderer);
};
