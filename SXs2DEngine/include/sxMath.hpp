#pragma once

namespace sxMath
{
  template<typename T>
  void orderAscending(T& first, T& second)
  {
    if(first > second){
      T temp = first;
      first = second;
      second = temp;
    }
  }

  template<typename T>
  sxBool posNextToEachOther(const T& pos1, const T& pos2)
  {
    if( abs(pos1.x - pos2.x) == 1 ||
        abs(pos1.y - pos2.y) == 1 )
    {
      return SDL_TRUE;
    }
    return SDL_FALSE;
  }

  sxBool posNextToEachOther(const sxMatrix2D& pos1, const sxMatrix2D& pos2)
  {
    if( abs(pos1.col - pos2.col) == 1 ||
        abs(pos1.row - pos2.row) == 1 )
    {
      return SDL_TRUE;
    }
    return SDL_FALSE;
  }

};