#pragma once
#include "LogManager.h"

class RawTileMap
{
public:
  RawTileMap() : numOfItemsInARow(0), numOfRows(0){}
  void addRow(const std::vector<sxUInt32>& row);
  sxUInt32 getItem(const sxMatrix2D& position);
  void clearMap(){map.clear(); numOfItemsInARow = 0; numOfRows = 0;}
  void shrinkMapCapacityToSize(){std::vector<std::vector<sxUInt32> >(map).swap(map);}
  sxUInt32 numberOfItemsInARow() const {return numOfItemsInARow;} // every row is same sized
  sxUInt32 numberOfRows() const {return numOfRows;}
  std::vector<sxUInt32>::iterator getMapRowBeginIterator(const sxUInt32 row){return map[row].begin();}

protected:
  sxUInt32 numOfItemsInARow;
  sxUInt32 numOfRows;
  std::vector<std::vector<sxUInt32> > map; // indexes of the tiles vector
  sxBool rowSizeIsOK(const std::vector<sxUInt32>& row);
};

inline
void RawTileMap::addRow(const std::vector<sxUInt32>& row)
{
  if(rowSizeIsOK(row)){
    map.push_back(row);
    ++numOfRows;
    if(0 == numOfItemsInARow){
      numOfItemsInARow = map[0].size();
    }
  }
}

inline
sxUInt32 RawTileMap::getItem(const sxMatrix2D& position)
{
#if _DEBUG
  return map.at(position.row).at(position.col);
#else
  return map[position.row][position.col];
#endif
}

inline
sxBool RawTileMap::rowSizeIsOK(const std::vector<sxUInt32>& row)
{
  if( map.empty() ||
      numOfItemsInARow == row.size() )
  {
    return SDL_TRUE;
  }

  sxLOG_E("Every row should be same-sized. Erroneous row size: %d, should be: %d",
    row.size(), map[0].size());
  return SDL_FALSE;
}