#pragma once
#include <iostream>
#ifdef __MACOSX__
#include <SDL2/SDL.h>
#include <SDL2_image/SDL_image.h>
#include <SDL2_ttf/SDL_ttf.h>
#else
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#endif

#include "SDLGFX_FramerateManager.hpp"

static const sxString MAIN_CONFIG_FILENAME = "config/main.config";

class MainLoop
{
public:
  MainLoop();
  ~MainLoop();
  sxBool run();

protected:
  SDL_Window   *mainWindow;
  SDL_Renderer *mainRenderer;
  SDLGFX_FramerateManager *fpsManager;
  sxRect screenSize;
  sxRect logicalSize;
  SDL_Texture *renderTexture;
  sxDouble remainingTicks;
  sxUInt32 maxAllowedFrameStep;

  ReturnCodes init();
  virtual void handleActChange() = 0;

  sxBool initSDL();
  sxBool createGameWindow();
  sxBool createRenderer();
  ReturnCodes initWindowAndRenderer();
  void initTTFPlugin();
  void setRenderLogicalSize();
  void setScreenSizeConfig();
  sxVec2Real calculateMousePosCorrector();
  sxUInt32 setupWindowFlags();
  void createRenderTexture();
  virtual void registerSceneMsgs();
  virtual void loadStartingAct();
  virtual void loadAct(const sxString& actName);
  virtual void handleWindowEvents();
  virtual void handleWindowOpacity();
  virtual void handleResizeWindow();
  void draw();
  void resizeWindow(sxVec2Int windowSize, sxVec2Int windowLogicalSize, sxVec2Int pos);
  void deleteMainWindow();
  void deleteRenderer();
  void runInnerLoopWithSDLGFX_framerateManager(sxUInt32& timePassed);
  void runInnerLoopMyFramerateManager(sxUInt32& timePassed);
  void stepAnimation(const sxUInt32 timePassed);
  void enableProfilerFPSCounter(sxBool enable);
  void checkObjectSizes();
#ifdef __sdlANDROID__
  void androidFocusChangeChecks();
  void appWillEnterBackground();
  void appWillEnterForeground();
  void appDidEnterBackground();
  void appDidEnterForeground();
#endif
};