/***
WindowScene
Just a simple ObjectScene what has collision detection.
Good for very basic UI windows where the only clickable Objects are buttons
with event and scene message sending props.
***/

#pragma once
#include "ObjectScene.h"
#include "GeneralCollisionDetector.hpp"

class WindowScene : public ObjectScene
{
public:
  virtual void draw();
  virtual sxBool handleMsgFromOtherScene(SceneMsg* msg);
  virtual sxBool processInputEvent(sxEvent& event);

private:
  GeneralCollisionDetector collDetector;
  
  sxBool objectHit(Object* obj, sxEvent& inputPressed);
};

