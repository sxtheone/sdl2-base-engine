#pragma once
#include "FileLoaderBase.h"

class ConfigFileLoader : public FileLoaderBase
{
public:
  ReturnCodes processFile(const sxString& filename);
  ~ConfigFileLoader();
  sxBool setBaseConfigSection(){return reader->setSection("BaseConfig");}
  sxBool setAudioConfigSection(){return reader->setSection("AudioConfig");}
  sxBool setStartupSection(){return reader->setSection("Startup");}
  void getWindowLogicalSize(sxVec2Int& windowLogicalSize);
  void getWindowScreenSize(sxVec2Int& windowScreenSize);
  void getDesiredFPS(sxUInt16& desiredFPS);
  void getWindowType(sxUInt32& windowType);
  void getLetterboxInfo(sxBool& letterbox);
  void getWindowName(sxString& windowName);
  void getSpriteScalingMethod(sxString& spriteScalingMethod);
  void getBackgroundColor(sxColor& backgroundColor);
  void getAudioFrequency(sxUInt16& frequency);
  void getAudioNumberOfChannels(sxUInt8& channels);
  void getAudioChunkSize(sxUInt16& chunksize);
  void getStartingActFilename(sxString& actFilename);

private:
  ReturnCodes parseConfigFile(const sxString& filename);
  sxInt32 getInt(const sxString& name, sxInt32 default_value, sxBool mandatory = SDL_TRUE);
  void processSystemFont();
};
