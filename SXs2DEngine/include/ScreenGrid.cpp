#include "ScreenGrid.h"
#include "LogManager.h"
#include "Profiler.h"

void ScreenGrid::setGridSize(const sxVec2Int& size, const sxVec2Int& sizeOfOneTile,
                             const sxVec2Int& sizeOfTheBiggestHitbox)
{
  grid.resize(size.x);
  for(sxInt32 i=0; i<size.x; ++i){
    grid[i].resize(size.y);
  }
  gridSize = size;
  biggestTileExtraPixels = sizeOfTheBiggestHitbox;
  biggestTileExtraPixels.x -= sizeOfOneTile.x;
  biggestTileExtraPixels.y -= sizeOfOneTile.y;
  /// set the search grid to maximum size by default
  searchRectTopLeft = sxVec2UInt8();
  searchRectBottomRight = sxVec2UInt8(255,255);
  tileSize = sizeOfOneTile;
}

void ScreenGrid::setElement(const GridElement& element)
{
  return;
/*
  if(SDL_FALSE == fillingJustStarted){
    currGridPos = getGridPos(element.position);
  }else{
    fillingJustStarted = SDL_FALSE;
    currGridPos = sxVec2UInt8();
  }
  if(SDL_TRUE == isGridPositionValid(currGridPos)){
    grid[currGridPos.x][currGridPos.y] = element;
  }
*/
}

sxVec2UInt8 ScreenGrid::getGridPos(const sxVec2Real& pixelPos)
{
  sxVec2UInt8 result;
  if(grid00pos.x != pixelPos.x){
    result.x = static_cast<sxUInt8>((abs(grid00pos.x) + pixelPos.x) / tileSize.x);
  }else{
    result.x = 0;
  }
  if(grid00pos.y != pixelPos.y){
    result.y = static_cast<sxUInt8>((abs(grid00pos.y) + pixelPos.y) / tileSize.y);
  }else{
    result.y = 0;
  }

  return result;
}

sxBool ScreenGrid::isGridPositionValid(const sxVec2UInt8& pos)
{
  if(pos.x < gridSize.x){
    if(pos.y < gridSize.y){
      return SDL_TRUE;
    }
    sxLOG_E("Incoming Y coord is invalid. Coord: %d, grid size: %d",
      pos.y, gridSize.y);
  }else{
    sxLOG_E("Incoming X coord is invalid. Coord: %d, grid size: %d",
      pos.x, gridSize.x);
  }
  return SDL_FALSE;
}

void ScreenGrid::setSearchRectangle(const sxVec2Real& pixelPos, const sxVec2Int& size)
{
  sxVec2Real startPos = pixelPos;
  startPos.x -= biggestTileExtraPixels.x;
  startPos.y -= biggestTileExtraPixels.y;
  searchRectTopLeft = getGridPos(startPos);
  //q = (x + y - 1) / y;
  searchRectBottomRight.x = searchRectTopLeft.x + ((size.x + tileSize.x - 1) / tileSize.x);
  searchRectBottomRight.y = searchRectTopLeft.y + ((size.y + tileSize.y - 1) / tileSize.y);

  correctInvalidSearchRectangleSize(searchRectTopLeft);
  correctInvalidSearchRectangleSize(searchRectBottomRight);

  searchGridSize.x = 1 + searchRectBottomRight.x - searchRectTopLeft.x;
  searchGridSize.y = 1 + searchRectBottomRight.y - searchRectTopLeft.y;
}

void ScreenGrid::setSearchRectNew(std::vector< std::vector<GridElement> >& sGrid)
{
  searchGridSize.x = sGrid.size();
  searchGridSize.y = sGrid[0].size();

  searchRectTopLeft = sxVec2UInt8();
  searchRectBottomRight = sxVec2UInt8(searchGridSize.x, searchGridSize.y);
  for(int col=0; col<searchGridSize.x; ++col){
    for(int row=0; row<searchGridSize.y; ++row){
      grid[col][row] = sGrid[col][row];
    }
  }

}

void ScreenGrid::correctInvalidSearchRectangleSize(sxVec2UInt8& searchRect)
{
  if(0 > searchRect.x){
    searchRect.x = 0;
  }
  if(0 > searchRect.y){
    searchRect.y = 0;
  }
  if(gridSize.x <= searchRect.x){
    searchRect.x = gridSize.x-1;
  }
  if(gridSize.y <= searchRect.y){
    searchRect.y = gridSize.y-1;
  }
}

GridElement* ScreenGrid::getGridElement(const sxUInt32 objectIndex)
{
  sxVec2UInt8 coord = convertIndexToGridCoord(objectIndex);
  if( coord.x > searchRectBottomRight.x ||
      coord.y > searchRectBottomRight.y )
  {
    sxLOG_E("objectIndex out of searchGrid bounds: %d", objectIndex);
    return NULL;
  }
  return &grid[coord.x][coord.y];
}

sxVec2UInt8 ScreenGrid::convertIndexToGridCoord(const sxUInt32 objectIndex)
{
  sxVec2UInt8 result = searchRectTopLeft;
  result.x += objectIndex % searchGridSize.x;
  result.y += objectIndex / searchGridSize.x;
  return result;
}

sxUInt32 ScreenGrid::getNumOfItemsInSearchGrid()
{
  return searchGridSize.x*searchGridSize.y;
}
