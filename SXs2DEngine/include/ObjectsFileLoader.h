#pragma once
#include "FileLoaderBase.h"
#include "ObjectBuilder.h"

static const sxString commandMakeTiles = "MakeTiles";

class ObjectFileLoader : public FileLoaderBase
{
public:
  sxBool checkIfFileAlreadyLoaded; // and don't process again if yes

  ObjectFileLoader() : checkIfFileAlreadyLoaded(SDL_TRUE), objectBuilder(nullptr){}
  ~ObjectFileLoader();
  ReturnCodes processFile(const sxString& filename);
  const std::vector<sxString>& getLoadedObjectsNames(){return loadedObjectsNames;}

private:
  ObjectBase* newObject;
  sxString sceneName;
  sxString collisionGroupID;
  ObjectBuilder* objectBuilder;
  std::vector<sxString> loadedObjectsNames;

  ReturnCodes processSections();
  void dealWithAncestorIfPresent();
  ReturnCodes processConfigSection();
  void processObjectSections();
  void processOneObjectSection();
  ReturnCodes loadAnObjectFromFile();
  ReturnCodes createObject();
  ObjectBase* createOrdinaryObject(); /// any  object except label
  ObjectBase* createLabelObject();
  ReturnCodes loadObjectData();
  ObjectBase* generateFontObject(const sxString& fontName, const sxString& text);
  void loadCollisionRelatedCommonFields();
  void loadCollisionRelatedCommonFieldsForTileElement(const sxString& sprName);
  void setClickable();
  void createCollisionProps();
  void loadCollidesWith();
  void loadIdleTimeAfterCollision();
  void createHitboxForObjectIfNotExistButShould(); // mainly for generated objects
  void processCommand();
  void processMakeTilesCommand();
  void loadAllTiles(std::vector<sxString>& spriteNameList);
  void createTilesWithSpriteList(const ObjectBase* referenceObject,
                                 const std::vector<sxString>& spriteNameList);
  void addObjectToObjectManager(const sxString& objectName, ObjectBase* object);
};

