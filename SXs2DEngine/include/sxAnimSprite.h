#pragma once
#include <vector>
#include "sxSprite.h"
#include "sxAnimFrame.hpp"

#define TINT_TIME_INVALID 60000000 /*1000 minutes*/

class sxAnimSprite
{
public:
  // tinting. Fade in/out also possible with tinting
  sxColor  targetColorTint;
  sxInt32  tintTimes[4]; // How long should tinting reach the destination color (in millisec)

  sxAnimSprite();
  ~sxAnimSprite();
  void addSprite(sxSprite* sprite);
  ReturnCodes addFrame(sxAnimFrame* newFrame);
  sxSprite* getSprite(sxUInt8 spriteNum);
  sxAnimFrame* getFrame(sxUInt8 frameNum);
  sxBool isFrameValid(sxUInt8 frameNum);
  void setTargetTintColor(const sxColor tintColor){targetColorTint = tintColor;}
  void setTintTimes(const sxInt32 r, const sxInt32 g, const sxInt32 b, const sxInt32 a);
  sxBool colorTintingSet(){return (tintTimes[0] == TINT_TIME_INVALID) ? SDL_FALSE : SDL_TRUE;}

private:
  std::vector<sxSprite*> sprites;
  std::vector<sxAnimFrame*> animFrames;
};
