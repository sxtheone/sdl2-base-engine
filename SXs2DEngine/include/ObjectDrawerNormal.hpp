#pragma once
#include "ObjectDrawerBase.hpp"

class ObjectDrawerNormal : public ObjectDrawerBase
{
public:
  ObjectDrawerNormal(){
    type = ObjectDrawerTypeEnum::NORMAL;
  }

  virtual void drawAll(SDL_Renderer* renderer)
  {
    sxUInt32 numOfObjs = objects.size();
    for(sxUInt32 i=0; i<numOfObjs; ++i){
      objects[i]->draw(renderer);
    }
  }

  virtual void draw(const sxUInt32 objIdx, SDL_Renderer* renderer)
  {
    objects[objIdx]->draw(renderer);
  }
};