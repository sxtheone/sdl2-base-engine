#pragma once
#include "sxTexture.h"
#include "sxTypes.h"

class sxSprite : public sxTexture
{
public:
  sxSprite();
  sxRect size; // position in textureMap, width, height
  sxQuad* hitbox; // don't call delete on this
  SDL_Texture* getTexture(){return texture;}
  /* setTexture() functions:
     - Sets width and height automatically to the size of the texture.
     - shouldDestroyOnExit defines if texture should be destroyed when new texture loaded
       or object gets destroyed. */
  void setTexture(SDL_Texture* sdlTexture, sxBool shouldDestroyOnExit = SDL_FALSE);
  void setTexture(sxTexture* otherTexture, sxBool shouldDestroyOnExit = SDL_FALSE);
  void setTopLeftPosition(sxVec2Int newPos);
  void setWidthHeight(sxVec2Int newPos);
  void setMaxWidthHeight();
};
