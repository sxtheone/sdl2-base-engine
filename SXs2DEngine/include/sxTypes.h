#pragma once
#include "ReturnCodes.hpp"

class sxColorRGB
{
public:
  sxUInt8 r;
  sxUInt8 g;
  sxUInt8 b;

  sxColorRGB() : r(0), g(0), b(0) {}
  sxColorRGB& operator=(const sxColor& v) {
      r = v.r;
      g = v.g;
      b = v.b;
      return *this;
  }
  bool operator==(const sxColor& v) {
      return r == v.r &&
             g == v.g &&
             b == v.b;
  }
  bool operator!=(const sxColorRGB& v) {
      return r != v.r ||
             g != v.g ||
             b != v.b;
  }
};

template<typename Number>
class sxVec2
{
public:
  sxVec2() : x(0), y(0) {}
  sxVec2(Number _x, Number _y) : x(_x), y(_y) {}
  sxBool operator==(const sxVec2& other) const {
    if(other.x == x && other.y == y){
      return SDL_TRUE;
    }
    return SDL_FALSE;
  }
  sxBool operator!=(const sxVec2& other) const {
    if(other.x != x || other.y != y){
      return SDL_TRUE;
    }
    return SDL_FALSE;
  }
  sxVec2 operator*(const sxVec2& other){
    sxVec2 result;
    result.x = x*other.x;
    result.y = y*other.y;
    return result;
  }
  sxVec2 operator+=(const sxVec2& other){
    x += other.x;
    y += other.y;
    return *this;
  }
  Number x;
  Number y;
};

typedef sxVec2<sxUInt8> sxVec2UInt8;
typedef sxVec2<sxInt32> sxVec2Int;
typedef sxVec2<sxFloat> sxVec2Real;

class sxMatrix2D
{
public:
  sxMatrix2D() : col(0), row(0) {}
  sxMatrix2D(sxUInt16 _col, sxUInt16 _row) : col(_col), row(_row) {}
  sxBool operator==(const sxMatrix2D& other) const {
    if(other.col == col && other.row == row){
      return SDL_TRUE;
    }
    return SDL_FALSE;
  }
  sxBool operator!=(const sxMatrix2D& other) const {
    if(other.col != col || other.row != row){
      return SDL_TRUE;
    }
    return SDL_FALSE;
  }
  sxMatrix2D& operator+=(const sxMatrix2D& other) {
      col += other.col;
      row += other.row;
      return *this;
  }  
  void set(const sxUInt16 _col, const sxUInt16 _row){col = _col; row = _row;}

  sxUInt16 col;
  sxUInt16 row;
};

template<typename VectorType>
class sxQuadTemplate
{
public:
  VectorType topLeft;
  VectorType bottomRight;

  sxQuadTemplate() : topLeft(0,0), bottomRight(0,0){}
  
  sxBool operator==(const sxQuadTemplate& other) const {
    if( other.topLeft == topLeft && 
        other.bottomRight == bottomRight )
    {
         return SDL_TRUE;
    }
    return SDL_FALSE;
  }

  sxVec2Int getSize() const
  {
    sxVec2Int result;
    result.x = static_cast<sxInt32>(bottomRight.x - topLeft.x);
    result.y = static_cast<sxInt32>(bottomRight.y - topLeft.y);
    return result;
  }
};
typedef sxQuadTemplate<sxVec2Real> sxQuad;
typedef sxQuadTemplate<sxVec2Int> sxQuadInt;

class CollisionProps // other possible name: InteractionProps ?
{
public:
  sxUInt32 collisionID; // other possible name: id ?
  sxUInt32 collidesWith; // other possible name: interactionMask ?

  CollisionProps(){
    collisionID = 0;
    collidesWith = 0;
  }
  virtual ~CollisionProps(){}
  sxBool canCollideWith(const sxUInt32 otherCollisionID){
    if(0 == (collidesWith & otherCollisionID)){
      return SDL_FALSE;
    }
    return SDL_TRUE;
  }
  sxBool canCollideWith(const CollisionProps* otherCollisionProps){
    return canCollideWith(otherCollisionProps->collisionID);
  }
};

class ObjCollisionProps : public CollisionProps
{
public:
  sxQuad* hitbox; // don't use 'delete' with this
  sxUInt32 idleAfterCollision; // ms. No collision handling will happen in this object until this time passes

  ObjCollisionProps(){
    hitbox = NULL;
    idleAfterCollision = 1;
  }
  ObjCollisionProps& operator=(const CollisionProps& v) {
      collisionID = v.collisionID;
      collidesWith = v.collidesWith;
      return *this;
  } 
};

typedef CollisionProps SceneCollisionProps;

class sxDirection
{
public:
  enum Directions { 
    INVALID = 0,
    LEFT = 1,
    RIGHT = 2,
    UP = 4,
    DOWN = 8 
  };
  sxDirection(){direction = INVALID;}
  void resetDirection(){direction = INVALID;}
  void addDirection(const Directions dir)
  {
    direction = static_cast<Directions>(direction + dir);
  }
  sxBool isLeftSet() const {return (direction & LEFT) ? SDL_TRUE : SDL_FALSE;}
  sxBool isRightSet() const {return (direction & RIGHT) ? SDL_TRUE : SDL_FALSE;}
  sxBool isUpSet() const {return (direction & UP) ? SDL_TRUE : SDL_FALSE;}
  sxBool isDownSet() const {return (direction & DOWN) ? SDL_TRUE : SDL_FALSE;}
  sxBool isInvalid() const {return (INVALID == direction) ? SDL_TRUE : SDL_FALSE;}
private:
  Directions direction;
};
