#pragma once
#include "sxSprite.h"
#include "FileLoaderBase.h"

static const sxString textureMapDataSectionName = "TextureMapData";
static const sxString commandSlice = "Slice";

class SpritesFileLoader : public FileLoaderBase
{
public:
  ReturnCodes processFile(const sxString& filename);

private:
  sxString textureMapId;
  sxSprite currentSprite;

  sxString processTextureMapDataSection();
  sxString loadTextureMap();
  void createHitbox(const sxString& hitboxName);
  void processHitboxField();
  void createHitboxFromSpritesize(const sxString& hitboxName);

  void loadAllSprites();
  void loadOneSprite();
  void loadAncestorsDataIfAvailable();
  void fillSpriteRelatedData(const sxString& spriteName);
  void fillTopLeftRelatedData();
  void fillWidthHeightRelatedData();
  void setSpriteWidthHeightIfInvalid();
  void processCommand(const sxString& sectionName);
  void processSliceCommand(sxString spriteNameBase);
};

