#pragma once
#include "LogManager.h"

class SceneProps
{
public:
  enum Properties{
    SCENE_ENABLED = 1,
    VISIBILE = 2,
    CATCH_ALL_INPUT = 4,
    INVALID = 128
  };

  SceneProps(){ value = SCENE_ENABLED | VISIBILE; }
  void setEnabled(sxBool enabled){changeProp(SCENE_ENABLED, enabled);}
  void setVisibility(sxBool enabled){changeProp(VISIBILE, enabled);}
  void setCatchAllInput(sxBool enabled){changeProp(CATCH_ALL_INPUT, enabled);}
  sxBool enabled() const {return tosxBool(value & SCENE_ENABLED);}
  sxBool visible() const {return tosxBool(value & VISIBILE);}
  sxBool catchAllInput() const {return tosxBool(value & CATCH_ALL_INPUT);}

  SceneProps& operator=(const SceneProps& v) {
      value = v.value;
      return *this;
  } 

private:
  sxUInt8 value;

  void changeProp(Properties prop, sxBool newValue)
  {
    if(prop >= 0 && prop < INVALID){
      if(SDL_TRUE == newValue){
        value |= prop;
      }else if(prop == (prop & value)){
        value ^= prop;
      }
    }else{
      sxLOG_E("Invalid incoming value: %d", prop);
    }
  }
  sxBool tosxBool(sxUInt8 num) const
  {
    if(0 == num){return SDL_FALSE;}
    return SDL_TRUE;
  }
};
