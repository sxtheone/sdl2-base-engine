#pragma once
#include "LogManager.h"

class ObjectProps
{
public:
  enum Properties{
    OBJECT_ENABLED = 1,
    COLLISION_POSSIBLE = 2,
    VISIBILE = 4,
    COLLISION_ENABLED = 8, // collision can be disabled even if it is possible
    SHOULD_REDRAW = 16,
    CLICKABLE = 32, // user can click on it with mouse/touch/etc.
    INVALID = 128
  };

  ObjectProps(){ value = OBJECT_ENABLED | VISIBILE; containerSceneID = -1;}
  void setSceneID(const sxUInt32 id){containerSceneID = id;}
  void setObjectEnabled(sxBool enabled){changeProp(OBJECT_ENABLED, enabled);}
  void setCollisionPossible(sxBool possible){changeProp(COLLISION_POSSIBLE, possible);}
  void setCollisionEnabled(sxBool enabled){changeProp(COLLISION_ENABLED, enabled);}
  void setClickable(sxBool enabled){changeProp(CLICKABLE, enabled);}
  void setVisibility(sxBool enabled){changeProp(VISIBILE, enabled);}
  void setShouldRedraw(sxBool enabled){changeProp(SHOULD_REDRAW, enabled);}
  sxUInt32 sceneID(){return containerSceneID;}
  sxBool objectEnabled() const {return tosxBool(value & OBJECT_ENABLED);}
  sxBool collisionPossible() const {return tosxBool(value & COLLISION_POSSIBLE);}
  sxBool collisionEnabled() const {return tosxBool(value & COLLISION_ENABLED);}
  sxBool clickable() const {return tosxBool(value & CLICKABLE);}
  sxBool objectVisible() const {return tosxBool(value & VISIBILE);}
  sxBool shouldRedraw() const {return tosxBool(value & SHOULD_REDRAW);}
  /*
  ObjectProps& operator=(const ObjectProps& v) {
    containerSceneID = v.containerSceneID;
    value = v.value;
    return *this;
  } 
  */
private:
  sxUInt32 containerSceneID;
  sxUInt8 value;

  void changeProp(Properties prop, sxBool newValue)
  {
    if(prop >= 0 && prop < INVALID){
      if(SDL_TRUE == newValue){
        value |= prop;
      }else if(prop == (prop & value)){
        value ^= prop;
      }
    }else{
      sxLOG_E("Invalid incoming value: %d", prop);
    }
  }
  sxBool tosxBool(sxUInt8 num) const
  {
    if(0 == num){return SDL_FALSE;}
    return SDL_TRUE;
  }
};
