#pragma once
#include "sxCharStr.hpp"

class SceneMsgID
{
public:
  sxUInt16 id;
  sxCharStr idStr;

  SceneMsgID() : id(0) {}
  SceneMsgID(const sxUInt16 _id, const sxCharStr& _idStr)
    : id(_id), idStr(_idStr) {}
  bool operator==(const SceneMsgID& other) const;
  bool operator==(const sxCharStr& other) const;
  bool operator!=(const SceneMsgID& other) const;
  bool operator<(const SceneMsgID& other) const;
private:
};

inline
bool SceneMsgID::operator==(const SceneMsgID& other) const
{
  return id == other.id;
}

inline
bool SceneMsgID::operator==(const sxCharStr& other) const
{
  return idStr == other;
}

inline
bool SceneMsgID::operator!=(const SceneMsgID& other) const
{
  return id != other.id;
}

inline
bool SceneMsgID::operator<(const SceneMsgID& other) const
{
  return id < other.id;
}