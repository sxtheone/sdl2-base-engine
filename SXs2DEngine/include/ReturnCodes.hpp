#pragma once
#include "sxTypeDefs.h"

class ReturnCodes
{
public:
  enum Codes{
    RC_SUCCESS = 0,
    RC_SDL_INIT_FAILED,
    RC_CANT_CREATE_MAIN_WINDOW,
    RC_CANT_CREATE_RENDERER,
    RC_RENDERER_IS_NULL,
    RC_OPEN_AUDIO_FAILED,
// FILE related
    RC_FILE_CANT_BE_OPENED,
    RC_FILE_PARSE_ERROR,
    RC_FILE_READ_ERROR,
    RC_END_OF_FILE,
    RC_FILE_ALREADY_LOADED,
    RC_FIELD_NOT_FOUND,
    RC_SECTION_NOT_FOUND,
    RC_LINE_EMPTY,
    RC_CANT_CREATE_DIRECTORY,
// Directory
    RC_DIRECTORY_READ_ERROR,
// Others
    RC_SCENEFILE_NOT_SET,
    RC_CANT_CREATE_OBJECT,
    RC_OBJECT_ALREADY_LOADED,
    RC_OBJECT_NOT_FOUND,
    RC_SERIOUS_ERROR,
    RC_INVALID_ANIMATION_NAME,
    RC_FONT_ALREADY_LOADED,
    RC_FONT_NOT_FOUND,
    RC_ACT_SCENE_ID_INVALID,
    RC_SPRITE_NOT_FOUND,
    RC_TEXTURE_NOT_FOUND,
    RC_INVALID_VALUE,
    RC_INVALID
  };

  ReturnCodes(){ value = RC_SUCCESS; }
  ReturnCodes(sxInt8 i){ assign(i); }

  ReturnCodes& operator=(const ReturnCodes& v) {
      value = v.value;
      return *this;
  }
  operator sxInt32() const { return value; }

private:
  Codes value;

  void assign(sxInt8 i)
  {
    if(i <= RC_INVALID && i >= RC_SUCCESS){
      value = static_cast<Codes>(i);
    }else{
      throw "ReturnCodes::assign: Value out of Bounds!";
    }
  }
};