#pragma once
#include <map>
#include "Object.h"
#include "sxAnimContainer.hpp"


/// if you use AnimObject as ancestor, don't forget to write a new copyEverythingTo()!
/// Otherwise the new variables won't be copied.
class AnimObject : public Object
{
public:
  AnimObject();
  virtual ~AnimObject(){}
  void addAnimation(sxString animName, sxAnimSprite* animSprite);
  virtual ReturnCodes setAnimation(sxString animName);
  void play(sxBool shouldPlay){spriteChangerPlaying = shouldPlay; tintPlaying = shouldPlay;}
  void resetAnimationPosition();
  sxBool isPlaying();
  virtual void stepAnimation(const sxUInt32 ticksPassed);
  virtual ReturnCodes loadData(INIReader* reader);
  sxString getActiveAnimName();
  void getAnimationNames(std::vector<sxString>& animNames);
  virtual void skipCurrentAnim();

protected:
  static const sxUInt32 MAX_ANIM_LENGTH = static_cast<sxUInt32>(-1); // the theoretical maximum
  //TODO: check if sxCharStr (with the conversions from/to sxString) worth the change
  std::map<sxString, sxAnimContainer> animations;
  sxAnimFrame* currentFrame;
  std::map<sxString, sxAnimContainer>::iterator currentAnimIt;

  // sprite changer
  sxDouble timePassed;
  sxBool spriteChangerPlaying; // step to the next sprite while the anim is rolling

  // tinting
  sxFloat targetColorTint[4]; // comes from the actual animation.targetTintColor
  sxFloat tintChangePerTick[4]; // amounts per color what should tinting change every millisec
  sxFloat preciseColorTint[4];
  sxBool tintPlaying;

  virtual void assign(const ObjectBase& source);
  sxBool stepSpriteChangerAnim();
  void setupColorTintAnim();
  void calculateTintChangeTicks();
  sxBool stepColorTintAnim(const sxUInt32 ticksPassed);
  void setDisplaySizeForCurrentSprite();
  void loadAnimSprites(const std::vector<sxString>& animNames);
  void loadAnimationSettings(INIReader* reader);
  virtual void callDrawMethodAndModifiers(SDL_Renderer*& renderer);

};
