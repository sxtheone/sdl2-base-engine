#pragma once
#include "StaticTiledScene.h"
#include "TiledSceneFileLoader.h"
#include "ScreenGrid.h"

class ScrollableTiledScene : public StaticTiledScene
{
public:
  ScrollableTiledScene();
  virtual void draw();
  virtual void stepAnimation(const sxUInt32 ticksPassed);
  virtual void handleCollision(CollisionInfo& info){}

protected:
  sxDirection scrollDirection;
  // loops are drawn after each other in a row. Rows drawn under each other are always the same!
  sxVec2Int velocity; // don't forget to call setScrollDirection() when changing velocity 

  virtual void init();
  ReturnCodes processFileContent(const sxString& filename, TiledSceneFileLoader& loader);
  void processOffsetChanges();
  sxBool cutOffsetIfNeeded(const sxInt32 stepSize, sxFloat& currOffset);
  void setScrollDirection(const sxVec2Int& dirVector){setScrollDirTemplate<sxVec2Int>(dirVector);}
  void setScrollDirection(const sxVec2Real& dirVector){setScrollDirTemplate<sxVec2Real>(dirVector);}
  
private:
  sxVec2Int prevPosToDraw; // the pixels already moved

  void dealWithAllTheDrawingStuff();
  void dealWithScrolling();
  sxBool doHorizontalScrolling(const sxVec2Int& shiftOffset);
  sxBool doVerticalScrolling(const sxVec2Int& shiftOffset);
  virtual void drawOneRow(); // returns with the number of columns drawn
  sxVec2Int shiftRenderedTextureWithOffset();
  template<typename VecType>
  void setScrollDirTemplate(const VecType& dirVector);
  sxVec2Int calcScrollOffset();
};

template<typename VecType>
void ScrollableTiledScene::setScrollDirTemplate(const VecType& dirVector)
{
  scrollDirection.resetDirection();
  if(0 < dirVector.x){
    scrollDirection.addDirection(sxDirection::RIGHT);
  }else if(0 > dirVector.x){
    scrollDirection.addDirection(sxDirection::LEFT);
  }
  if(0 < dirVector.y){
    scrollDirection.addDirection(sxDirection::DOWN);
  }else if(0 > dirVector.y){
    scrollDirection.addDirection(sxDirection::UP);
  }
}