#pragma once
#include "ObjectScene.h"
#include "FontManager.h"

class TestSceneFonts : public ObjectScene
{
public:
  TestSceneFonts();
  ~TestSceneFonts();
  ReturnCodes loadFile(const sxString& filename);
  virtual void draw();
  virtual sxBool processInputEvent(sxEvent& event);
  virtual void stepAnimation(const sxUInt32 ticksPassed);

private:
  BasicObject* numOfTexts;
  sxUInt8 blinkChangeCounter;
  sxBool blinkEnabled;
  BasicObject* blinkObj;
  sxVec2Real blinkPos;

  void createObjects();
  void destroyObjects();
};

inline
TestSceneFonts::TestSceneFonts()
{
  blinkChangeCounter = 0;
  numOfTexts = NULL;
  blinkObj = NULL;
  blinkEnabled = SDL_TRUE;
}

inline
TestSceneFonts::~TestSceneFonts()
{
  destroyObjects();
}

inline
ReturnCodes TestSceneFonts::loadFile(const sxString& filename)
{
  ReturnCodes result = ObjectScene::loadFile(filename);
  if(ReturnCodes::RC_SUCCESS == result){
    createObjects();
  }
  return result;
}

inline
void TestSceneFonts::createObjects()
{
  /// at this point there is already one object in objects vector.
  /// That is a BasicObject generated from font and instantiated in the
  /// .objectscene file. That BasicObject is stored in ObjectManager and
  /// available for every scene/act.

  BasicObject* obj;
  /// creating a font this way makes the font local, meaning no other scene
  /// can use it. It also will be deleted when this scene gets destroyed.
  /// If more than one instance gets created from this scene, the same amount
  /// will be created from these font BasicObjects.
  getFontManager->setFontToRender("Dragonian");
  sxColor fontColor;
  fontColor.r = 80; fontColor.g = 215; fontColor.b = 215; fontColor.a = 255;
  obj = getFontManager->generateText("I'm always happy", fontColor);
  obj->setPosition(sxVec2Real(150.0f, 250.0f));
  objects->push_back(obj);

  getFontManager->setFontToRender("Thetamax");
  obj = getFontManager->generateText("even when I'm sad");
  obj->setPosition(sxVec2Real(500.0f, 300.0f));
  objects->push_back(obj);

  getFontManager->setFontToRender("AZ Kiss");
  obj = getFontManager->generateText("even when I'm sad", fontColor);
  obj->setPosition(sxVec2Real(150.0f, 350.0f));
  objects->push_back(obj);

  // write the number of animated sprites
  numOfTexts = getFontManager->generateSystemText(strFormatter("Fonts: %d", objects->size()));
  numOfTexts->setPosition(sxVec2Real(0.0f,100.0f));
}

inline
void TestSceneFonts::destroyObjects()
{
  drawer->destroyObjects();
  delete numOfTexts;
  if(NULL != blinkObj){
    delete blinkObj;
  }
}

inline
void TestSceneFonts::draw()
{
  drawer->drawAll(renderer);
  if(NULL != blinkObj){
    blinkObj->draw(renderer);
  }
  numOfTexts->draw(renderer);
}

inline
sxBool TestSceneFonts::processInputEvent(sxEvent& event)
{
  if(event.type() == sxEventTypes::KEY_DOWN){
    sxLOG_T("a key pressed!: %c", event.key().keysym.sym);
    if(event.key().keysym.scancode == SDL_SCANCODE_A){
      blinkEnabled = SDL_FALSE;
      return SDL_TRUE;
    }
    if(event.key().keysym.scancode == SDL_SCANCODE_S){
      blinkEnabled = SDL_TRUE;
      return SDL_TRUE;
    }
  }else if(event.type() == sxEventTypes::KEY_UP){
    sxLOG_T("a key released!: %c", event.key().keysym.sym);
  }

  return SDL_FALSE;
}

inline
void TestSceneFonts::stepAnimation(const sxUInt32 ticksPassed)
{  
  if( 20 <= blinkChangeCounter &&
      SDL_TRUE == blinkEnabled )
  {
    blinkPos = sxVec2Real(0.0f + rand()%700, 0.0f + rand()%670);
    blinkChangeCounter = 0;
  }else{
    ++blinkChangeCounter;
  }
  sxColor fontColor;
  fontColor.a = 120 + rand() % 135;
  fontColor.r = rand() % 255;
  fontColor.g = rand() % 255;
  fontColor.b = rand() % 255;
  
  if(NULL != blinkObj){
    delete blinkObj;
  }
  getFontManager->setFontToRender("Thetamax");
  blinkObj = getFontManager->generateText("*Bling-Bling*", fontColor);
  blinkObj->setPosition(blinkPos);
}


