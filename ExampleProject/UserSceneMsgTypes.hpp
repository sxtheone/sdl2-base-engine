#pragma once
#include "SceneMsgID.hpp"

/// id number is used for quick identification, must be unique.
/// id string is used to create instance, also must be unique
namespace SceneMsgTypes{
  const SceneMsgID SceneMsgChangeCoinNumber(10, sxCharStr("SceneMsgChangeCoinNumber"));
}
