#include "TileMapTest.h"
#include "ObjectManager.h"
#include "MainConfig.h"


TileMapTest::TileMapTest()
{
  xGoLeft = SDL_TRUE;
  yGoUp = SDL_TRUE;
  xSizeReduce = SDL_TRUE;
  ySizeReduce = SDL_TRUE;
  ticksDone = 0;
}

TileMapTest::~TileMapTest()
{
  destroyObjects();
}

ReturnCodes TileMapTest::loadFile(const sxString& filename)
{
  ScrollableTiledScene::init();
  ReturnCodes result = ScrollableTiledScene::loadFile(filename);
  if(ReturnCodes::RC_SUCCESS == result){
    visibilityBorder = static_cast<Object*>(getObjectManager->createObject("testBorder"));
    visibilityBorder->setPosition(sxVec2Real(390,110));
    setSceneSize(sxVec2Int(300, 300));
  }
  return result;
}

void TileMapTest::destroyObjects()
{
  destroyTiles();
  delete visibilityBorder;
}

void TileMapTest::draw()
{
  ScrollableTiledScene::draw();
  visibilityBorder->draw(renderer);
}

void TileMapTest::stepAnimation(const sxUInt32 ticksPassed)
{
  ScrollableTiledScene::stepAnimation(ticksPassed);
  ticksDone += ticksPassed;
  doVelocityChanges(ticksDone);
  doSceneSizeChanges(ticksPassed);
  doSceneTopLeftPositionChanges(ticksPassed);
  
  // currently there is no flag for scene resize, only full redraw
  drawMethod = TiledSceneDrawMethodEnum::FULLREDRAW;
}

void TileMapTest::doVelocityChanges(sxUInt32& ticksDone)
{
  const sxInt32 changeSpeed = 1000;
  const sxVec2Int newVelocity = sxVec2Int(350,250);
  
  if(ticksDone / changeSpeed == 1){
    velocity.x = newVelocity.x;
    velocity.y = 0;
  }
  if(ticksDone / changeSpeed == 2){
    velocity.x = -newVelocity.x;
    velocity.y = 0;
  }
  if(ticksDone / changeSpeed == 3){
    velocity.x = -newVelocity.x;
    velocity.y = -newVelocity.y;
  }
  if(ticksDone / changeSpeed == 4){
    velocity.x = newVelocity.x;
    velocity.y = -newVelocity.y;
  }
  if(ticksDone / changeSpeed == 5){
    velocity.x = newVelocity.x;
    velocity.y = newVelocity.y;
  }

  if(ticksDone / changeSpeed == 6){
    velocity.x = -newVelocity.x;
    velocity.y = newVelocity.y;
  }
  if(ticksDone / changeSpeed == 7){
    velocity.x = 0;
    velocity.y = newVelocity.y;
  }
  if(ticksDone / changeSpeed == 8){
    velocity.x = 0;
    velocity.y = -newVelocity.y;
  }
  if(ticksDone / changeSpeed == 9){
    velocity.x = 0;
    velocity.y = 0;
  }
  if(ticksDone / changeSpeed == 10){
    ticksDone = 0;
  }/**/
}

void TileMapTest::doSceneSizeChanges(const sxUInt32& ticksPassed)
{
  sxFloat tickSec = ticksPassed/1000.0f;
  sxVec2Int sizeModifier = sxVec2Int(static_cast<sxInt32>(200*tickSec),
                                     static_cast<sxInt32>(200*tickSec));
  sxVec2Int currentSize = getSceneSize();
  if(currentSize.x < 55 && SDL_TRUE == xSizeReduce){xSizeReduce = SDL_FALSE;}
  if(currentSize.x > 550 && SDL_FALSE == xSizeReduce){xSizeReduce = SDL_TRUE;}
  if(currentSize.y < 55 && SDL_TRUE == ySizeReduce){ySizeReduce = SDL_FALSE;}
  if(currentSize.y > 550 && SDL_FALSE == ySizeReduce){ySizeReduce = SDL_TRUE;}

  if(SDL_TRUE == xSizeReduce){
    setSceneWidth(currentSize.x-sizeModifier.x);
  }else{
    setSceneWidth(currentSize.x+sizeModifier.x);
  }
  if(SDL_TRUE == ySizeReduce){
    setSceneHeight(currentSize.y-sizeModifier.y);
  }else{
    setSceneHeight(currentSize.y+sizeModifier.y);
  }
  visibilityBorder->setSize(getSceneSize());
}

void TileMapTest::doSceneTopLeftPositionChanges(const sxUInt32& ticksPassed)
{
  sxFloat tickSec = ticksPassed/1000.0f;
  sxVec2Int posModifier = sxVec2Int(static_cast<sxInt32>(150*tickSec),
                                     static_cast<sxInt32>(150*tickSec));
  sxVec2Int currentPos = getTopLeftPosition();
  sxVec2Int maxPos = getMainConfig->getWindowLogicalSize();
  maxPos.x -= getSceneSize().x;
  maxPos.y -= getSceneSize().y;
  if(currentPos.x < 0 && SDL_TRUE == xGoLeft){xGoLeft = SDL_FALSE;}
  if(currentPos.x > maxPos.x && SDL_FALSE == xGoLeft){xGoLeft = SDL_TRUE;}
  if(currentPos.y < 0 && SDL_TRUE == yGoUp){yGoUp = SDL_FALSE;}
  if(currentPos.y > maxPos.y && SDL_FALSE == yGoUp){yGoUp = SDL_TRUE;}

  if(SDL_TRUE == xGoLeft){
    currentPos.x -= posModifier.x;
  }else{
    currentPos.x += posModifier.x;
  }
  if(SDL_TRUE == yGoUp){
    currentPos.y -= posModifier.y;
  }else{
    currentPos.y += posModifier.y;
  }
  setTopLeftPosition(currentPos);
  visibilityBorder->setPosition(sxVec2Real(static_cast<sxFloat>(currentPos.x),
                                           static_cast<sxFloat>(currentPos.y)));
  visibilityBorder->setSize(getSceneSize());
}