#include "RazaScene.h"
#include "FontManager.h"
#include "ObjectManager.h"

RazaScene::RazaScene()
{
  numOfAnimObjs = NULL;
  objIndexWhatStops = 0;
}

RazaScene::~RazaScene()
{
  destroyObjects();
}

ReturnCodes RazaScene::loadFile(const sxString& filename)
{
  ReturnCodes result = ObjectScene::loadFile(filename);
  if(ReturnCodes::RC_SUCCESS == result){
    stepsSinceStart = 0;
    createObjects();
  }
  return result;
}

void RazaScene::createObjects()
{
  Object* animObj;
  for(sxUInt32 i=0; i<15; ++i){
    // raza is an AnimObject whoms ancestor is Object
    animObj = static_cast<Object*>(getObjectManager->createObject("raza"));
    animObj->setVelocity(sxVec2Real(32.0f+i*10, 0.0f));
    animObj->setPosition(sxVec2Real(10.0f*i, 50.0f*i));
    objects->push_back(animObj);
  }
  objIndexWhatStops = objects->size() - 10;
  sxColor colorMod;
  colorMod.g = colorMod.b = 0;
  colorMod.r = colorMod.a = 255;
  (*objects)[9]->setColorModifier(colorMod);
  // write the number of animated sprites
  numOfAnimObjs = getFontManager->generateSystemText(strFormatter("Animated Sprites: %d", objects->size()));
  numOfAnimObjs->setPosition(sxVec2Real(0.0f,50.0f));
}

void RazaScene::destroyObjects()
{
  delete numOfAnimObjs;
  drawer->destroyObjects();
}

void RazaScene::draw()
{
  drawer->drawAll(renderer);
  numOfAnimObjs->draw(renderer);
}

void RazaScene::stepAnimation(const sxUInt32 ticksPassed)
{    
  static sxVec2Real stopperRazaVelocity;
  ++stepsSinceStart;
  AnimObject* animObj;

  animObj = castToAnimObject(objIndexWhatStops);

  if(0 == stepsSinceStart%100){
    if(SDL_FALSE == animObj->isPlaying()){
      animObj->play(SDL_TRUE);
      animObj->setVelocity(stopperRazaVelocity);
    }else{
      stopperRazaVelocity = animObj->getVelocity();
      animObj->play(SDL_FALSE);
      animObj->setVelocity(sxVec2Real(0,0));
    }
  }
  for(sxUInt32 i=0; i<objects->size(); ++i){
    (*objects)[i]->stepAnimation(ticksPassed);
    if( ((*objects)[i]->getPosition().x > 1235 &&
         (*objects)[i]->getVelocity().x > 0) ||
        ((*objects)[i]->getPosition().x < 0 &&
         (*objects)[i]->getVelocity().x < 0))
    {
      if(1235 < (*objects)[i]->getPosition().x){
        castToAnimObject(i)->setSpriteFlip(SDL_FLIP_HORIZONTAL);
      }else{
        castToAnimObject(i)->setSpriteFlip(SDL_FLIP_NONE);
      }
      sxVec2Real newVelocity = (*objects)[i]->getVelocity();
      newVelocity.x *= -1;
      (*objects)[i]->setVelocity(newVelocity);
    }
    if( ((*objects)[i]->getPosition().y > 685 &&
         (*objects)[i]->getVelocity().y > 0) ||
        ((*objects)[i]->getPosition().y < 0 &&
         (*objects)[i]->getVelocity().y < 0))
    {
      sxVec2Real newVelocity = (*objects)[i]->getVelocity();
      newVelocity.y *= -1;
      (*objects)[i]->setVelocity(newVelocity);
    }
  } 
}

AnimObject* RazaScene::castToAnimObject(sxUInt32 index)
{
  if(objects->size() <= index){
    sxLOG_E("Index Invalid. Objects size: %d, Index: %d", objects->size(), index);
    return NULL;
  }
  if(ObjectTypes::AnimObject != (*objects)[index]->type){
    sxLOG_E("Obect is not Animated. Index: %d, Type: %s",
      index, (*objects)[index]->type.c_str());
    return NULL;
  }
  return static_cast<AnimObject*>((*objects)[index]);
}
