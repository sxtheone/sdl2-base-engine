#pragma once
#include <ScrollableTiledScene.h>
#include "Object.h"

class TileMapTest : public ScrollableTiledScene
{
public:
  TileMapTest();
  ~TileMapTest();
  virtual ReturnCodes loadFile(const sxString& filename);
  virtual void draw();
  virtual void stepAnimation(const sxUInt32 ticksPassed);

private:
  Object* visibilityBorder;
  sxBool xGoLeft;
  sxBool yGoUp;
  sxBool xSizeReduce;
  sxBool ySizeReduce;
  sxUInt32 ticksDone;

  void createObjects();
  void destroyObjects();
  void doVelocityChanges(sxUInt32& ticksDone);
  void doSceneSizeChanges(const sxUInt32& ticksPassed);
  void doSceneTopLeftPositionChanges(const sxUInt32& ticksPassed);
};
