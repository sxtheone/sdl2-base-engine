#pragma once
#include "BasicObject.h"
#include "UserObjectTypes.hpp"

class CoinObject : public BasicObject
{
public:
  CoinObject(){type = ObjectTypes::CoinObject;}
  virtual void objectHit(CollisionInfo& collInfo);
  virtual void objectHit(sxEvent& eventHappened){}

private:
  virtual void assign(const ObjectBase& source);
};

inline
void CoinObject::objectHit(CollisionInfo& collInfo)
{
  if(ObjectTypes::AnimObject == collInfo.otherObject.object->type){
    sxVec2Real newVelocity;
    newVelocity = static_cast<Object*>(collInfo.otherObject.object)->getVelocity();
    velocity = newVelocity;
  }
}

inline
void CoinObject::assign(const ObjectBase& source)
{
  BasicObject::assign(source); // don't forget to call the ancestor's assign function
}
