#pragma once
#include "WindowScene.h"
#include "GeneralCollisionDetector.hpp"

class TestSceneUI : public WindowScene
{
public:
  ~TestSceneUI();
  virtual ReturnCodes loadFile(const sxString& filename);
  virtual void draw();

private:
  GeneralCollisionDetector collDetector;

  void destroyObjects();
  //sxBool objectHit(sxUInt8 objNumber, sxEvent& inputPressed);
};

inline
TestSceneUI::~TestSceneUI()
{
  destroyObjects();
}

inline
ReturnCodes TestSceneUI::loadFile(const sxString& filename)
{
  return ObjectScene::loadFile(filename);
}

inline
void TestSceneUI::destroyObjects()
{
  drawer->destroyObjects();
}

inline
void TestSceneUI::draw()
{
  drawer->drawAll(renderer);
}

//inline
//sxBool TestSceneUI::objectHit(sxUInt8 objNumber, sxEvent& inputPressed)
//{
//  if(objNumber >= objects->size()){
//    sxLOG_E("Invalid incoming number: %d", objNumber);
//    return SDL_FALSE;
//  }
//  if(inputPressed.type() == sxEventTypes::MOUSE_BUTTON_UP){
//    sxLOG_T("Mouse button up: %d", inputPressed.button().button);
//    CollisionData obj = (*objects)[objNumber]->getCollisionData();
//    obj.objectIndex = objNumber;
//    return collDetector.objectCollidesWith(drawer->posOffset,
//                                           &obj,
//                                           inputPressed.button());
//  }
//  return SDL_FALSE;
//}

