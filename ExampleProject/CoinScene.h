#pragma once
#include "ObjectScene.h"
#include "BasicObject.h"
#include "UserObjectTypes.hpp"

class CoinScene : public ObjectScene
{
public:
  CoinScene();
  ~CoinScene();
  ReturnCodes loadFile(const sxString& filename);
  virtual sxBool handleMsgFromOtherScene(SceneMsg* msg);
  virtual void draw();
  virtual void stepAnimation(const sxUInt32 ticksPassed);
  virtual void handleCollision(CollisionInfo& info);

private:
  BasicObject* numOfCoins;
  sxUInt32 grayRectIdx;
  static const sxUInt16 coinWidth = 45;
  static const sxUInt16 spaceBetweenCoins = 5;

  void createObjects();
  void destroyObjects(sxInt32 startNum = 0);
  void setupCoinPositionAndVelocity();
  void addOneObject();
  void generateNumberOfCoinsFont();
  void changeObjectDirectionIfGoesOutOfScreen(sxUInt32 index);
};
