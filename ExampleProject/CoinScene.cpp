#include "CoinScene.h"
#include "AudioManager.h"
#include "FontManager.h"
#include "MainConfig.h"
#include "Profiler.h"
#include "SceneMsgChangeCoinNumber.hpp"
#include "ObjectManager.h"

CoinScene::CoinScene()
{
  numOfCoins = NULL;
}

CoinScene::~CoinScene()
{
  destroyObjects();
}

ReturnCodes CoinScene::loadFile(const sxString& filename)
{
  ReturnCodes result = ObjectScene::loadFile(filename);
  if(ReturnCodes::RC_SUCCESS == result){
    createObjects();
    getAudioManager->playSound("Start");
    if(SDL_FALSE == getAudioManager->isPlaying("Music")){
      getAudioManager->playSound("Music");
    }
  }
  return result;
}

void CoinScene::createObjects()
{
  setupCoinPositionAndVelocity();
  // write the number of coins to screen
  generateNumberOfCoinsFont();
  grayRectIdx = 0;
  if(ObjectTypes::BasicObject == (*objects)[grayRectIdx]->type){
    if(grayRectIdx < objects->size()){
      (*objects)[grayRectIdx]->setSize(sxVec2Int(getMainConfig->getWindowLogicalSize().x,50));
    }
  }else{
    sxLOG_E("Gray rectangle's position changed in objects vector.");
  }
}

void CoinScene::setupCoinPositionAndVelocity()
{
  sxVec2Real newVelocity;
  for(sxUInt32 i=0; i<objects->size(); i++){
    if(ObjectTypes::CoinObject == (*objects)[i]->type){
      newVelocity.x = static_cast<sxFloat>(rand() % 160);
      newVelocity.y = static_cast<sxFloat>(rand() % 160);
      (*objects)[i]->setVelocity(newVelocity);
      (*objects)[i]->setPosition(sxVec2Real(static_cast<sxFloat>(rand() % 1200), 
                                         static_cast<sxFloat>(rand() % 680)));
    }
  }
}

void CoinScene::generateNumberOfCoinsFont()
{
  if(NULL != numOfCoins){
    delete numOfCoins;
  }
  numOfCoins = getFontManager->generateSystemText(strFormatter("Coins: %d", objects->size()));
  numOfCoins->setPosition(sxVec2Real(0.0f,0.0f));
}

void CoinScene::destroyObjects(sxInt32 startNum)
{
 
  if (objects->size() > 0 && static_cast<sxUInt32>(startNum) < objects->size()) {
    getAudioManager->playSound("RemoveCoin");
  }

  // sxInt32 startNum, because infinite loop happened with sxUInt32 startNum.
  // 'i' went below 0 when startNum was 0
  if(0 > startNum){
    startNum = 0;
  }
  for(sxInt32 i=objects->size()-1; i>=startNum; i--){
    if(!objects->empty()){
      delete (*objects)[i];
    }
    if(i < 0){
      sxLOG_E("HOW. THE. FUCK.");
    }
  }
  objects->erase(objects->begin()+startNum, objects->end());
  
  if(0 == startNum){
    delete numOfCoins;
    numOfCoins = NULL;
  }
}

void CoinScene::draw()
{
  drawer->drawAll(renderer);
  if(NULL != numOfCoins){
    numOfCoins->draw(renderer);
  }
}

void CoinScene::stepAnimation(const sxUInt32 ticksPassed)
{ 
  getProfiler->startCountingFor("TSVector");

  for(sxUInt32 i=0; i<objects->size(); ++i){
    (*objects)[i]->stepAnimation(ticksPassed);
    changeObjectDirectionIfGoesOutOfScreen(i);
  } 
  getProfiler->stopCountingFor("TSVector");
}

void CoinScene::changeObjectDirectionIfGoesOutOfScreen(sxUInt32 index)
{
  if( ((*objects)[index]->getPosition().x > 1235 &&
        (*objects)[index]->getVelocity().x > 0) ||
      ((*objects)[index]->getPosition().x < 0 &&
        (*objects)[index]->getVelocity().x < 0))
  {
    sxVec2Real newVelocity = (*objects)[index]->getVelocity();
    newVelocity.x *= -1;
    (*objects)[index]->setVelocity(newVelocity);
  }
  if( ((*objects)[index]->getPosition().y > 685 &&
        (*objects)[index]->getVelocity().y > 0) ||
      ((*objects)[index]->getPosition().y < 0 &&
        (*objects)[index]->getVelocity().y < 0))
  {
    sxVec2Real newVelocity = (*objects)[index]->getVelocity();
    newVelocity.y *= -1;
    (*objects)[index]->setVelocity(newVelocity);
  }
}

sxBool CoinScene::handleMsgFromOtherScene(SceneMsg* msg)
{
  if(SceneMsgTypes::SceneMsgChangeCoinNumber == msg->type){
    
    sxInt32 num = static_cast<SceneMsgChangeCoinNumber*>(msg)->numToAddOrSubstract;
    if(0 < num){
      for(sxInt32 i=0; i<num; ++i){
        addOneObject();
      }
      getAudioManager->playSound("AddCoin");
    }
    if(0 > num){
      sxUInt32 start = objects->size()+num;
      destroyObjects(start);
    }
    
    generateNumberOfCoinsFont();
    
    sxLOG_T("OMG!!! This one was for me!!");
    
    return SDL_TRUE;
  }
  return SDL_FALSE;
}

void CoinScene::addOneObject()
{
  sxVec2Real newVelocity;
  /// I know that coin is a BasicObject whoms ancestor is Object
  Object* coinObject = static_cast<Object*>(getObjectManager->createObject("coin"));

  if( 0 == coinObject->getVelocity().x &&
      0 == coinObject->getVelocity().y )
  {
    newVelocity.x = static_cast<sxFloat>(rand() % 160);
    newVelocity.y = static_cast<sxFloat>(rand() % 160);
    coinObject->setVelocity(newVelocity);
    coinObject->setPosition(sxVec2Real(static_cast<sxFloat>(rand() % 1200), 
                                       static_cast<sxFloat>(rand() % 680)));
  }else{
    coinObject->setPosition(sxVec2Real(static_cast<sxFloat>(coinWidth),
                                       static_cast<sxFloat>(spaceBetweenCoins)));
  }
  objects->push_back(coinObject);
}

void CoinScene::handleCollision(CollisionInfo& info)
{
  (*objects)[info.objectIndex]->objectHit(info);
}