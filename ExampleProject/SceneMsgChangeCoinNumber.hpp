#pragma once
#include "SceneMsgTypes.hpp"
#include "UserSceneMsgTypes.hpp"

class SceneMsgChangeCoinNumber : public SceneMsg
{
public:
  sxInt32 numToAddOrSubstract;

  SceneMsgChangeCoinNumber(){
    type = SceneMsgTypes::SceneMsgChangeCoinNumber;
    numToAddOrSubstract = 0;
  }

  virtual ReturnCodes loadData(INIReader* reader){
    return reader->getInteger("sceneMessageData", numToAddOrSubstract);
  }

  virtual SceneMsg& operator=(const SceneMsg& other){
    SceneMsg::operator=(other);
    const SceneMsgChangeCoinNumber* castedValue =static_cast<const SceneMsgChangeCoinNumber*>(&other);
    numToAddOrSubstract = castedValue->numToAddOrSubstract;
    return *this;
  }
};