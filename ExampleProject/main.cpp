#ifdef _DEBUG
  #if __WIN32__
// remove this define to use Visual Studio's memleak detection
    #define _USE_VLD

    #ifdef _USE_VLD
      #include <vld.h>
    #else
      #define _CRTDBG_MAP_ALLOC
      #include <stdlib.h>
      #include <crtdbg.h>
    #endif
  #endif
#endif

#include "example.h"

int main(int argc, char **argv)
{
#if __WIN32__
#ifndef _USE_VLD
  _CrtSetDbgFlag ( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
#endif
#endif
  LogManager::init();
  sxLOG_T("Creating MainLoop");
  Example* mainLoop = new Example();
  sxLOG_T("Running Program");
  mainLoop->run();
  sxLOG_T("Deleting MainLoop");
  delete mainLoop;  
  sxLOG_T("Exiting from program.");  
  exit(0);
}