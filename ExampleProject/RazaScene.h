#pragma once
#include "ObjectScene.h"
#include "BasicObject.h"
#include "AnimObject.h"

class RazaScene : public ObjectScene
{
public:
  RazaScene();
  ~RazaScene();
  ReturnCodes loadFile(const sxString& filename);
  virtual void draw();
  virtual void stepAnimation(const sxUInt32 ticksPassed);

private:
  sxUInt32 objIndexWhatStops;
  sxUInt64 stepsSinceStart;
  BasicObject* numOfAnimObjs;
  void createObjects();
  void destroyObjects();
  AnimObject* castToAnimObject(sxUInt32 index);
};
