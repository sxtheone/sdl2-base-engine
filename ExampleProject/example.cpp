/// Engine includes
#include "example.h"
#include "ActManager.h"
#include "ResourceReleaser.h"
#include "EventManager.h"
#include "ObjectManager.h"

/// User includes
#include "CoinObject.hpp"
#include "CoinScene.h"
#include "SceneMsgChangeCoinNumber.hpp"
#include "RazaScene.h"
#include "TestSceneFonts.hpp"
#include "TestSceneUI.hpp"
#include "PlayerScene.hpp"
#include "TileMapTest.h"

Example::Example()
{
  init();
}

void Example::init()
{
  MainLoop::init();
  registerSceneTypes();
  registerObjectTypes();
  loadStartingAct();
}

void Example::registerSceneMsgs()
{
  MainLoop::registerSceneMsgs();
  getActManager->registerSceneMsgType(SceneMsgTypes::SceneMsgChangeCoinNumber,
                                      new SceneMsgFactory<SceneMsgChangeCoinNumber>());
}

void Example::registerSceneTypes()
{
  getActManager->registerSceneType("CoinScene", new SceneFactory<CoinScene>());
  getActManager->registerSceneType("RazaScene", new SceneFactory<RazaScene>());
  getActManager->registerSceneType("TestSceneFonts", new SceneFactory<TestSceneFonts>());
  getActManager->registerSceneType("TestSceneUI", new SceneFactory<TestSceneUI>());
  getActManager->registerSceneType("PlayerScene", new SceneFactory<PlayerScene>());
  getActManager->registerSceneType("CrazyTilesScene", new SceneFactory<TileMapTest>());
}

void Example::registerObjectTypes()
{
  getObjectManager->registerObjectType(ObjectTypes::CoinObject,
                                       new ObjectFactory<CoinObject>());
}

void Example::loadAct00()
{
  ResourceReleaser::release(WhatToReleaseEnum::PERMANENT_ALSO);
  loadAct("00_act.act");
}

void Example::loadAct01()
{
  ResourceReleaser::release(WhatToReleaseEnum::TEXTURES_ALSO);
  loadAct("01_act.act");
}

void Example::handleActChange()
{
  if(SDL_TRUE != getEventManager->isEventActive(sxEventTypes::CHANGE_ACT)){
    return;
  }
  sxEvent event;
  getEventManager->getNextEvent(sxEventTypes::CHANGE_ACT, event);
  sxActChange actChangeData = event.actChange();
  event.destroyEvent();
  if(sxActChange::DESTROY_OTHERS == actChangeData.changeType){
    getActManager->cleanSceneAndCollisionInfo();
    ResourceReleaser::release(WhatToReleaseEnum::TEXTURES_ALSO);
    loadAct(actChangeData.targetActName);
  }
  
  if(sxActChange::ADD_ON_TOP == actChangeData.changeType){
    loadAct(actChangeData.targetActName);
  }
  if(sxActChange::REMOVE_FROM_TOP == actChangeData.changeType){
    getActManager->removeTopmostAct();
  }
}