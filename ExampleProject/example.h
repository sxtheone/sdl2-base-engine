#pragma once

#include "MainLoop.h"

/* TODO
LIST:
- use object inheritance in every ini file where it is available!
- the other builders (ObjectBuilder, SceneBuilder, ??) could use the int - string identifier type maybe..?

- restructure current scenes, make a third act for the tilemap scene
- restructure code, make most of the .hpp files disappear, make directories
- lib from engine? Or just the big restructuring is enough?
- make list and deque scenes to acts or delete them
- make a menu/UI for each scene
- make a universal UI act what is present above every scene
- make a pop-up menu in the universal UI act
- refactor current example scene codes

IN THE FUTURE:
- make a possibility for scenes/acts to store what resources they loaded so it can be unloaded
  This provides better memory management, but not really needed for small games what changes the whole UI
  after competing a level..
- possibility to change scenes order in queue on-the-fly. I don't know when will I really need this. Implement on-demand.
- !NOPE! Objects should have a field what can be used to find them/easy place to put info what is
  too small to make a separate object. A string field? !NOPE!: most object don't need this 'cos the hitboxs
  identifies what is hit and it can behave as it's type requires
  */



class Example : public MainLoop
{
public:
  Example();
  void init();
  
private:
  void loadAct00();
  void loadAct01();
  virtual void handleActChange();
  void registerSceneMsgs();
  void registerSceneTypes();
  void registerObjectTypes();
};